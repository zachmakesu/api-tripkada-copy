class TransactionDataStore::DataCleanerWorker
  include Sidekiq::Worker

  def perform transaction_data_id
    transaction_data = TransactionDataStore.find_by(transaction_id: transaction_data_id)
    transaction_data&.destroy
  end
end
