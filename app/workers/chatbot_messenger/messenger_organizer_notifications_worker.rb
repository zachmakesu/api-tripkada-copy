class ChatbotMessenger::MessengerOrganizerNotificationsWorker
  include Sidekiq::Worker
  include Facebook::Messenger

  def perform event_id, membership_id
    @event       = Event.find event_id
    @membership  = EventMembership.find membership_id
    @invited_joiners = @membership.invited_members
    invitee_names = []
    if @invited_joiners.present?
      @invited_joiners.find_each do |invitee|
        invitee_names << invitee.first_name
      end
    end
    @member      = @membership.member
    @messenger_user = MessengerUser.fetch_by(
      first_name: @event.owner.first_name,
      last_name: @event.owner.last_name
    ).first
    @messenger_user.update(message_concern: "BOOKING NOTIFICATION")
    @invitee_msg = "with #{@invited_joiners.count} invited joiner/s (#{invitee_names.to_sentence})"
    @message = "#{@member.decorate.full_name} booked for #{@event.name} #{@invitee_msg unless @invited_joiners.empty?} this #{@membership.created_at.strftime("%B %d")}."
    @payment = "Payment was made via #{@membership.payment.mode}."

    Bot.deliver({
      recipient: {
        id: @messenger_user.user_fb_id
      },
      message: {
        text: "Congratulations you have a new booking!"
      }
    }, access_token: ENV['FB_PAGE_ACCESS_TOKEN'])

    Bot.deliver({
      recipient: {
        id: @messenger_user.user_fb_id
      },
      message: {
        text: "#{@message} #{@payment} #{@event.decorate.website}"
      }
    }, access_token: ENV['FB_PAGE_ACCESS_TOKEN'])

    Bot.deliver({
      recipient: {
        id: @messenger_user.user_fb_id
      },
      message: {
        attachment: {
        type: 'template',
          payload: {
            template_type: 'generic',
            elements:
              [
                {
                  title: @event.name.to_s,
                  image_url: @event.decorate.cover_photo_complete_url,
                  subtitle: @event.description.to_s,
                }
              ]
            }
          }
        }
      }, access_token: ENV['FB_PAGE_ACCESS_TOKEN'])
  end
end
