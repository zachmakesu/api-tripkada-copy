class ChatbotMessenger::MessengerBookingNotificationsWorker
  include Sidekiq::Worker
  include Facebook::Messenger

  def perform event_id, membership_id, text_msg, concern
    @event       = Event.find event_id
    membership   = EventMembership.find membership_id
    @member      = membership.member
    @messenger_user = MessengerUser.fetch_by(
      first_name: @member.first_name,
      last_name: @member.last_name
    ).first
    @messenger_user.update(message_concern: concern)
    @message = text_msg

    Bot.deliver({
      recipient: {
        id: @messenger_user.user_fb_id
      },
      message: {
        text: "#{@message} #{@event.decorate.website unless concern == 'REVIEW ORGANIZER'}"
      }
    }, access_token: ENV['FB_PAGE_ACCESS_TOKEN'])

    Bot.deliver({
      recipient: {
        id: @messenger_user.user_fb_id
      },
      message: {
        attachment: {
        type: 'template',
          payload: {
            template_type: 'generic',
            elements:
              [
                {
                  title: @event.name.to_s,
                  image_url: @event.decorate.cover_photo_complete_url,
                  subtitle: @event.description.to_s,
                }
              ]
            }
          }
        }
      }, access_token: ENV['FB_PAGE_ACCESS_TOKEN'])
  end
end
