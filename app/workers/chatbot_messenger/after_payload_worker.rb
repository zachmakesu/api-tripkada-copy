class ChatbotMessenger::AfterPayloadWorker
  include Sidekiq::Worker
  include Facebook::Messenger

  def perform messenger_user_id
    messenger_user = MessengerUser.find messenger_user_id
    messenger_user.update(message_concern: "ASK_EMAIL_CONCERN")

    Bot.deliver({
      recipient: {
        id: messenger_user.user_fb_id
      },
      message: {
        text: "I hope I served you well #{messenger_user.first_name}, if you need anything you can always tap the menu below or go to tripkada.com"
      }
    }, access_token: ENV['FB_PAGE_ACCESS_TOKEN'])

    Bot.deliver({
      recipient: {
        id: messenger_user.user_fb_id
      },
      message: {
        text: "One more thing, we are giving away discounts and exclusive free trips occasionally. If you are interested, you can send me your email address."
      }
    }, access_token: ENV['FB_PAGE_ACCESS_TOKEN'])

  end
end
