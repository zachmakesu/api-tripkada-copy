class AdminNotifications::PhotoMailerWorker
  include Sidekiq::Worker

  def perform(user_id, photo_ids)
    user = User.find(user_id)
    photos = user.verification_photos.where(id: photo_ids)
    AdminMailer.photo_verification(user, photos).deliver_now
  end

end
