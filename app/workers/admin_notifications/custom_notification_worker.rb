class AdminNotifications::CustomNotificationWorker
  include Sidekiq::Worker

  def perform message_id, option
    receivers       = User.send(option)
    notification    = CustomNotificationMessage.find(message_id)
    receivers.each do |receiver|
      Notifications::CustomNotificationHandler.new(custom_notification_id: notification.id, message: notification.message, receiver: receiver).deliver
    end
  end
end
