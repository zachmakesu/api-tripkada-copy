class AdminNotifications::EventMailerWorker
  include Sidekiq::Worker

  def perform(event_id, email_type)
    event = Event.find(event_id)
    case email_type
    when "new_trip" then AdminMailer.new_trip(event).deliver_now
    when "canceled_trip" then AdminMailer.canceled_trip(event).deliver_now
    end
  end

end
