class AdminNotifications::ReviewMailerWorker
  include Sidekiq::Worker

  def perform(review_id, review_type)
    review = Review.find(review_id)
    # Use case switch in case review_joiner
    # and review event mailer is added
    case review_type
    when "review_organizer" then AdminMailer.review_organizer(review).deliver_now
    end
  end

end
