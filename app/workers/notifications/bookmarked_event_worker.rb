class Notifications::BookmarkedEventWorker
  include Sidekiq::Worker

  def perform(event_id, bookmark_id)
    event       = Event.find event_id
    bookmark    = Bookmark.find bookmark_id
    joiner      = bookmark.user

    Notifications::BookmarkedReminderHandler.new(
      event: event,
      joiner: joiner
    ).deliver
  end
end
