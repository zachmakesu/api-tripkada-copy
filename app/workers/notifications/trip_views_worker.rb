# class Notifications::TripViewsWorker
class Notifications::TripViewsWorker
  include Sidekiq::Worker

  def perform(event_id)
    event = Event.find event_id
    Notifications::TripViewsReminderHandler.new(event: event).deliver
  end
end
