class Notifications::AfterEventWorker
  include Sidekiq::Worker
  include Rails.application.routes.url_helpers

  def perform event_id, membership_id
    event       = Event.find event_id
    membership  = event.approved_event_memberships.find membership_id
    member      = membership.member
    owner       = event.decorate.owner
    url         = profile_url(owner.slug)
    review_trip_msg = "We hope you and your trip barkada enjoyed your trip."\
                    " Tell us about your experience with #{owner&.full_name}."\
                    " Click here to review your organizer #{url}"\
                    " We'd love to know."
    unless event.organized_by?(user: member)
      Notifications::ReviewTripReminderHandler.new(event: event, member: member).deliver
      ChatbotMessenger::MessengerBookingNotificationsWorker.perform_async(
        event.id, membership.id, review_trip_msg, "REVIEW ORGANIZER")
      NotificationMailer.review_trip_reminder(event, member).deliver if Rails.env.production?
    end
  end
end
