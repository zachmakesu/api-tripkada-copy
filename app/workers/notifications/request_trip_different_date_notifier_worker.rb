class Notifications::RequestTripDifferentDateNotifierWorker
  include Sidekiq::Worker

  def perform(event_id, requester_uid, params, start_datetime, end_datetime)
    requester = User.find_by(uid: requester_uid)
    event = Event.find(event_id)
    mod_params = {}
    mod_params[:start_date_time] = start_datetime
    mod_params[:end_date_time] = end_datetime
    mod_params[:requested_pax] = params["requested_pax"]
    Notifications::RequestTripDifferentDateHandler.new(event: event, requester: requester, params: mod_params).deliver
  end
end
