class Notifications::TripsWorker
  include Sidekiq::Worker

  def perform(event_id, membership_id=nil)
    event       = Event.find(event_id)
    membership  = event.approved_event_memberships.find_by(id: membership_id)

    EventNotificationHandler.new(event: event, membership: membership).deliver
  end

end
