class Notifications::ReferralIncentiveWorker
  include Sidekiq::Worker
  
  def perform(referrer_id, joiner_id, event_id, recipient)
    case recipient
    when "referrer"
      ReferralMailer.send_to_referrer(referrer_id, joiner_id, event_id).deliver_now
    when "staff"
      ReferralMailer.send_to_staff(referrer_id, joiner_id, event_id).deliver_now
    end
  end
end
