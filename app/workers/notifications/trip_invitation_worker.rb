class Notifications::TripInvitationWorker
  include Sidekiq::Worker

  def perform(inviter_id, event_id, recipient_id)
    event = Event.find event_id
    inviter = User.find(inviter_id)
    recipient = User.find(recipient_id)
    NotificationMailer.trip_invitation(inviter, event, recipient).deliver_now
  end

end
