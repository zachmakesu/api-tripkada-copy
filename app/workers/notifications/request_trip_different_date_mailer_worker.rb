class Notifications::RequestTripDifferentDateMailerWorker
  include Sidekiq::Worker

  def perform(event_id, requester_uid, params, start_datetime, end_datetime)
    event = Event.find_by(id: event_id)
    requester = User.find_by(uid: requester_uid)
    mod_params = {}
    mod_params[:start_date_time] = start_datetime
    mod_params[:end_date_time] = end_datetime
    mod_params[:requested_pax] = params["requested_pax"]
    NotificationMailer.request_trip_different_date(requester, event, mod_params).deliver_now
    AdminMailer.request_trip_different_date(requester, event, mod_params).deliver_now
  end
end
