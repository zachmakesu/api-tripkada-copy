class Notifications::FollowerWorker
  include Sidekiq::Worker

  def perform(follower_id, event_id)
    follower = User.find follower_id
    event    = Event.find event_id

    Notifications::FollowerReminderHandler.new(follower: follower, event: event).deliver
  end

end
