class Notifications::BookmarkWorker
  include Sidekiq::Worker

  def perform(bookmark_id)
    bookmark    = Bookmark.find(bookmark_id)
    event       = bookmark.event
    Notifications::BookmarkedEventWorker.perform_at(
      event.start_at - 8.days,
      event.id, bookmark.id
    )
  end
end
