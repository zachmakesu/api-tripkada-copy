class Notifications::PaymentsWorker
  include Sidekiq::Worker

  def perform(membership_id)
    membership = EventMembership.find membership_id
    Notifications::JoinTripHandler.new(membership: membership).deliver
    Notifications::EmailWorker.perform_async(membership_id, "join_event")
    Notifications::EmailWorker.perform_async(membership_id, "booking_confirmation_joiner")
    Notifications::EmailWorker.perform_async(membership_id, "booking_notification_organizer")
  end

end
