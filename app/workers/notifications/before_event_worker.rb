class Notifications::BeforeEventWorker
  include Sidekiq::Worker

  def perform event_id, membership_id
    event       = Event.find event_id
    membership  = EventMembership.find membership_id
    member      = membership.member
    trip_slot_msg = "You still have #{event.decorate.slots_left} slots for your"\
          "#{event.name}. Time to share share share and promote!"\
          "Let’s fill up those slots!"
    trip_reminder_msg = "Hey #{member.first_name}! Your trip #{event.name} is"\
              " coming up. Time to get ready!"
    trip_payment_reminder =  "Are you ready for your trip? Dont forget to"\
          " bring the rest of your payment (if any )"\
          " before heading to the meeting place."
    reminder_options = {
      event: event,
      member: member
    }

    if event.owner == member
      unless event.full?
        Notifications::TripSlotReminderHandler.new(event: event).deliver
        ChatbotMessenger::MessengerBookingNotificationsWorker.perform_async(
          event.id, membership.id, trip_slot_msg, "TRIP SLOT REMINDER")
      end
    else
      unless membership.payment.fullpayment?
        Notifications::TripPaymentReminderHandler.new(reminder_options).deliver
        ChatbotMessenger::MessengerBookingNotificationsWorker.perform_async(
          event.id, membership.id, trip_payment_reminder, "TRIP PAYMENT REMINDER")
      end
    end
    Notifications::TripReminderHandler.new(reminder_options).deliver
    ChatbotMessenger::MessengerBookingNotificationsWorker.perform_async(
      event.id, membership.id, trip_reminder_msg, "TRIP REMINDER")
  end
end
