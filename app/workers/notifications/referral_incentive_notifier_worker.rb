class Notifications::ReferralIncentiveNotifierWorker
  include Sidekiq::Worker

  def perform(joiner_uid, referrer_uid, event_id)
    joiner = User.find_by(uid: joiner_uid)
    referrer = User.find_by(uid: referrer_uid)
    event = Event.find(event_id)
    Notifications::ReferralIncentiveNotifierHandler.new(event: event, referrer: referrer, joiner: joiner).deliver
  end
end
