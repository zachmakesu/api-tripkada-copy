class Notifications::EmailWorker
  include Sidekiq::Worker

  def perform(params, email_type)
    case email_type
    when "join_event"
      membership = EventMembership.find params
      UserMailer.join_event(membership).deliver_now
    when "trip_request"
      UserMailer.trip_request(params).deliver_now
    when "question"
      UserMailer.question(params).deliver_now
    when "apply_as_organiser"
      UserMailer.apply_as_organiser(params).deliver_now
    when "sign_me_up_email"
      UserMailer.sign_me_up_email(params).deliver_now
    when "signup_welcome_email"
      NotificationMailer.signup_welcome_email(params).deliver_now
    when "booking_confirmation_joiner"
      membership = EventMembership.find params
      NotificationMailer.booking_confirmation_joiner(membership).deliver_now
    when "booking_notification_organizer"
      membership = EventMembership.find params
      NotificationMailer.booking_notification_organizer(membership).deliver_now
    when "apply_as_organizer_from_organizer_app"
      UserMailer.apply_as_organizer_from_organizer_app(params).deliver_now
    when "invite_joiner_via_email"
      NotificationMailer.invite_joiner_via_email(params).deliver_now
    when "referral_invite"
      params.fetch("emails", []).each do |email|
        UserMailer.referral_invite(email, params).deliver_now
      end
    end
  end

end
