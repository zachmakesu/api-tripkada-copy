class Notifications::WaitlistMembershipMailerWorker
  include Sidekiq::Worker

  def perform(event_id, user_id, status)
    event       = Event.find(event_id)
    user  = event.waitlist_members.find_by(id: user_id)

    NotificationMailer.waitlist_membership_status(event: event, user: user, status: status).deliver
  end

end
