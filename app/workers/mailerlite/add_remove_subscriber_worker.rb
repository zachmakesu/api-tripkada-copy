class Mailerlite::AddRemoveSubscriberWorker
  include Sidekiq::Worker

  def perform subscriber_uid, booking_count
    @subscriber = User.find_by(uid: subscriber_uid)&.reload
    @import_service = Mailerlite::Groups::AddToGroup.call(subscriber: @subscriber, booking_count: booking_count)
    valid_segment_category = %w(repeat loyal)

    if @import_service.success? && valid_segment_category.include?(@import_service.category)
      group = get_group_by_category
      Mailerlite::Groups::RemoveSubscriber.call(group_id: group.group_id, subscriber_email: @subscriber.email)
    end
  end

  def get_group_by_category
    # fetch from previous group
    category = if @import_service.category == "repeat"
                 "one_off"
               else
                 "repeat"
               end
    MailerliteGroup.find_by(category: category)
  end
end
