module Sms
  class SenderWorker
  include Sidekiq::Worker
    def perform(message, mobile_number)
      @chikka = Chikka::Client.new(client_id: ENV["CHIKKA_CLIENT_ID"], secret_key: ENV["CHIKKA_SECRET_KEY"], shortcode: ENV["CHIKKA_SHORTCODE"])
      chikka_response = @chikka.send_message(message: message, mobile_number: mobile_number)
      case chikka_response.status
      when 200
        logger.info "[SMS] Message for number #{mobile_number} delivered!"
      else
        logger.error "[SMS] Message for number #{mobile_number} delivery failed, #{response}"
      end
    end
  end
end
