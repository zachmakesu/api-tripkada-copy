# class Notifications::TripViewsReminderHandler
class Notifications::TripViewsReminderHandler
  def initialize(event:)
    @event      = event.decorate
    @organizer  = @event.owner
  end

  def deliver
    FCMHandler.new(
      recipient: @organizer, payload: payload, role: "organizer"
    ).deliver

    @organizer.notifications.create(
      message: payload[:alert_message],
      category: payload[:category],
      notificationable_id: @event.id,
      notificationable_type: "Event"
    )
  end

  private

  def payload
    {
      category:       Notification.categories[:trip_views_notification],
      alert_message:  "Your trip #{@event.name} got a total of "\
                       "#{@event.number_of_views} views! People are looking "\
                       "at your trips, share more to get more interests.",
      event_id:       @event.id,
      event_photo:    @event.cover_photo_complete_url(:thumb),
      organizer_uid:  @organizer.uid
    }
  end
end
