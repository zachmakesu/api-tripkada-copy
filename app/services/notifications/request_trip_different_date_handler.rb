class Notifications::RequestTripDifferentDateHandler

  def initialize requester:, params:, event:
    @event = event.decorate
    @requester = requester
    @requested_pax = params.fetch(:requested_pax)
    @start_date = params.fetch(:start_date_time)
    @end_date = params.fetch(:end_date_time)
  end

  def deliver
    FCMHandler.new(recipient: @event.owner, payload: payload).deliver
    @event.owner.notifications.create(message: payload[:alert_message], category: payload[:category], notificationable_id: @event.id, notificationable_type: "Event")
  end

  private

  def payload
    {
      category:       Notification.categories[:trip_date_request],
      alert_message:  message,
      event_id:       @event.id,
      event_photo:    @event.cover_photo_complete_url(:thumb),
      organizer_uid:  @event.owner.uid,
      member_uid:     @requester.uid
    }
  end

  def parse(date)
    DateTime.parse(date).try(:strftime,"%B %d, %Y %I:%M %p")
  end

  def message
    "mabuhay you got a trip request"\
    " for your trip #{@event.name.titleize}"\
    " dated #{parse @start_date} to #{parse @end_date}"\
    " for #{@requested_pax} person."\
    " Keep creating amazing trips like this!"\
    " Congratulations!"
  end

end
