Notifications::TripReminderHandlerError = Class.new(StandardError)

class Notifications::TripReminderHandler

  def initialize event:, member:
    @event      = event.decorate
    @organizer  = @event.owner
    @member     = member
  end

  def deliver
    FCMHandler.new(recipient: @member, payload: payload).deliver
    @member.notifications.create(message: payload[:alert_message], category: payload[:category], notificationable_id: @event.id, notificationable_type: "Event")
  end

  private

  def payload
    {
      category:       Notification.categories[:trip_reminder],
      alert_message:  "Hey hey! Your next adventure is coming up. Time to get ready!",
      event_id:       @event.id,
      event_photo:    @event.cover_photo_complete_url(:thumb),
      organizer_uid:  @organizer.uid,
      member_uid:     @member.uid
    }
  end

end
