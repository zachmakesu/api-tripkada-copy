Notifications::BookmarkedReminderHandlerError = Class.new(StandardError)

class Notifications::BookmarkedReminderHandler
  def initialize(event:, joiner:)
    @event      = event.decorate
    @joiner     = joiner
    @days       = (@event.start_at.to_date - Time.zone.now.to_date).to_i
  end

  def deliver
    FCMHandler.new(
      recipient: @joiner,
      payload: payload,
      role: "joiner"
    ).deliver
    @joiner.notifications.create(
      message: payload[:alert_message],
      category: payload[:category],
      notificationable_id: @event.id,
      notificationable_type: "Event"
    )
  end

  private

  def payload
    {
      category:       Notification.categories[:bookmarked_event_reminder],
      alert_message:  "Hey there traveler! Only #{@days} #{'day'.pluralize(@days)} left before #{@event.name}, Book now!",
      event_id:       @event.id,
      event_photo:    @event.cover_photo_complete_url(:thumb),
      member_uid:     @joiner.uid
    }
  end
end
