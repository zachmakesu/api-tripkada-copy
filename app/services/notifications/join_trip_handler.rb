Notifications::JoinTripHandlerError = Class.new(StandardError)

class Notifications::JoinTripHandler

  def initialize membership:
    @membership = membership.decorate
    @event      = @membership.event
    @joiner     = @membership.member
    @organizer  = @membership.event.owner
  end

  def deliver
    FCMHandler.new(recipient: @organizer, payload: payload, role: 'organizer').deliver
    @organizer.notifications.create(message: payload[:alert_message], category: payload[:category], notificationable_id: @event.id, notificationable_type: "Event")
  end

  private

  def alert
    payment = @membership.payment
    if payment.promo_code.present?
      if payment.promo_code_value >= @event.rate
        "Good job! You just got a joiner using our promo: #{payment.promo_code} "
      else
        "Congratulations! You just got a joiner! Let’s keep promoting and fill up those slots! #{payment.promo_code} gives joiners a PHP #{payment.promo_code_value} discount."
      end
    else
      "Congratulations! You just got a joiner! Let's Keep promoting and fill up those slots!"
    end
  end

  def payload
    {
      category:           Notification.categories[:trip_join],
      alert_message:      alert,
      event_id:           @event.id,
      joiner_uid:         @joiner.uid,
      joiner_photo:       @joiner.avatar_complete_url(:thumb),
      organizer_uid:      @organizer.uid
    }
  end
end
