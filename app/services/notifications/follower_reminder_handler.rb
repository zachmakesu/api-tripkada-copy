Notifications::FollowerReminderHandlerError = Class.new(StandardError)

class Notifications::FollowerReminderHandler

  def initialize event:, follower:
    @event        = event.decorate
    @organizer    = @event.owner.decorate
    @follower     = follower.decorate
  end

  def deliver
    FCMHandler.new(recipient: @follower, payload: payload, role: 'joiner').deliver
    @follower.notifications.create(message: payload[:alert_message], category: payload[:category], notificationable_id: @event.id, notificationable_type: "Event")
  end

  private

  def payload
    {
      category:           Notification.categories[:follower_reminder],
      alert_message:      "#{@organizer.full_name} invites you to join #{@event.name}! Book a slot while you can!",
      event_id:           @event.id,
      event_photo:        @event.cover_photo_complete_url(:thumb),
      organizer_uid:      @organizer.uid,
      follower_uid:       @follower.uid
    }
  end

end
