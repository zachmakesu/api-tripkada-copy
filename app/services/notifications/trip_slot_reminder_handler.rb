Notifications::TripSlotReminderHandlerError = Class.new(StandardError)

class Notifications::TripSlotReminderHandler

  def initialize event:
    @event      = event.decorate
    @organizer  = @event.owner
  end

  def deliver
    FCMHandler.new(recipient: @organizer, payload: payload, role: 'organizer').deliver
    @organizer.notifications.create(message: payload[:alert_message], category: payload[:category], notificationable_id: @event.id, notificationable_type: "Event")
  end

  private

  def payload
    {
      category:       Notification.categories[:slot_reminder],
      alert_message:  "You still have #{@event.slots_left} slots for your #{@event.name}. Time to share share share and promote! Let’s fill up those slots!",
      event_id:       @event.id,
      event_photo:    @event.cover_photo_complete_url(:thumb),
      organizer_uid:  @organizer.uid
    }
  end

end
