Notifications::TripPaymentReminderHandlerError = Class.new(StandardError)

class Notifications::TripPaymentReminderHandler

  def initialize event:, member:
    @event        = event.decorate
    @organizer    = @event.owner
    @member       = member
  end

  def deliver
    FCMHandler.new(recipient: @member, payload: payload, role: 'joiner').deliver
    @member.notifications.create(message: payload[:alert_message], category: payload[:category], notificationable_id: @event.id, notificationable_type: "Event")
  end

  private

  def payload
    {
      category:       Notification.categories[:payment_reminder],
      alert_message:  "Are you ready for your trip? Dont forget to"\
                      " bring the rest of your payment (if any )"\
                      " before heading to the meeting place.",
      event_id:       @event.id,
      event_photo:    @event.cover_photo_complete_url(:thumb),
      organizer_uid:  @organizer.uid,
      member_uid:     @member.uid
    }
  end

end
