Notifications::ReviewTripReminderHandlerError = Class.new(StandardError)

class Notifications::ReviewTripReminderHandler

  def initialize event:, member:
    @event      = event.decorate
    @organizer  = @event.owner
    @member     = member
  end

  def deliver
    FCMHandler.new(recipient: @member, payload: payload, role: 'joiner').deliver
    @member.notifications.create(message: payload[:alert_message], category: payload[:category], notificationable_id: @event.id, notificationable_type: "Event")
  end

  private

  def payload
    {
      category:       Notification.categories[:trip_review_reminder],
      alert_message:  "We hope you and your trip barkada enjoyed your trip"\
                      " Tell us about your experience with #{@organizer&.full_name}."\
                      " We'd love to know.",
      event_id:        @event.id,
      event_photo:     @event.cover_photo_complete_url(:thumb),
      organizer_uid:   @organizer.uid,
      member_uid:      @member.uid
    }
  end

end
