class Notifications::CustomNotificationHandler

  def initialize custom_notification_id:, message:, receiver:
    @message                = message
    @receiver               = receiver
    @custom_notification_id = custom_notification_id
  end

  def deliver
    FCMHandler.new(recipient: @receiver, payload: payload, role: 'both').deliver
    @receiver.notifications.create(message: payload[:alert_message], category: payload[:category], notificationable_id: @custom_notification_id, notificationable_type: "CustomNotificationMessage")
  end

  private

  def payload
    {
      category:           Notification.categories[:custom],
      alert_message:      @message
    }
  end

end
