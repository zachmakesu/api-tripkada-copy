class Notifications::ReferralIncentiveNotifierHandler

  def initialize referrer:, joiner:, event:
    @event = event.decorate
    @referrer = referrer
    @joiner = joiner&.decorate
  end

  def deliver
    FCMHandler.new(recipient: @referrer, payload: payload, role: role).deliver
    @referrer.notifications.create(message: payload[:alert_message], category: payload[:category], notificationable_id: @event.id, notificationable_type: "Event")
  end

  private

  def message
    "Congratulations you have earned referral incentive from Tripkada for"\
      "success referral booking of joiner #{@joiner.full_name} ."\
      "You will receive trip credits worth of P #{ReferralIncentive.latest} ."
  end

  def payload
    {
      category:           Notification.categories[:referral_incentive],
      alert_message:      message,
      event_id:           @event.id,
      joiner_uid:         @joiner.uid,
      joiner_photo:       @joiner.avatar_complete_url(:thumb),
      referrer_uid:       @referrer.uid
    }
  end

  def role
    @referrer.joiner? ? "joiner" : "organizer"
  end

end
