class DeviceHandler

  attr_accessor :params, :current_user

  def initialize(params, current_user, opts={})
    @params        = params
    @current_user  = current_user
  end

  def create

    ActiveRecord::Base.transaction do

      unlink_user_with_same_devices

      @device = current_user.devices.create(device_options)

      response

    end

  end

  private
  def response
    @device.errors.full_messages.join(', ')
  end

  def unlink_user_with_same_devices
    Device.where(token: params[:token]).destroy_all
  end

  def device_options
    {
      udid:     params[:udid],
      token:    params[:token],
      platform: params[:platform],
      name:     params[:name],
      role:     params[:role]
    }.compact
  end

end
