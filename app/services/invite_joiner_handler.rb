class InviteJoinerHandler

  attr_accessor :user, :event_id, :invited_joiners, :errors, :response, :joiners

  def initialize(user, event_id, invited_joiners, user_membership_id)
    @user               = user
    @invited_joiners    = invited_joiners
    @event_id           = event_id
    @errors             = []
    @joiners            = []
    @response           = Hash.new
    @user_membership_id = user_membership_id
  end

  def find_or_create
    invited_joiners.each do |joiner|
      @joiner_params = joiner.symbolize_keys
      find || create
    end

    create_response
    self
  end

  def find
    return unless @invited_joiner = User.find_for_authentication(email: @joiner_params[:email])
    create_membership
  end

  def create
    @invited_joiner = User.new(joiner_params)
    if @invited_joiner.save
      create_referral
      create_membership
    else
      errors << @invited_joiner.errors.full_messages
    end
  end

  private

  def create_response
    if errors.blank?
      response[:type] = true
      response[:details] = @joiners
    else
      response[:type] = false
      response[:details] = errors.uniq.join(", ")
    end
  end

  def joiner_params
    {
      email: @joiner_params[:email],
      first_name: first_name,
      password: password
    }
  end

  def first_name
    @joiner_params[:name]
  end

  def password
    Devise.friendly_token[0, 10].downcase
  end

  def create_membership
    @membership = EventMembership.new(membership_params)
    @membership.build_payment(payment_params)
    # create and send invitation
    if @membership.save
      @user.sent_invitations.create(invitation_params)
      Notifications::TripInvitationWorker.perform_async(user.id, event_id, @membership.member.id)
      User::SendCreditsToReferrer.call(joiner: @invited_joiner, event: @event)
      @joiners << @membership.member
      Mailerlite::AddRemoveSubscriberWorker.perform_async(@membership.member.uid, @membership.member.approved_event_memberships.count) if Rails.env.production?
    else
      errors << @membership.errors.full_messages
    end
    return @membership
  end

  def membership_params
    {
      member: @invited_joiner,
      event_id: event_id
    }
  end

  def payment_params
    {
      mode: Payment.modes[:through_invite],
      made: Payment.mades[:downpayment],
      booking_fee: BookingFee.last,
      status: Payment.statuses[:approved]
    }
  end

  def invitation_params
    {
      invitee_id: @membership.member.id,
      event_membership_id: @user_membership_id
    }
  end

  def create_referral
    user.referrals.create(joiner_id: @invited_joiner.id)
  end

end
