class EventFilter
  def initialize(collection, params = {})
    @collection      = collection
    @type            = params.fetch(:type, nil)
    @search          = params.fetch(:search, "")
    @sort            = params[:sort] == "desc"
    @start_month     = params.fetch(:start_month, nil)
    @end_month       = params.fetch(:end_month, nil)
    @start_date      = params.fetch(:start_date, nil)
    @end_date        = params.fetch(:end_date, nil)
    @min_cost        = params.fetch(:min_cost){ Event::MINCOST }.to_f
    @max_cost        = params.fetch(:max_cost){ Event::MAXCOST }.to_f
    @min_days        = params.fetch(:min_days){ Event::MINDAYS }.to_i
    @max_days        = params.fetch(:max_days){ Event::MAXDAYS }.to_i
    @category_ids    = params.fetch(:category_ids, [])
    @destination_ids = params.fetch(:destination_ids, [])
    @category_id     = params[:category_id]
    @destination_id  = params[:destination_id]
    @price           = params[:price]
    @featured        = params[:featured]
  end

  def result
    @collection = @collection.not_deleted.includes(:photos, :cover_photo, owner:[:profile_avatar, :cover_photo], event_memberships: [member: [:profile_avatar, :cover_photo], payment: [:promo_code, :event]])
    by_price
    by_type
    by_search
    by_sort
    by_cost
    by_days
    by_activity
    by_common_places
    by_months
    by_date
    featured
    @collection.uniq
  end

  private
  def by_type
    case @type
    when "upcoming" then @collection = @collection.not_deleted.upcoming
    when "previous" then @collection = @collection.not_deleted.past
    else
      @collection = @collection.not_deleted
    end
  end

  def by_search
    @collection = @collection.filtered_search(@search)
  end

  def by_sort
    @collection = @sort ? @collection.sorted_event : @collection.order_by_start
  end

  def by_price
    @collection = case @price
      when 'high' then   @collection.order_by_rate_high
      when 'low'  then   @collection.order_by_rate
    else
      @collection
    end
  end
  
  def by_months
    first_event = Event.first_past_event&.start_at&.beginning_of_month
    last_event = Event.last_upcoming_event&.start_at&.end_of_month

    start_month = @start_month.nil? ? first_event : Time.strptime(@start_month, "%m/%Y").beginning_of_month rescue first_event

    end_month = @end_month.nil? ? last_event : Time.strptime(@end_month, "%m/%Y").end_of_month rescue last_event

    @collection = @collection.from_months(start_month, end_month)
  end

  def by_date
    first_event = Event.first_past_event&.start_at
    last_event = Event.last_upcoming_event&.start_at

    start_date = @start_date.nil? ? first_event : DateTime.strptime(@start_date, "%m/%d/%Y") rescue first_event
    end_date = @end_date.nil? ? last_event : DateTime.strptime(@end_date, "%m/%d/%Y") rescue last_event

    @collection = @collection.from_date(start_date, end_date)
  end

  def by_cost
    @max_cost = Event.maximum(:rate) + BookingFeeDecorator.latest_fee if @max_cost >= Event::MAXCOST
    @collection = @collection.cost(@min_cost, @max_cost)
  end

  def by_days
    @max_days = Event.maximum(:number_of_days) if @max_days >= Event::MAXDAYS
    @collection = @collection.days(@min_days, @max_days)
  end

  def by_activity
    @collection = if @category_ids.is_a?(Array) && @category_ids.any?
                    @collection.activity(@category_ids.map{ |n| n.to_i }.uniq.compact)
                  elsif @category_id && !@category_id.empty?
                    @collection.activity(@category_id&.to_i)
                  else
                    @collection
                  end
  end

  def by_common_places
    @collection = if @destination_ids.is_a?(Array) && @destination_ids.any?
                    @collection.common_places(@destination_ids.map{ |n| n.to_i }.uniq.compact)
                  elsif @destination_id && !@destination_id.empty?
                    @collection.common_places(@destination_id&.to_i)
                  else
                    @collection
                  end
  end

  def featured
    @collection = @featured ? @collection.featured : @collection
  end
end
