class Payments::Dragonpay::CreateMembershipAndLogPayment
  extend LightService::Organizer

  aliases invited_joiner_names: :names, invited_joiner_emails: :emails, data: :invited_joiners_details

  def self.call(event:, joiner:, params:)
    with(event: event, joiner: joiner, params: params).reduce(actions)
  end

  def self.actions
    [
      Actions::Payments::Dragonpay::ValidatePaymentStatus,
      Actions::Payments::Dragonpay::CheckPendingTransactionLog,
      Actions::Event::Booking::ApprovePayment,
      Actions::Event::Booking::InvitedJoiners::FindInvitedJoinersMembership,
      Actions::Event::Booking::InvitedJoiners::ApprovePayments,
      Actions::Event::Booking::ValidateInvitedJoiners,
      Actions::Event::Booking::ValidateAvailableSlots,
      Actions::User::IdentifyDevicePlatform,
      Actions::Event::Booking::CreateMembership,
      Actions::Event::Booking::CreatePayment,
      Actions::Event::Booking::Waitlist::FindUserFromWaitlists,
      Actions::Event::Booking::Waitlist::ApproveWaitlistMembership,
      Actions::Payments::Dragonpay::LogTransaction,
      Actions::ParseInvitedJoiners,
      Actions::Event::Booking::InvitedJoiners::FindExistingUsers,
      Actions::Event::Booking::InvitedJoiners::RegisterNewUsers,
      Actions::Event::Booking::InvitedJoiners::RegisterReferrer,
      Actions::Event::Booking::InvitedJoiners::CreateMemberships,
      Actions::Event::Booking::InvitedJoiners::CreatePayments,
      Actions::Event::Booking::InvitedJoiners::LogInvitees,
      Actions::Event::Booking::InvitedJoiners::NotifyInvitee,
      Actions::Event::Booking::InvitedJoiners::RunMailerliteAutomatedGroupSubscription,
      Actions::Event::Booking::NotifyFollowers,
      Actions::Event::Booking::SendPaymentNotifications,
      Actions::Event::Booking::RegisterTripNotifications,
      Actions::Event::Booking::MailerliteAutomatedGroupSubscription
    ]
  end
end
