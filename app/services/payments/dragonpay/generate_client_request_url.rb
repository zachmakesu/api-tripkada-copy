class Payments::Dragonpay::GenerateClientRequestURL
  extend LightService::Organizer
  aliases :joiner => :user

  def self.call(event:, params:, joiner:)
    with(
      event: event, params: params,
      joiner: joiner,
      transaction_exists: false,
      code: params[:code]
    ).reduce(actions)
  end

  def self.actions
    [
      Actions::Event::Booking::ValidateInvitedJoiners,
      Actions::Event::Booking::ValidateInviteesIfAlreadyMember,
      Actions::Event::Booking::ValidateJoinersEmail,
      Actions::Event::Booking::EnsureJoinerEmailNotUseForInvitedJoiners,
      Actions::Event::Booking::ValidateAvailableSlots,
      Actions::PromoCode::FindPromoCode,
      Actions::PromoCode::ValidateUsage,
      Actions::PromoCode::ValidateOwnership,
      Actions::Payments::Dragonpay::CalculateTotalAmount,
      Actions::Payments::Dragonpay::LessPromoCodeAmount,
      Actions::Payments::Dragonpay::GenerateRequestURL
    ]
  end
end
