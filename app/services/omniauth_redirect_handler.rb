class OmniauthRedirectHandler

  include Rails.application.routes.url_helpers

  attr_accessor :return_to, :flash

  WHITELISTED_ROLES = %w{ invitee from_referral }

  def initialize options

    @user             = options.fetch(:user, nil).try(:decorate)
    @path             = options.fetch(:path, nil)
    @event            = options.fetch(:event, nil)
    @request          = options.fetch(:request, nil)
    @handler_response = options.fetch(:handler_response, {})
    @app_role         = options.fetch(:app_role, nil)
    @auth_kind        = options.fetch(:kind, nil)
    @proceed_checkout = options.fetch(:proceed_checkout, false)
    @url              = options.fetch(:url, "")
    @flash            = Hash.new
  end
  
  def get_path
    initialize_flash
    @return_to = if user_is_admin?
                   admin_path
                 elsif user_has_important_notif?
                   user_notification_path(@notification_id)
                 elsif facebook_handler_is_false?
                   be_a_trip_organizer_path
                 elsif event_is_past?
                   trip_requests_path
                 elsif event_is_full?
                   flash[:full_trip] = true
                   trip_view_path(@event)
                 elsif user_can_proceed_checkout?
                   trip_checkout_path(@event)
                 elsif from_invite_or_referral?
                   @url
                 elsif redirect_using_referer?
                   @request.referer
                 else
                   dynamic_path
                 end
    self
  end

  private
  def initialize_flash
    flash[:incomplete_info] = !@user.has_completed_required_info?
    flash[:notice]          = if facebook_handler_is_false?
                                "Oooops!, #{@handler_response[:details][:message]}"
                              else
                                "Successfully authenticated from #{@auth_kind} account as #{@app_role}."
                              end
  end

  def facebook_handler_is_false?
    @handler_response.fetch(:type, true) == false
  end

  def user_is_admin?
    @user.admin?
  end

  def user_has_important_notif?
    @notification_id = @user.notifications.revised_terms_and_condition.not_seen.last.try(:id)
    @notification_id.present?
  end

  def event_is_past?
    @event && @event.is_past?
  end

  def event_is_full?
    @event && @event.full?
  end

  def user_can_proceed_checkout?
    #if user is in the trip page with valid required info
    @path && @path.fetch(:controller, nil) == "event" && @path.fetch(:action, nil) == "page" &&  @proceed_checkout
  end

  def redirect_using_referer?
    #if request referer is present and referer path is not equal to organizer sign in path and facebook url
    @request.referer && !%W( #{ organizer_sign_in_path } ).include?(URI(@request.referer).path) && !(@request.referer == "https://www.facebook.com/")
  end

  def dynamic_path
    case @user.role
    when "admin"                then admin_path
    when "joiner_and_organizer" then @app_role == 'joiner' ? trips_path : my_trips_path(@app_role)
    when "joiner"               then @user.sign_in_count == 1 ? root_path : trips_path
    end
  end

  def from_invite_or_referral?
    WHITELISTED_ROLES.include?(@app_role)
  end
end
