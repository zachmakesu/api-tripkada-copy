class PromoCode::ValidateByCategory
  extend LightService::Organizer

  def self.call(code:, user:)
    with(user: user, code: code).reduce(actions)
  end

  def self.actions
    [
      Actions::PromoCode::FindPromoCode,
      Actions::PromoCode::ValidateUsage,
      Actions::PromoCode::ValidateOwnership
    ]
  end
end
