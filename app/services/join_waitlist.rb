class JoinWaitlist
  extend LightService::Organizer

  def self.call(user:, event:)
    with(user: user, event: event).reduce(actions)
  end

  def self.actions
    [
      Actions::JoinWaitlist,
      Actions::NewWaitlistNotifier
    ]
  end
end
