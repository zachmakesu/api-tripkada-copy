class Logger::LogInvalidRequest
  extend LightService::Organizer
  
  def self.call(referrer:, method:, url:, params:)
    with(referrer: referrer, method: method, url: url, params: params).reduce(actions)
  end
  
  def self.actions
  [
    Actions::Logger::LogResourceNotFoundRequest
  ]
  end
end
