class Logger::SendInvalidRequestMailer
  extend LightService::Organizer

  def self.call
    with().reduce(actions)
  end

  def self.actions
    [
      Actions::Logger::SendInvalidRequestMailer,
      Actions::Logger::NotifySlackChannel,
      Actions::Logger::ClearLogger
    ]
  end
end
