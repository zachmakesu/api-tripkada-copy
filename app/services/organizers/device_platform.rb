# Organizers::DevicePlatform
class Organizers::DevicePlatform
  extend LightService::Organizer

  def self.call(params:)
    with(params: params).reduce(actions)
  end

  def self.actions
    Actions::User::IdentifyDevicePlatform
  end
end
