class GoogleAnalytics::UserHandler

  attr_reader :user

  def initialize(user)
    @user = user
  end

  def track!(request)
    update_tracked_fields request
    track_analytics_event
  end

  private
    def update_tracked_fields(request)
      old_current, new_current  = @user.current_sign_in_at, DateTime.now
      @user.last_sign_in_at     = old_current || new_current
      @user.current_sign_in_at  = new_current

      old_current, new_current  = @user.current_sign_in_ip, request.ip
      @user.last_sign_in_ip     = old_current || new_current
      @user.current_sign_in_ip  = new_current

      @user.sign_in_count ||= 0
      @user.sign_in_count += 1

      @user.save!
    end

    def track_analytics_event
      event = (@user.sign_in_count == 1) ? 'Register' : 'Login'
      GoogleAnalyticsApiHandler.new('Users', event, @user.try(:uid), 1).track_event!
    end
end
