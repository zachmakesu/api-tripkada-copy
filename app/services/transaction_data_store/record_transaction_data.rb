class TransactionDataStore::RecordTransactionData
  extend LightService::Organizer

  def self.call(transaction_id:, params:)
    with(transaction_id: transaction_id, params: params).reduce(actions)
  end

  def self.actions
    [
      Actions::TransactionDataStore::RecordTransactionData,
      Actions::TransactionDataStore::ScheduleForPurgingData
    ]
  end
end
