FollowHandlerError = Class.new(StandardError)

class FollowHandler

  attr_accessor :follower, :followed, :response

  def initialize(follower_uid,followed_uid)
    @follower    = User.find_by(uid: follower_uid)                      #joiner who follow
    @followed    = User.find_by(uid: followed_uid) #organizer who's being followed
    @response = Hash.new
  end

  def follow
    return not_found_message if @follower.blank? || @followed.blank?
    ActiveRecord::Base.transaction do
      @follow = @follower.decorate.follow(@followed)
    end
    create_response
    self

  end

  def unfollow
    return not_found_message if @follower.blank? || @followed.blank? || is_not_following?
    ActiveRecord::Base.transaction do
      @follow = @follower.decorate.unfollow(@followed)
    end
    create_response
    self

  end

  private

  def is_not_following?
    !@follower.decorate.following?(@followed)
  end

  def not_found_message
    response[:success] = false
    response[:details] = "User not found"
    self
  end

  def create_response(error=nil)
    if !@follow.nil? && @follow.errors.blank?
      response[:success] = true
      response[:details] = @followed
    else
      response[:success] = false
      response[:details] = error || @follow.errors.full_messages.join(', ')
    end
  end

end
