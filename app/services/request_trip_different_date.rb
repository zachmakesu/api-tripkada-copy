class RequestTripDifferentDate
  extend LightService::Organizer
  
  def self.call(event:, requester:, params:)
    with(event: event, requester: requester, params: params).reduce(
      Actions::RequestTripDifferentDateParametersValidator,
      Actions::ParseRequestedDateTime,
      Actions::ValidateRequestedDateTime,
      Actions::RequestTripDifferentDateMailer,
      Actions::RequestTripDifferentDateNotifier
    )
  end
end
