module Actions
  module Referrals
    class InvitationMailer
      extend LightService::Action
      expects :params, :referrer
      promises :mailer_response

      executed do |context|
        @context = context
        Notifications::EmailWorker.perform_async(email_params, "referral_invite")
        context.mailer_response = "Invitation Successfully Sent"
      end

      def self.email_params
        {
          emails:         @context.params.fetch(:emails, "").split(";"),
          referrer:       @context.referrer.full_name,
          url:            Rails.application.routes.url_helpers.profile_path(@context.referrer),
          referrer_uid:   @context.referrer.uid
        }
      end
    end
  end
end
