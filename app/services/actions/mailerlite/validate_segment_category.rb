module Actions
  module Mailerlite
    class ValidateSegmentCategory
      extend LightService::Action
      expects :booking_count
      promises :category

      executed do |context|
        @context = context
        context.category = category
      end

      def self.category
        membership_count = @context.booking_count
        if membership_count == 1
          "one_off"
        elsif membership_count == 2
          "repeat"
        elsif membership_count >= 3 
          "loyal"
        else
          "default"
        end
      end
    end
  end
end

