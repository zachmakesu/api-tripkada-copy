module Actions
  module Mailerlite
    class SendRequest
      extend LightService::Action
      expects :path, :query_params, :body_params, :request_type
      promises :response

      executed do |context|
        @context = context
        mailerlite_request = response
        mailerlite_error = MailerliteError.from_response(mailerlite_request)
        context.fail_and_return!(mailerlite_error.message) if mailerlite_error&.message
        context.response = mailerlite_request.body
      end
    
      def self.response
        client = MailerliteConnection.initialize
        client.send(@context.request_type.to_sym) do |request|
          request.url(@context.path, @context.query_params)
          request.body = @context.body_params.to_json
        end
      end
    end
  end
end

