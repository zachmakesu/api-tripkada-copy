module Actions
  module Mailerlite
    module Groups
      class BuildSubscribers
        extend LightService::Action
        expects :subscribers
        promises :subscribers

        executed do |context|
          @context = context
          context.fail_and_return!("Please Provide Subscribers") unless context.subscribers
          context.subscribers = subscribers
        end

        def self.subscribers
          [].tap do |subscribers|
            @context.subscribers.each do |subscriber|
              subscribers << { email: subscriber.email }
            end
          end
        end
      end
    end
  end
end
