module Actions
  module Mailerlite
    module Groups
      class ImportSubscribers
        extend LightService::Action
        expects :group_id, :subscribers, :body_params
        promises :path, :request_type, :body_params

        executed do |context|
          context.path = "groups/#{context.group_id}/subscribers/import"
          context.request_type = "post"
          body_params = context.body_params
          body_params[:subscribers] = context.subscribers
        end
      end
    end
  end
end
