module Actions
  module Mailerlite
    module Groups
      class DeleteGroup
        extend LightService::Action
        expects :group_id
        promises :path, :request_type

        executed do |context|
          context.path = "groups/#{context.group_id}"
          context.request_type = "delete"
        end
      end
    end
  end
end
