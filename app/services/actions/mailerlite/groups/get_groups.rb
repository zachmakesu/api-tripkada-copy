module Actions
  module Mailerlite
    module Groups
      class GetGroups
        extend LightService::Action
        promises :path, :request_type

        executed do |context|
          context.path = "groups"
          context.request_type = "get"
        end
      end
    end
  end
end
