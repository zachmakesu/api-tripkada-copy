module Actions
  module Mailerlite
    module Groups
      class RemoveSubscriber
        extend LightService::Action
        expects :group_id, :subscriber_email
        promises :path, :request_type

        executed do |context|
          context.path = "groups/#{context.group_id}/subscribers/#{context.subscriber_email}"
          context.request_type = "delete"
        end
      end
    end
  end
end
