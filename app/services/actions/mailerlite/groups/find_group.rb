module Actions
  module Mailerlite
    module Groups
      class FindGroup
        extend LightService::Action
        expects :category
        promises :group_id
        executed do |context|
          group = MailerliteGroup.find_by(category: context.category)
          context.fail_and_return!("MailerliteGroup not found") unless group
          context.group_id = group.group_id
        end
      end
    end
  end
end
