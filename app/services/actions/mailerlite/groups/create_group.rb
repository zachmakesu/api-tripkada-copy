module Actions
  module Mailerlite
    module Groups
      class CreateGroup
        extend LightService::Action
        expects :name, :body_params
        promises :path, :request_type, :body_params

        executed do |context|
          context.fail_and_return!("Please Provide Group Name") unless context.name
          context.path = "groups"
          context.request_type = "post"
          body_params = context.body_params
          body_params[:name] = context.name
          context.body_params = body_params
        end
      end
    end
  end
end
