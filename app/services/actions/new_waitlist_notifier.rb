module Actions
  class NewWaitlistNotifier
    extend LightService::Action
    expects :event, :user

    executed do |context|
      @event = context.event
      ::Notification.slot_reminder.create(notification_params)
    end

    def self.notification_params
      {
        user_id: @event.owner.id,
        message: notification_message
      }
    end

    def self.notification_message
      "A user joined the waitlist of your event "\
        "<a href=#{Rails.application.routes.url_helpers.trip_view_path(@event)}>"\
          "#{@event.name}</a>."
    end

  end
end
