module Actions
  class UpdateAvatar
  extend LightService::Action
  expects :user, :params


  executed do |context|
    @context = context
    context.fail_and_return!('Avatar params is not present') if context.params[:avatar].nil?

    photo = context.user.photos.find_or_initialize_by(primary: true, category: "avatar")
    photo.update(context.params[:avatar])
  end

  end
end
