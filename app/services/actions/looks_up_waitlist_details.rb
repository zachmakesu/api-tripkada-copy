module Actions
  class LooksUpWaitlistDetails
  extend LightService::Action
  expects :params, :current_user
  promises :event, :user, :waitlist, :event_membership

  executed do |context|
    @context = context
    context.event = ::Event.includes(:waitlist_members).find_by(id: context.params.fetch(:id, nil))
    context.fail_and_return!("Unauthorized access!") unless user_is_authorized?
    context.waitlist = ::Waitlist.find_by(id: context.params.fetch(:waitlist_id, nil))
    context.user = ::User.find_by(id: context.waitlist.user_id)
    context.fail_and_return!("Invalid Parameters") unless valid_params?
    context.event_membership = ::EventMembership.where(member_id: context.user.id, event_id: context.event.id).last
    context.fail_and_return!("User is already a joiner of this event.") if context.event_membership.present?
  end

  def self.user_is_authorized?
    @context.current_user.joiner_and_organizer? || @context.event.owner == @context.current_user
  end

  def self.valid_params?
    @context.waitlist && @context.event && @context.user
  end

  end
end
