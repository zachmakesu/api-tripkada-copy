module Actions
  module User
    class EarnReferralIncentive
      extend LightService::Action
      expects :referrer
      promises :referrer_credit_promo_code, :incentive_log

      executed do |context|  
        if referrer = context.referrer
          credit = referrer.credit_promo_code
          credit_value = credit.value + ReferralIncentive.latest
          incentive_log = credit.credit_logs.earned_incentive.create(amount: credit_value)
        end
        context.referrer_credit_promo_code = credit&.reload
        context.incentive_log = incentive_log
      end
    end
  end
end
