module Actions
  module User
    class ReferralIncentiveMailer
      extend LightService::Action
      expects :referrer, :event, :joiner
      promises :mailer_response

      executed do |context|  
        if context.referrer && context.event
          @referrer_id = context.referrer.id
          @event_id = context.event.id

          @joiner_id = context.joiner.id
          send_to_referrer
          send_to_staff
          mailer_response = "Referral Mailer Successfully Sent"
        else
          mailer_response = "Please provide all parameters"
        end
        context.mailer_response = mailer_response
      end

      def self.send_to_staff
        Notifications::ReferralIncentiveWorker.perform_async(@referrer_id, @joiner_id, @event_id, "staff")
      end

      def self.send_to_referrer
        Notifications::ReferralIncentiveWorker.perform_async(@referrer_id, @joiner_id, @event_id, "referrer")
      end
    end
  end
end
