module Actions
  module User
    class IdentifyDevicePlatform
      extend LightService::Action

      expects :params
      promises :device_platform

      executed do |ctx|
        @params = ctx.params
        case @params[:type]
        when "controller"
          ctx.device_platform = controller_device_platform
        when "endpoint"
          ctx.device_platform = endpoint_device_platform
        end
      end

      def self.controller_device_platform
        browser = Browser.new(user_agent: @params[:user_agent])
        if browser.ios?
          "ios_web"
        elsif browser.android?
          "android_web"
        else
          "desktop"
        end
      end

      def self.endpoint_device_platform
        browser = Browser.new(user_agent: @params[:user_agent])
        browser.ios? ? "ios_app" : "android_app"
      end
    end
  end
end
