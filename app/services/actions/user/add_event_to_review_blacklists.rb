module Actions
  module User
    class AddEventToReviewBlacklists
      extend LightService::Action
      expects :user, :event
      promises :event

      executed do |context|
        context.fail_and_return!("Please Provide User and Event") if context.event.nil? || context.user.nil?
        blacklist = context.user.review_blacklists.new(event_id: context.event.id)
        context.fail_and_return!(blacklist.errors.full_messages.to_sentence) unless blacklist.save
      end
    end
  end
end
