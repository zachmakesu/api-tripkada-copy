module Actions
  module User
    class PhotoMailer
      extend LightService::Action
      expects :user, :category, :photos
      promises :mailer_response

      executed do |context|
        if context.category == "verification_photos" 
          photo_ids = context.photos.map(&:id)
          AdminNotifications::PhotoMailerWorker.perform_async(context.user.id, photo_ids)
          mailer_response = "Photo Verification Email for Admin Successfully Sent"
        end
        context.mailer_response = mailer_response
      end
    end
  end
end
