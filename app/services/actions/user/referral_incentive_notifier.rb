module Actions
  module User
    class ReferralIncentiveNotifier
      extend LightService::Action
      expects :joiner, :referrer, :event
      promises :notifier_response

      executed do |context|
        if context.joiner && context.referrer && context.event
          Notifications::ReferralIncentiveNotifierWorker.perform_async(context.joiner.uid, context.referrer.uid, context.event.id)
          notifier_response = "Referral Incentive Notification Successfully Created"
        else
          notifier_response = "Please provide all parameters"
        end
        context.notifier_response = notifier_response
      end
    end
  end
end
