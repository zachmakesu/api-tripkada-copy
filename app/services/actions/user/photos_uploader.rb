module Actions
  module User
    class PhotosUploader
      extend LightService::Action
      expects :photos, :user, :category
      promises :photos

      executed do |context|
        @context = context
        context.fail_and_return!("Invalid Photo Category") unless valid_category?
        user_photos = context.user.send(context.category)
        photos = user_photos.build(context.photos)
        if photos.map(&:invalid?).any?
          error_messages = photos.map{ |c| c.errors.full_messages }.flatten
          context.fail_and_return!(error_messages.to_sentence)
        end
        photos.map(&:save)
        context.photos = photos
      end

      def self.valid_category?
        # validate category based on user association
        # primarily used for #send
        valid_categories = %w{ government_photos profile_photos certificates verification_photos }
        valid_categories.include? @context.category
      end
    end
  end
end
