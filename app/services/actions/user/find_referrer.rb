module Actions
  module User
    class FindReferrer
      extend LightService::Action
      expects :joiner
      promises :referrer

      executed do |context|  
        referrer = context.joiner.accepted_referral.try(:referrer)
        context.referrer = referrer
        context.skip_remaining! unless referrer
      end
    end
  end
end
