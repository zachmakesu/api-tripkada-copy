module Actions
  class WaitlistStatusValidator
    extend LightService::Action
    expects :event, :user, :waitlist, :event_membership, :params
    promises :status

    executed do |context|
      @context = context
      case @context.params[:waitlist_action]
      when "approve"
        event_full_message = "Event is full. You need to update the max pax "\
                               "of this event to approve user in the waitlist."
        context.fail_and_return!(event_full_message) if context.event.full_with_approved_waitlist?
        context.status = "approved"
      when "deny"
        context.status = "denied"
      else
        context.status = "pending"
      end
      update_waitlist_by(status: context.status)
    end

    def self.update_waitlist_by(status:)
      @context.waitlist.send("#{status}!")
      rescue NoMethodError => e
        @context.fail_and_return!("Invalid Status, #{e.message}")
      end
    end
end
