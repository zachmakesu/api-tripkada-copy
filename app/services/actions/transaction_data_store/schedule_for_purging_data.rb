module Actions
  module TransactionDataStore
    class ScheduleForPurgingData
      extend LightService::Action
      expects :transaction_data
      promises :transaction_data_cleaner_response
      
      executed do |ctx|
        transaction_data = ctx.transaction_data
        next ctx unless transaction_data
        # Purge After 2 weeks
        ::TransactionDataStore::DataCleanerWorker.perform_at(transaction_data.created_at + 2.weeks, transaction_data.transaction_id)
        ctx.transaction_data_cleaner_response = "Successfully Schedule for purging"
      end
    end
  end
end
