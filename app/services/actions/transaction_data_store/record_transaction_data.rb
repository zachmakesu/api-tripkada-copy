module Actions
  module TransactionDataStore
    class RecordTransactionData
      extend LightService::Action
      
      expects :transaction_id, :params
      promises :transaction_data

      executed do |ctx|
        @ctx = ctx
        data = ::TransactionDataStore.new(transaction_data_params)
        unless data.save
          ctx.fail_and_return!(data.errors.full_messages.to_sentence)
        end
        ctx.transaction_data = data
      end

      def self.transaction_data_params
        {
          transaction_id: @ctx.transaction_id,
          data: @ctx.params
        }
      end
    end
  end
end
