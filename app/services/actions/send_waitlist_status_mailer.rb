module Actions
  class SendWaitlistStatusMailer
    extend LightService::Action
    expects :event, :user ,:waitlist
    promises :waitlist_membership_mailer_response

    executed do |ctx|
      Notifications::WaitlistMembershipMailerWorker.perform_async(ctx.event.id, ctx.user.id, ctx.waitlist.status)
      ctx.waitlist_membership_mailer_response = "Waitlist Membership Status Mailer Successfully Sent!"
    end
  end
end
