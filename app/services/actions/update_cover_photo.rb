module Actions
  class UpdateCoverPhoto
  extend LightService::Action
  expects :user, :params

  executed do |context|
    @context = context
    cover_photo = context.user.photos.find_or_initialize_by(category: 'cover_photo', primary: true)
    if context.params[:cover_photo].present?
      context.fail!("Cover photo failed to update") unless cover_photo.update(context.params[:cover_photo])
    end    
  end

  end
end
