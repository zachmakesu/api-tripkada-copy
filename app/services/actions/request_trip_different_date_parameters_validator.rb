module Actions
  class RequestTripDifferentDateParametersValidator
    extend LightService::Action
    REQUIRED_PARAMETERS = %w{ start_date_time end_date_time requested_pax }
    expects :params

    executed do |context|
      missing_keys = REQUIRED_PARAMETERS.map(&:to_sym) - context.params.keys.map(&:to_sym)
      context.fail!("Please Complete All Details") if missing_keys.any?
    end
  end
end
