module Actions
  class ParseInvitedJoiners
    extend LightService::Action
    expects :names, :emails
    promises :data

    executed do |context|
      context.data = []
      next context if context.names == "undefined" || context.emails == "undefined"
      context.names.each_with_index do |name, i|
        hash = {}
        hash[:name] = name
        hash[:email] = context[:emails][i]
        context.data.push(hash)
      end
    end
  end
end
