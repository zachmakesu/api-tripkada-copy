module Actions
  class ParseRequestedDateTime
    extend LightService::Action
    expects :params
    promises :start_datetime, :end_datetime

    executed do |context|
      @context = context
      context.start_datetime = parse(date: context.params.fetch(:start_date_time))
      context.end_datetime = parse(date: context.params.fetch(:end_date_time))
    end

    def self.parse(date:)
      begin
        DateTime.parse(date).utc
      rescue => e
        @context.fail!("Invalid Date Time")
      end
    end
  end
end
