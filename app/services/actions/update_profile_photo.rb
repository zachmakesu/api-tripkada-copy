module Actions
  class UpdateProfilePhoto
  extend LightService::Action
  expects :user, :params

  executed do |context|
    @context = context
    next context unless profile_photos_params_present?('Profile photos params is not present')
    photos = context.params[:profile_photos].delete_if { |i| i.count == 1 }
    user_photos = context.user.profile_photos

    photos.each_with_index.map do |photo, i|
      if photo[:cover]
        context.user.cover_photo.update(primary: false, category: 'avatar') if user_photos[i].present? && context.user.cover_photo.present?
        user_photos[i].update(primary: true, category: 'cover_photo') if user_photos[i].present?
        context.user.cover_photo.destroy if photo[:delete] && context.user.cover_photo.present? && (photo[:delete] != photo[:cover])
      end
      if photo[:edit]
        photo_params = { image: existing?(photo[:photo]) ? user_photos[i].image : photo[:photo], label: photo[:label] }
        user_photos[i].present? ? user_photos[i].update(photo_params) : context.user.model.profile_photos.build(photo_params).save
      elsif changeable_photo?(photo, user_photos[i])
        photo_params = { image: photo[:photo], label: photo[:label] }
        if photo[:cover]
          context.user.cover_photo.update(photo_params)
        else
          photo[:delete].blank? ? context.user.model.profile_photos.build(photo_params).save : user_photos[i].update(photo_params)
        end
      end

      context.user.model.profile_photos.find_by(id: photo[:delete].to_i).destroy if deletable?(photo)
    end
  end

  def self.profile_photos_params_present?(message)
    if @context.params[:profile_photos].present?
      return true
    else
      @context.fail!(message)
      false
    end
  end

  def self.deletable?(photo_params)
    photo_params[:delete] && photo_params[:photo].blank? && photo_params[:cover].nil?
  end

  def self.existing?(photo)
    (photo.is_a? String) && photo.present?
  end

  def self.changeable_photo?(photo_param, user_photo)
    photo_param[:photo].present? && (user_photo&.label == photo_param[:label] || user_photo.nil?) && !(photo_param[:photo].is_a? String)
  end

  end
end
