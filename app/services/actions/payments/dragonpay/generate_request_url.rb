module Actions
  module Payments
    module Dragonpay
      class GenerateRequestURL
        extend LightService::Action
        expects :total_amount, :total_amount_less_promo_code,
                :joiner, :params, :event
        promises :generated_request_url, :generated_transaction_id

        executed do |context|
          @context = context
          @transaction_id = ::DragonpayTransactionLog.generate_transaction_id
          params = client_params

          client = DragonPay::Client.new(params)
          # set mode to '1' to filter payment options using online payment banking 
          # 5.4.1.1 https://www.dragonpay.ph/wp-content/uploads/2016/07/Dragonpay-PS-API.pdf
          # appended on generated url beacuse mode not supported in gem params, 
          # see http://www.rubydoc.info/gems/dragon_pay/0.5.2/DragonPay/Client 
          context.generated_request_url = client.generate_request_url
          context.generated_transaction_id = @transaction_id
        end

        def self.client_params
          amount = @context.total_amount_less_promo_code || @context.total_amount
          {
            transaction_id: @transaction_id,
            customer_email_address: @context.joiner.email,
            amount: amount,
            param1: @context.joiner.uid,
            param2: @context.event.id
          }
        end
      end
    end
  end
end
