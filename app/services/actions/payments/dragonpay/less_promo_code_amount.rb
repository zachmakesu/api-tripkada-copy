module Actions
  module Payments
    module Dragonpay
      class LessPromoCodeAmount
        extend LightService::Action
        expects :total_amount, :event, :promo_code
        promises :total_amount_less_promo_code

        executed do |ctx|
          event = ctx.event.decorate
          rate = event.rate_with_fee
          downpayment = event.downpayment_rate_with_fee
          subtotal = ctx.total_amount
          promo_code = ctx.promo_code
          ctx.total_amount_less_promo_code = nil
          next ctx unless promo_code && (rate == downpayment)
          ctx.total_amount_less_promo_code = subtotal - promo_code.value
        end
      end
    end
  end
end
