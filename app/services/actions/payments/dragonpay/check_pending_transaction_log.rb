module Actions
  module Payments
    module Dragonpay
      class CheckPendingTransactionLog
        extend LightService::Action
        expects :params
        promises :transaction_exists, :pending_transaction_log, :payment, :membership

        executed do |ctx|
          # set default values
          ctx.transaction_exists = false
          ctx.pending_transaction_log = nil
          ctx.payment = nil
          ctx.membership = nil

          transaction_id = ctx.params.fetch(:txnid, nil)
          transaction = ::DragonpayTransactionLog.find_by(transaction_id: transaction_id, status: "pending")
          if transaction
            ctx.transaction_exists = true
            ctx.pending_transaction_log = transaction
            ctx.payment = transaction.payment
            ctx.membership = transaction.payment.event_membership
          end
        end
      end
    end
  end
end
