module Actions
  module Payments
    module Dragonpay
      class ValidatePaymentStatus
        extend LightService::Action
        expects :params
        promises :status

        executed do |ctx|
          @ctx = ctx
          @status = ctx.params.fetch(:status, nil)
          # validate response status from callback params
          # https://www.dragonpay.ph/wp-content/uploads/2016/07/Dragonpay-PS-API.pdf
          # page 29
          ctx.status = status_in_words
        end

        def self.status_in_words
          case @status
          when "S" then "approved"
          when "P" then "pending"
          else
            "declined"
          end
        end
      end
    end
  end
end


