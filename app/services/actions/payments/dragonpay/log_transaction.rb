module Actions
  module Payments
    module Dragonpay
      class LogTransaction
        extend LightService::Action

        expects :payment, :params, :status, :approved_payment 
        promises :transaction_log

        executed do |ctx|
          @ctx = ctx
          @ctx_params = ctx.params
          @payment = ctx.payment || ctx.approved_payment
          log = ::DragonpayTransactionLog.new(transaction_log_params)
          ctx.fail_with_rollback!(log.errors.full_messages.to_sentence) unless log.save
          ctx.transaction_log = log
        end

        rolled_back do |ctx|
          ctx.transaction_log.destroy if ctx.transaction_log
        end

        def self.transaction_log_params
          {
            payment_id: @payment.id,
            transaction_id: @ctx_params.fetch(:txnid, nil),
            reference_number: @ctx_params.fetch(:refno, nil),
            status: @ctx.status,
            message: @ctx_params.fetch(:message, nil),
            response: @ctx_params
          }
        end
      end
    end
  end
end
