module Actions
  module Payments
    module Dragonpay
      class CalculateTotalAmount
        extend LightService::Action
        expects :event, :with_invited_joiners, :invited_joiners_count
        promises :total_amount

        executed do |context|
          @event = context.event
          @ctx = context
          transaction_fee = ::TransactionFee.latest(category: "dragonpay")

          context.fail_and_return!("Ooops!, Transaction fee not found") unless transaction_fee

          amount = if context.with_invited_joiners
            total_amount_with_invited_joiners
          else
            total_amount
          end
          context.total_amount = amount + transaction_fee.amount
        end

        def self.total_amount
          @event.decorate.downpayment_rate_with_fee
        end

        def self.total_amount_with_invited_joiners
          joiner_count = 1 + @ctx.invited_joiners_count
          joiner_count * total_amount
        end
      end
    end
  end
end
