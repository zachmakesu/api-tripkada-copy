module Actions
  class ValidateRequestedDateTime
    extend LightService::Action
    expects :start_datetime, :end_datetime, :event

    executed do |context|
      if context.start_datetime < DateTime.now.utc || context.end_datetime < context.start_datetime
          context.fail!("Invalid Date Range")
      end
    end
  end
end
