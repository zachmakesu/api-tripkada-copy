module Actions
  class WaitlistNotifier
    extend LightService::Action
    extend ActionView::Helpers::TagHelper
    expects :event, :user, :waitlist, :status, :params
    promises :feedback, :notification

    executed do |context|
      @context = context
      notification = Notification.waitlist_status.new(
        notification_params
      )
      unless notification.save
        context.fail_and_return!(notification.error.full_messages.to_sentence)
      end
      context.feedback = feedback
      context.notification = notification
    end

    def self.feedback
      {
        message: notification_message,
        action: @context.params[:waitlist_action],
        status: @context.status
      }
    end

    def self.notification_params
      {
        user_id: @context.user.id,
        message: notification_message,
        notificationable_id: @context.event.id,
        notificationable_type: "Event"
      }
    end

    def self.notification_message
      "Your waitlist for #{@context.event.name}"\
        " request has been #{@context.status}"
    end
  end
end
