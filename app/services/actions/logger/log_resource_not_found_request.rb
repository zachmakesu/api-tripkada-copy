module Actions
  module Logger
    class LogResourceNotFoundRequest
      extend LightService::Action
      expects :referrer, :method, :url, :params
      promises :logger_response

      executed do |ctx|
        @request = ctx
        @timestamp = DateTime.now
        save_path = Rails.root.join('public', 'invalid_request_logs.txt') 
        File.open(save_path, "a+"){|f| f << "#{logger_details} \n"}
        ctx.logger_response = "Successfully log request"
      end

      def self.logger_details
        "REQUEST DETAILS - #{@timestamp}: " \
        "Referrer: #{@request[:referrer]} || "\
        "Request URL: #{@request[:url]} || "\
        "Params: #{@request[:params]} || "\
        "Method: #{@request[:method]}"
      end
    end
  end
end
