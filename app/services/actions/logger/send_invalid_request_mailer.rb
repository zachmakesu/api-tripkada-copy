module Actions
  module Logger
    class SendInvalidRequestMailer
      extend LightService::Action
      promises :invalid_request_mailer_response
      executed do |ctx|
        AdminMailer.invalid_request.deliver
        ctx.invalid_request_mailer_response = "Mailer Successfully Sent!"
      end
    end
  end
end
