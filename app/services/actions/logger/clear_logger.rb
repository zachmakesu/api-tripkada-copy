module Actions
  module Logger
    class ClearLogger
      extend LightService::Action
      promises :clear_logger_response

      executed do |ctx|
        clear_logger
        ctx.clear_logger_response = "Logger Successfully Cleared!"
      end

      def self.clear_logger
        logger_path = Rails.root.join('public', 'invalid_request_logs.txt') 
        File.open(logger_path, "w"){}
      end
    end
  end
end
