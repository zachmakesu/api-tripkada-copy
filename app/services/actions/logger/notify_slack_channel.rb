module Actions
  module Logger
    class NotifySlackChannel
      extend LightService::Action
      promises :slack_notifier_response
      
      executed do |ctx|
        notify_slack_channel
        ctx.slack_notifier_response = "Notification Successfully Sent!"
      end

      def self.notify_slack_channel
        webhook_url = "https://hooks.slack.com/services/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"
        channel = "#tripkada-web-and-api"
        notifier = Slack::Notifier.new webhook_url, channel: channel, username: "Tripkada Bot #{Rails.env.titleize}"
        notifier.ping text: "Invalid Request Logs Mailer Successfully Sent!",  
          attachments: [
          {
            "fallback": "Invalid Request Log Mailer",
            "color": "good",
            "fields": [
              {
                "title": "Invalid Request Log Mailer Job",
                "value": "Success"
              }
            ]
          }
        ]
      end
    end
  end
end
