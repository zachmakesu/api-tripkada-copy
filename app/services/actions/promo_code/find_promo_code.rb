module Actions
  module PromoCode
    class FindPromoCode
      extend LightService::Action

      expects :code
      promises :promo_code

      executed do |context|
        if context.code.present?
          promo_code = ::PromoCode.find_by_code(context.code)
          context.fail_and_return!("Promo Code not found") unless promo_code
        end
        context.promo_code = promo_code
      end
    end
  end
end
