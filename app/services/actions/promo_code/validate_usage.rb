module Actions
  module PromoCode

    class ValidateUsage
      extend LightService::Action

      expects :promo_code
      promises :is_usable

      executed do |context|
        if context.promo_code&.corporate?
          response = context.promo_code&.usable?
          context.fail_and_return!("promo code is not usable") unless response
        end
        context.is_usable = response
      end
    end

  end
end
