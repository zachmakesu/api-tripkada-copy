module Actions
  module PromoCode

    class ValidateOwnership
      extend LightService::Action

      expects :promo_code, :user
      promises :belongs_to_user

      executed do |context|
        context.belongs_to_user = ""
        next context unless context.promo_code.present?
        unless context.promo_code.corporate?
          response = context.promo_code.belongs_to_owner?(user: context.user)
          context.fail_and_return!("promo code does not belong to user") unless response
        end
        context.belongs_to_user = response
      end
    end

  end
end
