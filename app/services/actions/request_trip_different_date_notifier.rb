module Actions
  class RequestTripDifferentDateNotifier
    extend LightService::Action
    expects :params, :requester, :event, :start_datetime, :end_datetime
    promises :notifier_response

    executed do |context|
      context.fail_and_return!("Please Provide Requester and Event") unless context.requester && context.event
      Notifications::RequestTripDifferentDateNotifierWorker.perform_async(context.event.id, context.requester.uid, context.params, context.start_datetime, context.end_datetime)
      context.notifier_response = "Notification Successfully Created"
    end
  end
end
