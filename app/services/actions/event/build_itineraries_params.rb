module Actions
  module Event

    class BuildItinerariesParams
      extend LightService::Action

      expects :params
      promises :itineraries_params

      executed do |context|
        @context = context
        @params = context.params

        context.itineraries_params = itineraries_params
      end

      def self.itineraries_params
        @params.fetch(:itineraries_description).reject(&:empty?).each_with_index.map do |x,i|
          { description: @params.fetch(:itineraries_description)[i] }
        end if @params.fetch(:itineraries_description, nil)
      end
    end

  end
end
