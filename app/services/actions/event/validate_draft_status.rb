module Actions
  module Event

    class ValidateDraftStatus
      extend LightService::Action
      using StringToBooleanRefinements

      expects :params
      promises :set_as_draft

      executed do |context|
        @context = context
        @params = context.params

        context.set_as_draft = set_as_draft?
      end

      def self.set_as_draft?
        begin
          @params.fetch(:draft, "false").to_s.to_bool
        rescue ArgumentError => e
          @context.fail!("#{e.message}, Invalid value for Drafts., should be true or false")
        end
      end
    end

  end
end
