module Actions
  module Event
    class CreateCategorizations
      extend LightService::Action

      expects :event, :categorizations_params, :unused_categorizations_ids
      promises :categorizations

      executed do |context|

        @existing_categorizations = context.event.categorizations.ids
        @categorizations = context.event.categorizations.create(context.categorizations_params)

        unused_categorization_ids = context.unused_categorizations_ids

        @unused_categories = context.event.categorizations.where(
          id: unused_categorization_ids
        ).map(&:category_id)
        
        context.fail_with_rollback!(categorizations_errors) if @categorizations.map(&:invalid?).any?

        context.event.categorizations.where(id: unused_categorization_ids).destroy_all if unused_categorization_ids.any?

        context.categorizations = @categorizations
      end

      rolled_back do |context|
        context.event.categorizations.where.not(id: @existing_categorizations).destroy_all
        context.event.categorizations.create(unused_categorizations_params) if unused_categorizations_params.any?
      end

      def self.categorizations_errors
        @categorizations.map{ |c| c.errors.full_messages }.uniq.flatten.to_sentence
      end

      def self.unused_categorizations_params
        Category.where(
          id: @unused_categories
        ).map{|category| { category_id: category.id } }
      end
    end
  end
end
