module Actions
  module Event
    class NotifyAdminNewTrip
      extend LightService::Action

      expects :event, :set_as_draft

      executed do |context|
        next context if context.set_as_draft
        event = context.event.reload
        AdminNotifications::EventMailerWorker.perform_async(event.id, "new_trip")
      end
    end
  end
end
