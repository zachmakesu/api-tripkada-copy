module Actions
  module Event
    module Booking
      class RegisterTripNotifications
        extend LightService::Action

        expects :event, :membership, :approved_payment
        promises :trip_notifications_response

        executed do |ctx|
          ctx.trip_notifications_response = ""
          payment = ctx.membership&.payment || ctx.approved_payment

          next ctx unless payment.approved?

          Notifications::TripsWorker.perform_async(ctx.event.id, payment.event_membership.id)
          ctx.trip_notifications_response = "Trip Notifications Successfully Sent!"
        end
      end
    end
  end
end
