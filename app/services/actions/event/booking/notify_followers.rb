module Actions
  module Event
    module Booking
      class NotifyFollowers
        extend LightService::Action

        expects :event, :joiner, :membership, :approved_payment
        promises :follower_notification_response

        executed do |ctx|
          ctx.follower_notification_response = ""
          payment = ctx.membership&.payment || ctx.approved_payment

          next ctx unless payment.approved?

          FollowHandler.new(ctx.joiner.uid, ctx.event.owner.uid).follow
          ctx.follower_notification_response = "Follower Notification Successfully Sent!"
        end
      end
    end
  end
end
