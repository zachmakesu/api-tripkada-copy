module Actions
  module Event
    module Booking
      class SendPaymentNotifications
        extend LightService::Action

        expects :membership, :approved_payment
        promises :payment_notification_response

        executed do |ctx|
          ctx.payment_notification_response = ""
          payment = ctx.membership&.payment || ctx.approved_payment

          next ctx unless payment.approved?
          
          Notifications::PaymentsWorker.perform_async(payment.event_membership.id)
          ctx.payment_notification_response = "Payment Notification Successfully Sent!"
        end
      end
    end
  end
end
