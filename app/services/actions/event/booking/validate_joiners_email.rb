module Actions
  module Event
    module Booking
      class ValidateJoinersEmail
        extend LightService::Action
        expects :with_invited_joiners, :invited_joiner_emails, :joiner
        promises :invited_joiner_emails_are_valid, :has_duplicate_emails

        executed do |ctx|
          emails = []
          emails << ctx.joiner.email
          (emails << ctx.invited_joiner_emails) if ctx.with_invited_joiners
          ctx.invited_joiner_emails_are_valid = false
          ctx.has_duplicate_emails = false

          if ctx.with_invited_joiners
            check_duplicate_emails(
              invitee_emails: ctx.invited_joiner_emails,
              ctx: ctx
            )
          end
          emails.flatten.each { |email| validate_email(email: email, ctx: ctx) }

          ctx.invited_joiner_emails_are_valid = true
          ctx.has_duplicate_emails = true
        end

        def self.validate_email(email:, ctx:)
          email_address = EmailAddress.new(email)
          error = "#{email} is not a valid email!, Please Provide Valid Email"
          exclude_fb = email.exclude?("facebook.com")
          ctx.fail_and_return!(error) unless email_address.valid? && exclude_fb
        end

        def self.check_duplicate_emails(invitee_emails:, ctx:)
          emails = invitee_emails.flatten
          duplicate_emails = emails.select { |e| emails.count(e) > 1 }
          error = "Please avoid duplicate emails! "\
                  "#{duplicate_emails.uniq.to_sentence}"
          ctx.fail_and_return!(error) if duplicate_emails.present?
        end
      end
    end
  end
end
