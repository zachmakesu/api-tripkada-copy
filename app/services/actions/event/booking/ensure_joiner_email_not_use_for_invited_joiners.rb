module Actions
  module Event
    module Booking
      # Validate if Joiner EMails is Use for Invited Joiners
      class EnsureJoinerEmailNotUseForInvitedJoiners
        extend LightService::Action
        expects :with_invited_joiners, :invited_joiner_emails, :joiner
        promises :invited_joiner_emails_are_valid

        executed do |ctx|
          ctx.invited_joiner_emails_are_valid = false
          if ctx.with_invited_joiners
            validate_emails(
              email: ctx.joiner.email,
              invited_joiner_emails: ctx.invited_joiner_emails,
              ctx: ctx
            )
          end
          ctx.invited_joiner_emails_are_valid = true
        end

        def self.validate_emails(email:, invited_joiner_emails:, ctx:)
          joiner_email_error = "#{email} "\
                               "cannot be used for invited joiners"
          is_included = invited_joiner_emails.include?(email)
          ctx.fail_and_return!(joiner_email_error) if is_included
        end
      end
    end
  end
end
