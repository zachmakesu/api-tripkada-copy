module Actions
  module Event
    module Booking
      class CreateMembership
        extend LightService::Action

        expects :event, :joiner, :transaction_exists
        promises :membership

        executed do |ctx|
          ctx.membership = nil
          next ctx if ctx.transaction_exists
          
          membership = ctx.joiner.event_memberships.new(event_id: ctx.event&.id)
          ctx.fail_and_return!("Failed to create membership for #{ctx.joiner.decorate&.full_name}, #{membership.errors.full_messages.to_sentence}") unless membership.save
          ctx.membership = membership
        end

        rolled_back do |ctx|
          ctx.membership.destroy if ctx.membership
        end
      end
    end
  end
end
