module Actions
  module Event
    module Booking
      class MailerliteAutomatedGroupSubscription
        extend LightService::Action

        expects :joiner, :membership, :approved_payment

        executed do |ctx|
          payment = ctx.membership&.payment || ctx.approved_payment

          next ctx unless Rails.env.production? && payment.approved?
          
          joiner = ctx.joiner
          ::Mailerlite::AddRemoveSubscriberWorker.perform_async(
            joiner.uid, 
            joiner.approved_event_memberships.count
        )
        end
      end
    end
  end
end
