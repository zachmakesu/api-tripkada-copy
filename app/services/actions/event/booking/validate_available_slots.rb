module Actions
  module Event
    module Booking
      class ValidateAvailableSlots
        extend LightService::Action

        expects :event, :with_invited_joiners, :invited_joiners_count, :transaction_exists
        promises :is_sufficient

        executed do |ctx|
          @ctx = ctx
          ctx.is_sufficient = false
          next ctx if ctx.transaction_exists
          slots = ctx.event.decorate.slots_left
          is_sufficient = if ctx.with_invited_joiners
                            slots >= total_joiners_count
                          else
                            slots >= 1
                          end
          ctx.fail_and_return!("Insufficient Trip Slot") unless is_sufficient
          ctx.is_sufficient = is_sufficient
        end

        def self.total_joiners_count
          @ctx.invited_joiners_count + 1 # add 1 for joiner
        end

      end
    end
  end
end
