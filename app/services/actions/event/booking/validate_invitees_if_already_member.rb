module Actions
  module Event
    module Booking
      # Validate Invited Joiners if already member of the trip
      class ValidateInviteesIfAlreadyMember
        extend LightService::Action
        expects :with_invited_joiners, :invited_joiner_emails, :event
        promises :invitee_is_already_trip_member

        executed do |ctx|
          @duplicate_invitee = []
          ctx.invitee_is_already_trip_member = false
          next ctx unless ctx.with_invited_joiners
            check_duplicate_invitee(
              invitee: ctx.invited_joiner_emails,
              event: ctx[:event],
            )
          has_duplicates = @duplicate_invitee.present?
          ctx.invitee_is_already_trip_member = has_duplicates
          ctx.fail_and_return!("#{@duplicate_invitee.to_sentence} is/are already member/s for this trip") if has_duplicates
        end

        def self.check_duplicate_invitee(invitee:, event:)
          invitee.flatten.each do |x|
            @duplicate_invitee << x if event.members.where(email: x).exists?
          end
        end
      end
    end
  end
end
