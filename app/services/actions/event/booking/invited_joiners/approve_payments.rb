module Actions
  module Event
    module Booking
      module InvitedJoiners
        class ApprovePayments
          extend LightService::Action

          expects :invited_joiners_memberships
          promises :approved_invited_joiners_payments, :memberships

          executed do |ctx|
            ctx.approved_invited_joiners_payments = []
            ctx.memberships = ctx.invited_joiners_memberships

            next ctx if ctx.memberships.empty?

            ctx.memberships.each do |membership|
              payment = membership.payment
              payment.approved!
              ctx.approved_invited_joiners_payments << payment
            end
          end

          rolled_back do |ctx|
            ctx.approved_invited_joiners_payments.update_all(status: "pending") unless ctx.approved_invited_joiners_payments.empty?
          end
        end
      end
    end
  end
end

