module Actions
  module Event
    module Booking
      module InvitedJoiners
        class RegisterNewUsers
          extend LightService::Action

          expects :new_users_details
          promises :new_users

          executed do |ctx|
            ctx.new_users = []

            next ctx if ctx.new_users_details.empty?
            ctx.new_users_details.each do |user|
              @user = user
              new_user = ::User.new(user_params)
              ctx.fail_and_return!(new_user.errors.full_messages.to_sentence) unless new_user.save
              ctx.new_users << new_user
            end
          end

          rolled_back do |ctx|
            ctx.new_users.map(&:destroy) if ctx.new_users.present?
          end

          def self.user_params
            {
              email: @user[:email],
              first_name: @user[:name],
              password: password
            }
          end

          def self.password
            Devise.friendly_token[0, 10].downcase
          end
        end
      end
    end
  end
end
