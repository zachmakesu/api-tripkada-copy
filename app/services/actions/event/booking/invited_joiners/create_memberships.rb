module Actions
  module Event
    module Booking
      module InvitedJoiners
        class CreateMemberships
          extend LightService::Action

          expects :existing_users, :new_users, :event, :with_invited_joiners
          promises :invited_joiners, :memberships

          executed do |ctx|
            ctx.memberships = []
            ctx.invited_joiners = []
            next ctx unless ctx.with_invited_joiners
            invited_joiners = ctx.existing_users << ctx.new_users
            invited_joiners.flatten.each do |user|
              membership = user.event_memberships.new(event_id: ctx.event&.id)
              ctx.fail_and_return!("Failed To Create Membership for Invited Joiner #{user.decorate.full_name}, #{membership.errors.full_messages.to_sentence}") unless membership.save
              ctx.memberships << membership
            end
            ctx.invited_joiners = invited_joiners
          end

          rolled_back do |ctx|
            ctx.memberships.destroy_all if ctx.memberships.present?
          end

        end
      end
    end
  end
end
