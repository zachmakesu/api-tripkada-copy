module Actions
  module Event
    module Booking
      module InvitedJoiners
        class FindExistingUsers
          extend LightService::Action

          expects :invited_joiners_details, :with_invited_joiners
          promises :existing_users, :new_users_details

          executed do |ctx|
            @ctx = ctx
            ctx.existing_users = []
            ctx.new_users_details = []
            next ctx unless ctx.with_invited_joiners
            find_by_email
          end

          def self.find_by_email
            @ctx.invited_joiners_details.each do |joiner|
              joiner_details = joiner.symbolize_keys
              user = ::User.find_for_authentication(email: joiner_details[:email])
              if user
                @ctx.existing_users << user
              else
                @ctx.new_users_details << joiner_details
              end
            end
          end
        end
      end
    end
  end
end
