module Actions
  module Event
    module Booking
      module InvitedJoiners
        class NotifyInvitee
          extend LightService::Action

          expects :memberships, :joiner, :event
          promises :invitee_notifier_response

          executed do |ctx|
            ctx.invitee_notifier_response = ""
            next ctx if ctx.memberships.empty?

            ctx.memberships.each do|membership|
              payment = membership.payment

              next unless payment.approved?
              
              ::Notifications::TripInvitationWorker.perform_async(
                ctx.joiner.id, 
                ctx.event.id, 
                membership.id
              )
            end
            ctx.invitee_notifier_response = "Invitee Notifier Successfully Sent!"
          end
        end
      end
    end
  end
end
