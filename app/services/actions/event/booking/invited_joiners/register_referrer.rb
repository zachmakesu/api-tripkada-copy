module Actions
  module Event
    module Booking
      module InvitedJoiners
        class RegisterReferrer
          extend LightService::Action

          expects :new_users, :joiner
          promises :referrals

          executed do |ctx|
            ctx.referrals = []
            next ctx if ctx.new_users.empty?
            ctx.new_users.each do |user|
              referral = ctx.joiner.referrals.new(joiner_id: user.id)
              ctx.fail_with_rollback!("Failed To Create Referral, #{referral.errors.full_messages.to_sentence}") unless referral.save
              ctx.referrals << referral
            end
          end

          rolled_back do |ctx|
            ctx.referrals.map(&:destroy) if ctx.referrals.present?
          end
        end
      end
    end
  end
end
