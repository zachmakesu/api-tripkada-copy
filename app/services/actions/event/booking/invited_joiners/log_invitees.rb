module Actions
  module Event
    module Booking
      module InvitedJoiners
        class LogInvitees
          extend LightService::Action

          expects :invited_joiners, :joiner
          promises :invitations

          executed do |ctx|
            ctx.invitations = []
            next ctx if ctx.invited_joiners.empty?
            @joiner_membership = ctx.joiner&.event_memberships&.last            
            ctx.invited_joiners.flatten.each do |invited_joiner|
              @invited_joiner = invited_joiner
              invitation = ctx.joiner.sent_invitations.new(invitation_params)
              ctx.fail_and_return!("Failed to create Invitation for #{invited_joiner.decorate.full_name}, #{invitation.errors.full_messages.to_sentence}") unless invitation.save
              ctx.invitations << invitation
            end
          end

          rolled_back do |ctx|
            ctx.invitations.map(&:destroy) if ctx.invitations.present?
          end

          def self.invitation_params
            {
              invitee_id: @invited_joiner&.id,
              event_membership_id: @joiner_membership&.id
            }
          end
        end
      end
    end
  end
end
