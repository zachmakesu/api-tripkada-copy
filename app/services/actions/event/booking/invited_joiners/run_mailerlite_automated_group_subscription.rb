module Actions
  module Event
    module Booking
      module InvitedJoiners
        class RunMailerliteAutomatedGroupSubscription
          extend LightService::Action

          expects :memberships

          executed do |ctx|
            memberships = ctx.memberships
            next ctx if memberships.empty? || !Rails.env.production?

            ctx.memberships.each do |membership|
              member = membership.member
              payment = membership.payment
              
              next unless payment&.approved?

              ::Mailerlite::AddRemoveSubscriberWorker.perform_async(
                member.uid, 
                member.approved_event_memberships.count
            )
            end
          end
        end
      end
    end
  end
end
