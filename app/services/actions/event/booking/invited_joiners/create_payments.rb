module Actions
  module Event
    module Booking
      module InvitedJoiners
        class CreatePayments
          extend LightService::Action

          expects :memberships, :status, :with_invited_joiners
          promises :payments

          executed do |ctx|
            @ctx = ctx
            ctx.payments = []
            next ctx unless ctx.with_invited_joiners
            ctx.memberships.each do |membership|
              payment = membership.build_payment(payment_params)
              ctx.fail_and_return!("Failed To Create Payment for Invited Joiner #{user.name}, #{payment.errors.full_messages.to_sentence}") unless payment.save
              ctx.payments << payment
            end
          end

          rolled_back do |ctx|
            ctx.payments.destroy_all if ctx.payments.present?
          end

          def self.payment_params
            {
              mode: ::Payment.modes[:through_invite],
              made: ::Payment.mades[:downpayment],
              booking_fee: ::BookingFee.last,
              status: @ctx.status
            }
          end

        end
      end
    end
  end
end
