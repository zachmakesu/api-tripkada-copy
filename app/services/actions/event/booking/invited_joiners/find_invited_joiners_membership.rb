module Actions
  module Event
    module Booking
      module InvitedJoiners
        class FindInvitedJoinersMembership
          extend LightService::Action

          expects :membership, :event, :transaction_exists
          promises :invited_joiners_memberships

          executed do |ctx|
            ctx.invited_joiners_memberships = []
            next ctx unless ctx.transaction_exists
            @event = ctx.event

            membership = ctx.membership
            invited_joiners = membership.invited_members
            next ctx if invited_joiners.empty?
            
            invited_joiners.each do |joiner|
              event_membership = joiner.event_memberships.find_by(event_id: @event.id)
              next unless event_membership
              ctx.invited_joiners_memberships << event_membership
            end
          end
        end
      end
    end
  end
end

