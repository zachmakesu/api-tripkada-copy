module Actions
  module Event
    module Booking
      class ValidateInvitedJoiners
        extend LightService::Action

        expects :event, :params, :transaction_exists
        promises :with_invited_joiners, :invited_joiners_count, :invited_joiner_names, :invited_joiner_emails

        executed do |ctx|
          @ctx = ctx
          @ctx_params = ctx.params
          ctx.with_invited_joiners = false
          ctx.invited_joiner_names = []
          ctx.invited_joiner_emails = []
          ctx.invited_joiners_count = 0
          next ctx if ctx.transaction_exists
          invited_joiner_names = @ctx.params.fetch(:invited_joiner_names, "undefined")
          invited_joiner_emails = @ctx.params.fetch(:invited_joiner_emails, "undefined")
          with_invited_joiners = invited_joiner_names == "undefined" || invited_joiner_emails == "undefined"
          ctx.with_invited_joiners = !with_invited_joiners
          ctx.invited_joiners_count = invited_joiners_count
          ctx.invited_joiner_names = invited_joiner_names
          ctx.invited_joiner_emails = invited_joiner_emails
        end

        def self.invited_joiners_count
          if @ctx.with_invited_joiners
            name_count = @ctx_params[:invited_joiner_names]&.count
            email_count = @ctx_params[:invited_joiner_emails]&.count
            @ctx.fail_and_return!("Invalid Invited Joiners Parameter") unless name_count == email_count
          end
          name_count.to_i
        end
      end
    end
  end
end
