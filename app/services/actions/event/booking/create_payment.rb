module Actions
  module Event
    module Booking
      class CreatePayment
        extend LightService::Action

        expects :params, :membership, :status, :device_platform, :transaction_exists
        promises :payment, :promo_code

        executed do |ctx|
          ctx.payment = nil
          ctx.promo_code = nil

          next ctx if ctx.transaction_exists

          @ctx = ctx
          @ctx_params = ctx.params
          @payment = ctx.membership.build_payment(payment_params)
          ctx.fail_and_return!(@payment.errors.full_messages.to_sentence) unless @payment.save
          ctx.payment = @payment
          ctx.promo_code = promo_code
        end

        rolled_back do |ctx|
          ctx.payment.destroy if ctx.payment
        end

        def self.payment_params
          {
            promo_code_id: promo_code&.id,
            mode: @ctx_params.fetch(:mode, nil),
            made: @ctx_params.fetch(:made, "downpayment"),
            booking_fee: ::BookingFee.last,
            status: @ctx.status,
            amount_paid: @ctx_params.fetch(:total_amount, 0.0)&.to_f,
            device_platform: @ctx.device_platform
          }
        end

        def self.promo_code
          code = @ctx_params.fetch(:code, nil)
          ::PromoCode.find_by_code(code) if code
        end
      end
    end
  end
end
