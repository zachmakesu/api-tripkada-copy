module Actions
  module Event
    module Booking
      class ApprovePayment
        extend LightService::Action

        expects :payment, :transaction_exists, :status
        promises :approved_payment, :membership

        executed do |ctx|
          ctx.membership = nil
          ctx.approved_payment = nil

          next unless ctx.transaction_exists && ctx.status == "approved"
          
          payment = ctx.payment
          @previous_status = payment.status
          # update status skipping normal validation
          # to avoid unused promo code error
          payment.update_attribute("status", ctx.status)
          ctx.approved_payment = payment
          ctx.membership = payment.event_membership
        end

        rolled_back do |ctx|
          ctx.approved_payment.update(status: @previous_status) if ctx.approved_payment
        end
      end
    end
  end
end
