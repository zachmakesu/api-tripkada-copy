module Actions
  module Event
    module Booking
      module Waitlist
        class FindUserFromWaitlists
          extend LightService::Action

          expects :event, :joiner
          promises :waitlist, :is_waitlist_member

          executed do |ctx|
            waitlist = ctx.event.waitlists.find_by(user_id: ctx.joiner.id)
            ctx.waitlist = waitlist
            ctx.is_waitlist_member = waitlist.present?
          end
        end
      end
    end
  end
end
