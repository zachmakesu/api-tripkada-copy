module Actions
  module Event
    module Booking
      module Waitlist
        class ApproveWaitlistMembership
          extend LightService::Action

          expects :waitlist, :is_waitlist_member
          promises :waitlist_response

          executed do |ctx|
            ctx.waitlist_response = ""
            next ctx unless ctx.is_waitlist_member
            waitlist = ctx.waitlist
            @waitlist_status = waitlist.status
            ctx.fail_with_rollback!("Approving Waitlist Membership failed, waitlist status is #{@waitlist_status}") unless waitlist.approved?
            waitlist.paid!
            ctx.waitlist_response = "Waitlist Membership Status Successfully Updated To Paid!"
          end

          rolled_back do |ctx|
            if ctx.waitlist
              waitlist = ctx.waitlist
              @waitlist_status == "pending" ? waitlist.pending! : waitlist.denied!
            end
          end
        end
      end
    end
  end
end
