module Actions
  module Event
    module Organizer
      class UpdateJoinerPaymentStatus
        extend LightService::Action

        expects :event, :membership, :status
        promises :updated_payment

        executed do |ctx|
          event = ctx.event
          membership = ctx.membership
          is_valid_membership = event.event_memberships.include?(membership)

          verify_status(ctx, ctx.status)

          payment = membership.payment

          is_updated = update_payment_status!(payment, ctx.status) if is_valid_membership
          ctx.fail_and_return!(payment.errors.full_messages.to_sentence) unless is_updated

          ctx.updated_payment = payment
        end

        def self.update_payment_status!(payment, status)
          payment.update!(status: status)
        end

        def self.verify_status(ctx, status)
          is_valid = ::Payment.statuses.keys.include?(status)
          ctx.fail_and_return!("Please provide valid status") unless is_valid
        end
      end
    end
  end
end
