module Actions
  module Event

    class BuildDestinationsParams
      extend LightService::Action

      expects :destination_ids, :event
      promises :sorting_destinations_params, :unused_sorting_destinations_ids

      executed do |context|
        @context = context

        context.sorting_destinations_params = destinations_params
        context.unused_sorting_destinations_ids = context.event.sorting_destinations.where.not(destination_id: context.destination_ids).try(:ids)
      end

      def self.destinations_params
        new_destinations = []
        event_destinations = @context.event.sorting_destinations
        @context.destination_ids.each do |destination_id|
          new_destinations << destination_id unless event_destinations.where(destination_id: destination_id).present?
        end
        Destination.where(
          id: new_destinations
        ).map{|destination| { destination_id: destination.id } }
      end
    end

  end
end
