module Actions
  module Event
    class ValidateDestinations
      extend LightService::Action

      expects :params, :set_as_draft
      promises :destination_ids

      executed do |context|
        @context = context
        @destination_ids = context.params.fetch(:destination_ids, [])
        unless context.set_as_draft
          context.fail_and_return!("Please select a valid destination") unless @destination_ids.present? && is_array? && invalid_destinations.empty?
        end
        context.destination_ids = @destination_ids.map(&:to_i)
      end

      def self.is_array?
        @destination_ids.is_a?(Array)
      end

      def self.invalid_destinations
        valid_destination_ids = Destination.ids
        invalid_ids = @destination_ids.map(&:to_i) - valid_destination_ids if is_array?
      end
    end
  end
end
