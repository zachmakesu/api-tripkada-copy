module Actions
  module Event
    class CreatePhotos
      extend LightService::Action

      expects :photos_params, :event

      executed do |context|
        next context if context.photos_params.empty?
        
        @event = context.event

        @existing_photo_ids = @event.photos.ids
        @existing_photos = context.event.photos.where(id: @existing_photo_ids)
        
        @event.photos.create(context.photos_params)

        @existing_photo_attributes = context.event.photos.new(existing_photo_attributes)

        context.event.photos.where(id: @existing_photos).destroy_all
        context.fail_with_rollback!(photo_errors) if @event.photos.any?(&:invalid?)
      end

      rolled_back do |context|
        context.event.photos.where.not(id: @existing_photo_ids).destroy_all
        @existing_photo_attributes.map(&:save) if @existing_photo_attributes && @event.photos_not_exists?(photo_ids: @existing_photo_ids)
      end

      def self.photo_errors
        @event.photos.map{ |p| p.errors.full_messages }.uniq.flatten.to_sentence
      end

      def self.existing_photo_attributes
        @existing_photos.map do |photo|
          {
            primary: photo.primary,
            label: photo.label,
            image: photo.image
          }
        end
      end
    end
  end
end
