module Actions
  module Event

    class BuildOrganizerMembershipParams
      extend LightService::Action

      expects :organizer
      promises :join_params, :organizer_payment_params

      executed do |context|
        @context = context

        context.join_params = join_params
        context.organizer_payment_params = organizer_payment_params
      end

      def self.join_params
        {
          member: @context.organizer
        }
      end

      def self.organizer_payment_params
        {
          mode:     "organizer",
          made:     "fullpayment",
          status:   Payment.statuses[:approved]
        }
      end
    end

  end
end
