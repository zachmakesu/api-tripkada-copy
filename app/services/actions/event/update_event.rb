module Actions
  module Event
    class UpdateEvent
      extend LightService::Action

      expects :event_params, :organizer, :event
      promises :updated_event

      executed do |context|
        event = context.event
        @existing_event_attr = event.attributes
        @existing_event_attr.delete("id")

        event.assign_attributes(context.event_params)
        context.fail!(event.errors.full_messages.to_sentence) unless event.save
        context.updated_event = event
      end

      rolled_back do |context|
        event = context.event
        event.assign_attributes(@existing_event_attr)
        # add validate false to rollback existing attributes
        # since error is added to event which prevent saving the record
        event.save(validate: false)
      end

    end
  end
end
