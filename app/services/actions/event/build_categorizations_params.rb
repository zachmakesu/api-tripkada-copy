module Actions
  module Event

    class BuildCategorizationsParams
      extend LightService::Action

      expects :category_ids, :event
      promises :categorizations_params, :unused_categorizations_ids

      executed do |context|
        @context = context

        context.categorizations_params = categorizations_params
        context.unused_categorizations_ids = context.event.categorizations.where.not(category_id: context.category_ids).try(:ids)
      end

      def self.categorizations_params
        new_categories = []
        event_categories = @context.event.categorizations
        @context.category_ids.each do |category_id|
          new_categories << category_id unless event_categories.where(category_id: category_id).present?
        end
        Category.where(
          id: new_categories
        ).map{|category| { category_id: category.id } }
      end
    end

  end
end
