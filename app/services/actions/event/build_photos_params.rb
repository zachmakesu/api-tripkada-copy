module Actions
  module Event

    class BuildPhotosParams
      extend LightService::Action
      extend ApiHelper

      expects :params
      promises :photos_params

      executed do |context|
        @context = context
        @params = context.params
        photos = []
        photos.push(dup_photo_params)
        photos.push(photos_params)
        context.photos_params = photos.compact.flatten
      end

      def self.photos_params
        @params.fetch(:photos).each_with_index.map do |x,i|
          {
            image: @params.fetch(:from_controller, nil) ? @params.fetch(:photos)[i] : build_attachment(@params.fetch(:photos)[i]),
            primary: @params.fetch(:primary, false).try(:[], i)
          }
        end if @params.fetch(:photos, nil)
      end

      def self.dup_photo_params
        if original_event = ::Event.find_by(id: @params.fetch(:event_id, nil))
          original_event.photos.map do |photo|
            { image: photo.image, label: photo.label, primary: photo.primary }
          end.compact
        end
      end
    end

  end
end
