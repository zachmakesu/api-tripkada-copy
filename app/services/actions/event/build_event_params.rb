module Actions
  module Event

    class BuildEventParams
      extend LightService::Action

      expects :params, :set_as_draft
      promises :event_params

      executed do |context|
        @context = context
        @params = context.params

        context.event_params = event_params
      end

      def self.event_params
        {
          name:                   @params.fetch(:name, nil),
          start_at:               @params.fetch(:start_at, nil),
          end_at:                 @params.fetch(:end_at, nil),
          description:            @params.fetch(:description, nil),
          rate:                   @params.fetch(:rate, nil),
          min_pax:                @params.fetch(:min_pax, nil),
          max_pax:                @params.fetch(:max_pax, nil),
          things_to_bring:        @params.fetch(:things_to_bring, nil),
          meeting_place:          @params.fetch(:meeting_place, nil),
          tags:                   @params.fetch(:tags, nil),
          highlights:             @params.fetch(:highlights, nil),
          inclusions:             @params.fetch(:inclusions, nil),
          lat:                    @params.fetch(:lat, nil),
          lng:                    @params.fetch(:lng, nil),
          downpayment_rate:       @params.fetch(:downpayment_rate, nil),
          published_at:           @context.set_as_draft ? nil : DateTime.now
        }
      end

    end

  end
end
