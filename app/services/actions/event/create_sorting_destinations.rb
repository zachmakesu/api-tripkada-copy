module Actions
  module Event
    class CreateSortingDestinations
      extend LightService::Action

      expects :event, :sorting_destinations_params, :unused_sorting_destinations_ids
      promises :sorting_destinations

      executed do |context|
        @existing_destinations = context.event.sorting_destinations.ids

        @destinations = context.event.sorting_destinations.create(context.sorting_destinations_params)

        unused_destination_ids = context.unused_sorting_destinations_ids

        @unused_destinations = context.event.sorting_destinations.where(
          id: unused_destination_ids
        ).map(&:destination_id)

        context.fail_with_rollback!(destinations_errors) if @destinations.map(&:invalid?).any?

        context.event.sorting_destinations.where(id: unused_destination_ids).destroy_all if unused_destination_ids.any?

        context.sorting_destinations = @destinations
      end

      rolled_back do |context|
        context.event.sorting_destinations.where.not(id: @existing_destinations).destroy_all
        context.event.sorting_destinations.create(unused_sorting_destinations_params) if unused_sorting_destinations_params.any?
      end

      def self.destinations_errors
        @destinations.map{ |c| c.errors.full_messages }.uniq.flatten.to_sentence
      end

      def self.unused_sorting_destinations_params
        Destination.where(
          id: @unused_destinations
        ).map{|destination| { destination_id: destination.id } }
      end
    end
  end
end
