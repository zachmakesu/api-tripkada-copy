module Actions
  module Event
    class CreateItineraries
      extend LightService::Action

      expects :event, :itineraries_params

      executed do |context|
        next context if context.itineraries_params.nil?

        @existing_itineraries_ids = context.event.itineraries.ids
        @itineraries = context.event.itineraries.create(context.itineraries_params)
        @existing_itineraries_descriptions = context.event.itineraries.where(id: @existing_itineraries_ids).map(&:description)
        context.fail_with_rollback!(itineraries_errors) if @itineraries.map(&:invalid?).any?
        context.event.itineraries.where(id: @existing_itineraries_ids).destroy_all if @existing_itineraries_ids.any? && !@itineraries.map(&:invalid?).any?
      end

      rolled_back do |context|
        context.event.itineraries.where.not(id: @existing_itineraries_ids).destroy_all
        context.event.itineraries.create(existing_itineraries_params) if existing_itineraries_params.any?
      end

      def self.itineraries_errors
        @itineraries.map{ |i| i.errors.full_messages }.uniq.flatten.to_sentence
      end

      def self.existing_itineraries_params
        @existing_itineraries_descriptions.each_with_index.map do |x,i|
          { description: @existing_itineraries_descriptions[i] }
        end
      end
    end
  end
end
