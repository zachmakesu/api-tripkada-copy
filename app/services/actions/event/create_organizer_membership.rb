module Actions
  module Event
    class CreateOrganizerMembership
      extend LightService::Action

      expects :event, :join_params, :organizer_payment_params
      promises :membership

      executed do |context|
        @membership = context.event.event_memberships.build(context.join_params)
        @payment = @membership.build_payment(context.organizer_payment_params)
        
        if @membership.save && @payment.save
          context.membership = @membership
        else
          context.fail_with_rollback!(set_error_messages.flatten.to_sentence)
        end
      end

      rolled_back do |context|
        @membership.destroy
      end

      def self.set_error_messages
        [].tap do |errors|
          errors << @membership.errors.full_messages
          errors << @payment.errors.full_messages
        end
      end
    end
  end
end
