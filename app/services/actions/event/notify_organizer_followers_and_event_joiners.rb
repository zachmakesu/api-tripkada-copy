module Actions
  module Event
    class NotifyOrganizerFollowersAndEventJoiners
      extend LightService::Action

      expects :event, :organizer, :params, :set_as_draft

      executed do |context|
        next context if context.set_as_draft
        event = context.event.reload
        Notifications::TripsWorker.perform_async(event.id)
        context.organizer.followers.ids.each do |id|
          Notifications::FollowerWorker.perform_async(id, event.id)
        end
      end
    end
  end
end
