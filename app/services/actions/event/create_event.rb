module Actions
  module Event
    class CreateEvent
      extend LightService::Action

      expects :event_params, :organizer
      promises :event

      executed do |context|
        @context = context
        @event = context.organizer.organized_events.new(context.event_params)
        @context.fail!(@event.errors.full_messages.to_sentence) unless @event.save
        context.event = @event
      end

      rolled_back do |context|
        @event.destroy
      end

    end
  end
end
