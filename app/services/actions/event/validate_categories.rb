module Actions
  module Event
    class ValidateCategories
      extend LightService::Action

      expects :params, :set_as_draft
      promises :category_ids

      executed do |context|
        @context = context
        @category_ids = context.params.fetch(:category_ids,[])
        unless context.set_as_draft
          context.fail_and_return!("Please select a valid category") unless @category_ids.present? && is_array? && invalid_categories.empty?
        end
        context.category_ids = @category_ids.map(&:to_i)
      end

      def self.is_array?
        @category_ids.is_a?(Array)
      end

      def self.invalid_categories
        valid_category_ids = Category.ids
        invalid_ids = @category_ids.map(&:to_i) - valid_category_ids if is_array?
      end
    end
  end
end
