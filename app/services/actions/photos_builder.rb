module Actions
  class PhotosBuilder
    extend LightService::Action
    extend ApiHelper

    expects :params
    promises :photos

    executed do |context|
      @photos = context.params.fetch(:photos, [])
      @photo_labels = context.params.fetch(:photo_labels, [])
      context.fail_and_return!("Invalid Parameters") if @photos.empty?
      context.photos = photo_params
    end

    def self.photo_params
      @photos.each_with_index.map do |photo, i|
        {
          image: build_attachment(photo),
          label: @photo_labels[i]
        }
      end
    end
  end
end
