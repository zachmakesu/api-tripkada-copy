module Actions
  module Chatbot

    class DeliverReplies
      extend LightService::Action

      expects :message_obj, :replies
      executed do |context|
        @context     = context
        @message_obj = context.message_obj
        @replies     = context.replies
        deliver!
      end

      def self.deliver!
        @replies.each do |reply|
          @message_obj.reply(reply)
        end
      end
    end

  end
end
