module Actions
  module Chatbot

    class FindOrCreateMessengerUser
      extend LightService::Action

      expects :fbuser_obj
      promises :messenger_user

      executed do |context|
        @context    = context
        @fbuser_obj = context.fbuser_obj
        context.messenger_user = messenger_user
      end

      rolled_back do |context|
        context.messenger_user.destroy
      end

      def self.messenger_user
        MessengerUser.find_or_create_by(user_fb_id: @fbuser_obj.fetch("id")) do |user|
          user.first_name = @fbuser_obj.fetch("first_name")
          user.last_name = @fbuser_obj.fetch("last_name")
        end
      end
    end

  end
end
