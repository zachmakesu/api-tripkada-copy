module Actions
  module Chatbot

    class OptinMessage
      extend LightService::Action

      expects :message_obj, :messenger_user
      promises :replies

      executed do |context|
        @context        = context
        @message_obj    = context.message_obj
        @messenger_user = context.messenger_user
        context.replies = replies
      end

      def self.replies
        #splitting the ref in optin
        ref = @message_obj.ref.split("=")
        optin_params = Hash.new

        if ref[1]
          ref[1].split("/").each do |field|
            parameter = field.split(":")
            key = parameter[0]
            val = parameter[1]
            optin_params[key] = val
          end
        end

        ::Chatbot::OptinHandler.response_from(message_obj: @message_obj, messenger_user: @messenger_user, ref: ref[0], optin_params: optin_params )
      end

    end

  end
end
