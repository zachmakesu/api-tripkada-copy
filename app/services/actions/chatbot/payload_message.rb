module Actions
  module Chatbot

    class PayloadMessage
      extend LightService::Action

      expects :message_obj, :messenger_user
      promises :replies

      executed do |context|
        @context        = context
        @message_obj    = context.message_obj
        @messenger_user = context.messenger_user
        context.replies = replies
      end

      def self.replies
        payload = @message_obj.payload
        ::Chatbot::PayloadHandler.response_from(message_obj: @message_obj, payload: payload, messenger_user: @messenger_user)
      end

    end

  end
end
