module Actions
  module Chatbot

    class FetchFbUser
      extend LightService::Action

      expects :message_obj
      promises :fbuser_obj

      executed do |context|
        @context     = context
        @message_obj = context.message_obj
        context.fbuser_obj = fbuser_obj
      end

      def self.fbuser_obj
        messenger_user = MessengerUser.find_by(user_fb_id: @message_obj.sender.fetch("id"))
        if messenger_user
          {
            "first_name"=>messenger_user.first_name,
            "last_name"=>messenger_user.last_name,
            "profile_pic"=>"",
            "locale"=>"en_US",
            "timezone"=>8,
            "gender"=>"",
            "id"=>messenger_user.user_fb_id
          }
        else
          # to prevent page rate limiting of facebook api. Due to calling of api using page access_token
          access_token = @message_obj.recipient.fetch("id") == ENV.fetch("FB_2ND_PAGE_ID") ? ENV.fetch("FB_2ND_PAGE_ACCESS_TOKEN") : ENV.fetch("FB_PAGE_ACCESS_TOKEN")
          graph = Koala::Facebook::API.new(access_token)
          graph.get_object( @message_obj.sender.fetch("id"))
        end

      end
    end

  end
end
