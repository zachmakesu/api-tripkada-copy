module Actions
  module Chatbot

    class AttachmentsMessage
      extend LightService::Action

      expects :message_obj, :messenger_user, :replies
      promises :replies

      executed do |context|
        @context        = context
        @message_obj    = context.message_obj
        @messenger_user = context.messenger_user
        context.replies = replies
      end

      def self.replies
        #   message.messaging["message"]["attachments"]["types"]
        #   example types are fallback, type, image
        #   As of now whatever they sent from typing will fall to "Random Response"
        #   Chatbot::Payloads::Random.replies(@message_obj)

        return @context.replies unless @message_obj.messaging["message"]["attachments"]
        if @messenger_user.message_concern
          ::Chatbot::MessageConcernHandler.response_from(message_obj: @message_obj, messenger_user: @messenger_user)
        else
          ::Chatbot::Payloads::Random.replies(message_obj: @message_obj, messenger_user: @messenger_user)
        end

      end

    end

  end
end
