module Actions
  class JoinWaitlist
    extend LightService::Action
    expects :event, :user

    executed do |context|
      @event = context.event
      @user = context.user
      @waitlist = ::Waitlist.create(waitlist_params)
      context.fail_and_return!(@waitlist.errors.full_messages.to_sentence) unless @waitlist.persisted?
    end

    def self.waitlist_params
      {
        user_id: @user.id,
        event_id: @event.id
      }
    end

  end
end
