module Actions
  class RequestTripDifferentDateMailer
    extend LightService::Action
    expects :params, :requester, :event, :start_datetime, :end_datetime
    promises :mailer_response

    executed do |context|
      Notifications::RequestTripDifferentDateMailerWorker.perform_async(context.event.id, context.requester.uid, context.params, context.start_datetime, context.end_datetime)
      context.mailer_response = "Mail Request Successfully Sent"
    end
  end
end
