TagcashHandlerError = Class.new(StandardError)

class TagcashHandler
  include Rails.application.routes.url_helpers
  ACCESS_TOKEN_URI = URI.parse("https://api.tagcash.com/oauth/accesstoken")

  attr_accessor :response
  def initialize code, state
    @code       = code
    @params     = state
    @response   = Hash.new
    @errors     = []
    @user       = nil
    @event      = nil
    @promo_code = nil
  end

  def get_access_token
    validate_and_initialize_params

    response = Net::HTTP.post_form(ACCESS_TOKEN_URI, access_token_options)
    json = JSON.parse(response.body)

    validate_response(json)

    if @errors.empty?
      provider = @user.providers.find_or_create_by(tagcash_options)
      provider.update(token: json["result"]["access_token"])
    end

    create_response
    self
  end

  private
  def validate_and_initialize_params
    begin
      arrValue    = Base64.urlsafe_decode64(@params).split(";")
      uid         = arrValue[0]
      @event      = arrValue[1]
      @promo_code = arrValue[3]
      unless @user = User.find_by(uid: uid)
        @errors << "Invalid user or malformed state"
      end
    rescue
      @errors << "Invalid or malformed state"
    end
  end

  def validate_response json
    if json["status"] == "failed"
      @errors << json["error_description"]
    end
  end

  def create_response
    if @errors.empty? 
      response[:type]       = true
      response[:details]    = "Successfully connected tagcash to your account."
      response[:event]      = @event
      response[:promo_code] = @promo_code
    else
      response[:type]     = false
      response[:details]  = @errors.join(", ") 
    end
  end

  def access_token_options
    {
      client_id:      ENV["TAGCASH_ID"],
      client_secret:  ENV["TAGCASH_SECRET"],
      grant_type:     "authorization_code",
      redirect_uri:   tagcash_callback_url(ActionController::Base.default_url_options),
      code:           @code
    }
  end

  def tagcash_options
    {
      role:     "joiner",
      provider: "tagcash"
    }
  end
end
