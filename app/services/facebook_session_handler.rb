require 'logger'

FacebookSessionHandlerError = Class.new(StandardError)

class FacebookSessionHandler

  PROVIDER = "facebook"

  FIELDS = "id,first_name,last_name,gender,email,birthday,link"

  attr_accessor :role, :auth, :token, :response

  def initialize(role, access_token, opts={})
    @role     = role
    @token    = access_token
    @uid      = opts.fetch(:uid, "")
    @referrer_uid = opts.fetch(:referrer_uid, nil)
    @platform = opts.fetch(:platform)

    begin
      @auth = Koala::Facebook::API.new(token).get_object("me?fields=#{FIELDS}")
    rescue Koala::Facebook::APIError => e
      BugMailer.invalid_koala_api_call(PROVIDER,@token,e).deliver_now
    end

    @response = Hash.new
  end

  def find
    @user = find_by_uid_and_provider || find_by_email
    return unless @user
    update_email if @user.email.include?("@facebook.com") || @user.email.blank?
    @user
  end

  def create
    # Initialize user
    @user = User.new(user_options)
    # Services like facebook does not return email via oauth sometimes
    # In this case, we generate temporary email and force user to fill it later
    @user.email = "#{uid}@facebook.com" if @user.email.blank?
    create_referral if @user.save && role == "from_referral"

    log.info "(OAuth) Creating user #{email} from login with uid => #{uid}"
    Notifications::EmailWorker.perform_async(@user.email, "signup_welcome_email") if @user.persisted?
    Mailerlite::AddRemoveSubscriberWorker.perform_async(@user.uid, @user.approved_event_memberships.count) if Rails.env.production?
    @user
  end

  def find_or_create
    if auth
      user = validate_action_using_role
      user.decorate.delete_previous_api_keys
      key = user.api_keys.new
      @api_token = key.access_token = SecureRandom.hex(16)
      key.save
      @user = user
      #refresh token on login
      update_providers
      create_response
      self
    else
      response[:type] = false
      response[:failed_facebook_auth] = true
      response[:details] = "failure authentication"
      return self
    end
  end

  private

  def create_response
    @user_details = { token: @api_token,
                      uid: @user.uid,
                      has_completed_required_information: @user.decorate.has_completed_required_info?,
                      with_government_id: @user.government_photos.present?,
                      role: @user.role  }

    if @provider.errors.blank?
      response[:type] = true
      response[:details] = @user_details
    else
      response[:type] = false
      response[:details] = { message: @provider.errors.full_messages.join(', ') }
      response[:details].merge!(@user_details) unless @user.decorate.has_completed_required_info? && @user.joiner_and_organizer?
    end
  end

  def find_by_email
    User.find_by(email: [email,"#{uid}@facebook.com"])
  end

  def find_by_uid_and_provider
    Provider.find_by(provider: provider, uid: uid).try(:user)
  end

  def user_options
    {
      email:        email,
      gender:       gender,
      birthdate:    birthday,
      password:     password,
      image_url:    image_url,
      last_name:    last_name,
      first_name:   first_name,
      registration_platform: @platform
    }
  end

  def provider_params
    {
      uid:          uid,
      role:         login_role,
      token:        token,
      url: facebook_url,
    }
  end

  def uid
    auth['id'].to_s
  end

  def provider
    PROVIDER
  end

  def first_name
    auth['first_name']
  end

  def last_name
    auth['last_name']
  end

  def email
    auth['email'].try(:downcase)
  end

  def gender
    User.genders[auth['gender'].to_s.downcase].to_i
  end

  def birthday
    Date.strptime(auth['birthday'], '%m/%d/%Y') rescue nil
  end

  def facebook_url
    auth['link']
  end

  def password
    Devise.friendly_token[0, 10].downcase
  end

  def image_url
    "https://graph.facebook.com/#{uid}/picture?type=large"
  end

  def koala
    Koala::Facebook::API
  end

  def log
    Logger.new(STDOUT)
  end

  def login_role
    #assign default joiner role for newly created accounts
    @user.providers.empty? ? "joiner" : role
  end

  def update_email
    login_email = email || "#{uid}@facebook.com"
    @user.update(email: login_email)
  end

  def update_providers
    @provider = @user.providers.find_or_initialize_by(uid: uid, provider: provider)
    @provider.update(provider_params) && @provider.reload
  end

  def validate_action_using_role
    if role == "invitee"
      find_by_uid_and_provider || find_by_uid_and_update
    else
      find || create
    end
  end

  def find_by_uid_and_update
    user = begin
             User.find_by!(uid: @uid)
           rescue
             find || create
           end
    user.tap{ |u| u.update(user_options) }
  end

  def create_referral
    referrer = User.find_by(uid: @referrer_uid)
    referrer.referrals.create(joiner_id: @user.id) if referrer
  end

end
