class User::UploadPhotos
  extend LightService::Organizer

  def self.call(user:, params:)
    with(user: user, params: params, category: params.fetch(:category, "")).reduce(actions)
  end

  def self.actions
    [
      Actions::PhotosBuilder,
      Actions::User::PhotosUploader,
      Actions::User::PhotoMailer
    ]
  end
end
