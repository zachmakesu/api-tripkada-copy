class User::SendCreditsToReferrer
  extend LightService::Organizer

  def self.call(joiner:, event:)
    with(joiner: joiner, event: event).reduce(
      Actions::User::FindReferrer,
      Actions::User::EarnReferralIncentive,
      Actions::User::ReferralIncentiveMailer,
      Actions::User::ReferralIncentiveNotifier
    )
  end
end
