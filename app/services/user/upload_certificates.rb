class User::UploadCertificates
  extend LightService::Organizer

  def self.call(user:, params:)
    with(user: user, params: params, category: "certificates").reduce(actions)
  end

  def self.actions
    [
      Actions::PhotosBuilder,
      Actions::User::PhotosUploader
    ]
  end
end
