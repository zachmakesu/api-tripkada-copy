class User::AddEventToReviewBlacklists
  extend LightService::Organizer

  def self.call(user:, event:)
    with(user: user, event: event).reduce(actions)
  end

  def self.actions
    [
      Actions::User::AddEventToReviewBlacklists
    ]
  end
end
