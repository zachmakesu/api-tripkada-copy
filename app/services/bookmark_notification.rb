class BookmarkNotification
  def initialize(bookmark)
    @bookmark_id = bookmark
  end

  def deliver
    Notifications::BookmarkWorker.perform_async(@bookmark_id)
  end
end
