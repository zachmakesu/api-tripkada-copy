class PaypalHandler
  include Rails.application.routes.url_helpers

  TRANSACTION = 'sale'
  PAYMENT_METHOD = 'paypal'
  CURRENCY = 'PHP'
  QUANTITY = '1'
  PAYPAL_TRANSACTION_RATE = 0.044 # default transaction fee
  PAYPAL_FIXED_RATE = 15 # default fixed fee

  attr_accessor :params, :response, :errors

  def initialize(params, event)
    #Paypal Payment Params
    @event = event
    @params = params
    @user = User.find_by(id: params[:user_id])
    @response = Hash.new
    @invited_joiner_name_list = params.fetch(:invited_joiner_names){ nil }
    @invited_joiner_email_list = params.fetch(:invited_joiner_emails){ nil }
    @platform = params.fetch(:platform, "desktop")
    @errors = []
  end

  # Initialize Payment and generate Redirect Links to Paypal
  def create_instant_payment
    if @event.present?
      if @params[:code].present?
        check_promo_code = validate_promo_code(
                            code: @params[:code],
                            user: @user
                           )
        @promo_code = check_promo_code&.promo_code
      end
      @payment = PayPal::SDK::REST::Payment.new(paypal_payment_params)
      @payment.create
    else
      errors << "Event is required"
    end

    create_response
    self
  end

  # Validate Payment Transaction
  def execute_payment
    if valid_payment
      begin
        errors << "Failed to execute Payment #{@payment.error[:message]}" unless @payment.execute(payer_id: params[:PayerID])
      rescue => e
        notify_slack e.message, @platform
        errors << "Rescue: Connection Error Please Try Again"
      end
    end

    execute_trip_join if errors.blank?

    create_response
    self
  end

  # Validate Payment From APP SDKs
  def validate_payment_and_join
    if valid_payment
      begin
        errors << "Payment is already used" if ExpressPayment.find_by(payment_tracking_id: @payment.id)
        errors << "Payment state is not valid" if @payment.state != "approved"
        item = @payment.transactions.first.item_list.items.first
        errors << "Payment item doesn't match the event" if @event.id != item.sku.gsub("TRIP-","").to_i
      rescue => e
        notify_slack e.message, @platform
        errors << "Rescue: Payment is not valid"
      end
    end

    execute_trip_join if errors.blank?

    create_response
    self
  end

  private

  def notify_slack error, client
    notifier = Slack::Notifier.new "https://hooks.slack.com/services/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", channel: '#tripkada-web-and-api', username: "Tripkada Bot #{Rails.env.titleize}"
    notifier.ping text: "Error Payment for trip #{@event.name} via Paypal (#{client})",  attachments: [
      {
        "fallback": "Paypal Payment Error",
        "color": "danger",
        "title": @event.name,
        "title_link": trip_view_url(trip_url_options),
        "fields": [
          {
            "title": "Error",
            "value": error
          }
        ]
      }
    ]
    BugMailer.paypal_rescue_payment(@payment, params, client, @event, error).deliver_now
  end

  def valid_payment
    begin
      @payment = PayPal::SDK::REST::Payment.find(params[:paymentId])
    rescue => e
      notify_slack e.message, @platform
      errors << "Invalid Payment ID"
    end
    errors.blank?
  end

  def create_response
    if errors.blank?
      response[:type] = true
      response[:details] = @payment
      response[:membership] = @membership
    else
      response[:type] = false
      response[:details] = errors.uniq.join(', ')
    end
  end

  def trip_slug
    @event.slug
  end

  def trip_name
    @event.name
  end

  def amount
    rate = @event.decorate.rate_with_fee
    downpayment = @event.decorate.downpayment_rate_with_fee
    subtotal = if @invited_joiner_name_list && @invited_joiner_email_list
                 with_invited_joiners_payment
               else
                 single_payment
               end
    if @promo_code.present? && (rate == downpayment)
      subtotal - @promo_code.value
    else
      subtotal
    end
  end

  def booking_fee
    BookingFeeDecorator.latest_fee
  end

  def with_invited_joiners_payment
    joiner_count = @invited_joiner_name_list.split(',').flatten.count + 1
    @event.decorate.downpayment_rate_with_fee * joiner_count
  end

  def single_payment
    @event.decorate.downpayment_rate_with_fee
  end

  def item_list
    {
      items: [{ name: trip_name, price: total_amount_with_transaction_fee, sku: "TRIP-#{@event.id}", currency: CURRENCY, quantity: QUANTITY }]
    }
  end

  def paypal_payment_verification_options
    {
      slug: trip_slug,
      code: params[:code],
      invited_joiner_names: params[:invited_joiner_names],
      invited_joiner_emails: params[:invited_joiner_emails],

      event_id: @event.id,
      downpayment: total_amount_with_transaction_fee
    }.merge(trip_url_options)
  end

  def amount_list
    {
      total: total_amount_with_transaction_fee,
      currency: CURRENCY
    }
  end

  def trip_url_options
    { slug: trip_slug }.merge(ActionController::Base.default_url_options)
  end

  def redirect_urls
    {
      return_url: paypal_payment_verification_url(paypal_payment_verification_options),
      cancel_url: trip_view_url(trip_url_options)
    }
  end

  def paypal_payment_params
    {
      intent: TRANSACTION,
      payer: { payment_method: PAYMENT_METHOD },
      redirect_urls: redirect_urls,
      transactions: [{ item_list: item_list, amount: amount_list }]
    }
  end

  def execute_trip_join
    ActiveRecord::Base.transaction do
      @membership = EventMembership.new(join_params)

      payment = Payment.new
      payment.event_membership = @membership if @membership.validate
      payment.attributes = payment_params
      payment.build_express_payment(express_payment_params)

      errors << payment.errors.full_messages unless payment.save
      if payment.save
      User::SendCreditsToReferrer.call(joiner: @user, event: @event)
        @waitlist = @event.waitlists.find_by(user_id: @user)
        raise_waitlist_errors_and_rollback! if @waitlist&.denied?
        @waitlist.paid! if @waitlist&.approved?
        Mailerlite::AddRemoveSubscriberWorker.perform_async(@user.uid, @user.approved_event_memberships.count) if Rails.env.production?
      end
      check_invited_joiners if payment.persisted? && @invited_joiner_name_list && @invited_joiner_email_list
      Notifications::PaymentsWorker.perform_async(@membership.id)
      FollowHandler.new(@user.uid,@event.owner.uid).follow #this will return response only.
    end
  end

  def raise_waitlist_errors_and_rollback!
    errors << "Waitlist request has been denied."
    raise ActiveRecord::Rollback
  end

  def payment_params
    {
      promo_code_id:  params[:promo_code_id],
      mode:           PAYMENT_METHOD,
      made:           "downpayment",
      booking_fee:    BookingFee.last,
      status:         Payment.statuses[:approved],
      amount_paid:    amount,
      device_platform: @platform
    }
  end

  def join_params
    {
      member:   @user,
      event_id: @event.id
    }
  end

  def express_payment_params
    sale = @payment.transactions.map(&:related_resources).flatten.map(&:sale)
    {
      payer_id:             @payment.payer.payer_info.payer_id,
      payment_tracking_id:  @payment.id,
      payment_status:       @payment.state,
      payment_transaction_id: sale.map(&:id).reduce("")
    }
  end

  def amount_with_fixed_rate
    amount + PAYPAL_FIXED_RATE
  end

  def transaction_fee
    1 - PAYPAL_TRANSACTION_RATE
  end

  def total_amount_with_transaction_fee
    total = amount_with_fixed_rate / transaction_fee
    total.round(2)
  end

  def check_invited_joiners
    parser = InvitedJoinersParser.parse(names: @invited_joiner_name_list, emails: @invited_joiner_email_list)
    create_membership_for_invited_joiners(data: parser.data)
  end

  def create_membership_for_invited_joiners(data:)
    invite_joiner = InviteJoinerHandler.new(@user, @event.id, data, @membership.id).find_or_create
    unless invite_joiner.response[:type]
      errors << "Cannot process invited joiners!, #{invite_joiner.response[:details]}"
      raise ActiveRecord::Rollback
    end
  end

  def validate_promo_code(code:, user:)
    promo_code = PromoCode::ValidateByCategory.call(code: code, user: user)
    errors << promo_code.message unless promo_code.success?
    return promo_code
  end
end
