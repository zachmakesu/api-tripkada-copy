class UpdateWaitlist
  extend LightService::Organizer

  def self.call(params:, current_user:)
    with(params: params, current_user: current_user).reduce(actions)
  end

  def self.actions
    [
      Actions::LooksUpWaitlistDetails,
      Actions::WaitlistStatusValidator,
      Actions::WaitlistNotifier,
      Actions::SendWaitlistStatusMailer
    ]
  end
end
