ReviewHandlerError = Class.new(StandardError)

class ReviewHandler

  attr_accessor :reviewer, :params, :response

  def initialize(reviewer,params)
    @reviewer    = reviewer
    @params      = params
    @response    = Hash.new
  end


  def review_event
    event = Event.includes(:members).find_by!(id: params[:id])
    @review = event.received_reviews.create(review_params)
    create_response
    self
  end

  def review_organizer
    organizer = User.all.find_by!(uid: params[:uid])
    if organizer.organized_events.present?
      @review = organizer.received_reviews.as_joiner.create(review_params)
      AdminNotifications::ReviewMailerWorker.perform_async(@review.id, "review_organizer") if @review.persisted?
      create_response
    else
      create_response(error="User is not a Trip Organizer")
    end
    self
  end

  def review_joiner
    joiner = User.find_by!(uid: params[:uid])
    @review = joiner.received_reviews.as_organizer.create(review_params)
    create_response
    self
  end

  private

  def review_params
    {
      reviewer: reviewer,
      rating: params[:rating],
      message: params[:message],
      platform: params[:platform]
    }
  end

  def create_response(error=nil)
    if !@review.nil? && @review&.errors.blank?
      response[:success] = true
      response[:details] = @review
    else
      response[:success] = false
      response[:details] = error || @review.errors.full_messages.join(', ')
    end
  end

end
