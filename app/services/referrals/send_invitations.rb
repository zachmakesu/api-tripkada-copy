class Referrals::SendInvitations
  extend LightService::Organizer

  def self.call(params:, referrer:)
    with(params: params, referrer: referrer).reduce(actions)
  end

  def self.actions
    [
      Actions::Referrals::InvitationMailer
    ]
  end
end
