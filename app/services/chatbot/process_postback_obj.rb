class Chatbot::ProcessPostbackObj
  extend LightService::Organizer

  def self.call(message_obj:)
    if message_obj.messaging["recipient"].fetch("id") == ENV.fetch("FB_2ND_PAGE_ID")
      with(message_obj: message_obj).reduce(actions)
    else
      #do nothing
    end
  end

  def self.actions
    [
      #FETCH FB USER FROM GRAPH. returns context.fbuser_obj
      Actions::Chatbot::FetchFbUser,

      #REGISTER IT TO DATABASE MESSENGER USER. returns context.messenger_user
      Actions::Chatbot::FindOrCreateMessengerUser,

      #DELETE WORKER THAT `SENDS MESSAGE` WHENEVER A USER RESPONSE BELOW 2MINS
      Actions::Chatbot::DeletePayloadWorker,

      #Every postback delete messenger_user.message_concern
      Actions::Chatbot::DeleteMessageConcern,

      #returns context.replies
      Actions::Chatbot::PayloadMessage,

      #BOT WILL DELIVER context.replies
      Actions::Chatbot::DeliverReplies,

      #CREATE WORKER THAT `SENDS MESSAGE` WHENEVER A USER IS IDLE AFTER 2MINS
      Actions::Chatbot::CreatePayloadWorker,
    ]
  end
end
