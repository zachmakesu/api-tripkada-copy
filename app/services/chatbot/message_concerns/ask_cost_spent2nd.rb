class Chatbot::MessageConcerns::AskCostSpent2nd
  def self.replies(message_obj:, messenger_user:)
    #AI conditional
    @text_message = message_obj.messaging["message"]["text"]

    if @text_message =~ /^\d+$/
      @concern_answers = messenger_user.concern_answers
      @concern_answers['cost_spent'] = @text_message
      messenger_user.update(message_concern: "ASK_DAYS_SPENT_2nd_CONCERN", concern_answers: @concern_answers)
      [
        { text: "Also kindly adjust the amount of days greater than 100. Please provide a valid integer like 1 - 100" }
      ]
    else
      [{ text: "Please provide a valid integer." }]
    end
  end
end
