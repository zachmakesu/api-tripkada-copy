class Chatbot::MessageConcerns::AskActivity
  def self.replies(message_obj:, messenger_user:)
    #AI conditional
    @text_message = message_obj.messaging["message"]["text"]

    @categories = Category.order(name: :asc)

    selections = @categories.map(&:name)
    selections << "See more activity"
    selections << "back"

    if selections.include?(@text_message)
      messenger_user.update(message_concern: "ASK_ACTIVITY_CONCERN")
      if @text_message == "See more activity"
        [
          {
            text: "Please select an activity to do",
            quick_replies: Chatbot::ActivityQuickReplies.quick_replies_2nd_set
          }
        ]
      elsif @text_message == "back"
        [
          {
            text: "Please select an activity to do",
            quick_replies: Chatbot::ActivityQuickReplies.quick_replies_1st_set
          }
        ]
      else
        messenger_user.update(message_concern: "ASK_COST_SPENT_CONCERN", concern_answers: {category_id: @text_message})
        [
          { text: "How much are you willing to spend for a trip? (In PHP). Please provide a valid integer like 1 - 100000" }
        ]
      end
    else
      [
        {
          text: "Please select an activity to do",
          quick_replies: Chatbot::ActivityQuickReplies.quick_replies_1st_set
        }
      ]
    end
  end

end
