class Chatbot::MessageConcerns::AskEmail
  def self.replies(message_obj:, messenger_user:)
    #AI conditional
    if message_obj.messaging["message"]["text"] =~ /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
      messenger_user.update(message_concern: nil, sendable: false, email: message_obj.messaging["message"]["text"] )
      [{ text: "Thanks #{messenger_user.first_name}, expect an email notification to verify your email address on #{message_obj.messaging["message"]["text"]}." }]
    else
      [{ text: "Please provide a valid email address." }]
    end
  end
end
