class Chatbot::MessageConcerns::AskActivity2nd
  def self.replies(message_obj:, messenger_user:)
    #AI conditional
    @text_message = message_obj.messaging["message"]["text"]

    @categories = Category.order(name: :asc)

    selections = @categories.map(&:name)
    selections << "See more activity"
    selections << "back"

    if selections.include?(@text_message)
      messenger_user.update(message_concern: "ASK_ACTIVITY_2nd_CONCERN")
      if @text_message == "See more activity"
        [
          {
            text: "Please select an activity to do",
            quick_replies: Chatbot::ActivityQuickReplies.quick_replies_2nd_set
          }
        ]
      elsif @text_message == "back"
        [
          {
            text: "Please select an activity to do",
            quick_replies: Chatbot::ActivityQuickReplies.quick_replies_1st_set
          }
        ]
      else
        messenger_user.update(message_concern: "ASK_COST_SPENT_2nd_CONCERN", concern_answers: {category_id: @text_message})
        [
          { text: "kindly adjust the amount of trip cost to greater than 1000 pesos. Please provide a valid integer like 1 - 100000" }
        ]
      end
    else
      [
        {
          text: "Please select an activity to do",
          quick_replies: Chatbot::ActivityQuickReplies.quick_replies_1st_set
        }
      ]
    end
  end
end
