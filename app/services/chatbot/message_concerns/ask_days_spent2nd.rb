class Chatbot::MessageConcerns::AskDaysSpent2nd
  def self.replies(message_obj:, messenger_user:)
    #AI conditional
    @messenger_user = messenger_user
    @text_message = message_obj.messaging["message"]["text"]

    if @text_message =~ /^\d+$/
      process_possible_trips
    else
      [{ text: "Please provide a valid integer." }]
    end
  end

  def self.process_possible_trips
    @concern_answers = @messenger_user.concern_answers
    @concern_answers['days_spent'] = @text_message
    @messenger_user.update(concern_answers: @concern_answers)

    process_concern_answers
    @upcoming = EventFilter.new(Event.not_deleted.upcoming, process_concern_answers).result
    if @upcoming.empty?
      process_empty_trips
    else
      possible_trips
    end


  end

  def self.process_concern_answers
    @user_inputs = @messenger_user.concern_answers
    category = Category.find_by!(name: @user_inputs["category_id"])
    {
      category_ids: [category.id.to_s],
      min_cost: "0",
      max_cost: @user_inputs["cost_spent"],
      min_days: "0",
      max_days: @user_inputs["days_spent"],
    }
  end

  def self.possible_trips
    @messenger_user.update(message_concern: nil)
    elements = @upcoming.limit(9).decorate.map do |event|
      {
        title: event.name,
        subtitle: "Organizer: #{event.owner.full_name}
Date: #{event.travel_dates.upcase}
Description: #{event.description}
        ",
        image_url: event.cover_photo_complete_url(:large),
        buttons: [
          {
            type: "web_url",
            url: event.website,
            title: "View Details",
            webview_height_ratio: "tall"
          },
          {
            type: "web_url",
            url: event.generate_checkout_link,
            title: "Book Now",
            webview_height_ratio: "tall"
          }
        ]
      }
    end

    see_more = {
                  title: "See more Upcoming Trips",
                  buttons: [
                    {
                      type: "web_url",
                      url: "#{ActionController::Base.asset_host}/trips",
                      title: "See more",
                      webview_height_ratio: "tall"
                    }
                  ]
                }
    elements << see_more
    [

      { text: "Thank you #{@messenger_user.first_name} for providing those information." },
      { text: "I'll be back, I'll just look for the trips that will best suite you. This will be quick." },
      { text: "Thanks for waiting. Here are the trips that you will surely like." },
      {
        attachment: {
          type: 'template',
          payload: {
            template_type: "generic",
            image_aspect_ratio: "square",
            elements: elements
          }
        }
      }
    ]
  end

  def self.process_empty_trips
    @messenger_user.update(message_concern: nil, concern_answers: {})
    [
      { text: "Thank you #{@messenger_user.first_name} for providing those information." },
      { text: "I'll be back, I'll just look for the trips that will best suite you. This will be quick." },
      { text: "Thank you for waiting #{@messenger_user.first_name}, I am having a hard time looking trips for you at the moment." },
      {
        text: "But I do still have options for you to be able to browse our trips.",
        quick_replies: [
          {
            "content_type": "text",
            "title": "Recommend a trip again?",
            "payload": "RECOMMEND_TRIP_PAYLOAD"
          },
          {
            "content_type": "text",
            "title": "Show Upcoming Trips",
            "payload": "UPCOMING_PAYLOAD"
          },
        ]
      }
    ]
  end
end
