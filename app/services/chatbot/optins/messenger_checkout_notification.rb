class Chatbot::Optins::MessengerCheckoutNotification

  def self.replies(message_obj:, messenger_user:, optin_params:)
    @membership = EventMembership.approved.find_by(id: optin_params["membership_id"])
    if @membership
      @joiner = @membership.member.decorate
      @event = @membership.event.decorate
      @payment = @membership.payment.decorate
      [
        { "text": "MABUHAY, #{@joiner.first_name}!" },
        { "text": "Congratulations! Your reservation for event this coming #{@event.start_month_day_year} has been confirmed." },
        { "text": "Thank you for traveling with Tripkada!" },
        {
          "attachment":{
            "type":"template",
            "payload":{
              "template_type":"generic",
              "elements":[
                 {
                  "title":"#{@event.name}",
                  "image_url":"#{@event.cover_photo_complete_url}",
                  "subtitle":"#{@event.description}",
                  "default_action": {
                    "type": "web_url",
                    "url":"#{ActionController::Base.asset_host}/trips/#{@event.slug}",
                    "messenger_extensions": true,
                    "webview_height_ratio": "tall"
                  },
                  "buttons":[
                    {
                      "type":"web_url",
                      "url":"#{ActionController::Base.asset_host}/messenger_webview/checkout_notif/#{@membership.id}/#{message_obj.sender.fetch("id")}",
                      "messenger_extensions": true,
                      "webview_height_ratio": "tall",
                      "title":"See more details"
                    }
                  ]
                }
              ]
            }
          }
        }
      ]
    else
      []
    end
  end
end
