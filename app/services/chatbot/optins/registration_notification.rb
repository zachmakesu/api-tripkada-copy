class Chatbot::Optins::RegistrationNotification
  def self.replies(message_obj:, messenger_user:, optin_params:)
    return [] unless @user = User.find_by(id: optin_params["user_id"])
      [
        { "text": "MABUHAY, #{@user.first_name}! Welcome to Tripkada!" },
        { "text": "You will now get your booking notifications, updates and announcements directly via your FB Messenger" },
      ]
  end
end
