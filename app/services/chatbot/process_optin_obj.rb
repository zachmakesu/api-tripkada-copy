class Chatbot::ProcessOptinObj
  extend LightService::Organizer

  def self.call(message_obj:)
    if message_obj.messaging["recipient"].fetch("id") == ENV.fetch("FB_PAGE_ID")
      with(message_obj: message_obj).reduce(actions)
    else
      #do nothing
    end
  end

  def self.actions
    [
      #FETCH FB USER FROM GRAPH. returns context.fbuser_obj
      Actions::Chatbot::FetchFbUser,

      #REGISTER IT TO DATABASE MESSENGER USER. returns context.messenger_user
      Actions::Chatbot::FindOrCreateMessengerUser,

      #returns context.replies
      Actions::Chatbot::OptinMessage,

      #BOT WILL DELIVER context.replies
      Actions::Chatbot::DeliverReplies,
    ]
  end
end
