class Chatbot::ActivityQuickReplies
  def self.quick_replies_1st_set
    @categories = Category.order(name: :asc)

    quick_replies = @categories.page(1).per(9).map do |c|
      {
        "content_type": "text",
        "title": c.name,
        "payload": ""
      }
    end

    see_more = {
      "content_type": "text",
      "title": "See more activity",
      "payload": ""
    }

    quick_replies <<  see_more if @categories.count > 9
    quick_replies
  end

  def self.quick_replies_2nd_set
    @categories = Category.order(name: :asc)

    quick_replies = @categories.page(2).per(9).map do |c|
      {
        "content_type": "text",
        "title": c.name,
        "payload": ""
      }
    end

    back_button = {
      "content_type": "text",
      "title": "back",
      "payload": ""
    }

    quick_replies << back_button
    quick_replies
  end

end
