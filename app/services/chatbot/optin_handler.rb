class Chatbot::OptinHandler
  def self.response_from(message_obj:,messenger_user:, ref:, optin_params:)
    case ref
    when 'checkout_notification_ref'  then Chatbot::Optins::MessengerCheckoutNotification.replies(message_obj: message_obj, messenger_user: messenger_user, optin_params: optin_params)
    when 'registration_form_ref' then Chatbot::Optins::RegistrationNotification.replies(message_obj: message_obj, messenger_user: messenger_user, optin_params: optin_params)
    when 'CHECK_PAYLOAD' then [ {text: "Nice test"} ]

    else
    []
    end
  end
end
