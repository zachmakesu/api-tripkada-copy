class Chatbot::PayloadHandler
  def self.response_from(message_obj:, payload:, messenger_user:)
    case payload
    when 'GET_STARTED_PAYLOAD'                then Chatbot::Payloads::GetStarted.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'UPCOMING_PAYLOAD'                   then Chatbot::Payloads::UpcomingTrips.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'RECOMMEND_TRIP_PAYLOAD'             then Chatbot::Payloads::RecommendTrip.replies(message_obj: message_obj, messenger_user: messenger_user)

    when 'CHECK_PAYLOAD' then [ {text: "Nice test"} ]
    else

    # require "pry"
    # binding.pry

    []
    end
  end
end
