class Chatbot::Payloads::GetStarted
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true)
    [
      { text: "Hey there fellow traveller #{messenger_user.first_name}, I'm your Tripkada ChatGenie. Are you looking for your next travel experience?" },
      { text: "How can I help you today?" },
      {
        attachment: {
          type: "template",
          payload: {
            template_type: "list",
            top_element_style: "large",
            elements: [
              {
                title: "Show Trips This Upcoming Weekend",
                subtitle: "Hurry!",
                image_url: "https://i.imgur.com/DPVobQ0.png",
                buttons: [
                  {
                    title: "Click me!",
                    type: 'postback',
                    payload: 'UPCOMING_PAYLOAD'
                  }
                ]
              },
              {
                title: "Show Trips On Specific Date",
                subtitle: "A Date to remember ❤️",
                image_url: "https://i.imgur.com/Lgkf13x.png",
                buttons: [
                  {
                    title: "Show Date Filter",
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/messenger_webview/specific_date_trips/#{messenger_user.user_fb_id}",
                    messenger_extensions: true,
                    webview_height_ratio: "tall"
                  }
                ]
              },
              {
                title: "Show Trips based on Destination",
                subtitle: "Bestiny where Bes is your Destiny",
                image_url: "https://i.imgur.com/qYvB3AT.png",
                buttons: [
                  {
                    title: "Show Destination Filter",
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/messenger_webview/destination_based_trips/#{messenger_user.user_fb_id}",
                    messenger_extensions: true,
                    webview_height_ratio: "tall"
                  }
                ]
              },
              {
                title: "Show Trips based on Activity",
                subtitle: "Choose where you suits!",
                image_url: "https://i.imgur.com/qWTfOFC.png",
                buttons: [
                  {
                    title: "Show Activity Filter",
                    type: "web_url",
                    url: "#{ActionController::Base.asset_host}/messenger_webview/activity_based_trips/#{messenger_user.user_fb_id}",
                    messenger_extensions: true,
                    webview_height_ratio: "tall"
                  }
                ]
              },
            ],
            buttons: [
              {
                title: "Recommend A Trip",
                type: 'postback',
                payload: 'RECOMMEND_TRIP_PAYLOAD'
              }
            ]
          }
        }
      }
    ]
  end
end
