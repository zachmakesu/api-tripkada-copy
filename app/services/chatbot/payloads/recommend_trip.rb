class Chatbot::Payloads::RecommendTrip
  def self.replies(message_obj:, messenger_user:)
    messenger_user.update(sendable: true, message_concern: "ASK_ACTIVITY_CONCERN", concern_answers: {})

    [
      { text: "Hello #{messenger_user.first_name}, seems like you are having a hard time choosing which trips you should book." },
      { text: "Worry no more, I can help you with this. I will ask you few questions so that we can recommend the best trips for you." },
      { text: "If you may.." },
      { text: "What do you want to do on a trip?" },
      { text: "Please select an activity to do",
        quick_replies: Chatbot::ActivityQuickReplies.quick_replies_1st_set
      }
    ]
  end
end
