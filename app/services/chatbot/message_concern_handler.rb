class Chatbot::MessageConcernHandler
  def self.response_from(message_obj:, messenger_user:)
    case messenger_user.message_concern
    when 'ASK_ACTIVITY_CONCERN'         then Chatbot::MessageConcerns::AskActivity.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'ASK_EMAIL_CONCERN'            then Chatbot::MessageConcerns::AskEmail.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'ASK_COST_SPENT_CONCERN'       then Chatbot::MessageConcerns::AskCostSpent.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'ASK_DAYS_SPENT_CONCERN'       then Chatbot::MessageConcerns::AskDaysSpent.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'ASK_ACTIVITY_2nd_CONCERN'     then Chatbot::MessageConcerns::AskActivity2nd.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'ASK_COST_SPENT_2nd_CONCERN'   then Chatbot::MessageConcerns::AskCostSpent2nd.replies(message_obj: message_obj, messenger_user: messenger_user)
    when 'ASK_DAYS_SPENT_2nd_CONCERN'   then Chatbot::MessageConcerns::AskDaysSpent2nd.replies(message_obj: message_obj, messenger_user: messenger_user)

    when 'CHECK_CONCERN'                then [ {text: "Nice test"} ]
    else
    []
    end
  end
end
