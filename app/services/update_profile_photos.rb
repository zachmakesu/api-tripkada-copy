class UpdateProfilePhotos
  extend LightService::Organizer

  def self.call(user:, params:)
    with(user: user, params: params).reduce(actions)
  end

  def self.actions
    [
      Actions::UpdateProfilePhoto,
      Actions::UpdateCoverPhoto,
      Actions::UpdateAvatar,
    ]
  end
end
