module Mailerlite
  module Groups
    class RemoveSubscriber
      extend LightService::Organizer
      
      def self.call(group_id:, subscriber_email:)
        with(
          group_id: group_id,
          subscriber_email: subscriber_email,
          query_params: {},
          body_params: {}
        ).reduce(actions)
      end

      def self.actions
        [
          Actions::Mailerlite::Groups::RemoveSubscriber,
          Actions::Mailerlite::SendRequest
        ]
      end
    end
  end
end
