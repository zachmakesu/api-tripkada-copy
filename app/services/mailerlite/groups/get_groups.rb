module Mailerlite
  module Groups
    class GetGroups
      extend LightService::Organizer

      def self.call
        with(query_params: {}, body_params: {}).reduce(actions)
      end

      def self.actions
        [
          Actions::Mailerlite::Groups::GetGroups,
          Actions::Mailerlite::SendRequest
        ]
      end
    end
  end
end
