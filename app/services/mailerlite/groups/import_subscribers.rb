module Mailerlite
  module Groups
    class ImportSubscribers
      extend LightService::Organizer
      
      def self.call(group_id:, subscribers:)
        with(
          group_id: group_id,
          subscribers: subscribers,
          query_params: {},
          body_params: {}
        ).reduce(actions)
      end

      def self.actions
        [
          Actions::Mailerlite::Groups::BuildSubscribers,
          Actions::Mailerlite::Groups::ImportSubscribers,
          Actions::Mailerlite::SendRequest
        ]
      end
    end
  end
end
