module Mailerlite
  module Groups
    class DeleteGroup
      extend LightService::Organizer

      def self.call(group_id:)
        with(group_id: group_id, query_params: {}, body_params: {}).reduce(actions)
      end

      def self.actions
        [
          Actions::Mailerlite::Groups::DeleteGroup,
          Actions::Mailerlite::SendRequest
        ]
      end
    end
  end
end
