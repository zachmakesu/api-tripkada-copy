module Mailerlite
  module Groups
    class CreateGroup
      extend LightService::Organizer

      def self.call(name:)
        with(name: name, query_params: {}, body_params: {}).reduce(actions)
      end

      def self.actions
        [
          Actions::Mailerlite::Groups::CreateGroup,
          Actions::Mailerlite::SendRequest
        ]
      end
    end
  end
end
