module Mailerlite
  module Groups
    class AddToGroup
      extend LightService::Organizer
      
      def self.call(subscriber:, booking_count:)
        with(
          subscribers: [subscriber],
          booking_count: booking_count,
          query_params: {},
          body_params: {}
        ).reduce(actions)
      end

      def self.actions
        [
          Actions::Mailerlite::ValidateSegmentCategory,
          Actions::Mailerlite::Groups::BuildSubscribers,
          Actions::Mailerlite::Groups::FindGroup,
          Actions::Mailerlite::Groups::ImportSubscribers,
          Actions::Mailerlite::SendRequest
        ]
      end
    end
  end
end
