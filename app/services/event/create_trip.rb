class Event::CreateTrip
  extend LightService::Organizer

  def self.call(params:, organizer:)
    with(organizer: organizer, params: params).reduce(actions)
  end

  def self.actions
    [
      Actions::Event::ValidateDraftStatus,
      Actions::Event::ValidateDestinations,
      Actions::Event::ValidateCategories,
      Actions::Event::BuildEventParams,
      Actions::Event::BuildItinerariesParams,
      Actions::Event::BuildPhotosParams,
      Actions::Event::BuildOrganizerMembershipParams,
      Actions::Event::CreateEvent,
      Actions::Event::CreatePhotos,
      Actions::Event::CreateOrganizerMembership,
      Actions::Event::BuildDestinationsParams,
      Actions::Event::CreateSortingDestinations,
      Actions::Event::BuildCategorizationsParams,
      Actions::Event::CreateCategorizations,
      Actions::Event::CreateItineraries,
      Actions::Event::NotifyOrganizerFollowersAndEventJoiners,
      Actions::Event::NotifyAdminNewTrip
    ]
  end
end
