class Event::Booking::EnsureSufficientSlotsAndValidInviteeEmails
  extend LightService::Organizer

  def self.call(event:, params:, joiner:)
    with(
      event: event, params: params,
      joiner: joiner,
      transaction_exists: false
    ).reduce(actions)
  end

  def self.actions
    [
      Actions::Event::Booking::ValidateInvitedJoiners,
      Actions::Event::Booking::ValidateInviteesIfAlreadyMember,
      Actions::Event::Booking::ValidateJoinersEmail,
      Actions::Event::Booking::EnsureJoinerEmailNotUseForInvitedJoiners,
      Actions::Event::Booking::ValidateAvailableSlots
    ]
  end
end
