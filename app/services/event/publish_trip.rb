class Event::PublishTrip
  extend LightService::Organizer

  def self.call(organizer:, event:, params:)
    with(
      organizer: organizer,event: event,
      params: params
    ).reduce(actions)
  end

  def self.actions
    [
      Actions::Event::ValidateDraftStatus,
      Actions::Event::ValidateDestinations,
      Actions::Event::ValidateCategories,
      Actions::Event::BuildEventParams,
      Actions::Event::BuildItinerariesParams,
      Actions::Event::BuildPhotosParams,
      Actions::Event::UpdateEvent,
      Actions::Event::CreatePhotos,
      Actions::Event::BuildDestinationsParams,
      Actions::Event::CreateSortingDestinations,
      Actions::Event::BuildCategorizationsParams,
      Actions::Event::CreateCategorizations,
      Actions::Event::CreateItineraries,
      Actions::Event::NotifyOrganizerFollowersAndEventJoiners,
      Actions::Event::NotifyAdminNewTrip
    ]
  end
end
