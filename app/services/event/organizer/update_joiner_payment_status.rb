class Event::Organizer::UpdateJoinerPaymentStatus
  extend LightService::Organizer

  def self.call(event:, membership:, status:)
    with(event: event, membership: membership, status: status).reduce(actions)
  end

  def self.actions
    [
      Actions::Event::Organizer::UpdateJoinerPaymentStatus
    ]
  end
end
