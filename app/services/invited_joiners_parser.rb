class InvitedJoinersParser
  extend LightService::Organizer

  def self.parse(names:, emails:)
    with(names: names, emails: emails).reduce(Actions::ParseInvitedJoiners)
  end
end
