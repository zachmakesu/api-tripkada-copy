class Pusher::NotificationHandler
  def initialize recipient_id
    @recipient = User.find(recipient_id)
  end

  def deliver
    begin
      Pusher.trigger(
        channel_name,
        event_name,
        message
      )
    rescue Pusher::Error => e
      Rails.logger.error(e)
    end
  end

  private
  def channel_name
    "notification-#{@recipient.uid}"
  end

  def event_name
    "update_users_notification"
  end

  def message
    { :notification_count => @recipient.notifications.unread.count }
  end
end
