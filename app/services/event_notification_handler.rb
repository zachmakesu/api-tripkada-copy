class EventNotificationHandler

  attr_accessor :event, :job_ids

  def initialize event:, membership:
    @event      = event
    @membership = membership
    @job_ids    = []
  end

  def deliver
    if @membership
      create_new_job_for @membership if @membership.payment.approved?
    else
      clear_pending_jobs
      create_new_jobs
    end

    event.update(job_ids_options)
    self
  end

  private
  def clear_pending_jobs
    return unless event.job_ids
    event_job_ids.each do |jid|
      Sidekiq::ScheduledSet.new.select{|j| j.jid == jid}.each(&:delete)
    end
  end

  def create_new_jobs
    event.approved_event_memberships.each do |membership|
      create_new_job_for membership
    end
  end

  def create_new_job_for membership
    job_ids << Notifications::BeforeEventWorker.perform_at(event.start_at-1.day, event.id, membership.id)
    job_ids << Notifications::AfterEventWorker.perform_at(event.end_at, event.id, membership.id)
  end

  def job_ids_options
    { job_ids:  (@membership ? merged_job_ids : new_job_ids) }
  end

  def merged_job_ids
    (job_ids+event_job_ids).uniq.join(';')
  end

  def event_job_ids
    event.job_ids.split(";").reject(&:empty?)
  end

  def new_job_ids
    job_ids.join(";")
  end
end
