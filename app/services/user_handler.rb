UserHandlerError = Class.new(StandardError)

class UserHandler

  include ApiHelper
  include UsersHelper
  using StringToBooleanRefinements

  attr_accessor :user, :params, :photos, :response

  def initialize(current_user, params)
    @user     = current_user
    @params   = params
    @photos   = user.send(params[:category]) if valid_category?
    @response = Hash.new
  end

  def update_profile
    validate_user_params
    if @user.errors.empty?
      ActiveRecord::Base.transaction do
       if @user.update(user_params)
          # Automatically send application for Organizer
          send_application = validate_organizer_application_request(
            apply_as_organizer: params.fetch("apply_as_organizer", false)
          )
          Notifications::EmailWorker.perform_async(user_params, "apply_as_organizer_from_organizer_app") if send_application && @user.joiner?
       end
      end
      # Create or Update Bank Account Details
      if params[:bank_name].present? && params[:account_number].present?
        @user_bank_account = BankAccount.find_or_initialize_by(user_id: @user.id)
        @user_bank_account.update(bank_account_params)
      end
    end

    create_response
    self
  end

  def upload_photos
    if valid_category?
      if photo_params.present?
        ActiveRecord::Base.transaction do
          @photos.build(photo_params.map{|a| a.except(:id)})
          @photos.map(&:save)
        end
      end

      create_response
    else
      response[:type]    = false
      response[:details] = "Invalid category to upload."
    end

    self
  end

  def update_photos
    if valid_category?
      if photo_params.present?
        ActiveRecord::Base.transaction do
          photo_params.each do |obj|
            @photo = photos.find_by(id: obj[:id])
            next if @photo.nil?
            @photo.image   = obj[:image]
            @photo.primary = obj[:primary]
            @photo.label   = obj[:label]
            @photo.save
          end
        end
      end

      create_response
    else
      response[:type]    = false
      response[:details] = "Invalid category to upload."
    end

    self
  end

  def update_profile_avatar
    if params[:photo].present?
      @photo = @user.profile_avatar || @user.build_profile_avatar
      @photo.update(photo_params)
    end

    create_response
    self
  end

  def delete_photos
    if valid_category?
      @photos.where(id: [params[:photo_ids]]).destroy_all

      create_response
    else
      response[:type]    = false
      response[:details] = "Invalid category to upload."
    end

    self
  end

  def apply_as_organizer

  end

  def create_or_update_cover_photo
    if cover_photo_params.present?
      @photo = @user.cover_photo || @user.build_cover_photo
      @photo.update(cover_photo_params)
    end

    create_response
    self
  end

  def delete_cover_photo
    @user.cover_photo.destroy if user.cover_photo.present?
    create_response
    self
  end

  def upload_photo
    @photo = @user.profile_photos.build(photo_params)
    photo_params.empty? ? invalid_photo_params : @photo.save
    create_response
    self
  end

  def update_photo
    @photo = @user.profile_photos.find(params[:photo_id])
    photo_params.empty? ? invalid_photo_params : @photo.update(photo_params)
    create_response
    self
  end

  def delete_photo
    @user.profile_photos.find(params[:photo_id]).destroy
    create_response
    self
  end

  private
  def validate_user_params
    @user.errors.add(:email, "please provide a valid email address") unless valid_email?(email: user_params[:email])
    @user.errors.add(:gender, "Invalid gender, please specify a proper gender") if !User.genders.keys.include?(user_params[:gender])
    @user.errors.add(:mobile, "is invalid please provide a valid mobile number.") if user_params[:mobile].empty?
  end

  def invalid_photo_params
    @photo.errors.add(:base, "Invalid null parameters")
  end

  def valid_category?
    %w( profile_photos government_photos ).include?(params[:category])
  end

  def create_response
    error_msgs = []
    error_msgs << @user.errors.full_messages                      if @user
    error_msgs << @photos.map{|e| e.errors.full_messages}         if @photos
    error_msgs << @photo.errors.full_messages                     if @photo
    error_msgs.flatten!

    if error_msgs.blank?
      response[:type]     = true
      response[:details]  = @user.decorate
    else
      response[:type]     = false
      response[:details]  = error_msgs.join(', ')
    end
  end

  def user_params
    {
      first_name: params[:first_name],
      last_name: params[:last_name],
      email:     params[:email],
      gender:    params[:gender].downcase,
      birthdate: params[:birthdate],
      country:   params[:country],
      state:     params[:state],
      city:      params[:city],
      address:   params[:address],
      mobile:    params[:mobile],
      tell_us_about_yourself: params[:tell_us_about_yourself],
      emergency_contact_name: params[:emergency_contact_name],
      emergency_contact_number: params[:emergency_contact_number]
    }
  end

  def photo_params
    unless params[:photos].nil?
      params[:photos].each_with_index.map do |photo,i|
        { id: params[:photo_ids].try(:[], i), image: build_attachment(photo), primary: params[:primary].try(:[], i), label: params[:photo_labels].try(:[], i) }
      end
    end
  end

  def bank_account_params
    {
      bank_name:      params[:bank_name],
      account_number: params[:account_number]
    }
  end

  def cover_photo_params
    unless params[:cover_photo].nil?
      {
        primary: true,
        image: build_attachment(params[:cover_photo]),
        label: params[:label]
      }
    end
  end

  def photo_params
    hash = Hash.new
    hash[:image] = build_attachment(params[:photo]) if params[:photo]
    hash[:label] = params[:label]                   if params[:label]
    hash
  end

  def validate_organizer_application_request(apply_as_organizer:)
    apply_as_organizer.to_s.to_bool
  rescue ArgumentError
    false
  end
end
