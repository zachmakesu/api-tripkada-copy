EventHandlerError = Class.new(StandardError)

class EventHandler

  include ApiHelper

  attr_accessor :user, :params, :response

  def initialize(user,params)
    @user     = user
    @params   = params
    @mobile_number = params.fetch(:mobile_number){ nil }
    @free_downpayment = params.fetch(:free_downpayment){ false }
    @free_trip = params.fetch(:free_trip){ false }
    @invited_joiner_name_list = params.fetch(:invited_joiner_names){ nil }
    @invited_joiner_email_list = params.fetch(:invited_joiner_emails){ nil }
    @platform = params.fetch(:platform, "desktop")
    @response = Hash.new
  end

  def create
    return self if invalid_duplicate

    ActiveRecord::Base.transaction do
      @event = user.organized_events.new(event_params)
      if @event.save
        @event.photos.create(photos_params)                             unless params[:photos].nil?
        @event.photos.create(dup_photo_params)                          unless params[:photo_ids].nil? || dup_photo_params.empty?
        @event.itineraries.create(itineraries_params)                   unless params[:itineraries_description].nil?
        membership = @event.event_memberships.build(join_params)
        membership.build_payment(organizer_payment_params)
        membership.save

        append_photo_errors_and_rollback if @event.photos.any?(&:invalid?)

        raise ActiveRecord::Rollback if invalid_category_and_destination?
        @event.categorizations.create(categorizations_params)
        @event.sorting_destinations.create(sorting_destinations_params)

        @event.reload
        AdminNotifications::EventMailerWorker.perform_async(@event.id, "new_trip")
        Notifications::TripsWorker.perform_async(@event.id)
        user.followers.ids.each do |id|
          Notifications::FollowerWorker.perform_async(id, @event.id)
        end
      end
    end

    create_response
    self

  end

  def update

    travel_dates_changed = false

    ActiveRecord::Base.transaction do

      @event = user.admin? ? Event.find(params[:id]) : user.organized_events.not_deleted.find(params[:id])

      @event.assign_attributes(event_params)
      travel_dates_changed = @event.start_at_changed? || @event.end_at_changed?
      @event.save

      unless params[:photos].nil? && params[:photo_ids].nil?
        photo_ids = params[:photo_ids].reject(&:empty?) if params[:photo_ids].present?
        @event.photos.where.not(id: photo_ids).destroy_all
        @event.photos.create(photos_params) if params[:photos].present?
      end

      unless params[:itineraries_description].nil?
        @event.itineraries.destroy_all
        @event.itineraries.create(itineraries_params)
      end

      append_photo_errors_and_rollback if @event.photos.any?(&:invalid?)

      raise ActiveRecord::Rollback if invalid_category_and_destination?
      @event.categorizations.destroy_all
      @event.categorizations.create(categorizations_params)
      @event.sorting_destinations.destroy_all
      @event.sorting_destinations.create(sorting_destinations_params)
      upload_brand_sponsor_logo if user.admin?
    end

    create_response
    Notifications::TripsWorker.perform_async(@event.id) if response[:type] && travel_dates_changed
    self

  end

  def join
    set_payment_mode if @free_downpayment || @free_trip

    ActiveRecord::Base.transaction do

      @membership = EventMembership.new(join_params)

      @payment = Payment.new
      @payment.event_membership = @membership if @membership.validate
      build_payment_attributes
      @payment.payment_photos.build(joiner_photo_params) unless params[:photos].blank?
      @event = Event.find(params[:id])
      if @payment.save
        validate_details_of(joiner: @user)
        User::SendCreditsToReferrer.call(joiner: @user, event: @event)
        check_invited_joiners if @invited_joiner_name_list && @invited_joiner_email_list
        @waitlist = @event.waitlists.find_by(user_id: @user)
        raise ActiveRecord::Rollback if @waitlist&.denied?
        @waitlist.paid! if @waitlist&.approved?
        Mailerlite::AddRemoveSubscriberWorker.perform_async(@user.uid, @user.approved_event_memberships.count) if Rails.env.production?
      end

      FollowHandler.new(user.uid,@event.owner.uid).follow #this will return response only.
    end

    if @payment.errors.blank? && @membership.errors.blank?
      response[:type] = true
      response[:details] = Event.all.not_deleted.upcoming.find_by(id: params[:id]).try(&:decorate)
      response[:membership] = @membership
      Notifications::PaymentsWorker.perform_async(@membership.id)
      Notifications::TripsWorker.perform_async(@event.id,@membership.id)
      ChatbotMessenger::MessengerOrganizerNotificationsWorker.perform_async(
        @event.id,
        @membership.id
      )
    else
      response[:type] = false
      response[:details] = [@payment, @membership].map{|e| e.errors.full_messages }.flatten.join(", ")
    end

    self

  end

  # def invite_joiner
  #   ActiveRecord::Base.transaction do
  #     @event = user.decorate.upcoming_organized_events.find(params[:id])
  #     @event.errors.add(:base, "Full name can't be blank") if params[:full_name].blank?
  #     #Validate Mobile No.
  #     if is_valid_mobile?(@mobile_number)
  #       @mobile_number.prepend("63")
  #       message = @event.generate_invite_joiner_message(params[:full_name])
  #       Sms::SenderWorker.perform_async(message, @mobile_number)
  #     else
  #       @event.errors.add(:base, "Please Provide a 10 digit Mobile Number with format 9221234567")
  #     end
  #   end

  #   create_response
  #   self
  # end

  def invite_joiner_via_email
    @event = user.decorate.upcoming_organized_events.find(params[:id])
    @event.errors.add(:base, "Full name can't be blank") if params[:full_name].blank?
    @event.errors.add(:base, "Please provide a valid email") if !(params[:email] =~ Devise.email_regexp)
    if @event.errors.empty?
      Notifications::EmailWorker.perform_async(params, "invite_joiner_via_email")
    end

    create_response
    self
  end

  def upload_photo
    @event = user.organized_events.not_deleted.find(params[:id])

    if validate_photo_params
      @photo = @event.photos.build(photo_params)
      unset_existing_cover_photo
      add_photo_errors unless @photo.save
    end

    create_response
    self
  end

  def update_photo
    @event = user.organized_events.not_deleted.find(params[:id])
    @photo = @event.photos.find(params[:photo_id])

    if validate_photo_params
      @photo.assign_attributes(photo_params)
      unset_existing_cover_photo
      add_photo_errors unless @photo.save
    end

    create_response
    self
  end

  def delete_photo
    @event = user.organized_events.not_deleted.find(params[:id])
    @event.photos.find(params[:photo_id]).destroy

    create_response
    self
  end

  private

  def validate_details_of(joiner:)
    if !joiner.decorate.has_completed_required_info?
      @payment.errors.add(:base, "Joiner should fill up registration form!")
      raise ActiveRecord::Rollback
    end
  end

  def validate_photo_params
    photo_params.present?.tap do |has_params|
      add_photo_params_error unless has_params
    end
  end

  def add_photo_params_error
    @event.errors.add(:base, "Invalid null parameters")
  end

  def add_photo_errors
    @event.errors.add(:base, @photo.errors.full_messages.join(', '))
  end

  def create_response
    if @event.errors.blank?
      response[:type] = true
      response[:details] = @event.decorate
    else
      response[:type] = false
      response[:details] = @event.errors.full_messages.join(', ')
    end
  end

  def event_params
    {
      duplicated_from_event:  @parent_event,
      name:                   params[:name],
      start_at:               params[:start_at],
      end_at:                 params[:end_at],
      description:            params[:description],
      rate:                   params[:rate],
      min_pax:                params[:min_pax],
      max_pax:                params[:max_pax],
      things_to_bring:        params[:things_to_bring],
      meeting_place:          params[:meeting_place],
      tags:                   params[:tags],
      highlights:             params[:highlights],
      inclusions:             params[:inclusions],
      lat:                    params[:lat],
      lng:                    params[:lng],
      downpayment_rate:       params[:downpayment_rate],
      published_at:           DateTime.now
    }
  end

  def photos_params
    params[:photos].each_with_index.map do |x,i|
      { image: params[:from_controller] ? params[:photos][i] : build_attachment(params[:photos][i]), primary: params[:primary].try(:[], i) }
    end
  end

  def dup_photo_params
    params[:photo_ids].map do |id|
      if photo = Photo.find_by(id: id)
        { image: photo.image, label: photo.label, primary: photo.primary }
      end
    end.compact
  end

  def itineraries_params
    params[:itineraries_description].reject(&:empty?).each_with_index.map do |x,i|
      { description: params[:itineraries_description][i] }
    end
  end

  def categorizations_params
    Category.where(id: params[:category_ids]).map{|category| { category_id: category.id } }
  end

  def sorting_destinations_params
    Destination.where(id: params[:destination_ids]).map{|destination| { destination_id: destination.id } }
  end

  def payment_params
    {
      promo_code_id:    params[:promo_code_id],
      mode:             params[:mode],
      made:             params[:made],
      booking_fee:      BookingFee.last,
      status:           Payment.statuses[:approved],
      device_platform:  @platform
    }
  end

  def join_params
    {
      member: user,
      event_id: params[:id],
    }
  end

  def joiner_photo_params
    params[:photos].compact.each_with_index.map do |x,i|
      { image: params[:from_controller] ? params[:photos][i] : build_attachment(params[:photos][i]) }
    end
  end

  def photo_params
    {
      primary:  params.fetch(:primary, false),
      image:    (build_attachment(params[:photo]) if params[:photo]),
      label:    params.fetch(:label, nil),
      category: params.fetch(:category, "uncategorized")
    }.compact
  end

  def is_valid_mobile?(mobile_number)
    # Ensure String only contains numbers 0-9 and length = 10
    /\A\d{10,10}\z/ === mobile_number
  end

  def append_photo_errors_and_rollback
    @event.errors.add(:base, @event.photos.map{ |p| p.errors.full_messages }.uniq.join(','))
    raise ActiveRecord::Rollback
  end

  def invalid_category_and_destination?
    @event.errors.add(:base, "Please select atleast one category") if !params[:category_ids].is_a?(Array) || categorizations_params.empty?
    @event.errors.add(:base, "Please select atleast one destination") if !params[:destination_ids].is_a?(Array) || sorting_destinations_params.empty?
    @event.errors.any?
  end

  def invalid_duplicate
    return false unless params.key?(:event_id)
    unless @parent_event = user.organized_events.not_deleted.find_by(id: params[:event_id])
      response[:type] = false
      response[:details] = "Invalid parent event for this duplicate"
      return true
    end
  end

  def set_payment_mode
    if @free_trip
      params[:mode] = Payment.modes[:free_trip]
      params[:made] = Payment.mades[:fullpayment]
    else
      params[:mode] = Payment.modes[:free_downpayment]
    end
  end

  def check_invited_joiners
    parser = InvitedJoinersParser.parse(names: @invited_joiner_name_list, emails: @invited_joiner_email_list)
    create_membership_for_invited_joiners(data: parser.data)
  end

  def create_membership_for_invited_joiners(data:)
    invite_joiner = InviteJoinerHandler.new(@user, @event.id, data, @membership.id).find_or_create
    unless invite_joiner.response[:type]
      @membership.errors.add(:base, "Cannot process invited joiners!, #{invite_joiner.response[:details]}")
      raise ActiveRecord::Rollback
    end
  end

  def organizer_payment_params
    {
      mode:     "organizer",
      made:     "fullpayment",
      status:   Payment.statuses[:approved]
    }
  end

  def unset_existing_cover_photo
    # if new/built @photo is a cover_photo
    # and event has existing cover photo
    # and new/built photo is not the same photo record as the existing event's cover photo
    if @photo.cover_photo? && @event.cover_photo && @photo.id != @event.cover_photo.id
      @event.cover_photo.update(category: 'uncategorized', primary: false)
    end
  end

  def build_payment_attributes
    begin
      @payment.attributes = payment_params
    rescue ArgumentError => e
      @payment.errors.add(:base, "#{e.message}")
      raise ActiveRecord::Rollback
    end
  end

  def brand_sponsor_logo_params
    { image: params[:brand_sponsor_logo] }
  end

  def upload_brand_sponsor_logo
    brand_sponsor_logo = @event.sponsors.new(brand_sponsor_logo_params)
    @event.sponsors.destroy_all if params[:brand_sponsor_id] == ""
    if brand_sponsor_logo_params[:image]
      @event.errors.add(
        :base,
        "Failure to upload brand sponsor logo, #{brand_sponsor_logo.errors.full_messages.to_sentence}"
      ) unless brand_sponsor_logo.save
    end
  end

end
