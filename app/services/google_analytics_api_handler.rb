GoogleAnalyticsApiHandlerError = Class.new(StandardError)
class GoogleAnalyticsApiHandler

  attr_accessor :category, :action, :label, :value

  def initialize(category, action, label, value)
    @category     = category
    @action       = action
    @label        = label
    @value        = value
  end

  def track_event!
    event = tracker.build_event(event_options)
    event.track!
  end

  private

  def tracker
    Staccato.tracker(ENV["GOOGLE_ANALYTICS_TRACKING_ID"], nil, ssl: true)
  end

  def event_options
    {
      category: category,
      action:   action,
      label:    label,
      value:    value
    }
  end

end
