FCMHandlerError = Class.new(StandardError)

class FCMHandler
  DEFAULT_ROLE = 'both'

  def initialize recipient:, payload:, role: DEFAULT_ROLE
    @fcm        = FCM.new(ENV['FCM_KEY'])
    @recipient  = recipient
    @payload    = payload
    @role       = validate_role role.downcase
  end

  def deliver
    @fcm.send(devices.android.pluck(:token), data_payload)
    @fcm.send(devices.ios.pluck(:token), data_payload.merge(notification_payload))
  end

  private

  def devices
    devices = @recipient.devices
    devices = case @role
              when 'joiner' then devices.joiner
              when 'organizer' then devices.organizer
              else
                devices
              end
  end

  def data_payload
    { data: @payload }
  end

  def notification_payload
    {
      notification: {
        body: @payload.fetch(:alert_message, nil)
      }
    }
  end

  def validate_role role
    Device::ROLES.include?(role) ? role : DEFAULT_ROLE
  end
end
