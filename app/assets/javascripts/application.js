// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui/slider
//= require foundation
//= require jquery.slick
//= require jquery.tagsinput
//= require moment
//= require angular
//= require angular.circular.timepicker
//= require datepicker.min
//= require highcharts
//= require chartkick
//= require pusher.min
//= require_tree .
//= stub invite_joiner
//= stub base
//= stub admin
//= stub update_payment_status
//

var popupNotification;

$(function(){
  $(document).foundation();

  // Render Preloader on Page load
  setTimeout(function(){
      $('body').addClass('loaded');
  }, 1200);

  // Trigger Form Submit
  $('*[data-submit-form]').on("keyup", function(){
  	$(this).closest("form").submit();
  });

  $('[data-date-picker]').datepicker({
    autoHide: true,
    format: "mm-dd-yyyy"
  });

  $('*.actions__show-import-modal-btn').on('click', function(){
    url = $(this).data('link');
    form = $('#import-subscribers-modal__form');
    updateFormUrl(url, form)
  });

  $('*.actions__show-delete-group-confirmation-modal-btn').on('click', function(){
    url = $(this).data('link');
    form = $('#delete-group-confirmation-form').closest("form");
    updateFormUrl(url, form)
  });

  function updateFormUrl(url, form) {
    $(form).attr('action', url);
  }

  $('#add-more-features').on('click', function(e){
    e.preventDefault();
    var wrapper = $('#features-wrapper');
    var counter = $(wrapper).children('div').length;
    counter++;
    var template = JST['views/admin/app_version_features']({counter: counter})
    $(wrapper).append(template);
    $('*.remove-features-btn').on('click', function(e){
      e.stopPropagation();
      $(this).parent().remove();
    });
  });

  $('*.send_notification').on('click', function(){
    url = $(this).data('link');
    form = $('#sendNotificationForm');
    updateFormUrl(url, form)
  });

  var t={
    delay:125,
    overlay:$(".fb-overlay"),
    widget:$(".fb-widget"),
    button:$(".fb-button")
  };
  setTimeout(function(){$("div.fb-livechat").fadeIn()},8*t.delay),
  $(".ctrlq").on("click",function(e){
    e.preventDefault(),
    t.overlay.is(":visible")?(t.overlay.fadeOut(t.delay),t.widget.stop().animate({bottom:0,opacity:0},
    2*t.delay,function(){$(this).hide("slow"),t.button.show()})):t.button.fadeOut("medium",function(){t.widget.stop().show().animate({bottom:"30px",opacity:1},2*t.delay),t.overlay.fadeIn(t.delay)})
  })

  $(document).on('click', '.notification__close-btn',function(){
    $('#notification').slideUp("slow", function(){
      $(this).remove();
    });
  });

  popupNotification = function generateNotification(type, message){
    var template = JST['views/shared/notification'];
    icon = generateIcon(type);
    $(document.body).prepend(template({
      type: type,
      icon: icon,
      message: message
    }));
    if ($(window).scrollTop() > 0){
      $('#notification').addClass("notification--fixed");
    }
    setTimeout(function(){
      $('#notification').slideUp("slow", function() {
        $(this).remove();
      })
    }, 5000);
  }

  function generateIcon(type){
    switch(type) {
      case "success":
        icon = "checkmark-round";
        break;
      case "alert":
        icon = "close-round";
        break;
      case "info":
        icon = "information";
        break;
    }
    return icon;
  }
  $(document).on('change','#file_input',function() {
    var fileValue = $('#file_input').val()
    if (fileValue) {
      $('#import_promo').removeClass('hide')
    }
  });

  $(document).on('click', function(event) {
    if (!$(event.target).closest('.user-info-body').length) {
      $('.user-info-modal').addClass('hide');
    }
  });
});

// Google Web App Install Banner Service Worker
if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {
    navigator.serviceWorker.register('/sw.js', {
      scope: '/'
    }).then(function(registration) {
      // Registration was successful
      console.log('ServiceWorker registration successful with scope: ', registration.scope);
    }, function(err) {
      // registration failed :(
      console.log('ServiceWorker registration failed: ', err);
    });
  });
}
