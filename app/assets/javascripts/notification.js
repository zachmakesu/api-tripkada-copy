$(function() {
  $('*.display-notification').on('click', function(){
    var container = $(this).parent().parent();
    var imageURL = $(this).data("image-url");
    var message = $(this).data("message");
    var tripData = $(this).data("trip-data");
    var notificationLink = $(this).data("notification-link");
    var hasRead = $(this).data("has-read");
    var template;
    if(tripData) {
      if (tripData.waitlist_checkout_url) {
        var waitlistCheckoutMessage = 'Proceed To Checkout'
      }
      template = JST['views/notification/event']({
        imageURL: imageURL,
        message: message,
        tripName: tripData.trip_name,
        tripURL: tripData.trip_url,
        organizerAvatar: tripData.organizer_avatar,
        waitlistCheckoutURL: tripData.waitlist_checkout_url,
        waitlistCheckoutMessage: waitlistCheckoutMessage

      });
    }else{
      template = JST['views/notification/custom']({
        imageURL: imageURL,
        message: message
      });
    }
    $('#notificationModal').append(template);

    if(!hasRead){
      $.ajax({
        type: "PUT",
        url: notificationLink,
        success: function() {
          $(container).removeClass("unread")
        },
        error: function(xhr) {
          var data = xhr.responseJSON;
          popupNotification("alert", data.message);
        }
      });
    }
  });

  $(document).on('closed.zf.reveal', '#notificationModal', function() {
    $('.notification-modal__content').remove();
  })
});
