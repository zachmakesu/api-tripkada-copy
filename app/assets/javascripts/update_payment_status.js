$(function(){
  $("[data-payment-url]").on("change", function(){
    var status = this.value;
    var payment_url = this.getAttribute('data-payment-url');
    $.ajax({
        type: 'PUT',
        url: payment_url,
        data: {payment: { status: status }},
        success: function() {
          popupNotification("success", "Payment was successfully updated");
        }
    });
  });
});
