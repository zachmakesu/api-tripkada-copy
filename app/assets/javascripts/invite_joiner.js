$(function() {
  var wrapper = $('.joiner-input-wrapper');
  var form = $('#addJoinerForm');
  var downpaymentRateWithFee = $(form).data('downpayment-with-fee');
  var rateWithFee = $(form).data('rate-with-fee');
  var name_arr = [];
  var email_arr = [];

  function updateInvitedJoinerCount() {
    var joinerCount = $(wrapper).children('div').length;
    //update invited joiner count in DOM
    $('[data-joiner-count]').html(joinerCount);
    if ($('.joiner-count-right').length == 0) {
      $('.add-joiner-wrapper').append('<p class="joiner-count-right">X' + joinerCount + '</p>');
    } else {
      $('.joiner-count-right').html('X' + joinerCount);
    }
  }

  function resetInvitedJoinerCount() {
    $('[data-joiner-count]').html('');
    $('.joiner-count-right').html('');
    $('.add-joiner-modal').append('<span class="ion-plus"><span>');
  }

  function updateRateAndDownpayment(rate, downpayment) {
    var downpaymentContainer = $('#requiredDownpayment');
    var rateContainer = $('#totalRate');
    $(downpaymentContainer).html('P ' + downpayment);
    if (sessionStorage.promoCodeValue) {
      total = rate - sessionStorage.promoCodeValue;
      totalDownpayment = downpayment - sessionStorage.promoCodeValue;
      $(rateContainer).html('P ' + total);
      if (rate == downpayment) { $(downpaymentContainer).html('P ' + totalDownpayment)}
      freeRateAndDownpaymentEligibility(sessionStorage.promoCodeValue, rate, downpayment);
    } else {
      $(rateContainer).html('P ' + rate);
    }
  }

  function calculate(amount, memberCount) {
    //parse values
    var value = parseFloat(amount);
    //calculate sub total and total downpayment
    var total = value * memberCount;
    return parseFloat(total).toFixed(2);
  }

  function serializeFormData(form) {
    //serialize data
    var formValues = form.serializeArray();
    var data = {};
    //transaform into a hash
    for (i in formValues) {
      data[formValues[i]['name']] = formValues[i]['value'];
    }
    //remove unnecessary key valu pairs
    delete data['utf8'];
    delete data['authenticity_token'];
    name_arr.length = 0;
    email_arr.length = 0;
    groupBy(data);
  }

  function toArrayOfHashes(data) {
    var arr = [];
    $.each(data, function(i) {
      var new_data = {};
      split = i.split('_');
      key = split[0];
      d = new_data[key] = data[i];
      arr.push(new_data);
    });
    return arr;
  }

  function groupBy(data) {
    var arr = toArrayOfHashes(data);
    var val = {};
    arr.forEach(function(obj) {
      for (key in obj) {
        if (val[key] === undefined) val[key] = [];
        val[key].push(obj[key]);
      }
    });

    name_arr.push(val['name']);
    email_arr.push(val['email']);

    sessionStorage.names = name_arr;
    sessionStorage.emails = email_arr;
  }

  $('[data-add-field]').on('click', function(e) {
    fieldCount = $(wrapper).children('div').length;
    fieldCount++;
    template = JST['views/event/dynamic_invite_joiner_fields'](fieldCount);
    $(wrapper).append(template);
    $('.submit').focus();
    $('*#removeFields').on('click', function(e) {
      e.stopPropagation();
      $(this)
        .parent()
        .parent()
        .remove();
      count = 0;
      $(wrapper)
        .children('div')
        .each(function() {
          count++;
          $(this)
            .children('div:first')
            .children('p:first')
            .html('Joiner' + ' ' + count);
        });

      inputCount = $(wrapper).children('div').length + 1;
      var totalDownpayment = calculate(downpaymentRateWithFee, inputCount);
      var totalRate = calculate(rateWithFee, inputCount);

      updateRateAndDownpayment(totalRate, totalDownpayment);
      updateInvitedJoinerCount();
      sessionStorage.rateWithInvitedJoiners = totalRate;
      sessionStorage.downpaymentRateWithInvitedJoiners = totalDownpayment;
    });
  });

  $('#addJoinerForm').on('submit', function(e) {
    e.preventDefault();
    $('#addJoinerModal').foundation('close');
    serializeFormData($(this));
    //count fields including user
    var count = $(wrapper).children('div').length + 1;
    //calculate downpayment and update DOM
    var totalDownpayment = calculate(downpaymentRateWithFee, count);
    var totalRate = calculate(rateWithFee, count);

    updateRateAndDownpayment(totalRate, totalDownpayment);
    updateInvitedJoinerCount();
    sessionStorage.rateWithInvitedJoiners = totalRate;
    sessionStorage.downpaymentRateWithInvitedJoiners = totalDownpayment;
  });

  $('#clearFields').on('click', function() {
    sessionStorage.removeItem('names');
    sessionStorage.removeItem('emails');
    $(form)[0].reset();
    $(wrapper)
      .children('div')
      .not(':first')
      .empty();

    updateRateAndDownpayment(rateWithFee, downpaymentRateWithFee);
    resetInvitedJoinerCount();
    sessionStorage.removeItem('rateWithInvitedJoiners');
    sessionStorage.removeItem('downpaymentRateWithInvitedJoiners');
  });

  $(document).on('keydown','*.input-container :input', function(e){
    /* prevent input of semi-colon(186) */
    /* https://css-tricks.com/snippets/javascript/javascript-keycodes/ */
    if (e.which == 186) { e.preventDefault() }
  })
});
