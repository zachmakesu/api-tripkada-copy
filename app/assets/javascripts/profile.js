function profilePhotoReadURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      var parent = $(input).parent();
      parent.addClass('has-image');
      parent.css("background-image", "url('"+e.target.result+"')");
      parent.attr('onClick', 'change_profile_photo_modal(this)');
      $(input).parent().find('#profile_photo_label' + $(input).index()).val('')
      $.when($(input).parent().find($("input[type='text'][name='user[profile_photos][][photo]']")).remove()).then(
        $(input).append("<input type='text' name='user[profile_photos][][edit]' id='editProfilePhoto" + index + "' value='" + index + "'>")
      );
    }
    reader.readAsDataURL(input.files[0]);
  }
}

function changeProfilePhotoModal(input) {
  var modal = $('#profilePhotoModal');
  var index = $(input).index()
  var label = $(input).parent().find('#profile_photo_label' + index);
  var modalLabel = modal.find('label');
  var modalDetails = modal.find('.edit-details');
  var modalInput = modalDetails.find('input');

  modalLabel.text(label.val());
  modal.find('img').attr('src', $(input).css('background-image').slice(5, -2));
  modal.foundation('open');

  $('p.ion-image').unbind("click").on('click', function() {
    var photoId = $(input).find($("input[type='text'][name='user[profile_photos][][photo]']")).val();
    $.when($(input).parent().find($("input[name='user[profile_photos][][cover]']")).remove()).then(
      $(input).append("<input type='text' name='user[profile_photos][][cover]' id='cover' value='" + photoId + "'>")
    );
    var photo = $(input).css("background-image");
    var cover = $(document).find('.cover-photo.edit');
    var coverLabel = $(cover).find('p.cover-photo-label');
    var coverLabelTemp = coverLabel.text();
    $(input).css("background-image", cover.css("background-image"));
    cover.css("background-image", photo)
    coverLabel.text(label.val());
    label.val(coverLabelTemp);
    modal.foundation('close');
  });

  $('p.ion-trash-b').off().on('click', function(){
    var photoId = $(input).find($("input[type='text'][name='user[profile_photos][][photo]']")).val();
    $(input).append("<input type='text' name='user[profile_photos][][delete]' id='deleteProfilePhoto" + index + "' value='" + photoId + "'>")
    $(input).find($("input[name='user[profile_photos][][photo]']")).each(function() { $(this).val(''); });
    $(input).removeClass('has-image');
    $(input).css("background-image", "");
    $(input).attr('onClick', '');
    modal.foundation('close');
  });

  $('p.ion-edit').off().on('click', function() {
    $(input).append("<input type='text' name='user[profile_photos][][edit]' id='editProfilePhoto" + index + "' value='" + index + "'>")
    modalLabel.toggle();
    modalDetails.toggle();
    modalInput.val('');
    modalDetails.find('#edit_save').off().on('click', function(){
      $('#more-dropdown').foundation('close');
      modalLabel.toggle();
      modalDetails.toggle();
      modalLabel.text(modalInput.val());
      label.val(modalLabel.text());
    });
    modalDetails.find('#edit_clear').off().on('click', function(){
      modalInput.val('');
    });
  });

  $(modal).on('closed.zf.reveal', function(){
    $('#more-dropdown').foundation('close');
    modalLabel.show();
    modalDetails.hide();
  });
}

function showProfilePhotoModal(image, label) {
  var modal = $('#profilePhotoModal');
  modal.find('label').text(label);
  modal.find('img').attr('src', image);
  modal.foundation('open');
}
