# == Schema Information
#
# Table name: review_blacklists
#
#  created_at :datetime         not null
#  event_id   :integer          not null
#  id         :integer          not null, primary key
#  updated_at :datetime         not null
#  user_id    :integer          not null
#
# Indexes
#
#  index_review_blacklists_on_event_id  (event_id)
#  index_review_blacklists_on_user_id   (user_id)
#

class ReviewBlacklist < ActiveRecord::Base
  belongs_to :user
  belongs_to :event

  validates :user_id, :event_id, presence: true
  validates :event_id, uniqueness: { scope: :user_id }
end
