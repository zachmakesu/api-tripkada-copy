# == Schema Information
#
# Table name: itineraries
#
#  arrival     :time
#  created_at  :datetime         not null
#  description :text
#  event_id    :integer
#  id          :integer          not null, primary key
#  name        :string
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_itineraries_on_event_id  (event_id)
#
# Foreign Keys
#
#  fk_rails_7dda9619a9  (event_id => events.id)
#

class Itinerary < ActiveRecord::Base
  belongs_to :event
end
