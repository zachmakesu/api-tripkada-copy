# == Schema Information
#
# Table name: events
#
#  created_at         :datetime         not null
#  deleted_at         :datetime
#  description        :text
#  downpayment_rate   :float            default(0.0)
#  duplicated_from_id :integer
#  end_at             :datetime
#  featured_at        :datetime
#  highlights         :text
#  id                 :integer          not null, primary key
#  inclusions         :text
#  job_ids            :text             default(""), not null
#  lat                :float
#  lng                :float
#  max_pax            :integer
#  meeting_place      :text
#  min_pax            :integer
#  name               :string
#  number_of_days     :integer
#  number_of_views    :integer          default(0)
#  published_at       :datetime
#  rate               :decimal(, )
#  slug               :string
#  start_at           :datetime
#  tags               :text
#  things_to_bring    :text
#  trip_cancel_reason :string
#  updated_at         :datetime         not null
#  user_id            :integer
#
# Indexes
#
#  index_events_on_published_at  (published_at)
#  index_events_on_user_id       (user_id)
#
# Foreign Keys
#
#  fk_rails_0cb5590091  (user_id => users.id)
#

class Event < ActiveRecord::Base
  MINCOST = 0
  MAXCOST = 10000
  MINDAYS = 0
  MAXDAYS = 10
  EVENT_SCHEME = { :upcoming => "upcoming", :previous => "previous", :bookmarks => "bookmarks"}
  TYPE = Struct.new(:type) do
    def params_type_not_present?
      type.nil?
    end

    def invalid_params_type?
      Event::EVENT_SCHEME[:"#{type}"].nil?
    end

    def upcoming?
      type == "upcoming"
    end

    def previous?
      type == "previous"
    end

    def bookmarks?
      type == "bookmarks"
    end
  end

  extend FriendlyId
  friendly_id :name, use: :slugged
  geocoded_by :meeting_place, latitude: :lat, longitude: :lng
  after_validation :geocode, if: ->(obj){ obj.lat.nil? || obj.lng.nil? }

  scope :order_by_start, -> { order(:start_at) }
  scope :sorted_event, -> { order(start_at: :desc) }
  scope :order_by_rate, -> { order(:rate) }
  scope :order_by_rate_high, -> { order(rate: :desc) }
  scope :published, -> { where.not(published_at: nil) }
  scope :unpublished, -> { where(published_at: nil) }
  scope :filtered_search, -> (search){ where('events.name ILIKE :search', search: "%#{search}%")}
  scope :this_month, -> { where('extract(month from start_at) = ? AND extract(year from start_at) = ?', Date.today.month, Date.today.year) }
  scope :from_months, ->(start_month, end_month) { where(start_at: start_month..end_month) }
  scope :from_date, ->(start_date, end_date) { where(start_at: start_date..end_date) }
  scope :cost, ->(min,max) { where('(rate + :booking_fee) >= :min AND (rate + :booking_fee) <= :max', { min: min, max: max, booking_fee: BookingFeeDecorator.latest_fee } ) }
  scope :days, ->(min,max) { where('number_of_days >= ? AND number_of_days <= ?', min, max) }
  scope :activity, ->(category_ids) { joins(:categories).where('categorizations.category_id IN (?)', category_ids) }
  scope :common_places, ->(destination_ids) { joins(:destinations).where('sorting_destinations.destination_id IN (?)', destination_ids) }

  scope :first_upcoming_event, -> { upcoming.order_by_start.first }
  scope :last_upcoming_event, -> { upcoming.sorted_event.first }
  scope :first_past_event, -> { past.order_by_start.first }
  scope :last_past_event, -> { past.sorted_event.first }

  scope :featured, -> { where.not(featured_at: nil) }

  scope :order_by_views, -> { order('(number_of_views) desc') } # order views except nil

  belongs_to :owner, class_name: "User", foreign_key: :user_id

  has_many :categorizations, foreign_key: :event_id, dependent: :destroy
  has_many :categories, :through => :categorizations

  has_many :sorting_destinations, foreign_key: :event_id, dependent: :destroy
  has_many :destinations, :through => :sorting_destinations

  has_many :duplicate_events, class_name: "Event", foreign_key: :duplicated_from_id
  belongs_to :duplicated_from_event, class_name: "Event", foreign_key: :duplicated_from_id

  has_many :event_memberships, dependent: :destroy
  has_many :members, through: :event_memberships

  has_many :pending_event_memberships,-> { pending }, class_name: "EventMembership", dependent: :destroy
  has_many :pending_members, -> { pending_members_payment }, through: :pending_event_memberships, source: :member

  has_many :approved_event_memberships,-> { approved }, class_name: "EventMembership", dependent: :destroy
  has_many :approved_members, -> { approved_members_payment }, through: :approved_event_memberships, source: :member

  has_many :declined_event_memberships,-> { declined }, class_name: "EventMembership", dependent: :destroy
  has_many :declined_members, -> { declined_members_payment }, through: :declined_event_memberships, source: :member

  has_many :expired_event_memberships, -> { expired }, class_name: "EventMembership", dependent: :destroy
  has_many :expired_members, -> { expired_members_payment }, through: :expired_event_memberships, source: :member

  has_many :itineraries, -> { order(id: :ASC) }, dependent: :destroy
  has_many :photos, as: :imageable, dependent: :destroy
  has_one  :cover_photo, -> { cover_photo }, as: :imageable, class_name: "Photo"

  has_many :received_reviews, class_name: "Review", as: :reviewable, dependent: :destroy
  has_many :reviewers, through: :received_reviews

  has_many :bookmarks, dependent: :destroy

  has_many :waitlists, dependent: :destroy
  has_many :waitlist_members, through: :waitlists, source: :user

  has_many :sponsors, dependent: :destroy

  has_many :notifications, as: :notificationable
  validates :name, :start_at, :end_at, :min_pax, :max_pax, :rate, :meeting_place, presence: true, if: Proc.new{ |event| event.published_at.present? }
  validates :min_pax, :max_pax, numericality: {
    greater_than: 0,
    only_integer: true
  }
  validates :min_pax, numericality: { less_than_or_equal_to: :max_pax }
  validates :max_pax, numericality: { greater_than_or_equal_to: Proc.new { |obj| obj.approved_event_memberships.count }}
  validates :rate, numericality: { greater_than_or_equal_to: :downpayment_rate }
  validate :ensure_valid_job_ids

  before_validation do
    if self.start_at.present? && self.end_at.present? && self.published_at.present?
      self.errors.add(:base, "Invalid date range.") if self.start_at < DateTime.now || self.start_at > self.end_at
    end
  end

  before_save do
    self.number_of_days = (self.end_at.to_date - self.start_at.to_date).to_i if self.published_at.present?
  end

  def self.upcoming(published: true)
    default = where("start_at > ?", DateTime.now)
    return default unless published
    default.where.not(published_at: nil)
  end

  def self.past(published: true)
    default = where("start_at < ?", DateTime.now)
    return default unless published
    default.where.not(published_at: nil)
  end

  def self.not_deleted(published: true)
    default = where(deleted_at: nil)
    return default unless published
    default.where.not(published_at: nil)
  end

  def self.past_not_deleted(published: true)
    default = where("start_at < ? AND deleted_at IS null", DateTime.now)
    return default unless published
    default.where.not(published_at: nil).where(deleted_at: nil)
  end

  def self.pending_payments
    not_deleted.includes(event_memberships: :payment).where(payments: { status: Payment.statuses[:pending] })
  end

  def self.approved_payments
    not_deleted.includes(event_memberships: :payment).where(payments: { status: Payment.statuses[:approved] })
  end

  def self.declined_payments
    not_deleted.includes(event_memberships: :payment).where(payments: { status: Payment.statuses[:declined] })
  end

  def self.expired_payments
    not_deleted.includes(event_memberships: :payment).where(payments: { status: Payment.statuses[:expired] })
  end

  def self.past_approved_payments
    not_deleted.past.includes(event_memberships: :payment).where(payments: { status: Payment.statuses[:approved] })
  end

  def self.search(query: nil)
    query = "%#{query}%"
    where("name ILIKE ?", query).order(:start_at)
  end

  def soft_delete
    self.update(deleted_at: DateTime.now)
  end

  def has_expired_membership_for?(user)
    expired_event_memberships.where(member_id: user.id).exists?
  end

  def joinable_by?(user)
    !(members.include?(user) || owner == user)
  end

  def full?
    waitlists_and_joiners_count >= self.max_pax
  end

  def waitlists_and_joiners_count
    self.waitlists.approved.count + self.waitlists.pending.count + self.approved_event_memberships.count
  end

  def full_and_greater_than_max_pax?
    self.approved_members.count + self.waitlists.approved.count > self.max_pax
  end

  def full_with_approved_waitlist?
    self.approved_members.count + self.waitlists.approved.count == self.max_pax
  end

  def is_upcoming?
    start_at > DateTime.now
  end

  def is_past?
    start_at <  DateTime.now
  end

  def min_pax_attain?
    members.count >= min_pax
  end

  def is_ongoing?
    DateTime.now.between?(start_at, end_at)
  end

  def is_canceled?
    deleted_at.present?
  end

  def happening_on_same_day?
    self.start_at.to_date == self.end_at.to_date
  end

  def happening_on_same_month?
    self.start_at.strftime("%Y%m") == self.end_at.strftime("%Y%m")
  end

  def featured?
    !self.featured_at.nil?
  end

  def feature!
    self.update_attribute("featured_at", DateTime.current)
  end

  def unfeature!
    self.update_attribute("featured_at", nil)
  end

  def photos_not_exists?(photo_ids:)
    self.photos.where(id: photo_ids).empty?
  end

  def zero_waitlist?
    self.waitlists.count.zero?
  end

  def waitlist_and_joiners_less_than_max_pax?
    self.waitlists_and_joiners_count < self.max_pax.to_i
  end

  def organized_by?(user:)
    self.owner == user
  end

  def waitlisted?(user)
    (!self.members.include?(user)) && self.waitlist_members.include?(user)
  end

  def base_max_pax
    members_count = self.approved_event_memberships.count + self.pending_event_memberships.count
    members_count > self.min_pax ? members_count : self.min_pax
  end

  private

  def should_generate_new_friendly_id?
    name_changed?
  end

  def ensure_valid_job_ids
    self.errors.add(:job_ids, "cannot be nil") if self.job_ids.nil?
  end

end
