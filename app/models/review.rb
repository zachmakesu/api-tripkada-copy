# == Schema Information
#
# Table name: reviews
#
#  created_at      :datetime         not null
#  id              :integer          not null, primary key
#  message         :text
#  platform        :string
#  rating          :float
#  reviewable_id   :integer
#  reviewable_type :string
#  reviewer_id     :integer
#  reviewer_type   :integer
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_reviews_on_reviewer_id  (reviewer_id)
#

class Review < ActiveRecord::Base
  REVIEWABLE_TYPES = %w{ Event User }

  belongs_to :reviewer, class_name: "User"
  belongs_to :reviewable, polymorphic: true

  scope :user, -> { where reviewable_type: 'User' }
  scope :event, -> { where reviewable_type: 'Event' }
  scope :desc_order, -> { order(created_at: :desc) }

  enum reviewer_type: { as_joiner: 0, as_organizer: 1 }

  validates :reviewer, presence: true
  validates :reviewer, uniqueness: { scope: [:reviewable_id, :reviewable_type], if: :for_event? }
  validates :rating, inclusion: (0.0..5.0)

  before_validation do
    self.errors.add(:base, "Reviewer is not a member of this trip") if self.for_event? && !self.reviewable.members.include?(reviewer)
    self.errors.add(:base, "Event is ongoing and cannot be reviewed yet") if self.for_event?  && self.reviewable.decorate.is_upcoming?

    if self.reviewer_type == "as_joiner"                                              #this refers to the reviewer as_joiner.
      joiners = EventMembershipDecorator.organizer_all_valid_joiners(self.reviewable) #get all valid joiners in organizer's past events
      review_count = joiners.where(member_id: self.reviewer.id).count                 #check if the reviewer is a valid joiner.
      self.errors.add(:base, "You need to join at least 1 trip to this organizer before you can submit a review") if review_count == 0
      self.errors.add(:base, "You already reviewed this organizer") if self.reviewable.reviewers.where(id: self.reviewer.id).count >= review_count && review_count > 0
      self.errors.add(:base, "You can't review yourself, review other organizers instead") if self.reviewer.id == self.reviewable_id

    elsif self.reviewer_type == "as_organizer"                                        #this refers to the reviewer as_joiner.
      joiners = EventMembershipDecorator.organizer_all_valid_joiners(self.reviewer)   #get all valid joiners in organizer's past events
      review_count = joiners.where(member_id: self.reviewable_id).count               #check if the reviewable is a valid joiner.
      self.errors.add(:base, "This joiner hasn't joined a trip from you yet") if review_count == 0
      self.errors.add(:base, "You already reviewed this joiner") if self.reviewer.sent_reviews.where(reviewable_id: self.reviewable_id).count >= review_count && review_count > 0
      self.errors.add(:base, "You can't review yourself, review other joiner instead") if self.reviewer.id == self.reviewable_id
    else
      #do nothing
    end
  end

  REVIEWABLE_TYPES.map(&:downcase).each do |type|
    define_method "for_#{type}?".to_sym do
      reviewable_type == type.titleize
    end
  end

end
