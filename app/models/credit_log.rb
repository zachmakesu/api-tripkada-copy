# == Schema Information
#
# Table name: credit_logs
#
#  amount        :decimal(, )      not null
#  category      :integer          default(0)
#  created_at    :datetime         not null
#  id            :integer          not null, primary key
#  payment_id    :integer
#  promo_code_id :integer          not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_credit_logs_on_category  (category)
#

class CreditLog < ActiveRecord::Base
  enum category: { deduction: 0, earned_incentive: 1}
  validates :promo_code_id, :amount, presence: true

  belongs_to :promo_code
  belongs_to :payment
end
