# == Schema Information
#
# Table name: booking_fees
#
#  created_at :datetime         not null
#  created_by :string
#  id         :integer          not null, primary key
#  updated_at :datetime         not null
#  value      :float            default(0.0)
#

class BookingFee < ActiveRecord::Base
  scope :search, -> (query) { where("created_by ILIKE :query", query: "%#{query}%")}
  validates :value, :created_by, presence: true
  belongs_to :payment
end
