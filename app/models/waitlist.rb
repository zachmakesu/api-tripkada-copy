# == Schema Information
#
# Table name: waitlists
#
#  created_at :datetime         not null
#  event_id   :integer
#  id         :integer          not null, primary key
#  status     :integer          default(0), not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_waitlists_on_event_id  (event_id)
#  index_waitlists_on_user_id   (user_id)
#
# Foreign Keys
#
#  fk_rails_0e25b61968  (event_id => events.id)
#  fk_rails_0ed579a160  (user_id => users.id)
#

class Waitlist < ActiveRecord::Base
  belongs_to :user
  belongs_to :event

  enum status: { pending: 0, denied: 1, approved: 2 , paid: 3 }
  validates :user, uniqueness: { scope: :event, message: "is already on the waitlist."}, presence: true
  validates :event, presence: true
end
