# == Schema Information
#
# Table name: event_tags
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string
#  tag_type   :integer
#  updated_at :datetime         not null
#

class EventTag < ActiveRecord::Base
  enum tag_type: { tags: 0, highlights: 1, inclusions: 2, things: 3 }
  validates :name, :tag_type, presence: true
end
