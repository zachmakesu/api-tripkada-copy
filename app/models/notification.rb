# == Schema Information
#
# Table name: notifications
#
#  category              :integer          default(0), not null
#  created_at            :datetime         not null
#  deleted_at            :datetime
#  id                    :integer          not null, primary key
#  message               :text
#  notificationable_id   :integer
#  notificationable_type :string
#  read                  :datetime
#  seen                  :boolean          default(FALSE), not null
#  updated_at            :datetime         not null
#  user_id               :integer
#

class Notification < ActiveRecord::Base
  REMINDERS = [1, 2, 3, 4, 5, 6, 8, 12, 13].freeze

  belongs_to :user
  belongs_to :notificationable, polymorphic: true
  scope :not_deleted, -> { where(deleted_at: nil) }
  scope :order_by_created_at, -> { order(created_at: :desc) }
  scope :order_by_seen, -> { order('(case when seen then 1 else 0 end)') }
  scope :order_by_read, -> { order(read: :desc) }
  scope :not_seen, -> { where(seen: false) }
  scope :unread, -> { where(deleted_at: nil, read: nil)}

  enum category: { uncategorized: 0, trip_join: 1, slot_reminder: 2,
                   payment_reminder: 3, trip_reminder: 4, follower_reminder: 5,
                   trip_review_reminder: 6, revised_terms_and_condition: 7,
                   trip_date_request: 8, referral_incentive: 9,
                   custom: 10, trip_views_notification: 11,
                   waitlist_status: 12, bookmarked_event_reminder: 13 }

  after_commit { Pusher::NotificationHandler.new(self.user.id).deliver }

  def see
    self.update(seen: true)
  end

  def unsee
    self.update(seen: false)
  end

  def read!
    self.update(read: DateTime.now)
  end

  def category_key
    self.read_attribute(:category)
  end
end
