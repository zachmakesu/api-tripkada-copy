# == Schema Information
#
# Table name: promo_codes
#
#  active      :boolean          default(TRUE)
#  category    :integer          default(0), not null
#  code        :string
#  created_at  :datetime         not null
#  end_at      :datetime
#  id          :integer          not null, primary key
#  start_at    :datetime
#  updated_at  :datetime         not null
#  usage_limit :integer
#  user_id     :integer
#  value       :float
#
# Indexes
#
#  index_promo_codes_on_user_id  (user_id)
#

class PromoCode < ActiveRecord::Base
  enum category: { corporate: 0, credit: 1, private_use: 2 }

  validates :code, uniqueness: true
  validates :code, :value, presence: true
  validates :usage_limit, presence: true, unless: Proc.new { |code| code.credit? }
  validates :user_id, presence: true, unless: Proc.new { |code| code.corporate? }
  validate on: :create do
    check_existing_credit_promo_code if self.user && self.credit?
  end

  belongs_to :user
  has_many :payments
  has_many :event_memberships, through: :payments
  has_many :credit_logs, dependent: :destroy

  def self.find_by_code(code)
    self.find_by("active = true and lower(code) = ?", code.downcase)
  end

  def self.generate_unique_code
    loop do
      random_code = SecureRandom.hex(3)
      shuffle_code = random_code.chars.shuffle!.join('')
      break shuffle_code unless self.exists?(code: shuffle_code)
    end
  end

  def available_credits
    self.credit_logs.earned_incentive.sum(:amount) - self.credit_logs.deduction.sum(:amount)
  end
  
  def usable?
    self.usage_limit > self.payments.count
  end

  def belongs_to_owner?(user:)
    self.user == user
  end

  private

  def check_existing_credit_promo_code
    self.errors.add(:base, "Credit Promo Code Already Exist") if self.user.promo_codes.credit.exists?
  end

end
