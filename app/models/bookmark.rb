# == Schema Information
#
# Table name: bookmarks
#
#  created_at :datetime         not null
#  event_id   :integer
#  id         :integer          not null, primary key
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_bookmarks_on_event_id  (event_id)
#  index_bookmarks_on_user_id   (user_id)
#
# Foreign Keys
#
#  fk_rails_0dcdde8d06  (event_id => events.id)
#  fk_rails_c1ff6fa4ac  (user_id => users.id)
#

class Bookmark < ActiveRecord::Base
  belongs_to :user
  belongs_to :event

  validates :event, uniqueness: { scope: :user, message: "is already in your bookmarks."}
end
