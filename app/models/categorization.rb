# == Schema Information
#
# Table name: categorizations
#
#  category_id :integer
#  created_at  :datetime         not null
#  event_id    :integer
#  id          :integer          not null, primary key
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_categorizations_on_category_id  (category_id)
#  index_categorizations_on_event_id     (event_id)
#

class Categorization < ActiveRecord::Base
  belongs_to :event
  belongs_to :category

  validates :category_id, :event_id, presence: true
  validates :event_id, uniqueness: { scope: :category_id }
end
