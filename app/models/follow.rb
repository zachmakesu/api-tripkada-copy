# == Schema Information
#
# Table name: follows
#
#  created_at  :datetime         not null
#  followed_id :integer          not null
#  follower_id :integer          not null
#  id          :integer          not null, primary key
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_follows_on_followed_id                  (followed_id)
#  index_follows_on_follower_id                  (follower_id)
#  index_follows_on_follower_id_and_followed_id  (follower_id,followed_id) UNIQUE
#

class Follow < ActiveRecord::Base
  belongs_to :follower, class_name: "User", foreign_key: "follower_id"
  belongs_to :followed, class_name: "User", foreign_key: "followed_id"

  validates :follower_id, presence: true
  validates :followed_id, presence: true

  validates :followed_id, uniqueness: { scope: :follower_id, message: "user already followed." }

  before_validation do
    self.errors.add(:base, "User can't be followed.") if self.followed_id == self.follower_id
  end
end
