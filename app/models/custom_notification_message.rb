# == Schema Information
#
# Table name: custom_notification_messages
#
#  created_at         :datetime         not null
#  created_by         :string
#  id                 :integer          not null, primary key
#  image_content_type :string
#  image_file_name    :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  message            :text
#  updated_at         :datetime         not null
#

class CustomNotificationMessage < ActiveRecord::Base
  has_many :notifications, as: :notificationable
  has_attached_file :image, styles: { xxlarge: "2000x2000>", xlarge: "1000x1000>" , large: "800x800>", medium: "300x300>", thumb: "100x100>" }, default_url: "/assets/notification-logo.png"
  validates_attachment_content_type :image,:content_type => /^image\/(jpg|jpeg|png)$/, :message => 'not allowed.'
  validates_with AttachmentSizeValidator, attributes: :image, less_than: 10.megabytes
  validates :message, :created_by, presence: true
end
