# == Schema Information
#
# Table name: dragonpay_transaction_logs
#
#  created_at       :datetime         not null
#  id               :integer          not null, primary key
#  message          :string           not null
#  payment_id       :integer
#  reference_number :string           not null
#  response         :text             not null
#  status           :string           not null
#  transaction_id   :string           not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_dragonpay_transaction_logs_on_payment_id  (payment_id)
#

class DragonpayTransactionLog < ActiveRecord::Base
  STATUSES = %w{ approved pending declined }
  TWO_DAY_DEADLINE = %w{ BAYD 711 ACHD AUB BDOA BDOX BDRX BPIX BPXB
  CBCX CEBP CEBL CSHU ECPY EWBX LBC LBXB MBTX MLH PNBR PNXB RCXB RSB RSBB RSXB
  RDS SKYF SBCA SBCB SMR UBXB UBP UCXB VNTJ
  }.freeze
  ONE_HOUR_DEADLINE = %w{ BDO BITC BPI BPIB CBC DPAY LBPA MBTC PNBB
  PSB RCBC UCPB UBP UBE
  }.freeze
  FIVE_MINUTES = %w{ GCSH }.freeze
  serialize :response

  belongs_to :payment

  validates :reference_number, :status, :transaction_id,
    :message, :response, :payment_id, presence: true
  validates :status, inclusion: { in: STATUSES , message: "%{value} is not a valid status" }

  def self.generate_transaction_id
    loop do
      random_code = SecureRandom.hex(6)
      shuffle_code = random_code.chars.shuffle!.join('')
      break shuffle_code unless self.exists?(transaction_id: shuffle_code)
    end
  end

  def self.mark_expired_payments!
    pending = Payment.dragonpay.pending.where('created_at BETWEEN ? AND ?',
      10.days.ago, Time.zone.now)
    check_for_expired(payments: pending)
  end

  def self.check_for_expired(payments:)
    payments.find_each do |pay|
      payment_channel = pay.dragonpay_transaction_logs.last.response["procid"]
      if TWO_DAY_DEADLINE.include?(payment_channel)
        pay.expired! if pay.created?(2.days.ago)
      elsif ONE_HOUR_DEADLINE.include?(payment_channel)
        pay.expired! if pay.created?(60.minutes.ago)
      elsif FIVE_MINUTES.include?(payment_channel)
        pay.expired! if pay.created?(5.minutes.ago)
      end
    end
  end
end
