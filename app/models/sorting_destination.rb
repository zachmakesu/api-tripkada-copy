# == Schema Information
#
# Table name: sorting_destinations
#
#  created_at     :datetime         not null
#  destination_id :integer
#  event_id       :integer
#  id             :integer          not null, primary key
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_sorting_destinations_on_destination_id  (destination_id)
#  index_sorting_destinations_on_event_id        (event_id)
#

class SortingDestination < ActiveRecord::Base
  belongs_to :event
  belongs_to :destination

  validates :destination_id, :event_id, presence: true
  validates :event_id, uniqueness: { scope: :destination_id }
end
