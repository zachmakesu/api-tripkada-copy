# == Schema Information
#
# Table name: invitations
#
#  created_at          :datetime         not null
#  event_membership_id :integer
#  id                  :integer          not null, primary key
#  invitee_id          :integer          not null
#  updated_at          :datetime         not null
#  user_id             :integer          not null
#
# Indexes
#
#  index_invitations_on_event_membership_id  (event_membership_id)
#  index_invitations_on_invitee_id           (invitee_id)
#  index_invitations_on_user_id              (user_id)
#
# Foreign Keys
#
#  fk_rails_7eae413fe6  (user_id => users.id)
#

class Invitation < ActiveRecord::Base

  belongs_to :inviter, class_name: "User", foreign_key: "user_id"
  belongs_to :invitee, class_name: "User", foreign_key: "invitee_id"
  belongs_to :event_membership

  validates :event_membership_id, :invitee_id, :user_id, presence: true
end
