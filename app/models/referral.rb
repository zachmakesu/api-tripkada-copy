# == Schema Information
#
# Table name: referrals
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  joiner_id  :integer          not null
#  updated_at :datetime         not null
#  user_id    :integer          not null
#
# Indexes
#
#  index_referrals_on_joiner_id  (joiner_id)
#  index_referrals_on_user_id    (user_id)
#
# Foreign Keys
#
#  fk_rails_1bb2ba0c15  (user_id => users.id)
#

class Referral < ActiveRecord::Base
  belongs_to :referrer, class_name: "User", foreign_key: "user_id"
  belongs_to :referred_joiner, class_name: "User", foreign_key: "joiner_id"
end
