# == Schema Information
#
# Table name: destinations
#
#  active     :boolean          default(TRUE)
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string           not null
#  updated_at :datetime         not null
#

class Destination < ActiveRecord::Base
   has_many :sorting_destinations, foreign_key: :destination_id, dependent: :destroy
   has_many :events, through: :sorting_destinations

   validates :name, presence: true
end
