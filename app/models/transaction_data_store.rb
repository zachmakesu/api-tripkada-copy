# == Schema Information
#
# Table name: transaction_data_stores
#
#  created_at     :datetime         not null
#  data           :text
#  id             :integer          not null, primary key
#  transaction_id :string
#  updated_at     :datetime         not null
#

class TransactionDataStore < ActiveRecord::Base
  serialize :data
  validates :transaction_id, presence: true, uniqueness: true
end
