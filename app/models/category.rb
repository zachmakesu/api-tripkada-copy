# == Schema Information
#
# Table name: categories
#
#  active     :boolean          default(TRUE)
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string           not null
#  updated_at :datetime         not null
#

class Category < ActiveRecord::Base
  has_many :categorizations, foreign_key: :category_id, dependent: :destroy
  has_many :events, through: :categorizations

  validates :name, presence: true
end
