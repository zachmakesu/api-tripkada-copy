# == Schema Information
#
# Table name: users
#
#  address                  :string
#  applied_as_organizer_at  :datetime
#  approved_at              :datetime
#  approved_organizer_at    :datetime
#  birthdate                :date
#  calendar_sync            :boolean          default(FALSE)
#  city                     :string
#  country                  :string
#  created_at               :datetime         not null
#  current_sign_in_at       :datetime
#  current_sign_in_ip       :inet
#  email                    :string           default(""), not null
#  emergency_contact_name   :string
#  emergency_contact_number :string
#  encrypted_password       :string           default(""), not null
#  first_name               :string
#  gender                   :integer          default(0)
#  hobbies                  :text
#  id                       :integer          not null, primary key
#  image_url                :string
#  last_name                :string
#  last_sign_in_at          :datetime
#  last_sign_in_ip          :inet
#  mobile                   :string
#  old_birthdate            :string
#  rating                   :string
#  registration_platform    :integer
#  remember_created_at      :datetime
#  reset_password_sent_at   :datetime
#  reset_password_token     :string
#  role                     :integer          default(0)
#  sign_in_count            :integer          default(0), not null
#  slug                     :string
#  state                    :string
#  tell_us_about_yourself   :string
#  uid                      :string
#  updated_at               :datetime         not null
#  website                  :string
#
# Indexes
#
#  index_users_on_calendar_sync         (calendar_sync)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class User < ActiveRecord::Base
  include UID

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  scope :approved, -> { where.not(approved_at: nil) }
  scope :pending,  -> { where(approved_at: nil) }
  scope :all_except, ->(owner){ where.not(id:  owner) }
  scope :organizer_applicants, -> { where.not(applied_as_organizer_at:  nil).where(role: "joiner") }
  scope :sorted_organizer_applicants, -> { order(applied_as_organizer_at: :desc) }
  scope :valid_event_joiners, -> { joins(event_memberships: :payment).where.not(payments: {mode: Payment.modes[:organizer]}).uniq }

  extend FriendlyId
  friendly_id :full_name, use: :slugged
  alias_attribute :username, :slug

  devise :database_authenticatable, :rememberable, :trackable, :validatable, :omniauthable, :omniauth_providers => [:facebook]

  enum gender: { not_specified: 0, male: 1, female: 2 }
  enum role: { joiner: 0, joiner_and_organizer: 1, admin: 2 }
  enum registration_platform: { android_app: 0, android_web: 1, ios_app: 2,
                                ios_web: 3, desktop: 4 }

  has_one  :instagram_provider, -> { instagram }, class_name: "Provider"
  has_one  :facebook_provider, -> { facebook }, class_name: "Provider"

  has_one  :cover_photo, -> { cover_photo }, as: :imageable, class_name: "Photo"
  has_one  :profile_avatar, -> { profile_avatar }, as: :imageable, class_name: "Photo", dependent: :destroy
  has_many :profile_photos, -> { profile_photos }, as: :imageable, class_name: "Photo", dependent: :destroy
  has_many :government_photos, -> { government }, as: :imageable, class_name: "Photo", dependent: :destroy
  has_many :verification_photos, -> { verification }, as: :imageable, class_name: "Photo", dependent: :destroy
  has_many :certificates, -> { certificates }, as: :imageable, class_name: "Photo", dependent: :destroy
  has_many :photos, as: :imageable, dependent: :destroy

  has_many :api_keys, dependent: :destroy
  has_many :organized_events, -> { not_deleted }, class_name: "Event", dependent: :destroy
  has_many :organized_events_members, through: :organized_events, source: :members
  has_many :all_organized_events, class_name: "Event", dependent: :destroy

  has_many :event_memberships, foreign_key: :member_id, dependent: :destroy

  has_many :approved_event_joiner_payments, -> { approved_joiner }, through: :event_memberships, source: :payment
  has_many :approved_past_joined_events, ->{ past_not_deleted }, through: :approved_event_joiner_payments, source: :event

  has_many :joined_events, -> { not_deleted }, through: :event_memberships, source: :event

  has_many :pending_event_memberships, -> { pending }, class_name: "EventMembership", foreign_key: :member_id, dependent: :destroy
  has_many :pending_events, -> { pending_payments }, through: :pending_event_memberships, source: :event

  has_many :approved_event_memberships, -> { approved }, class_name: "EventMembership", foreign_key: :member_id, dependent: :destroy

  has_many :approved_joiner_event_memberships, -> { approved_joiner }, class_name: "EventMembership", foreign_key: :member_id, dependent: :destroy

  has_many :approved_events, -> { approved_payments }, through: :approved_event_memberships, source: :event

  has_many :declined_event_memberships, -> { declined }, class_name: "EventMembership", foreign_key: :member_id, dependent: :destroy
  has_many :declined_events, -> { declined_payments }, through: :declined_event_memberships, source: :event

  has_many :expired_event_memberships, -> { expired }, class_name: "EventMembership", foreign_key: :member_id, dependent: :destroy
  has_many :expired_events, -> { expired_payments }, through: :expired_event_memberships, source: :event

  has_many :providers, dependent: :destroy

  has_many :bookmarks, dependent: :destroy
  has_many :bookmarked_events, -> { not_deleted }, through: :bookmarks, source: :event

  has_many :waitlists, dependent: :destroy
  has_many :waitlist_events, -> { not_deleted }, through: :waitlists, source: :event

  #received reviews
  has_many :received_reviews, -> { desc_order }, class_name: "Review", as: :reviewable, dependent: :destroy
  has_many :reviewers, through: :received_reviews
  #reviews to user
  has_many :sent_reviews, -> { user }, class_name: "Review", foreign_key: "reviewer_id", dependent: :destroy
  has_many :reviewees, through: :sent_reviews, source: :reviewable, source_type: "User"
  #reviews to event
  has_many :event_reviews, -> { event }, class_name: "Review", foreign_key: "reviewer_id", dependent: :destroy
  has_many :reviewed_events, through: :event_reviews, source: :reviewable, source_type: "Event"

  has_many :devices, dependent: :destroy

  has_many :active_relationships,  class_name:  "Follow", foreign_key: "follower_id", dependent: :destroy
  has_many :following, through: :active_relationships,  source: :followed
  has_many :passive_relationships, class_name:  "Follow", foreign_key: "followed_id", dependent: :destroy
  has_many :followers, through: :passive_relationships, source: :follower

  has_many :notifications, dependent: :destroy

  has_one :bank_account, dependent: :destroy

  has_many :promo_codes, dependent: :destroy

  # received invitations
  has_many :received_invitations, class_name: "Invitation", foreign_key: "invitee_id", dependent: :destroy
  # sent invitations
  has_many :sent_invitations, class_name: "Invitation", dependent: :destroy
  has_many :invited_joiners, through: :sent_invitations, source: :invitee
  #referrals
  has_many :referrals, dependent: :destroy
  has_one :accepted_referral, class_name: "Referral", foreign_key: "joiner_id",
    dependent: :destroy
  has_many :referred_joiners, through: :referrals, source: :referred_joiner

  has_one :credit_promo_code, -> { PromoCode.credit }, class_name: PromoCode, foreign_key: "user_id"
  has_one :private_use_promo_code, -> { PromoCode.private_use }, class_name: PromoCode, foreign_key: "user_id"

  has_many :unpublished_events, -> { unpublished }, class_name: "Event", dependent: :destroy

  has_many :review_blacklists, dependent: :destroy
  has_many :review_blacklist_events, through: :review_blacklists, source: :event

  validate :valid_birthdate, on: :update

  after_save do
    self.decorate.generate_avatar if self.image_url && self.profile_avatar.nil?
  end

  after_create :create_credit_promo_code

  def self.approved_members_payment
    includes(event_memberships: :payment).where(payments: { status: Payment.statuses[:approved] })
  end

  def self.pending_members_payment
    includes(event_memberships: :payment).where(payments: { status: Payment.statuses[:pending] })
  end

  def self.declined_members_payment
    includes(event_memberships: :payment).where(payments: { status: Payment.statuses[:declined] })
  end

  def self.expired_members_payment
    includes(event_memberships: :payment).where(payments: { status: Payment.statuses[:expired] })
  end

  def self.most_active_joiners
    self.all.sort_by do |joiner|
      joiner.approved_joiner_event_memberships.count
    end.reverse
  end

  def self.lead
    # user's with no event memberships
    where.not(id: valid_event_joiners.ids)
  end

  def self.one_off
    # user's with only one event membership
    valid_event_joiners.group("users.id").having("count(event_memberships.id) = ?", 1)
  end

  def self.repeat
    # user's with only two event memberships
    valid_event_joiners.group("users.id").having("count(event_memberships.id) = ?", 2)
  end

  def self.loyal
    # user's with three or more event memberships
    valid_event_joiners.group("users.id").having("count(event_memberships.id) >= ?", 3)
  end

  def self.active
    [].tap do |collection|
      valid_event_joiners.each do |member|
        last_booking_date = member.approved_joiner_event_memberships.last&.created_at
        next unless last_booking_date
        # with last booking not exceed 2 months ago
        collection << member if  last_booking_date >= 2.months.ago
      end
    end
  end

  def self.at_risk
    [].tap do |collection|
      valid_event_joiners.each do |member|
        last_booking_date = member.approved_joiner_event_memberships.last&.created_at&.to_date
        next unless last_booking_date
        # with last booking more than 2 months ago but less than 4 months ago
        collection << member if  last_booking_date.between?(4.months.ago, 2.months.ago)
      end
    end
  end

  def self.lapsed
    [].tap do |collection|
      valid_event_joiners.each do |member|
        last_booking_date = member.approved_joiner_event_memberships.last&.created_at
        next unless last_booking_date
        # with last booking more than 4 months ago
        collection << member if  last_booking_date < 4.months.ago
      end
    end
  end

  def self.search(collection:, query: nil)
    query = "%#{query}%"
    collection.where('first_name ILIKE ? OR last_name ILIKE ?',query, query).order(:created_at)
  end

  def full_name
    "#{self.first_name} #{self.last_name}"
  end

  def past_joined_events
    self.joined_events.past.order_by_start
  end

  def upcoming_joined_events
    self.joined_events.upcoming.order_by_start
  end

  def upcoming_bookmarked_events
    self.bookmarked_events.upcoming
  end

  def upcoming_organized_events
    self.organized_events.upcoming.order_by_start
  end

  def past_organized_events
    self.organized_events.past.sorted_event
  end

  def organized_events_active_members
    members = organized_events_members.all_except(self.id).pluck(:id)
    active_joiners_id = members.uniq.sort_by{|e| members.count(e) }.reverse

    User.where(id: active_joiners_id)
  end

  def is_approved?
    self.approved_at.present?
  end

  def credit_code
    self.credit_promo_code.try(:code)
  end

  def credit_value
    self.credit_promo_code.try(:value)
  end

  def sync_calendar!
    self.update(calendar_sync: true)
  end

  def unsync_calendar!
    self.update(calendar_sync: false)
  end

  def waitlist_for(event)
    self.waitlists.find_by(event_id: event)
  end

  def last_booking_date
    approved_memberships = self.approved_joiner_event_memberships
    last_booking_date = approved_memberships.last&.created_at
    begin
      last_booking_date.to_date
    rescue => e
      "No Booking Record"
    end
  end

  def has_instagram?
    !self.instagram_provider.nil?
  end

  def instagram_access_token
    self.instagram_provider.token
  end

  def reviewable_events
    reviewable_events = []
    approved_past_joined_events.each do |event|
      reviewable_events << event.owner unless sent_reviews.map(&:reviewable).include?(event.owner)
    end
    approved_past_joined_events.where(user_id: reviewable_events)
  end

  def reviewable_events_exclude_blacklist
    blacklist_event_ids = self.review_blacklist_events&.ids
    reviewable_events.where.not(id: blacklist_event_ids)
  end

  def has_completed_required_data?
    self.first_name.present? && self.last_name.present? && self.mobile.present? && self.email.exclude?("@facebook.com")
  end

  def apply_as_organizer!
    self.update(applied_as_organizer_at: DateTime.now)
  end

  def latest_device_platform
    self.devices.enabled.last&.platform || "desktop"
  end

  def total_bookings
    organized_events_members.where.not(id: self.id).count
  end

  def publish_trips_at
    days = Hash.new(0)
    organized_events.each{ |trip| days[trip.created_at.strftime("%A")] += 1 }
    days.max{|a,b| a[1] <=> b[1] }.first
  end

  def device_used_for_creating_trips
    user_devices = Hash.new(0)
    self.devices.organizer.each{ |device| user_devices[device.platform] += 1 }
    user_devices.max{|a,b| a[1] <=> b[1] }.first
  end

  private

  def valid_birthdate
    self.errors.add(:birthdate, "is invalid please provide a valid format MM-DD-YYYY eg. 01-30-2000.") if self.birthdate_changed? && self.birthdate.nil?
  end

  def create_credit_promo_code
    self.promo_codes.credit.create(credit_promo_code_params)
  end

  def credit_promo_code_params
    {
      code:     PromoCode.generate_unique_code,
      value:    0.0,
    }
  end

end
