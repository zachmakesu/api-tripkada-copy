# == Schema Information
#
# Table name: event_memberships
#
#  created_at :datetime         not null
#  event_id   :integer
#  id         :integer          not null, primary key
#  member_id  :integer
#  message    :text
#  no_show    :boolean          default(FALSE)
#  updated_at :datetime         not null
#
# Indexes
#
#  index_event_memberships_on_event_id  (event_id)
#
# Foreign Keys
#
#  fk_rails_fe17726190  (event_id => events.id)
#

class EventMembership < ActiveRecord::Base
  scope :downpayments, -> { where(id: includes(:payment).map{|em| em.id if em.payment.try(:downpayment?) }.compact) }

  belongs_to :member, class_name: "User", foreign_key: "member_id"
  belongs_to :event

  has_one :payment, dependent: :destroy
  has_many :invitations, dependent: :destroy
  has_many :invited_members, through: :invitations, source: :invitee

  validates :member, :event, presence: true
  validates :member, uniqueness: { scope: :event }, unless: :expired_member?

  validate :event_members_limit, if: Proc.new{ |member| member.event.published_at.present? }

  def self.approved
    includes(:payment).where(payments: { status: Payment.statuses[:approved] })
  end

  def self.pending
    includes(:payment).where(payments: { status: Payment.statuses[:pending] })
  end

  def self.declined
    includes(:payment).where(payments: { status: Payment.statuses[:declined] })
  end

  def self.expired
    includes(:payment).where(payments: { status: Payment.statuses[:expired] })
  end

  def self.approved_joiner
    includes(:payment).where(payments: { status: Payment.statuses[:approved] }).where.not(payments: { mode: Payment.modes[:organizer] } )
  end

  def self.approved_joiners_except_invitee
    includes(:payment).where(payments: { status: Payment.statuses[:approved] })
                      .where.not(payments: { mode: Payment.modes[:organizer] })
                      .where.not(
                        payments: { mode: Payment.modes[:through_invite] }
                      )
  end

  def mark_as_no_show!
    self.update(no_show: true)
  end

  private

  def expired_member?
    if member.present?
      event.has_expired_membership_for?(member)
    end
  end

  def event_members_limit
    self.errors.add(:base, "Too much members in specific event.") if self.event.approved_members.count >= self.event.max_pax.to_i
  end
end
