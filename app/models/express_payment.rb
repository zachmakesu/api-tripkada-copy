# == Schema Information
#
# Table name: express_payments
#
#  created_at             :datetime         not null
#  id                     :integer          not null, primary key
#  payer_id               :string
#  payment_id             :integer          not null
#  payment_status         :string
#  payment_tracking_id    :string
#  payment_transaction_id :string
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_express_payments_on_payment_id  (payment_id)
#

class ExpressPayment < ActiveRecord::Base
  belongs_to :payment

  validates :payer_id, :payment_status, :payment_tracking_id, presence: true
end
