# == Schema Information
#
# Table name: referral_incentives
#
#  created_at :datetime         not null
#  created_by :string
#  id         :integer          not null, primary key
#  updated_at :datetime         not null
#  value      :float            default(0.0)
#

class ReferralIncentive < ActiveRecord::Base
  scope :search, -> (query) { where("created_by ILIKE :query", query: "%#{query}%")}
  validates :value, :created_by, presence: true  

  def self.latest
    last.try(:value) || 0
  end
end
