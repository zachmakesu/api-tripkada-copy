# == Schema Information
#
# Table name: payments
#
#  amount_paid         :float            default(0.0), not null
#  booking_fee_id      :integer
#  created_at          :datetime         not null
#  device_platform     :integer          default(0)
#  event_membership_id :integer
#  id                  :integer          not null, primary key
#  made                :integer
#  mode                :integer
#  promo_code_id       :integer
#  status              :integer          default(0), not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_payments_on_booking_fee_id       (booking_fee_id)
#  index_payments_on_event_membership_id  (event_membership_id)
#  index_payments_on_promo_code_id        (promo_code_id)
#
# Foreign Keys
#
#  fk_rails_aa7575f208  (event_membership_id => event_memberships.id)
#  fk_rails_d6c0a15f06  (promo_code_id => promo_codes.id)
#

class Payment < ActiveRecord::Base
  PAYMENT_PHOTO_REQUIRED_LIST = %w{ bank paymaya coins.ph }

  enum mode: { "bank" => 0, "paymaya" => 1, "coins.ph" => 2, "organizer" => 3, "tagcash" => 4, "paypal" => 5, "free_downpayment" => 6, "free_trip" => 7, "through_invite" => 8, "dragonpay" => 9 }
  enum made: { "downpayment" => 0, "fullpayment" => 1 }
  enum status: { pending: 0, approved: 1, declined: 2, expired: 3 }
  enum device_platform: { desktop: 0, android_app: 1, ios_app: 2,
                          android_web: 3, ios_web: 4 }

  belongs_to :event_membership
  belongs_to :promo_code
  belongs_to :booking_fee

  has_one :event, through: :event_membership
  has_one :member, through: :event_membership

  has_one :express_payment, dependent: :destroy

  has_many :payment_photos, class_name: "Photo", as: :imageable, dependent: :destroy

  has_many :dragonpay_transaction_logs, dependent: :destroy

  validates :event_membership, :mode, :device_platform, presence: true
  validates :booking_fee, presence: true, unless: :organizer?
  before_validation do
    validate_free_downpayment_eligibility if self.free_downpayment?
    validate_free_trip_eligibility if self.free_trip?
    self.errors.add(:base, "Reach the maximum of 10 images") if self.payment_photos.count >= 10
    if promo_code.present? && event_membership.present? && !expired?
      validate_promo_code
    end
    validate_proof_of_payment if self.event_membership.present? && PAYMENT_PHOTO_REQUIRED_LIST.include?(self.mode)
  end

  after_create :deduct_credits, if: Proc.new { |payment| payment&.promo_code&.credit? }

  def self.approved_joiner
    where(payments: { status: Payment.statuses[:approved] }).where.not(payments: { mode: Payment.modes[:organizer] } )
  end

  def created?(date)
    created_at <= date
  end

  private
  def validate_proof_of_payment
    self.errors.add(:base, "Need Proof of Payment image atleast 1") if self.payment_photos.blank?
  end

  def validate_promo_code
    promo_code = PromoCode::ValidateByCategory.call(code: self.promo_code.code, user: self.event_membership.member)
    self.errors.add(:base, promo_code.message) unless promo_code.success?
  end

  def validate_free_downpayment_eligibility
    downpayment_rate_with_fee = self&.event_membership&.event&.decorate&.downpayment_rate_with_fee

    if downpayment_rate_with_fee.nil?
      self.errors.add(:base, "Trip Details is missing please contact Admin")
      Rails.logger.info("#{DateTime.now} : FREE-DOWNPAYMENT-MISSING-TRIP: MEMBER_ID: #{self&.event_membership&.member&.id}|| EVENT: #{self&.event_membership&.event}")
    end

    promo_code_value = self.promo_code&.credit? ? self.promo_code&.available_credits : self.promo_code&.value

    self.errors.add(:base, "Insufficient Promo Code Value, not eligible for free downpayment") unless self.promo_code && promo_code_value.to_f >= downpayment_rate_with_fee.to_f
  end

  def validate_free_trip_eligibility
    rate_with_fee = self&.event_membership&.event&.decorate&.rate_with_fee

    if rate_with_fee.nil?
      self.errors.add(:base, "Trip Details is missing please contact Admin")
      Rails.logger.info("#{DateTime.now} : FREE-TRIP-MISSING-TRIP: MEMBER_ID: #{self&.event_membership&.member&.id}|| EVENT: #{self&.event_membership&.event}")
    end

    promo_code_value = self.promo_code&.credit? ? self.promo_code&.available_credits : self.promo_code&.value

    self.errors.add(:base, "Insufficient Credit Value, not eligible for free trip") unless self.promo_code && promo_code_value.to_f >= rate_with_fee.to_f
  end

  def deduct_credits
    self.promo_code.credit_logs.deduction.create(amount: calculated_credits_deduction, payment_id: self.id)
  end

  def calculated_credits_deduction
    event = self.event.decorate
    downpayment_with_fee = event.downpayment_rate_with_fee
    rate_with_fee = event.rate_with_fee
    available_credits = self.promo_code.available_credits

    if available_credits >= rate_with_fee
      rate_with_fee
    elsif available_credits >= downpayment_with_fee && available_credits < rate_with_fee
      downpayment_with_fee
    else
      available_credits
    end
  end
end
