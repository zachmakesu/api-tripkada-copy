# == Schema Information
#
# Table name: providers
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  provider   :string
#  role       :string
#  token      :string
#  uid        :string
#  updated_at :datetime         not null
#  url        :string
#  user_id    :integer
#
# Indexes
#
#  index_providers_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_901136bfff  (user_id => users.id)
#

class Provider < ActiveRecord::Base
  ROLES = %w{joiner organizer invitee from_referral}
  PROVIDERS = %w{ facebook instagram tagcash }
  PROVIDERS_WITH_UNIQ_USER = %w{ facebook instagram }
  PROVIDERS_WITH_VIEWABLE_TOKEN = %w{ instagram }

  scope :social_accounts, -> { select('DISTINCT on (provider) id, provider, url, token, uid') }
  scope :social_account, ->(provider) { find_by(provider: provider).url }
  scope :instagram, -> { find_by(provider: "instagram") }
  scope :facebook, -> { find_by(provider: "facebook") }
  belongs_to :user

  validates :role, inclusion: { in: ROLES, message: "%{value} is not a valid role" }, allow_nil: false
  validates :provider, inclusion: { in: PROVIDERS, message: "%{value} is not a valid provider" }, allow_nil: false
  validates :user, uniqueness: { scope: :uid }, if: Proc.new{|p| PROVIDERS_WITH_UNIQ_USER.include?(p.provider)}

  ROLES.each do |role|
    scope role.to_sym, -> { where(role: role) }
    define_method "#{role}?" do
      self.role == role
    end
  end

  PROVIDERS.each do |provider|
    scope provider.to_sym, -> { where(provider: provider) }
    define_method "#{provider}?" do
      self.provider == provider
    end
  end

  before_validation do
    self.errors.add(:base, "User is not allowed to login as organizer") if self.organizer? && self.user.present? && !self.user.joiner_and_organizer? && self.facebook?
  end

  def can_view_token?
    PROVIDERS_WITH_VIEWABLE_TOKEN.include?(self.provider)
  end
end
