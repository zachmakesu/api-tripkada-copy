# == Schema Information
#
# Table name: mailerlite_groups
#
#  category   :string
#  created_at :datetime         not null
#  group_id   :integer
#  id         :integer          not null, primary key
#  name       :string
#  updated_at :datetime         not null
#

class MailerliteGroup < ActiveRecord::Base
  CATEGORIES = %w(default one_off repeat loyal active at_risk lapsed)
  validates :group_id, :name, :category, presence: true
  validates :category, uniqueness: true
  validates :category, inclusion: { in: CATEGORIES, message: "%{value} is not a valid category"} 
end
