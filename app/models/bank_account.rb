# == Schema Information
#
# Table name: bank_accounts
#
#  account_number :string
#  bank_name      :string
#  created_at     :datetime         not null
#  id             :integer          not null, primary key
#  updated_at     :datetime         not null
#  user_id        :integer
#

class BankAccount < ActiveRecord::Base
  belongs_to :user
end
