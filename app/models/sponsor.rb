# == Schema Information
#
# Table name: sponsors
#
#  created_at         :datetime         not null
#  event_id           :integer          not null
#  id                 :integer          not null, primary key
#  image_content_type :string
#  image_file_name    :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_sponsors_on_event_id  (event_id)
#

class Sponsor < ActiveRecord::Base
  belongs_to :event
  
  has_attached_file :image, styles: { xxlarge: "2000x2000>", xlarge: "1000x1000>" , large: "800x800>", medium: "300x300>", thumb: "100x100>" }, default_url: "/images/missing.png"
  validates_attachment_content_type :image,:content_type => /^image\/(jpg|jpeg|png)$/, :message => 'not allowed.'
  validates :image, attachment_presence: true, on: :create
  validates_with AttachmentSizeValidator, attributes: :image, less_than: 10.megabytes
  validates :event_id, presence: true
end
