# == Schema Information
#
# Table name: photos
#
#  category           :string           default("uncategorized")
#  created_at         :datetime         not null
#  id                 :integer          not null, primary key
#  image_content_type :string
#  image_file_name    :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  imageable_id       :integer
#  imageable_type     :string
#  label              :string
#  primary            :boolean          default(FALSE)
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_photos_on_imageable_id  (imageable_id)
#

class Photo < ActiveRecord::Base

  scope :cover_photo, -> { where(category: "cover_photo", primary: true) }
  scope :profile_avatar, -> { where(category: "avatar", primary: true) }
  scope :profile_photos, -> { where(category: "avatar", primary: false) }
  scope :government, -> { where(category: "government") }
  scope :certificates, -> { where(category: "certificate") }
  scope :verification, -> { where(category: "verification_photo") }

  CATEGORIES = %w{uncategorized government avatar cover_photo certificate verification_photo}

  belongs_to :imageable, polymorphic: true

  validates :category, inclusion: { in: CATEGORIES , message: "%{value} is not a valid category." }

  has_attached_file :image, styles: { xxlarge: "2000x2000>", xlarge: "1000x1000>" , large: "800x800>", medium: "300x300>", thumb: "100x100>" }, default_url: "/images/missing.png"
  validates_attachment_content_type :image,:content_type => /^image\/(jpg|jpeg|png)$/, :message => 'not allowed.'
  validates :image, attachment_presence: true, on: :create
  validates_with AttachmentSizeValidator, attributes: :image, less_than: 10.megabytes

  validate :primary_photo, :ensure_valid_dimensions

  CATEGORIES.each do |category|
    define_method "#{category}?" do
      bool_value = self.category == category
      %w{avatar cover_photo}.include?(category) ? bool_value && self.primary : bool_value
    end
  end

  private
  def ensure_valid_dimensions
    uploaded_image = image.queued_for_write[:original]
    unless self.imageable_type == "User" || self.category == "avatar" || uploaded_image.nil?
      dimensions = Paperclip::Geometry.from_file(uploaded_image)
      width = dimensions.width
      height = dimensions.height

      errors.add(:base, " dimensions are too small. For a good quality background" +
                 "please upload a larger image. Minimum width: 300px, minimum height: 300px"
                ) if width < 300 && height < 300
    end
  end

  def primary_photo
    self.errors.add(:base, "Cover photo was already set") if self.decorate.cover_photo? && self.imageable.cover_photo && self.imageable.cover_photo.id != self.id 
    self.errors.add(:base, "Avatar photo was already set") if self.decorate.avatar? && self.imageable.profile_avatar && self.imageable.profile_avatar.id != self.id 
  end
end
