# == Schema Information
#
# Table name: messenger_users
#
#  concern_answers :text
#  created_at      :datetime         not null
#  email           :string
#  first_name      :string
#  id              :integer          not null, primary key
#  job_id          :string
#  last_name       :string
#  last_reply      :datetime
#  message_concern :string
#  sendable        :boolean          default(TRUE)
#  updated_at      :datetime         not null
#  user_fb_id      :integer          not null
#

class MessengerUser < ActiveRecord::Base
  store :concern_answers, accessors: [ :category_id, :cost_spent, :days_spent], coder: JSON

  def self.fetch_by(first_name:, last_name:)
    return [] unless first_name.present? && last_name.present?
    where(["first_name ILIKE ? and last_name ILIKE ?", first_name, last_name])
  end
end
