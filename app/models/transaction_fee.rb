# == Schema Information
#
# Table name: transaction_fees
#
#  amount     :float            not null
#  category   :integer          not null
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  updated_at :datetime         not null
#

class TransactionFee < ActiveRecord::Base
  enum category: { uncategorized: 0, dragonpay: 1 }
  validates :amount, :category, presence: true

  def self.latest(category:)
    converted_category = self.categories[category]
    where(category: converted_category)&.last
  end
end
