class ReferralIncentivesController < ApplicationController
  layout 'application'
  before_filter :ensure_admin
  set_tab :referral_incentives

  # GET /booking_fees
  def index
    @referral_incentives = ReferralIncentive.search(params[:q]).order(created_at: :desc).page(params[:page]).per(10)
    @referral_incentive = ReferralIncentive.new
  end

  # POST /booking_fees
  def create
    @referral_incentive = ReferralIncentive.new(referral_incentive_params)

    if @referral_incentive.save
      redirect_to referral_incentives_path, notice: 'Referral Incentive was successfully created.'
    else
      redirect_to referral_incentives_path, alert: @referral_incentive.errors.full_messages.to_sentence
    end
  end

  private
  # Only allow a trusted parameter "white list" through.
  def referral_incentive_params
    params.require(:referral_incentive).permit(:value, :created_by)
  end
end
