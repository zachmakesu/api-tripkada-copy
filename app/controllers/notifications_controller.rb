class NotificationsController < ApplicationController

  def index
    current_user.notifications.not_seen.update_all(seen: true) # set all current notifications to seen
    @notifications = Kaminari.paginate_array(current_user.notifications.not_deleted.order_by_read.order_by_created_at.decorate).page(params[:page]).per(5)
  end

  def show
    @notification = current_user.notifications.find_by(id: params[:id])
    @notification.read!

    render nothing: true
  end

  def count_users_unseen_notifications
    @count_unseen_notifications = current_user.notifications.not_seen.count
    render json: @count_unseen_notifications
  end

end
