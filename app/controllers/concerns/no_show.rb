module NoShow
  extend ActiveSupport::Concern

  def no_show
    request.format = "html"
    event = Event.find(params[:event_id]);
    @event_membership = event&.event_memberships.find(params[:id])
    @event_membership.update(no_show: params[:no_show])
    respond_to do |format|
      format.html { render :nothing => true}
      format.js
    end
  end

end
