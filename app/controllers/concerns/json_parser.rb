module JsonParser
  extend ActiveSupport::Concern
  
  def check_for_invited_joiners
    if params[:invited_joiner_names] && params[:invited_joiner_emails] && params[:invited_joiner_names] != "undefined" && params[:invited_joiner_emails] != "undefined"

      @mod_params[:invited_joiner_names] = parse_invited_joiners_data(params[:invited_joiner_names])
      @mod_params[:invited_joiner_emails] = parse_invited_joiners_data(params[:invited_joiner_emails])
    end
  end

  def parse_invited_joiners_data(data)
    data.split(",")
  end
  
end
