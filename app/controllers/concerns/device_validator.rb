# module DeviceValidator
module DeviceValidator
  extend ActiveSupport::Concern

  private

  def check_device_platform(user_agent:, type:)
    mod_params = {}
    mod_params[:user_agent] = user_agent
    mod_params[:type] = type
    handler = Organizers::DevicePlatform.call(params: mod_params)
    handler.device_platform
  end
end
