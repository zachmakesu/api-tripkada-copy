# Controller Email Validator Concern
module EmailValidator
  extend ActiveSupport::Concern
  include JsonParser

  def validate_email(email:)
    email_verifier = EmailAddress.new(email)
    exclude_facebook = email.exclude?("facebook.com")
    {}.tap do |response|
      response[:valid_email] = email_verifier.valid? && exclude_facebook
      unless response[:valid_email]
        response[:message] = "Invalid Email #{email}."\
                             "Please provide a valid email"
      end
    end
  end

  def ensure_sufficient_slots_and_valid_email
    @mod_params = {}
    check_for_invited_joiners
    service = Event::Booking::EnsureSufficientSlotsAndValidInviteeEmails.call(
      event: @event,
      params: @mod_params,
      joiner: current_user
    )
    url = { path: trip_checkout_path(@event), message: service.message }
    redirect_to url[:path], alert: url[:message] unless service.success?
  end
end
