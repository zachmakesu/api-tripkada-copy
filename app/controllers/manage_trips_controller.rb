class ManageTripsController < ApplicationController
  include NoShow

  set_tab :overview,  :organizer_sidenav, only: :index
  set_tab :trips,  :organizer_sidenav, only: :trips
  set_tab :joiner,  :organizer_sidenav, only: :joiner
  set_tab :payments,  :organizer_sidenav, only: :payments
  set_tab :funds,  :organizer_sidenav, only: :funds

  before_action :ensure_organizer, if: :user_signed_in?

  def index
    @most_booked_events = current_user.decorate.most_booked_trips
    @most_viewed_events = current_user.decorate.most_viewed_trips
    @active_joiners = current_user.decorate.active_joiners
    @upcoming_events = current_user.decorate.upcoming_organized_events
  end

  def trips
    @organized_events = Kaminari.paginate_array(current_user.decorate.list_of_organized_events).page(params[:page]).per(10)
  end

  def joiner
    @organized_event_joiners = Kaminari.paginate_array(current_user.decorate.organized_event_joiners).page(params[:page]).per(10)
    @upcoming_events = current_user.decorate.upcoming_organized_events
  end

  def payments
    @organized_event_joiners = Kaminari.paginate_array(current_user.decorate.organized_event_joiners).page(params[:page]).per(10)
  end

  def funds
    @upcoming_organized_events = Kaminari.paginate_array(current_user.decorate.upcoming_organized_events).page(params[:upcomingTripsPaginator]).per(10)
    @past_organized_events = Kaminari.paginate_array(current_user.decorate.past_organized_events.reverse).page(params[:previousTripsPaginator]).per(10)
  end

  def cancel_trip
    @event = Event.find_by(slug: params[:slug])
    if @event.update(trip_cancel_reason: params[:reason])
      @event.soft_delete
      redirect_to manage_trips_my_trips_path, notice: "Trip #{@event.name} Successfully Canceled"
    else
      redirect_to manage_trips_my_trips_path, notice: @event.errors.full_messages.join(", ")
    end
  end

  def invite_joiner
    handler = EventHandler.new(current_user, params).invite_joiner
    @response = {}
    @response[:type] = handler.response[:type]
    @response[:receiver] = handler.params[:full_name]
    @response[:details] = handler.response[:details]
    respond_to do |format|
      format.html
      format.js{ render json: @response }
    end
  end

  def invite_joiner_via_email
    handler = EventHandler.new(current_user, params).invite_joiner_via_email
    @response = {}
    @response[:type] = handler.response[:type]
    @response[:receiver] = handler.params[:full_name]
    @response[:details] = handler.response[:details]
    respond_to do |format|
      format.html
      format.js{ render json: @response }
    end
  end

  def organized_trip_views
    #monthly organized events no. of views
    organized_event_views = current_user.organized_events.group_by_month(:start_at).sum(:number_of_views)
    mod_result = organized_event_views.map { |k, v| [k.strftime("%b-%Y"), v] }
    render json: mod_result
  end

  def organized_trip_bookings
    #monthly organized events bookings
    organized_event_members = current_user.organized_events.includes(:event_memberships).collect(&:event_memberships)
    memberships_per_month = organized_event_members.flatten.group_by_month(&:created_at).map { |k, v| [k.strftime("%b-%Y"), v.size] }
    render json: memberships_per_month
  end

  def monthly_organized_trips
    #monthly organized events
    organized_events = current_user.organized_events
    trips = organized_events.flatten.group_by_month(&:created_at).map { |k, v| [k.strftime("%b-%Y"), v.size] }
    render json: trips
  end

  def monthly_organized_trips_booking_count
    #monthly bookings
    organized_event_members = current_user.organized_events.includes(:event_memberships).collect(&:event_memberships).flatten
    bookings = organized_event_members.select do |members|
      members[:member_id] != current_user.id
    end
    booking_count = bookings.group_by_month(&:created_at).map { |k, v| [k.strftime("%b-%Y"), v.size] }
    render json: booking_count
  end

  def waitlists
    @event = Event.find_by(slug: params[:slug])&.decorate
    @waitlist_options = Waitlist.statuses.except("paid").keys
    @event_waitlists = Kaminari.paginate_array(@event.waitlists).page(params[:paginator]).per(10)
    @event_joiners = Kaminari.paginate_array(@event.event_memberships).page(params[:paginator_joiner]).per(10)
  end

  def update_waitlist
    handler = UpdateWaitlist.call(params: params, current_user: current_user)

    if handler.success?
      render json: handler[:feedback], status: :created
    else
      render json: { message: handler.message }, status: :unprocessable_entity
    end

  end

  private

end
