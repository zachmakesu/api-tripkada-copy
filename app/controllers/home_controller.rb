class HomeController < ApplicationController
  include UsersHelper
  include DeviceValidator
  before_action :ensure_organizer_or_joiner, if: :user_signed_in?
  before_action :ensure_valid_email, only: [:create_requests, :create_questions, :create_form_organiser]
  def index
    upcoming_events = Event.not_deleted.upcoming
    @user = User.new
    @featured_events = upcoming_events.featured.order_by_start.limit(3).decorate
    @upcoming_events = upcoming_events.order_by_start.limit(3).decorate
    @this_months_events = upcoming_events.this_month.order_by_start.limit(3).decorate
    @destinations = Destination.all
    @categories = Category.all
  end

  def how_it_works
  end

  def privacy_policy
  end

  def who_we_are
  end

  def collaborate
  end

  def apply_as_organiser
  end

  def apply_for_a_job
  end

  def requests
  end

  def create_requests
    Notifications::EmailWorker.perform_async(params, "trip_request")
    redirect_to trip_requests_path, notice: "Successfully sent!."
  end

  def questions
  end

  def create_questions
    params[:registration_platform] = check_device_platform(
      user_agent: request.user_agent,
      type: "controller"
    )
    Notifications::EmailWorker.perform_async(params, "question")
    redirect_to questions_path, notice: "Successfully sent!."
  end

  def sign_in_as_organizer
  end

  def sign_in_as_joiner
  end

  def create_form_organiser
    current_user.apply_as_organizer!
    params[:organizer_application_platform] = check_device_platform(
      user_agent: request.user_agent,
      type: "controller"
    )
    Notifications::EmailWorker.perform_async(params, "apply_as_organiser")
    redirect_to organizer_landing_page_path,
    notice: "Application Request Successfully sent!."
  end

  def sign_me_up
    @user = User.new(sign_me_up_params)
    @user.password = 'password'
    Notifications::EmailWorker.perform_async(@user, "sign_me_up_email")
    redirect_to root_url, notice: "Successfully sent!."
  end

  def authenticated_redirect
    flash[flash.first[0].to_sym] = flash.first[1] if flash.present?
    redirect_to dynamic_path_for(current_user)
  end

  def tagcash_callback
    handler = TagcashHandler.new(params[:code], params[:state]).get_access_token
    path = if event = Event.find_by(slug: handler.response[:event])
             payment_details_path(event, code: handler.response[:promo_code], mode: "tagcash")
           else
             root_path
           end

    if handler.response[:type]
      redirect_to path, notice: handler.response[:details]
    else
      redirect_to root_path, alert: handler.response[:details]
    end
  end

  private
  def sign_me_up_params
    params.require(:user).permit(:first_name, :last_name, :email)
  end

  def ensure_valid_email
    valid = valid_email?(email: params[:email])
    redirect_to :back, alert: "Please use a valid email." unless valid
  end
end
