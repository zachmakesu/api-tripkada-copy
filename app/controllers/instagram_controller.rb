class InstagramController < ApplicationController
  before_action :check_for_params_provider_id, only: [:connect]
  before_action :check_for_existing_ig_provider

  def connect
    redirect_to Instagram.authorize_url(:redirect_uri => IG_CALLBACK_URL)
  end

  def callback
    begin
      response = Instagram.get_access_token(params[:code], :redirect_uri => IG_CALLBACK_URL)
      client = Instagram.client(:access_token => response.access_token)
      current_user.providers.instagram.create(new_instagram_provider_params(response.access_token, client.user))
      flash = { notice: "Successfully connected to instagram."}
    rescue Instagram::BadRequest
      flash = { alert: "Failed to connect to instagram."}
    end
    redirect_to profile_path(current_user.slug), flash
  end

  private

  def new_instagram_provider_params(token, user)
    {
      role: app_role,
      uid: user.id,
      url: "https://www.instagram.com/#{user.username}/",
      token: token
    }
  end

  def check_for_params_provider_id
    current_user.providers.instagram.find_by(id: params[:provider_id])&.destroy if params.has_key?(:provider_id)
  end

  def check_for_existing_ig_provider
    redirect_to profile_path(current_user.slug), alert: "Instagram account is already connected" if current_user.instagram_provider.present?
  end
end
