class BookingFeesController < ApplicationController
  layout 'application'
  before_filter :ensure_admin
  set_tab :booking_fees

  # GET /booking_fees
  def index
    @booking_fees = BookingFee.search(params[:q]).order(created_at: :desc).page(params[:page]).per(10)
    @booking_fee = BookingFee.new
  end

  # POST /booking_fees
  def create
    @booking_fee = BookingFee.new(booking_fee_params)

    if @booking_fee.save
      redirect_to booking_fees_path, notice: 'Booking fee was successfully created.'
    else
      redirect_to booking_fees_path, alert: @booking_fee.errors.full_messages.join(", ")
    end
  end

  private
  # Only allow a trusted parameter "white list" through.
  def booking_fee_params
    params.require(:booking_fee).permit(:value, :created_by)
  end
end
