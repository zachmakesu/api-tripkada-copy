class MessengerWebviewController < ApplicationController
  include Facebook::Messenger
  before_action :set_categories, only: [:event_filters, :activity_based_trips, :fill_up_form]
  before_action :set_destinations, only: [:event_filters, :destination_based_trips, :fill_up_form]
  before_action :fetch_fb_user, only: [:event_filters, :specific_date_trips, :activity_based_trips, :destination_based_trips, :fill_up_form, :filtered_events]

  def set_profile
    # CHATBOT PROFILE SETTING FOR PAGE 1
    Facebook::Messenger::Profile.set({
      whitelisted_domains:[
        ActionController::Base.asset_host,
        "https://staging.tripkada.com",
        "https://tripkada.com"
      ]
    }, access_token: ENV['FB_PAGE_ACCESS_TOKEN'])

    # CHATBOT PROFILE SETTING FOR PAGE 2
    Facebook::Messenger::Profile.set({
      greeting: [
        {
          locale: 'default',
          text: "Hello {{user_first_name}}, I'm your Tripkada Chatgenie. How can I serve you today?"
        }
      ],
      get_started: {
        payload: 'GET_STARTED_PAYLOAD'
      },
      whitelisted_domains:[
        ActionController::Base.asset_host,
        "https://staging.tripkada.com",
        "https://tripkada.com"
      ]
    }, access_token: ENV['FB_2ND_PAGE_ACCESS_TOKEN'])


    Facebook::Messenger::Profile.set({
      persistent_menu: [
        {
          locale: 'default',
          composer_input_disabled: false,
          call_to_actions: [
            {
              title: 'Chat with Tripkada Staff',
              type: 'postback',
              payload: 'CHECK_PAYLOAD'
            },
            {
              title: 'Browse Trips',
              type: 'nested',
              call_to_actions: [
                {
                  title: 'Trips This Upcoming Weekend',
                  type: 'postback',
                  payload: 'UPCOMING_PAYLOAD'
                }
              ]
            },
            {
              title: "Recommend A Trip",
              type: 'postback',
              payload: 'RECOMMEND_TRIP_PAYLOAD'
            }
          ]
        }
      ]
    }, access_token: ENV['FB_2ND_PAGE_ACCESS_TOKEN'])
  end

  def unset_profile
    CHATBOT PROFILE UNSETTING
    Facebook::Messenger::Profile.unset({
      fields: [
            "greeting",
            "get_started",
            "persistent_menu"
          ]
    }, access_token: ENV['FB_2ND_PAGE_ACCESS_TOKEN'])
  end

  def subscribe
    Facebook::Messenger::Subscriptions.subscribe(access_token: ENV['FB_PAGE_ACCESS_TOKEN'])
    Facebook::Messenger::Subscriptions.subscribe(access_token: ENV['FB_2ND_PAGE_ACCESS_TOKEN'])
  end

  def unsubscribe
    Facebook::Messenger::Subscriptions.unsubscribe(access_token: ENV['FB_PAGE_ACCESS_TOKEN'])
    Facebook::Messenger::Subscriptions.unsubscribe(access_token: ENV['FB_2ND_PAGE_ACCESS_TOKEN'])
  end

  def specific_date_trips
    @first_upcoming_event = Event.first_upcoming_event.decorate
    @last_upcoming_event = Event.last_upcoming_event.decorate
  end

  def destination_based_trips

  end

  def activity_based_trips

  end

  def event_filters
    @min_cost = Event::MINCOST
    @max_cost = Event::MAXCOST
    @min_days = Event::MINDAYS
    @max_days = Event::MAXDAYS

    @first_upcoming_event = Event.first_upcoming_event.decorate
    @last_upcoming_event = Event.last_upcoming_event.decorate
  end

  def fill_up_form
    @min_cost = Event::MINCOST
    @max_cost = Event::MAXCOST
    @min_days = Event::MINDAYS
    @max_days = Event::MAXDAYS
  end

  def filtered_events
    events = EventFilter.new(Event.not_deleted.upcoming, params).result

    delete_payload_wodker!

    elements = events.limit(9).decorate.map do |event|
      {
        title: event.name,
        subtitle: "Organizer: #{event.owner.full_name}
Date: #{event.travel_dates.upcase}
Description: #{event.description}
        ",
        image_url: event.cover_photo_complete_url(:large),
        buttons: [
          {
            type: "web_url",
            url: event.website,
            title: "View Details",
            webview_height_ratio: "tall"
          },
          {
            type: "web_url",
            url: event.generate_checkout_link,
            title: "Book Now",
            webview_height_ratio: "tall"
          }
        ]
      }
    end

    see_more = {
                  title: "See more Trips",
                  buttons: [
                    {
                      type: "web_url",
                      url: "#{ActionController::Base.asset_host}/trips",
                      title: "See more",
                      webview_height_ratio: "tall"
                    }
                  ]
                }

    elements << see_more if events.count >= 9

    messages = if events.count > 0
                  if params[:recommended]
                    [
                      { text: "Thank you #{@fbuser_obj.fetch('first_name')} for providing those information." },
                      { text: "Here are the events we gathered for you 😘" },
                      {
                        attachment: {
                          type: 'template',
                          payload: {
                            template_type: "generic",
                            image_aspect_ratio: "square",
                            elements: elements
                          }
                        }
                      },
                      {
                        attachment: {
                          type: "template",
                          payload: {
                            template_type: "button",
                            text: " You can use the Trip Filters here or the main menu to further refine the trip results based on your selected settings",
                            buttons:[
                              {
                                title: "Show filter",
                                type: "web_url",
                                url: "#{ActionController::Base.asset_host}/messenger_webview/event_filters/#{@fbuser_obj.fetch('id')}",
                                messenger_extensions: true,
                                webview_height_ratio: "tall"
                              }
                            ]
                          }
                        }
                      }
                    ]
                  else
                    [
                      {  text: "Thank you #{@fbuser_obj.fetch('first_name')} for filtering the events to suits your desire" },
                      {
                        attachment: {
                          type: 'template',
                          payload: {
                            template_type: "generic",
                            image_aspect_ratio: "square",
                            elements: elements
                          }
                        }
                      },
                      {
                        attachment: {
                          type: "template",
                          payload: {
                            template_type: "button",
                            text: " You can use the Trip Filters here or the main menu to further refine the trip results based on your selected settings",
                            buttons:[
                              {
                                title: "Show filter",
                                type: "web_url",
                                url: "#{ActionController::Base.asset_host}/messenger_webview/event_filters/#{@fbuser_obj.fetch('id')}",
                                messenger_extensions: true,
                                webview_height_ratio: "tall"
                              }
                            ]
                          }
                        }
                      }
                    ]
                  end
                else
                  [
                    {  text: "Thank you #{@fbuser_obj.fetch('first_name')} for filtering the events to suits your desire" },
                    {  text: "Unfortunately, we currently do not have events based on the filter you input" },
                    {
                      attachment: {
                        type: "template",
                        payload: {
                          template_type: "button",
                          text: "Kindly try it again with the other options. Thank you!",
                          buttons:[
                            {
                              title: "Show filter",
                              type: "web_url",
                              url: "#{ActionController::Base.asset_host}/messenger_webview/event_filters/#{@fbuser_obj.fetch('id')}",
                              messenger_extensions: true,
                              webview_height_ratio: "tall"
                            }
                          ]
                        }
                      }
                    }
                  ]
                end



    after_payload_worker!
    bot_deliver(messages)
  end

  def checkout_notif
    @membership = EventMembership.approved.find(params[:membership_id])
    graph = Koala::Facebook::API.new(ENV['FB_PAGE_ACCESS_TOKEN'])
    @fbuser_obj = graph.get_object(params[:fbuser_obj_id].to_i)
    @joiner = @membership.member.decorate
    @event = @membership.event.decorate
    @payment = @membership.payment.decorate

    respond_to do |format|
      format.html { render :layout => false } # your-action.html.erb
      format.mobile { render :layout => false } # your-action.html.erb
    end
  end

  private

  def bot_deliver messages
    messages.each do |message|
      Bot.deliver({
        recipient: {
          id: params[:fbuser_obj_id]
        },
        message: message
      }, access_token: ENV['FB_2ND_PAGE_ACCESS_TOKEN'])
    end
  end

  def fetch_fb_user
    graph = Koala::Facebook::API.new(ENV['FB_2ND_PAGE_ACCESS_TOKEN'])
    @fbuser_obj = graph.get_object(params[:fbuser_obj_id].to_i)
    @fbuser = MessengerUser.find_by(user_fb_id: params[:fbuser_obj_id])
  end

  def set_categories
    @categories = Category.order(name: :asc)
  end

  def set_destinations
    @destinations = Destination.order(name: :asc)
  end

  def delete_payload_wodker!
    Sidekiq::ScheduledSet.new.select{|j| j.jid == @fbuser.job_id }.each(&:delete) if @fbuser.job_id
  end

  def after_payload_worker!
    job_id = ChatbotMessenger::AfterPayloadWorker.perform_at(DateTime.now + 2.minutes, @fbuser.id)
    @fbuser.update(last_reply: DateTime.now, job_id: job_id)
  end

end
