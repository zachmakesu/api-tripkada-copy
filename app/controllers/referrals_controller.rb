class ReferralsController < ApplicationController
  def send_invitations
    service = Referrals::SendInvitations.call(params: params, referrer: current_user.decorate)
    redirect_to request.referrer, notice: service.mailer_response
  end
end
