class MailerliteController < AdminController
  set_tab :mailerlite, only: %w(index)
  before_filter :set_groups, only: [:index]
  before_filter :set_user_segment_options, only: [:index, :import_subscribers]
  
  def index
  end

  def create
    @service = Mailerlite::Groups::CreateGroup.call(name: params[:name])
    redirect_path_with_notification(message: "Mailerlite Group Successfully Created")
  end

  def delete
    @service = Mailerlite::Groups::DeleteGroup.call(group_id: params[:id])
    redirect_path_with_notification(message: "Mailerlite Group Successfully Deleted")
  end

  def import_subscribers
    segment_options = @segment_options_booking_count + @segment_options_booking_duration
    subscribers = User.send(params[:options]) if segment_options.include?(params[:options])
    @service = Mailerlite::Groups::ImportSubscribers.call(group_id: params[:id], subscribers: subscribers)
    redirect_path_with_notification(message: "Mailerlite Group Subscribers Successfully Imported")
  end

  private

  def set_groups
    service = Mailerlite::Groups::GetGroups.call
    if service.success?
      groups = JSON.parse service.response
      @groups = groups.map(&:symbolize_keys)
    else
      redirect_to root_path,  error: service.message 
    end
  end

  def set_user_segment_options
    @segment_options_booking_count = %w{ lead one_off repeat loyal }
    @segment_options_booking_duration = %w{ active at_risk lapsed }
  end

  def redirect_path_with_notification(message:)
    notification = {}
    if @service.success?
      notification[:notice] = message
    else
      notification[:alert] = @service.message
    end
    redirect_to mailerlite_groups_path, notification
  end
end
