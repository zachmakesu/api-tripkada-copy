class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  include DeviceValidator
  before_action :set_path_and_event

  def facebook
    #temporary remove due to facebook bug loop on rerequest auth
    #if request.env["omniauth.auth"].info.email.blank?
    #  redirect_to "/users/auth/facebook?auth_type=rerequest&scope=email"
    #  return
    #end
    # You need to implement the method below in your model (e.g. app/models/user.rb)
    token   = request.env["omniauth.auth"]["credentials"]["token"]
    role    = request.env['omniauth.params']["role"]
    uid     = request.env['omniauth.params']["uid"]
    url     = request.env['omniauth.params']["url"]
    referrer_uid = request.env['omniauth.params']["referrer_uid"]
    session[:return_to] ||= request.env['omniauth.params']["return_to"]

    options = {}
    options[:uid] = uid
    options[:referrer_uid] = referrer_uid
    options[:platform] = check_device_platform(user_agent: request.user_agent,
                                               type: "controller")
    handler = FacebookSessionHandler.new(role, token, options).find_or_create

    return failure if handler.response[:failed_facebook_auth]

    @user = User.find_by(uid: handler.response[:details][:uid])

    if @user && @user.is_approved?
      sign_in @user

      options                     = Hash.new
      options[:path]              = @path
      options[:event]             = @event
      options[:user]              = @user
      options[:request]           = request
      options[:handler_response]  = handler.response
      options[:app_role]          = role
      options[:kind]              = "Facebook"
      options[:proceed_checkout]  = request.env['omniauth.params']["checkout"]
      options[:url]               = url
      redirect_handler = OmniauthRedirectHandler.new(options).get_path
      Rails.logger.info("#{DateTime.now} : FACEBOOK-LOGIN: REDIRECT-URL: #{redirect_handler.return_to} || #{params}")
      Rails.logger.info("#{DateTime.now} : FACEBOOK-LOGIN: RETURN-URL: #{session[:return_to]}")

      session[:app_role] = role
      session[:return_to] ||= redirect_handler.return_to

      # ensure return_to path is a valid route
      # new users redirected in
      # https://m.facebook.com/v2.6/dialog/oauth/read
      ensure_valid_route(url: redirect_handler.return_to) unless session[:return_to]

      redirect_to session[:return_to], flash: redirect_handler.flash
      session.delete(:return_to)
    else
      redirect_to root_path, alert: "User account not found."
    end
  end

  def failure
    redirect_to @event ? trip_view_path(@event) : root_path, alert: 'Could not authenticate you from Facebook'
  end

  private

  def set_path_and_event
    url = request.env['omniauth.params']['url']
    @path   = begin
                url ? Rails.application.routes.recognize_path(url) : Hash.new
              rescue
                Hash.new
              end
    @event  = Event.find_by(slug: @path.fetch(:slug, "")).try(:decorate)
  end

  def user_role
    @user.joiner_and_organizer? ? "organizer" : @user.role
  end

  def ensure_valid_route(url:)
    begin
      Rails.application.routes.recognize_path(url)
    rescue
      session[:return_to] = root_path
    end
  end
end
