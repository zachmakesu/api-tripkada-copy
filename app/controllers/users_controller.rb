class UsersController < ApplicationController
  before_action :set_decorated_user, only: [:show, :update, :edit, :update_profile_photos, :reviews, :paginated_reviews]
  before_action :set_user, only: [:update_role, :trips, :follow, :unfollow, :paginated_trips]
  before_action :ensure_admin, only: [:index, :update_role, :segmentations, :export_segmentations, :export, :organizer_applicants], if: :user_signed_in?
  before_action :ensure_current_user, only: [:edit, :update_profile_photos]
  before_action :ensure_valid_params_type, only: [:trips, :paginated_trips]
  before_action :ensure_all_fields, only: [:update_profile_photos]
  before_action :ensure_xhr_request, only: [:paginated_trips, :paginated_reviews]
  set_tab :users, only: %w(index)
  set_tab :users_segmentation, only: :segmentations
  set_tab :organizer_applicants, only: :organizer_applicants

  def index
    @users = UserDecorator.search(params[:q]).page(params[:page]).per(10)
  end

  def update
    modified_params = user_params
    modified_params[:birthdate] = Date.strptime(user_params[:birthdate], '%m-%d-%Y') rescue nil
    modified_params[:gender] = user_params[:gender].downcase

    handler = UserHandler.new(current_user, modified_params).update_profile

    path   = begin
               url = request.referrer
               url ? Rails.application.routes.recognize_path(url) : Hash.new
             rescue
               Hash.new
             end
    event  = Event.find_by(slug: path.fetch(:slug, nil)).try(:decorate)

    options                     = Hash.new
    options[:path]              = path
    options[:event]             = event
    options[:user]              = current_user
    options[:request]           = request
    options[:app_role]          = app_role
    options[:proceed_checkout]  = params[:checkout]

    redirect_handler = OmniauthRedirectHandler.new(options).get_path
    session[:return_to] ||= if event
                              trip_checkout_path(event)
                            elsif params[:controller] == "home"
                              profile_path current_user.slug
                            else
                              redirect_handler.return_to
                            end

    respond_to do |format|
      format.html {
        if handler.response[:type]
          redirect_to session.delete(:return_to), notice: "Successfully updated!."
        else
          flash[:incomplete_info] = true
          flash[:registration] = modified_params
          redirect_to request.referrer, alert: "Error encountered!, #{handler.response[:details]}"
        end
      }
      format.mobile {
        if handler.response[:type]
          redirect_to session.delete(:return_to), notice: "Successfully updated!."
        else
          flash[:incomplete_info] = true
          flash[:registration] = modified_params
          redirect_to request.referrer, alert: "Error encountered!, #{handler.response[:details]}"
        end
      }
    end
  end

  def update_role
    update_params = role_update_params
    if params[:user][:role] == "joiner_and_organizer"
      update_params[:approved_organizer_at] =  Time.zone.now
    end
    @user.update(update_params)
    respond_to do |format|
      format.json { render json: { full_name: @user.decorate.full_name, role: @user.role.titleize } }
    end
  end

  def cities
    country = params[:country] || "PH"
    @cities = CS.states(country).flat_map{|s| CS.cities(s[0],country)}

    respond_to do |format|
      format.json { render json: @cities.sort }
    end
  end

  def export
    @users = User.all

    respond_to do |format|
      format.csv{ headers['Content-Disposition'] = "attachment; filename='Export-User.csv'" }
    end
  end

  def show
    @client = Instagram.client(:access_token => @user.instagram_access_token) if @user.has_instagram?
    @reviews = @user.received_reviews.take(3)
    @upcoming = @user.upcoming_joined_events.take(3)
    @past = @user.past_joined_events.take(3)
  end

  def trips
    trips = if @type == "joined"
              @user.joined_events
            elsif @type == "upcoming"
              @user.upcoming_organized_events
            elsif @type == "organized"
              @user.past_organized_events
            end
    @trips = EventFilter.new(trips, params).result.page(params[:page]).per(6).decorate
  end

  def paginated_trips
    trips = if @type == "joined"
              @user.joined_events
            elsif @type == "upcoming"
              @user.upcoming_organized_events
            elsif @type == "organized"
              @user.past_organized_events
            end
    @trips = EventFilter.new(trips, params).result.page(params[:page]).per(6).decorate
    fresh_when(@trips)
  end

  def reviews
    @reviews = Kaminari.paginate_array(@user.received_reviews).page(params[:page]).per(6)
  end

  def paginated_reviews
    @reviews = Kaminari.paginate_array(@user.received_reviews).page(params[:page]).per(6)
    fresh_when(@reviews)
  end

  def review
    reviewable = User.find_by(slug: params[:slug]).decorate
    handler = ReviewHandler.new(current_user, review_params.merge(uid: reviewable.uid, platform: "web"))
    if reviewable.joiner?
      handler.review_joiner
    else
      handler.review_organizer
    end

    if handler.response[:success]
      redirect_to profile_path(reviewable.slug), notice: "Successfully reviewed '#{reviewable.decorate.full_name}' "
    else
      redirect_to profile_path(reviewable.slug), alert: "Oooops! #{handler.response[:details]}"
    end
  end

  def edit
  end

  def update_profile_photos
    mod_params = {}
    mod_params[:avatar] = profile_avatar_params[:avatar]
    mod_params[:cover_photo] = cover_photo_params[:cover_photo]
    mod_params[:profile_photos] = profile_photos_params[:profile_photos]
    handler = UpdateProfilePhotos.call(user: @user, params: mod_params)

    update
  end

  def export_segmentations
    @users = User.most_active_joiners.map(&:decorate)

    respond_to do |format|
      format.csv{ headers['Content-Disposition'] = "attachment; filename='Export-Segmentation.csv'" }
    end
  end

  def current_user_uid
    render json: { signed_in: user_signed_in?, uid: current_user.try(:uid) }
  end

  def segmentations
    @most_active_joiners = Kaminari.paginate_array(
      UserDecorator.search_in_active_joiners(params[:q]).map(&:decorate)
    ).page(params[:page]).per(10)
  end

  def follow
    handler = FollowHandler.new(current_user.uid,@user.uid).follow

    if handler.response[:success]
      redirect_to profile_path(@user.slug), notice: "Successfully followed #{@user.full_name}."
    else
      redirect_to profile_path(@user.slug), alert: "Ooops! #{handler.response[:details]}"
    end
  end

  def unfollow
    handler = FollowHandler.new(current_user.uid,@user.uid).unfollow

    if handler.response[:success]
      redirect_to profile_path(@user.slug), notice: "Successfully unfollowed #{@user.full_name}."
    else
      redirect_to profile_path(@user.slug), alert: "Ooops! #{handler.response[:details]}"
    end
  end

  def organizer_applicants
    applicants = User.organizer_applicants.sorted_organizer_applicants
    @users = User.search(collection: applicants, query: params[:q]).page(params[:page]).per(10)
    respond_to do |format|
      format.html
      format.csv do
        export_headers("OrganizerApplicants")
        self.response_body = export_for_organizer(applicants)
      end
    end
  end

  def export_all_organizers
    respond_to do |format|
      format.csv do
        organizers = User.joiner_and_organizer
        export_headers("TripkadaOrganizers")
        self.response_body = export_for_organizer(organizers)
      end
    end
  end

  def user_info
    @user_info = User.find_by(id: params[:user])
  end

  def export_stats_organizer
    respond_to do |format|
      format.csv do
        organizers = User.find(params[:user])
        export_headers("#{organizers.first_name}'s Statistics")
        self.response_body = export_for_stats_organizer(organizers)
      end
    end
  end

  private

  def export_for_organizer(entries)
    header = ["First Name", "Last Name", "Mobile", "Email"].to_csv
    Enumerator.new do |row|
      entries.each_with_index do |x, y|
        row << header if y == 0
        row << [x.first_name, x.last_name, x.mobile, x.email].to_csv
      end
    end
  end

  def export_for_stats_organizer(entry)
    organizer = entry
    members = organizer.organized_events_members.where.not(id: organizer.id).ids
    returnee = members.group_by(&:itself).select { |k, v| v.count > 1 }
    canceled_trips = organizer.organized_events.where.not(deleted_at: nil).count
    new_joiners = members.uniq.count - returnee.count
    date = organizer.created_at.strftime("%B %d %Y")
    joiners = []
    Enumerator.new do |row|
      row << ["#{organizer.first_name} #{organizer.last_name} #{organizer.mobile} #{organizer.email}"].to_csv
      if organizer.approved_organizer_at.nil?
        row << [" - Joined Tripkada since #{date}"].to_csv
      else
        row << ["Organizer since
          #{organizer.approved_organizer_at.strftime("%B %d %Y")}"].to_csv
      end
      row << [" - Returning joiners: #{returnee.count}"].to_csv
      row << [" - New joiners: #{new_joiners}"].to_csv
      row << [" - Canceled Trips: #{canceled_trips}"].to_csv
      row << [" - Total Bookings: #{organizer.total_bookings}"].to_csv
      row << [" - Frequently publish trips at: #{organizer.publish_trips_at}"].to_csv
      device = organizer.device_used_for_creating_trips
      device ||= "desktop"
      row << [" - Platform most used for publishing: #{device.capitalize}"].to_csv
      row << [" - Organized Trips: #{organizer.organized_events.count}"].to_csv
      organizer.organized_events.each_with_index do |x, y|
        row << ["#{y + 1}: #{x.name}"].to_csv
        x.members.each do |a|
          joiners << "#{a.first_name} #{a.last_name}" unless a == organizer
        end
      row << ["Joiners: #{joiners.size} #{joiners}"].to_csv
      joiners.clear
      end
    end
  end

  def export_headers(label)
    filename = "#{label}-#{Time.zone.today}"
    response.headers["Cache-Control"] = "no-cache"
    response.headers["X-Accel-Buffering"] = "no"
    response.headers["Content-Type"] ||= "text/csv"
    response.headers["Content-Disposition"] =
      "attachment; filename=#{filename}.csv"
    response.headers["Content-Transfer-Encoding"] = "binary"
  end

  def ensure_current_user
    redirect_to dynamic_path_for(@user), alert: "Unauthorized access." unless @user == current_user
  end

  def set_user
    @user = params[:slug] ? User.find_by(slug: params[:slug]) : User.find(params[:id])
    redirect_to trip_view_path, alert: "User not found." if @user.nil?
  end

  def set_decorated_user
    @user = User.find_by(slug: params[:slug])&.decorate
    redirect_to trip_view_path, alert: "User not found." if @user.nil?
  end

  def user_params
    permit_params = :username, :first_name, :last_name, :gender, :birthdate,
                    :country, :city, :address, :mobile, :email,
                    :emergency_contact_name, :emergency_contact_number,
                    :tell_us_about_yourself
    params.require(:user).permit(permit_params)
  end

  def profile_avatar_params
    params.require(:user).permit(:avatar => [:image])
  end

  def cover_photo_params
    params.require(:user).permit(:cover_photo => [:image])
  end

  def profile_photos_params
    params.require(:user).permit(:profile_photos => [:label, :photo, :delete, :edit, :cover])
  end

  def review_params
    params[:review][:rating] = round(params[:review][:rating].to_f)
    params.require(:review).permit(:message, :rating).merge(reviewer: current_user)
  end

  def role_params
    params.require(:user).permit(:role)
  end

  def ensure_all_fields
    redirect_to profile_path(@user), alert: "All fields are required." unless user_params.values.all?(&:present?)
  end

  def round(x)
    num = (x * 2).round / 2.0
    num.to_i == num ? num.to_i : num
  end

  def ensure_valid_params_type
    @type = params[:type]
    redirect_to profile_path(@user), alert: "Invalid URL." unless %w{ joined organized upcoming }.include?(@type)
  end

  def role_update_params
    {
      role: params[:user][:role],
      approved_organizer_at: nil
   }
end
end
