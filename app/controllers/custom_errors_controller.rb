class CustomErrorsController < ApplicationController
  def page_not_found
    url = request.original_url
    whitelist_keywords = %w[ system png ico xml ]
    whitelisted = whitelist_keywords.any? { |key| url.include? key }
    Logger::LogInvalidRequest.call(referrer: request.referrer, method: request.method, url: request.original_url, params: request.params) unless whitelisted && Rails.env.staging?
    render(:status => 404)
  end
end
