class CustomNotificationMessagesController < AdminController
  SEGMENT_OPTIONS = %w{ lead one_off repeat loyal active at_risk lapsed joiner joiner_and_organizer all}
  set_tab :notifications, only: %w(index)

  before_action :set_notifications, only: [:show, :edit, :update, :destroy]
  
  def index
    @notifications = CustomNotificationMessageDecorator.search(params[:q]).page(params[:page]).per(10)
    @segment_options_booking_count = %w{ lead one_off repeat loyal }
    @segment_options_booking_duration = %w{ active at_risk lapsed }
  end

  def new
    @notification = CustomNotificationMessage.new.decorate
  end

  def create
    @notification = CustomNotificationMessage.new(notification_params)
    if @notification.save
      redirect_to custom_notification_messages_path, notice: "Successfully Created"
    else
      render :new
      flash[:alert] = @notification.errors.full_messages.to_sentence
    end
  end
  
  def edit
  end

  def update
    mod_notification_params = notification_params
    mod_notification_params[:image] = nil if notification_params[:image].nil? && params[:image_file_name] == ""
    if @notification.update(mod_notification_params)
      redirect_to custom_notification_messages_path, notice: 'Notification was successfully updated.'
    else
      render :edit
      flash[:alert] = @notification.errors.full_messages.to_sentence
    end
  end

  def deliver
    notification = {}
    if SEGMENT_OPTIONS.include? params[:options]
      AdminNotifications::CustomNotificationWorker.perform_async(params[:id], params[:options])
      notification[:notice] = "Notification Successfully Delivered"
    else
      notification[:alert] = "Please Provide Valid Option"
    end
    redirect_to custom_notification_messages_path, notification
  end

  def destroy
    @notification.destroy
    redirect_to custom_notification_messages_path, notice: 'Notification was successfully deleted.'
  end

  private

  def set_notifications
    @notification = CustomNotificationMessage.find(params[:id])&.decorate
  end

  def notification_params
    params.require(:custom_notification_message).permit(:message, :created_by, :image)
  end
end
