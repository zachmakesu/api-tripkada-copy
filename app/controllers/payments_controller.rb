class PaymentsController < ApplicationController
  include JsonParser
  include FormatHelper
  include EmailValidator
  include DeviceValidator

  before_action :ensure_organizer_or_admin, only: [:index, :update], if: :user_signed_in?
  before_action :set_payment, only: [:show, :edit, :update, :destroy]
  before_action :set_event, only: [:paypal_transaction, :paypal_payment_verification]
  before_action :ensure_sufficient_slots_and_valid_email,
    only: :paypal_transaction
  before_action :ensure_admin, only: :export_all_payments

  skip_before_action :verify_authenticity_token, only: :dragonpay_postback
  set_tab :payments

  # GET /payments
  def index
    @payments = PaymentDecorator.search(params[:q])
    .where.not(mode: Payment.modes[:organizer],
    status: Payment.statuses[:expired]).page(params[:page]).per(10)
  end

  # GET /payments/1
  def show
  end

  # GET /payments/new
  def new
    @payment = Payment.new
  end

  # GET /payments/1/edit
  def edit
  end

  # POST /payments
  # def create
  #   @payment = Payment.new(payment_params)

  #   if @payment.save
  #     redirect_to @payment, notice: 'Payment was successfully created.'
  #   else
  #     render :new
  #   end
  # end

  # PATCH/PUT /payments/1
  def update
    @payment.update(payment_params)
    request.format = "json"
    respond_to do |format|
      format.js { render nothing: true }
      format.json { render json: {amount_paid: @payment.amount_paid} }
    end
  end

  # DELETE /payments/1
  def destroy
    @payment.destroy
    redirect_to payments_url, notice: 'Payment was successfully destroyed.'
  end

  def paypal_transaction
    @mod_params = {}
    @mod_params[:code] = params[:code]
    @mod_params[:user_id] = current_user.id

    check_for_invited_joiners

    handler = PaypalHandler.new(@mod_params, @event).create_instant_payment

    if handler.response[:type]
      redirect_to handler.response[:details].links.find{|v| v.method == "REDIRECT" }.href
    else
      redirect_to trip_checkout_path(@event), alert: "Ooops!, #{handler.response[:details]}"
    end
  end

  def paypal_payment_verification
    mod_params = params
    mod_params[:promo_code_id] = PromoCode.find_by_code(params[:code]).try(:id)
    mod_params[:user_id] = current_user.id
    mod_params[:from_controller] = true
    mod_params[:platform] = check_device_platform(
      user_agent: request.user_agent,
      type: "controller"
    )

    handler = PaypalHandler.new(mod_params, @event).execute_payment

    if handler.response[:type]
      set_url_options(response: handler.response)
    else
      redirect_to trip_view_path(@event.slug), alert: "Ooops!, #{handler.response[:details]}"
    end
  end

  def dragonpay_callback
    log = DragonpayTransactionLog.where(transaction_id: params[:txnid])&.last
    event = Event.find_by(id: params[:param2])
    if log
      notification_message = {}
      if log.status == "approved"
        notification_message[:share_trip] = true
      else
        notification_message[:alert] = "Transaction status is #{log.status}, Please Complete Your Transaction!"
      end
      membership = log.payment&.event_membership
      redirect_to trip_view_path(event, membership_id: membership&.id), notification_message
    else
      redirect_to trip_view_path(event), alert: "Ooops. Something went wrong."
    end
  end

  def dragonpay_postback
    Rails.logger.info("#{DateTime.now} : DRAGONPAY-REQUEST-#{params[:txnid]} || #{params}")

    @mod_params = params
    @mod_params[:mode] = "dragonpay"
    @mod_params[:made] = "downpayment"
    joiner_uid = params[:param1]
    event_id = params[:param2]
    joiner = User.find_by(uid: joiner_uid)
    event = Event.upcoming.not_deleted.find_by(id: event_id)

    recorded_transaction = TransactionDataStore.find_by(transaction_id: params[:txnid])

    code = recorded_transaction&.data&.fetch("code", "")
    invited_joiner_names = recorded_transaction&.data&.fetch("invited_joiner_names", nil)
    invited_joiner_emails = recorded_transaction&.data&.fetch("invited_joiner_emails", nil)
    @mod_params[:total_amount] = recorded_transaction&.data&.fetch("total_amount", nil)

    @mod_params[:user_agent] = request.user_agent
    @mod_params[:code] = code
    @mod_params[:invited_joiner_names] = invited_joiner_names || "undefined"
    @mod_params[:invited_joiner_emails] = invited_joiner_emails || "undefined"
    @mod_params[:type] = "controller"


    Payments::Dragonpay::CreateMembershipAndLogPayment.call(
      event: event, params: @mod_params, joiner: joiner
    )
    render plain: "result=OK"
  end

  def export_all_payments
    respond_to do |format|
      format.csv do
        payments = Payment.includes(event_membership: %i[event member]).decorate
        export_headers("Payments")
        self.response_body = export_for_payments(payments)
      end
    end
  end

  def mark_dragonpay_as_expired
    current_count = Payment.expired.count
    DragonpayTransactionLog.mark_expired_payments!
    updated_count = Payment.expired.count - current_count
    redirect_to payments_path, notice:
    "#{updated_count} Dragonpay Payments has expired"
  end

  private

  def export_for_payments(payments)
    header = ["Event Name", "Event Date", "Transaction ID", "Payer", "Mobile",
              "Payment Mode", "Amount Paid", "Payment Made", "Date of Payment",
              "Promo Code", "Device Platform", "Status"].to_csv
    Enumerator.new do |row|
      payments.each_with_index do |x, y|
        row << header if y == 0
        row << [x.event.name, month_day_year_format(x.event.start_at),
                x.transaction_id, x.member.full_name, x.member.mobile, x.mode,
                x.amount_paid, x.made, month_day_year_format(x.created_at),
                x.promo_code_in_words, x.device_platform, x.status].to_csv
      end
    end
  end

  def export_headers(label)
    # Ref: https://medium.com/table-xi/stream-csv-files-in-rails-because-you-can-46c212159ab7
    filename = "#{label}-#{Time.zone.today}"
    response.headers["Cache-Control"] = "no-cache"
    response.headers["X-Accel-Buffering"] = "no"
    response.headers["Content-Type"] ||= "text/csv"
    response.headers["Content-Disposition"] =
      "attachment; filename=#{filename}.csv"
    response.headers["Content-Transfer-Encoding"] = "binary"
  end

  def set_event
    @event = Event.upcoming.find_by(id: params[:event_id])
    redirect_to url_for(controller: :event, action: :index), alert: "Trip not found." if @event.nil?
  end
  # Use callbacks to share common setup or constraints between actions.
  def set_payment
    @payment = Payment.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def payment_params
    params.require(:payment).permit(:status,:amount_paid)
  end

  def set_url_options(response:)
    if params[:invited_joiner_names].present? && params[:invited_joiner_names] != "undefined"
      invited_joiners = params[:invited_joiner_names].join(',').titleize
      redirect_to trip_view_path(@event, membership_id: response[:membership].id), share_trip: true, notice: "Invited Joiners #{invited_joiners} Successfully Added"
    else
      redirect_to trip_view_path(@event, membership_id: response[:membership].id), share_trip: true
    end
  end

  def ensure_organizer_or_admin
    redirect_to dynamic_path_for(current_user), alert: "Unauthorized access!" if current_user.joiner?
  end
end
