class AdminController < ApplicationController
  layout 'application'
  before_filter :ensure_admin
  set_tab :events, only: %w(index event)

  def index
    @events = EventDecorator.search(params[:q]).object.page(params[:page]).per(25)
  end

  def event
    @event = Event.not_deleted.friendly.find_by(slug: params[:slug])&.decorate
    @members = @event&.object&.event_memberships
               &.approved_joiners_except_invitee&.decorate
    redirect_to root_url, alert: "Event not found." if @event.nil?
  end

  def remove_joiner
    membership = EventMembership.find_by(id: params[:id])
    event = membership.event
    membership.destroy
    redirect_to get_event_path(event.slug), notice: "joiner #{membership.member.decorate.full_name} was successfully removed from trip #{event.name}."
  end

  def make_featured
    event = Event.find(params[:id])
    if params[:checked] == "true"
      message = event.feature! ? "Successfully added to featured trips" : event.errors.full_messages.to_sentence
      status = event.feature! ? :created : :unprocessable_entity
      render json: { message: message }, status: status
    else
      message = event.unfeature! ? "Successfully removed from featured trips" : event.errors.full_messages.to_sentence
      status = event.unfeature! ? :created : :unprocessable_entity
      render json: { message: message }, status: status
    end
  end

end
