class PromoCodesController < ApplicationController
  before_action :ensure_admin, except: [:validate], if: :user_signed_in?
  before_action :set_promo_code, only: [:show, :edit, :update, :destroy]
  set_tab :promo_codes

  # GET /promo_codes
  def index
    @promo_codes = PromoCodeDecorator.search(params[:q]).page(params[:page]).per(10)
  end

  # GET /promo_codes/1
  def show
    @memberships = EventMembership.includes(:member,:event,:payment).where(id: @promo_code.event_memberships.map(&:id)).decorate
  end

  # GET /promo_codes/new
  def new
    @promo_code = PromoCode.new
  end

  # GET /promo_codes/1/edit
  def edit
  end

  # POST /promo_codes
  def create
    @promo_code = PromoCode.new(promo_code_params)

    if @promo_code.save
      redirect_to @promo_code, notice: 'Promo code was successfully created.'
    else
      render :new
    end
  end

  def import
    errors = []
    file = params.fetch("file", nil)
    ActiveRecord::Base.transaction do
      CSV.foreach(file.path, :headers => true) do |row|
        promo = PromoCode.corporate.new(row.to_hash)
        (errors << promo.errors.full_messages.to_sentence) unless promo.save
      end
    end
    if errors.empty?
      redirect_to promo_codes_path, notice:
        "Promo code was successfully imported."
    else
      redirect_to promo_codes_path, alert:
        "Error!, #{errors.flatten.to_sentence}"
    end
  end

  # PATCH/PUT /promo_codes/1
  def update
    if @promo_code.update(promo_code_params)
      redirect_to @promo_code, notice: 'Promo code was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /promo_codes/1
  def destroy
    if @promo_code.payments.blank?
      @promo_code.destroy
      redirect_to promo_codes_url, notice: 'Promo code was successfully deleted.'
    else
      redirect_to promo_codes_url, alert: 'Promo code cannot be deleted because of its record association to payment this serve as record, but you can change it to inactive.'
    end
  end

  def validate
    code = PromoCode::ValidateByCategory.call(code: params[:code], user: current_user)
    result = {}
    if code.success?
      promo_code = code.promo_code
      result[:response] = code.is_usable || code.belongs_to_user
      result[:value] = promo_code.credit? ? promo_code.available_credits : code.promo_code.value
    else
      result[:response] = false
    end
    respond_to do |format|
      format.mobile { render json: result }
      format.json { render json: result }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_promo_code
    @promo_code = PromoCode.find(params[:id]).decorate
  end

  # Only allow a trusted parameter "white list" through.
  def promo_code_params
    params.require(:promo_code).permit(:code, :value, :usage_limit, :active)
  end
end
