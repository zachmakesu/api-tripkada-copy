class EventController < ApplicationController
  include JsonParser
  include NoShow
  include EmailValidator
  include DeviceValidator

  MEMBERS_PER_PAGE = 20

  before_action :set_event, only: [:page, :show, :edit, :duplicate, :update, :destroy, :checkout, :payment_details, :messenger_notification, :invited_payment_details, :join, :join_waitlist, :join_using_promo_code, :joiners, :dragonpay]
  before_action :set_categories, only: [:index, :edit, :new, :duplicate]
  before_action :set_destinations, only: [:index, :edit, :new, :duplicate]
  before_action :set_mod_params, only: [:create, :update]
  before_action :set_ajax_params, only: [:index]
  before_action :ensure_organizer_or_joiner, only: [:index, :page, :checkout, :payment_details, :join, :messenger_notification]
  before_action :ensure_organizer, only: [:show, :new, :duplicate, :create]
  before_action :ensure_organizer_or_admin, only: [:edit, :update, :destroy]
  before_action :ensure_payable_event, only: [:checkout, :payment_details, :join]
  before_action :ensure_xhr_request, only: [:paginated_page, :joiners]
  before_action :log_trip_visit, only: :page
  before_action :ensure_sufficient_slots_and_valid_email, only: :payment_details

  after_action :set_session_trip_slug, only: :page
  skip_before_action :clear_session_trip_slug!, only: :page

  set_tab :my_trips, only: :my_trips
  set_tab :create_trip, only: :new
  set_tab :trips_page, only: [:index, :page, :checkout, :payment_details, :invited_payment_details]

  def index
    @events = if params[:search_trips].blank?
                EventFilter.new(Event.upcoming.not_deleted, params).result
                  .page(params[:page]).per(6).decorate
              else
                Event.upcoming.not_deleted.search(query: params[:search_trips])
                  .page(params[:page]).per(6).decorate
              end
    @first_upcoming_event = Event.first_upcoming_event.decorate
    @last_upcoming_event = Event.last_upcoming_event.decorate

    @newest_upcoming_events = EventDecorator.newest_upcoming_trips
    fresh_when(@events)
  end

  def paginated_page
    @events = EventFilter.new(Event.upcoming.not_deleted, params).result
                         .page(params[:page]).per(6).decorate
    fresh_when(@events)
  end

  def show
    @event = @event.decorate
  end

  def new
    @event = Event.new.decorate
    @original_event = @event.decorate
    @action = create_trip_path
    @method = :post
  end

  def edit
    @original_event = @event.decorate
    @event = @event.decorate
    @action = organizer_trips_path(@event)
    @method = :put
  end

  def duplicate
    @original_event = @event.decorate
    @event = @event.deep_clone(include: [:itineraries, :categories, :destinations]).decorate
    @action = create_trip_path
    @method = :post
  end

  def create
    handler = Event::CreateTrip.call(organizer: current_user, params: @mod_params)

    if handler.success?
      redirect_to trip_view_path(handler.event), notice: "Successfully created '#{handler.event.name}' "
    else
      redirect_to my_trips_path(app_role), alert: "Oooops!, #{handler.message}"
    end
  end

  def update
    handler = EventHandler.new(current_user,@mod_params).update
    successful_update_path = current_user.admin? ? get_event_path(@event) : trip_view_path(handler.response[:details])
    if handler.response[:type]
      redirect_to successful_update_path, notice: "Successfully updated '#{handler.response[:details].name}' "
    else
      redirect_to organizer_trip_edit_path(@event), alert: "Oooops!, #{handler.response[:details]}"
    end
  end

  def destroy
    @event.soft_delete
    AdminNotifications::EventMailerWorker.perform_async(
      @event.id,
      "canceled_trip"
    ) unless current_user.admin?
    message = "Trip #{@event.name} was successfully deleted."
    path = current_user&.admin? ? admin_path : my_trips_path(app_role)
    redirect_to path, notice: message
  end

  def page
    @reviews = @event.received_reviews.page(params[:page]).per(5)
    @event = @event.decorate
    @valid_memberships = @event.approved_event_memberships.decorate
    @valid_members = @event.approved_members.page(params[:page]).per(20).decorate
    @valid_members_count = @valid_memberships.count
    @members_per_page = MEMBERS_PER_PAGE
    @brand_sponsor_logo = @event.brand_sponsor_logo_url
    @membership = EventMembership.approved.find_by(id: params[:membership_id])
  end

  def checkout
    if current_user.expired_event_memberships.any? && @event.expired_event_memberships.any?
      if @event.has_expired_membership_for?(current_user)
        @event = @event.decorate
        @booking_fee = BookingFeeDecorator.latest_fee
      else
        redirect_to trip_view_path(@event)
      end
    else
      if @event.joinable_by?(current_user)
        @event = @event.decorate
        @booking_fee = BookingFeeDecorator.latest_fee
      else
        redirect_to trip_view_path(@event)
      end
    end
  end

  def waitlist_checkout
    if waitlist = current_user.waitlists.find_by(id: params[:id])
      redirect_to trip_checkout_path(waitlist.event)
    else
      redirect_to trips_path, alert: "Waitlist not found."
    end
  end

  def payment_details
    redirect_to trip_view_path(@event), alert: "Invalid Url." unless params[:mode].present? && Payment.modes.include?(params[:mode])
  end

  def join
    if params[:mode] == "tagcash"
      endpoint = URI.parse("https://api.tagcash.com/wallet/transfer")
      body = {
        access_token: current_user.providers.tagcash.last.token,
        from_wallet_id: 1,
        to_id: ENV["TRIPKADA_MERCHANT_ID"],
        to_type: ENV["TRIPKADA_TYPE"],
        amount: params[:amount],
        narration: params[:notes]
      }

      response = Net::HTTP.post_form(endpoint, body)
      status = JSON.parse(response.body)["status"]
      if (status == "failed")
        redirect_to trip_view_path(@event), alert: JSON.parse(response.body)["error"].gsub(/_/, " ")
        return
      end
    end
    @mod_params = Hash.new
    @mod_params[:id] = @event.id
    @mod_params[:promo_code_id] = PromoCode.find_by_code(params[:code]).try(:id)
    @mod_params[:mode] = params[:mode]
    @mod_params[:made] = "downpayment"
    @mod_params[:photos] = [params[:payment_photos]]
    @mod_params[:from_controller] = true
    @mod_params[:platform] = check_device_platform(
      user_agent: request.user_agent,
      type: "controller"
    )

    check_for_invited_joiners

    handler = EventHandler.new(current_user, @mod_params).join

    if handler.response[:type]
      set_url_options(response: handler.response)
    else
      redirect_to trip_checkout_path(@event), alert: "Ooops!, #{handler.response[:details]}"
    end
  end

  def join_waitlist
    handler = JoinWaitlist.call(user: current_user, event: @event&.decorate)

    if handler.success?
      redirect_to trip_view_path(@event), notice: "Successfully joined the waitlist."
    else
      redirect_to trip_view_path(@event), alert: "Ooops! #{handler.message}"
    end
  end

  def my_trips
    @event_type = Event::TYPE.new(params[:type])
    events = joiner_mode? ? trips_for_joiner : trips_for_organizer
    @events = EventFilter.new(events, params).result.page(params[:page]).per(6).decorate
  end

  def invited_payment_details
    redirect_to trip_view_path(@event), alert: "Already an Event member" if current_user.present? && @event.members.include?(current_user)
    redirect_to trip_view_path(@event), alert: "Invalid Url." unless params[:mode].present? && Payment.modes.include?(params[:mode])
  end

  def join_using_promo_code
    @mod_params = Hash.new
    @mod_params[:id] = @event.id
    @mod_params[:promo_code_id] = PromoCode.find_by_code(params.fetch(:code, "")).try(:id)
    @mod_params[:mode] = params.fetch(:mode, nil)
    @mod_params[:made] = @mod_params[:mode] == "free_trip" ? "fullpayment" : "downpayment"
    @mod_params[:from_controller] = true

    check_for_invited_joiners

    handler = EventHandler.new(current_user, @mod_params).join

    if handler.response[:type]
      set_url_options(response: handler.response)
    else
      redirect_to trip_checkout_path(@event), alert: "Ooops!, #{handler.response[:details]}"
    end
  end

  def joiners
    @members_per_page = MEMBERS_PER_PAGE
    @valid_members = @event.approved_members.page(params[:page]).per(@members_per_page).decorate
    respond_to do |format|
      format.mobile { render nothing: true }
      format.js { render layout: false }
    end
  end

  def dragonpay
    @mod_params = params
    check_for_invited_joiners
    service = Payments::Dragonpay::GenerateClientRequestURL.call(
      event: @event, params: @mod_params, joiner: current_user
    )
    if service.success?
      @mod_params[:total_amount] = service.total_amount

      transaction_data = TransactionDataStore::RecordTransactionData.call(
        transaction_id: service.generated_transaction_id, params: @mod_params
      )
      if transaction_data.success?
        redirect_to service.generated_request_url
      else
        redirect_to trip_checkout_path(@event), alert: "Ooops!, #{transaction_data.message}"
      end
    else
      redirect_to trip_checkout_path(@event), alert: "Ooops!, #{service.message}"
    end
  end

  def validate_invitee_email
    # https://github.com/afair/email_address
    validator = validate_email(email: params[:email])
    render json: validator
  end

  private
  def set_event
    @event = Event.not_deleted.friendly.find_by(slug: params[:slug])
    redirect_to url_for(controller: :event, action: :index), alert: "Trip not found." if @event.nil?
  end

  def set_categories
    @categories = Category.order(name: :asc)
  end

  def set_destinations
    @destinations = Destination.order(name: :asc)
  end

  def set_mod_params
    @mod_params = params[:event]
    @mod_params[:category_ids] = Category.where(name: params[:event].fetch(:categories){ "" }.split(";")).ids
    @mod_params[:destination_ids] = Destination.where(name: params[:event].fetch(:destinations){ "" }.split(";")).ids
    @mod_params[:booking_fee] = BookingFee.last
    @mod_params[:id] = @event.id if @event
    @mod_params[:start_at] = begin
                               DateTime.parse(@mod_params[:start_at])
                             rescue ArgumentError
                             end
    @mod_params[:end_at] = begin
                             DateTime.parse(@mod_params[:end_at])
                           rescue ArgumentError
                           end
    @mod_params[:from_controller] = true
  end

  def set_ajax_params
    @min_cost = Event::MINCOST
    @max_cost = Event::MAXCOST
    @min_days = Event::MINDAYS
    @max_days = Event::MAXDAYS

    @params_min_cost = params.fetch(:min_cost){ @min_cost }.to_i
    @params_max_cost = params.fetch(:max_cost){ @max_cost }.to_i
    @params_min_days = params.fetch(:min_days){ @min_days }.to_i
    @params_max_days = params.fetch(:max_days){ @max_days }.to_i
    @params_category_ids = params.fetch(:category_ids){ [] }.to_a
    @params_destination_ids = params.fetch(:destination_ids){ [] }.to_a
  end

  def log_trip_visit
    @event.increment!(:number_of_views) if session[:trip_slug] != @event.slug || session[:trip_slug].blank?
    notify_trip_views_count(event: @event)
  end

  def notify_trip_views_count(event:)
    case event.number_of_views
    when 100, 500, 1000, 5000
      Notifications::TripViewsWorker.perform_async(event.id)
    end
  end

  def set_session_trip_slug
    session[:trip_slug] = @event.slug
  end

  def trips_for_joiner
    if type_is_upcoming?
      current_user.upcoming_joined_events
    elsif @event_type.bookmarks?
      current_user.upcoming_bookmarked_events
    else
      current_user.past_joined_events
    end
  end

  def trips_for_organizer
    if type_is_upcoming?
      current_user.upcoming_organized_events
    elsif @event_type.bookmarks?
      current_user.upcoming_bookmarked_events
    else
      current_user.past_organized_events
    end
  end

  def ensure_payable_event
    waitlist = @event.waitlists.find_by(user_id: current_user)
    if waitlist&.denied?
      redirect_to trip_view_path(@event), alert: "Waitlist request has been denied."
    elsif waitlist&.pending?
      redirect_to trip_view_path(@event), alert: "Waitlist request is still pending."
    elsif waitlist&.paid?
      redirect_to trip_view_path(@event), alert: "Already joined this trip."
    elsif @event.decorate.is_past? || (@event.full? && !waitlist&.approved?)
      redirect_to trip_view_path(@event), alert: "Trip #{@event.name} is unavailable for booking."
    end
  end

  def type_is_upcoming?
    @event_type.params_type_not_present? || @event_type.upcoming? || @event_type.invalid_params_type?
  end

  def set_url_options(response:)
    if check_for_invited_joiners
      invited_joiners = params[:invited_joiner_names].titleize
      redirect_to trip_view_path(@event, membership_id: response[:membership].id), share_trip: true, notice: "Invited Joiners #{invited_joiners} Successfully Added"
    else
      redirect_to trip_view_path(@event, membership_id: response[:membership].id), share_trip: true
    end
  end
end
