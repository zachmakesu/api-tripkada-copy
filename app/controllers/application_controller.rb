class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :prepare_for_mobile
  before_action :clear_session_trip_slug!
  add_flash_types :share_trip

  private

  def mobile_device?
    request.user_agent =~ /Mobile|webOS/
  end
  helper_method :mobile_device?

  def prepare_for_mobile
    request.format = :mobile if mobile_device? && !request.format.js?
  end

  def ensure_admin
    redirect_to dynamic_path_for(current_user), alert: "Unauthorized access!" unless current_user.admin?
  end

  def ensure_joiner
    redirect_to dynamic_path_for(current_user), alert: "Unauthorized access!" if current_user.admin? || organizer_mode?
  end

  def ensure_organizer
    redirect_to dynamic_path_for(current_user), alert: "Unauthorized access!" unless current_user.joiner_and_organizer?
  end

  def ensure_organizer_or_joiner
    redirect_to dynamic_path_for(current_user), alert: "Unauthorized access!" if current_user&.admin?
  end

  def ensure_organizer_or_admin
    redirect_to dynamic_path_for(current_user), alert: "Unauthorized access!" unless current_user.admin? || current_user.joiner_and_organizer?
  end

  def dynamic_path_for(user)
    case user.role
    when "admin"                then admin_path
    when "joiner_and_organizer" then joiner_mode? ? trips_path : my_trips_path(app_role)
    when "joiner"               then user.sign_in_count == 1 ? root_path : trips_path
    end
  end

  def after_sign_in_path_for(resource)
    dynamic_path_for(current_user) || stored_location_for(resource) || root_path
  end

  def app_role
    session[:app_role] || default_role
  end
  helper_method :app_role

  def organizer_mode?
    app_role == "organizer"
  end
  helper_method :organizer_mode?

  def joiner_mode?
    app_role == "joiner"
  end
  helper_method :joiner_mode?

  def clear_session_trip_slug!
    session.delete(:trip_slug)
  end

  def restrict_mobile_devices!
    redirect_to root_path, alert: "Sorry this Page can only be accessed in web browsers" if mobile_device?
  end

  def default_role
    #will work for better fix(missing required keys[:role])
    current_user&.joiner_and_organizer? ? "organizer" : "joiner"
  end

  def ensure_xhr_request
    unless request.xhr? && request.format.js?
      Rails.logger.warn "\n[#{DateTime.now}] WARN -- xhr: Invalid access!, Platform: #{browser.platform}, Browser: #{browser.name}, Version: #{browser.version}, Modern: #{browser.modern?}, Bot: #{browser.bot?}\n"
      redirect_to '/404'
    end
  end
end
