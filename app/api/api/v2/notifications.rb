module API
  module V2
    class Notifications < Grape::API

      resource :notifications do

        desc 'List of not deleted notifications'
        get do
          data = { data: current_user.notifications.includes(:notificationable).not_deleted.order_by_created_at }
          present data, with: Entities::V2::Notification::Index, current_user: current_user
        end

        desc 'List of not deleted notifications'
        post '/seen_all' do
          current_user.notifications.not_seen.update_all(seen: true)
          data = { data: current_user.notifications.includes(:notificationable).not_deleted.order_by_created_at }
          present data, with: Entities::V2::Notification::Index, current_user: current_user
        end

        desc 'Specific not deleted notification'
        get '/:id' do
          notification = current_user.notifications.includes(:notificationable).not_deleted.find(params[:id])
          data = { data: notification}
          present data, with: Entities::V2::Notification::Index, current_user: current_user
        end

        desc 'Seen a not deleted notification'
        post '/:id' do
          notification = current_user.notifications.unread.find(params[:id])
          notification.read!
          data = { data: current_user.notifications.not_deleted }
          present data, with: Entities::V2::Notification::Index, current_user: current_user
        end

        desc 'Remove a notification'
        delete '/:id' do
          notification = current_user.notifications.find(params[:id])
          notification.update(deleted_at: DateTime.now)
          data = { data: current_user.notifications.not_deleted }
          present data, with: Entities::V2::Notification::Index, current_user: current_user
        end
      end
    end
  end
end
