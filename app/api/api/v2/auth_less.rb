module API
  module V2
    class AuthLess < Grape::API

      mount API::V2::Login
      mount API::V2::AppVersions
    end
  end
end
