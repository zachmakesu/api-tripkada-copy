module API
  module V2
    class Referrals < Grape::API

      resource :referrals do
        desc 'Send Referral Invites'
        post '/send_invitations' do
          service = ::Referrals::SendInvitations.call(params: params, referrer: current_user.decorate)
          present service.mailer_response, with: Entities::V2::Referrals::InvitationResponse
        end
      end

    end
  end
end
