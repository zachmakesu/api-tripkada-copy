module API
  module V2
    class Users < Grape::API

      resource :profile do

        desc "User Profile"
        get do
          present current_user.decorate, with: Entities::V2::User::Profile, current_user: current_user
        end

        desc "Update Profile"
        put "/update" do
          handler = UserHandler.new(current_user,params).update_profile
          if handler.response[:type]
            present handler.response[:details], with: Entities::V2::User::Profile, current_user: current_user
          else
            error!({messages: handler.response[:details]},404)
          end
        end

        desc "Update profile avatar"
        put  "/update/profile_avatar" do
          handler = UserHandler.new(current_user,params).update_profile_avatar
          if handler.response[:type]
            present handler.response[:details], with: Entities::V2::User::Profile, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "Update photos base on category"
        put  "/update/:category" do
          handler = UserHandler.new(current_user,params).update_photos
          if handler.response[:type]
            present handler.response[:details], with: Entities::V2::Photo::List, category: params[:category]
          else
            error!({messages: handler.response[:details]},404)
          end
        end

        desc "Upload photos base on category"
        post  "/upload/:category" do
          handler = User::UploadPhotos.call(user: current_user,params: params)
          if handler.success?
            present current_user.decorate, with: Entities::V2::Photo::List, category: params[:category]
          else
            error!({messages: handler.message},400)
          end
        end

        desc "Delete photos base on category"
        delete  "/remove/:category" do
          handler = UserHandler.new(current_user,params).delete_photos
          if handler.response[:type]
            present handler.response[:details], with: Entities::V2::Photo::List, category: params[:category]
          else
            error!({messages: handler.response[:details]},404)
          end
        end

        desc "Create or Update Cover Photo"
        post  "/cover_photo" do
          handler = UserHandler.new(current_user,params).create_or_update_cover_photo
          if handler.response[:type]
            present handler.response[:details], with: Entities::V2::User::Profile, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "Delete Cover Photo"
        delete  "/cover_photo" do
          handler = UserHandler.new(current_user,params).delete_cover_photo
          if handler.response[:type]
            present handler.response[:details], with: Entities::V2::User::Profile, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "Upload photo"
        post "/photos" do
          handler = UserHandler.new(current_user,params).upload_photo
          if handler.response[:type]
            present handler.response[:details], with: Entities::V2::User::Profile, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "Edit photo"
        put "/photos/:photo_id" do
          handler = UserHandler.new(current_user,params).update_photo
          if handler.response[:type]
            present handler.response[:details], with: Entities::V2::User::Profile, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "Delete photo"
        delete "/photos/:photo_id" do
          handler = UserHandler.new(current_user,params).delete_photo
          if handler.response[:type]
            present handler.response[:details], with: Entities::V2::User::Profile, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "Upload certificate"
        post "/certificate" do
          handler = User::UploadCertificates.call(user: current_user,params: params)
          if handler.success?
            present current_user.decorate, with: Entities::V2::User::Profile, current_user: current_user
          else
            error!({messages: handler.message},400)
          end
        end

        desc 'Connect instagram'
        post '/connect/instagram' do
          provider = current_user.providers.build(uid: params[:uid], role: params[:role], url: params[:url], token: params[:token], provider: 'instagram')
          if provider.save
            present current_user, with: Entities::V2::Providers::SocialAccountsDataGrp, current_user: current_user
          else
            error!(provider.errors.full_messages.join(', '), 400)
          end
        end

        desc 'Update instagram provider'
        put '/connect/instagram' do
          provider = current_user.providers.find_by!(provider: 'instagram')
          if provider.update(role: params[:role], url: params[:url], token: params[:token])
            present current_user, with: Entities::V2::Providers::SocialAccountsDataGrp, current_user: current_user
          else
            error!(provider.errors.full_messages.join(', '), 400)
          end
        end

        desc 'Unlink all instagram accounts'
        delete '/connect/instagram' do
          current_user.providers.instagram.destroy_all
          present current_user, with: Entities::V2::Providers::SocialAccountsDataGrp, current_user: current_user
        end

        desc "Sync calendar"
        put '/calendar/sync'do
          current_user.sync_calendar!
          present current_user, with: Entities::V2::User::AppSettings
        end

        desc "Unsync calendar"
        put '/calendar/unsync'do
          current_user.unsync_calendar!
          present current_user, with: Entities::V2::User::AppSettings
        end

        desc "Get reviewable joined event organizers"
        get "/reviewable_organizers" do
          events = { data: current_user.reviewable_events_exclude_blacklist.decorate }
          present events, with: Entities::V2::Event::ReviewableEventAndOrganizerListing
        end

        desc "Remove Event from list of reviewable events"
        post "/reviewable_organizers/blacklist" do
          event= current_user.approved_past_joined_events.where(id: params[:id])&.last
          service = User::AddEventToReviewBlacklists.call(user: current_user, event: event)
          if service.success?
            events = { data: current_user.review_blacklist_events.decorate }
            present events, with: Entities::V2::Event::ReviewableEventAndOrganizerListing
          else
            error!( { messages: service.message } , 400)
          end
        end
      end

      resource :user do
        desc "Get your followed organizers"
        get "/following" do
          present current_user.decorate.following, with: Entities::V2::User::Index, current_user: current_user
        end

        desc "Get joiners who followed you"
        get "/followers" do
          present current_user.decorate.followers, with: Entities::V2::User::Index, current_user: current_user
        end

        desc "Get User Profile"
        get "/:uid" do
          user = User.find_by!(uid: params[:uid])
          GoogleAnalyticsApiHandler.new('Users', "Show - #{user.uid}", current_user.try(:uid), 1).track_event!
          present user.decorate, with: Entities::V2::User::Profile, current_user: current_user
        end

        desc "Follow an organizer"
        post "/:uid/follows" do
          handler = FollowHandler.new(current_user.uid,params[:uid]).follow
          if handler.response[:success]
            present handler.response[:details].decorate, with: Entities::V2::User::Profile, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "Unfollow an organizer"
        delete "/:uid/follows" do
          handler = FollowHandler.new(current_user.uid,params[:uid]).unfollow
          if handler.response[:success]
            present handler.response[:details].decorate, with: Entities::V2::User::Profile, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "List of user's joined events"
        paginate per_page: 10
        get "/:uid/joined_events/:type" do
          user = User.find_by!(uid: params[:uid])
          events = EventFilter.new(user.joined_events, params).result
          paginated_events = { data: paginate(events).decorate }
          present paginated_events, with: Entities::V2::Event::FilteredEvents, current_user: current_user, events: events
        end

        desc "Get user's sent_reviews"
        get "/:uid/sent_reviews" do
          user = User.find_by!(uid: params[:uid])
          present user.decorate, with: Entities::V2::Review::SentReviews, current_user: current_user
        end

        desc "Get user's received_reviews"
        get "/:uid/received_reviews" do
          user = User.find_by!(uid: params[:uid])
          present user.decorate, with: Entities::V2::Review::ReceivedReviews, current_user: current_user
        end

        desc "User Organized Trips"
        paginate per_page: 10
        get "/:uid/trips/:type" do
          user = User.find_by!(uid: params[:uid])
          events = EventFilter.new(user.organized_events, params).result
          paginated_events = { data: paginate(events).decorate }
          present paginated_events, with: Entities::V2::Event::FilteredEvents, current_user: current_user, events: events
        end
      end


      resource :joiner do
        desc "Create a review for the organizer"
        post "/:uid/review_organizer" do
          mod_params = params
          mod_params[:platform] = current_user.latest_device_platform
          handler = ReviewHandler.new(current_user, mod_params).review_organizer
          if handler.response[:success]
            present handler.response[:details].decorate, with: Entities::V2::Review::Details, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end
      end

      resource :organizer do
        desc "Reviewable Trips"
        get "/:uid/reviewables" do
          user = User.find_by!(uid: params[:uid])
          present user.decorate, with: Entities::V2::Event::Reviewable, current_user: current_user
        end

        desc "Organizer Reviews from trips"
        get "/:uid/reviews" do
          user = User.find_by!(uid: params[:uid])
          present user.decorate, with: Entities::V2::Review::OrganizerGet, current_user: current_user
        end

        desc "Create a review for the joiner"
        post "/:uid/review_joiner" do
          mod_params = params
          mod_params[:platform] = current_user.latest_device_platform
          handler = ReviewHandler.new(current_user, mod_params).review_joiner
          if handler.response[:success]
            present handler.response[:details].decorate, with: Entities::V2::Review::Details, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end
      end

    end
  end
end
