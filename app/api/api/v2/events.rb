module API
  module V2
    class Events < Grape::API

      resource :events do
        desc 'List of all upcoming events'
        paginate per_page: 10
        get '/upcoming' do
          events = EventFilter.new(Event.upcoming, params).result
          paginated_events = { data: paginate(events).decorate }
          present paginated_events, with: Entities::V2::Event::FilteredEvents, current_user: current_user, events: events
        end

        desc 'List of all past events'
        paginate per_page: 10
        get '/history' do
          events = EventFilter.new(Event.past.sorted_event, params).result
          paginated_events = { data: paginate(events).decorate }
          present paginated_events, with: Entities::V2::Event::FilteredEvents, current_user: current_user, events: events
        end

        desc 'List of upcoming events for organizer'
        paginate per_page: 10
        get "/organizer/upcoming" do
          events = EventFilter.new(current_user.organized_events.upcoming, params).result
          paginated_events = { data: paginate(events).decorate }
          present paginated_events, with: Entities::V2::Event::FilteredEvents, current_user: current_user, events: events
        end

        desc 'Specific detailed payment upcoming events for organizer'
        get "/organizer/upcoming/:id" do
          event = current_user.decorate.upcoming_organized_events.find(params[:id])
          present event.decorate, with: Entities::V2::Event::EventDetailsAndMembers, current_user: current_user
        end

        # desc 'Invite Joiner'
        # post "/organizer/upcoming/:id/invite_joiner" do
        #   handler = EventHandler.new(current_user,params).invite_joiner
        #   if handler.response[:type]
        #     present handler.response[:details], with: Entities::V2::Event::EventDetailsAndMembers, current_user: current_user
        #   else
        #     error!({messages: handler.response[:details]},400)
        #   end
        # end

        desc 'Invite Joiner via Email'
        post "/organizer/upcoming/:id/invite_joiner_via_email" do
          handler = EventHandler.new(current_user,params).invite_joiner_via_email
          if handler.response[:type]
            present handler.response[:details], with: Entities::V2::Event::EventDetailsAndMembers, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc 'List of past events organizer'
        paginate per_page: 10
        get "/organizer/history" do
          events = EventFilter.new(current_user.organized_events.past.sorted_event, params).result
          paginated_events = { data: paginate(events).decorate }
          present paginated_events, with: Entities::V2::Event::FilteredEvents, current_user: current_user, events: events
        end

        desc 'Specific detailed payment past events organizer'
        get "/organizer/history/:id" do
          event = current_user.decorate.past_organized_events.find(params[:id])
          present event.decorate, with: Entities::V2::Event::EventDetailsAndMembers, current_user: current_user
        end

        namespace :organizer do
          desc "Confirm Payment"
          post "/:id/member/mark_as_no_show" do
            event = current_user.organized_events.find_by!(id: params[:id])
            membership = event&.event_memberships.find_by!(id: params[:membership_id])
            mark_as_no_show = membership.mark_as_no_show!
            if membership.mark_as_no_show!
              present "Joiner Membership Successfully Marked As No Show", with: Entities::V2::GenericResponse::Message
            else
              error!({ message: "Failed to mark joiner membership as no show!"}, 400)
            end
          end

          desc "Confirm Payment"
          post "/:id/update_payment_status" do
            event = current_user.organized_events.find_by!(id: params[:id])
            membership = event&.event_memberships.find_by!(id: params[:membership_id])
            status = params[:status]
            service = Event::Organizer::UpdateJoinerPaymentStatus.call(event: event, membership: membership, status: status)

            if service.success?
              present "Payment Successfully Updated to #{status}", with: Entities::V2::GenericResponse::Message
            else
              error!({ message: service.message}, 400)
            end
          end
          resource :drafts do
            desc 'List of unpublished events for organizer'
            get do
              events = current_user.unpublished_events
              events = { data: events.decorate }
              present events, with: Entities::V2::Event::Drafts
              GoogleAnalyticsApiHandler.new('Trips', 'Create', current_user.try(:uid), 1).track_event!
            end

            desc 'Specific unpublished event for organizer'
            get "/:id" do
              event = current_user.decorate.unpublished_events.find(params[:id])
              present event.decorate, with: Entities::V2::Event::Get, current_user: current_user
            end

            desc "Publish draft event"
            put "/:id" do
              #raise authorization error
              raise Errors::AuthorizationError unless current_user.joiner_and_organizer?

              event = current_user.unpublished_events.find(params[:id])
              handler = Event::PublishTrip.call(organizer: current_user, event: event, params: params)
              if handler.success?
                GoogleAnalyticsApiHandler.new('Trips', 'Create', current_user.try(:uid), 1).track_event! unless handler.set_as_draft
                present handler.updated_event.decorate, with: Entities::V2::Event::Get, current_user: current_user
              else
                error!({ messages: handler.message }, 400)
              end
            end

            desc "Delete draft event"
            delete '/:id' do
              #raise authorization error
              raise Errors::AuthorizationError unless current_user.joiner_and_organizer?

              event = current_user.unpublished_events.find_by!(id: params[:id])
              if event.destroy
                {messages: "Successfully deleted."}
              else
                error!( { messages: event.errors.full_messages.to_sentence } , 400)
              end
            end
          end
        end

        desc 'List of upcoming events joiner'
        paginate per_page: 10
        get "/joiner/upcoming" do
          events = EventFilter.new(current_user.joined_events.upcoming, params).result
          paginated_events = { data: paginate(events).decorate }
          present paginated_events, with: Entities::V2::Event::FilteredEvents, current_user: current_user, events: events
        end

        desc 'List of past events joiner'
        paginate per_page: 10
        get "/joiner/history" do
          events = EventFilter.new(current_user.joined_events.past.sorted_event, params).result
          paginated_events = { data: paginate(events).decorate }
          present paginated_events, with: Entities::V2::Event::FilteredEvents, current_user: current_user, events: events
        end

        desc 'Events waitlist'
        get "/:id/waitlists" do
          event = Event.find(params[:id])
          present event.decorate, with: Entities::V2::Event::WaitlistMembers, event: event
        end

        desc 'New waitlist'
        post "/:id/waitlists" do
          event = Event.not_deleted.find(params[:id])&.decorate
          handler = JoinWaitlist.call(user: current_user, event: event)

          if handler.success?
            present handler.event, with: Entities::V2::Event::EventAndWaitlistDetails, current_user: current_user, event: handler.event
          else
            error!({messages: handler.message},400)
          end
        end

        desc 'Update waitlist status'
        put "/:id/waitlists" do
          handler = UpdateWaitlist.call(params: params, current_user: current_user)

          if handler.success?
            present handler.event.decorate, with: Entities::V2::Event::WaitlistMembers, event: handler.event
          else
            error!({messages: handler.message}, 400)
          end
        end

        resource :bookmarks do
          desc 'Add event to bookmarks'
          post do
            bookmark = current_user.bookmarks.build(event_id: params[:event_id])
            if bookmark.save
              BookmarkNotification.new(bookmark.id).deliver
              { messages: "Successfully added to bookmarks" }
            else
              error!({messages: "Failed to add event on your bookmarks"},400)
            end
          end

          desc 'remove event from bookmarks'
          delete do
            bookmark = current_user.bookmarks.find_by(event_id: params[:event_id])
            if bookmark && bookmark.destroy
              { messages: "Successfully removed event from bookmarks" }
            else
              error!({messages: "Failed to remove event on your bookmarks"},400)
            end
          end

          desc 'get list of bookmarked events'
          paginate per_page: 10
          get do
            events = { data: paginate(current_user.bookmarked_events).decorate }
            present events, with: Entities::V2::Event::Bookmarks, current_user: current_user
          end
        end

        desc "Create event"
        post do
          #raise authorization error
          raise Errors::AuthorizationError unless current_user.joiner_and_organizer?

          handler = Event::CreateTrip.call(organizer: current_user, params: params)
          if handler.success?
            present handler.event.decorate, with: Entities::V2::Event::Get, current_user: current_user
            GoogleAnalyticsApiHandler.new('Trips', 'Create', current_user.try(:uid), 1).track_event! if handler.event.published_at.present?
          else
            error!({ messages: handler.message }, 400)
          end
        end

        desc "Get event"
        get '/:id' do
          event = Event.not_deleted.find(params[:id])
          GoogleAnalyticsApiHandler.new('Trips', "Show - #{event.try(:id)}", current_user.try(:iud), 1).track_event!
          present event.decorate, with: Entities::V2::Event::Get, current_user: current_user
        end

        desc "Update event"
        put "/:id/update" do
          #raise authorization error
          raise Errors::AuthorizationError unless current_user.joiner_and_organizer?

          handler = EventHandler.new(current_user,params).update
          if handler.response[:type]
            present handler.response[:details], with: Entities::V2::Event::Get, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "Upload image for event"
        post "/:id/photos" do
          handler = EventHandler.new(current_user,params).upload_photo
          if handler.response[:type]
            present handler.response[:details], with: Entities::V2::Event::Get, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "Update uploaded image for event"
        put "/:id/photos/:photo_id" do
          handler = EventHandler.new(current_user,params).update_photo
          if handler.response[:type]
            present handler.response[:details], with: Entities::V2::Event::Get, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "delete uploaded image for event"
        delete "/:id/photos/:photo_id" do
          handler = EventHandler.new(current_user,params).delete_photo
          if handler.response[:type]
            present handler.response[:details], with: Entities::V2::Event::Get, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "Delete event"
        delete '/:id/delete' do
          #raise authorization error
          raise Errors::AuthorizationError unless current_user.joiner_and_organizer?

          event = Event.not_deleted.find_by(id: params[:id]).decorate
          if event.soft_delete
            AdminNotifications::EventMailerWorker.perform_async(event.id, "canceled_trip")
            {messages: "Successfully deleted."}
          else
            error!({messages: event.errors.full_messages.join(", ")},400)
          end
        end

        desc "validate joiner"
        get "/:id/validate_joiner" do
          if current_user.decorate.mobile.present?
            { message: "Valid joiner." }
          else
            error!({messages: "Invalid joiner."},400)
          end
        end

        desc "Join event"
        post "/:id/join" do
          event = Event.upcoming.find(params[:id])
          handler = EventHandler.new(current_user,params).join
          membership = event.event_memberships.find_by_member_id(current_user.id)
          if handler.response[:type]
            GoogleAnalyticsApiHandler.new('Trips', "Join - #{event.try(:id)}", current_user.try(:uid), 1).track_event!
            present handler.response[:details], with: Entities::V2::Event::Join, current_user: current_user, membership: membership, event: handler.response[:details].event
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "Join event via Paypal"
        post "/:id/paypal_join" do
          event = Event.upcoming.find(params[:id])
          params[:platform] = validate_platform(user_agent: request.user_agent,
                                                type: "endpoint")
          params[:user_id] = current_user.id
          paypal = PaypalHandler.new(params, event).validate_payment_and_join
          membership = event.event_memberships.find_by_member_id(current_user.id)
          if paypal.response[:type]
            present event.decorate, with: Entities::V2::Event::Join, current_user: current_user, membership: membership, event: event.decorate
          else
            error!({messages: paypal.response[:details]},400)
          end
        end

        desc "Request Different Trip Date"
        post "/:id/request_different_date" do
          event = Event.find(params[:id])
          handler = RequestTripDifferentDate.call(event: event, requester: current_user, params: params)
          if handler.success?
            response_message = "#{handler.mailer_response} and #{handler.notifier_response}"
            present response_message, with: Entities::V2::RequestDifferentTripDate::Response
          else
            error!({messages: handler.message}, 400)
          end
        end

        desc "Get Trip Members"
        paginate per_page: 10
        get "/:id/members" do
          event = Event.find(params[:id])
          memberships = { data: paginate(event&.approved_event_memberships).decorate }
          if memberships
            present memberships, with: Entities::V2::Event::Members
          else
            error!({messages: handler.message}, 400)
          end
        end

        desc "Generate Dragonpay Client URL"
        post "/:id/generate_dragonpay_client_url" do
          event = Event.upcoming.find(params[:id])
          params[:platform] = validate_platform(user_agent: request.user_agent,
                                                type: "endpoint")
          # Find Promo Code Using ID
          #  as default App sends promo_code_id
          #  we will retain this flow to minimize changes in App

          promo_code = PromoCode.find_by(id: params[:promo_code_id])
          params[:code] = promo_code&.code

          service = Payments::Dragonpay::GenerateClientRequestURL.call(
            event: event,
            params: params,
            joiner: current_user
          )
          unless service.success?
            return error!({ messages: service.message }, 400)
          end

          params[:total_amount] = service.total_amount
          transaction_data = TransactionDataStore::RecordTransactionData.call(
            transaction_id: service.generated_transaction_id, params: params
          )

          if transaction_data.success?
            present service, with: Entities::V2::Dragonpay::Get
          else
            error!({ messages: transaction_data.message }, 400)
          end
        end
      end
    end
  end
end
