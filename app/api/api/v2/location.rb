module API
  module V2
    class Location < Grape::API

      resource :location do

        desc 'Get List of Countries'
        get '/countries' do
          present :countries, with: Entities::V2::Location::CountriesListing
        end

        desc 'Get List of States'
        get '/countries/:country/states' do
          present :states, with: Entities::V2::Location::StatesListing, country: params[:country]
        end

        desc 'Get List of Cities'
        get '/countries/:country/states/:state/cities' do
          present :cities, with: Entities::V2::Location::CitiesListing, params: params
        end
      end

    end
  end
end
