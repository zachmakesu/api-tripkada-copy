module API
  module V2
    class Destinations < Grape::API

      resource :destinations do

        desc 'List of all event destinations'
        get do
          present DestinationDecorator, with: Entities::V2::Destination::Get
        end
      end

    end
  end
end
