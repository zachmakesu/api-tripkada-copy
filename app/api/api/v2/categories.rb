module API
  module V2
    class Categories < Grape::API

      resource :categories do

        desc 'List of all event categories'
        get do
          present CategoryDecorator, with: Entities::V2::Category::Get
        end
      end

    end
  end
end
