module API
  module V2
    class AuthRequired < Grape::API

      helpers do
        def authenticate!
          /\ATripkada ([\w]+):([\w\+\=]+)\z/ =~ headers['Authorization']
          @uid = $1
          @signature = $2
          @token = params[:access_token]

          # Check if Authorization header is present, else return 401
          error!(Errors::MissingAuthorization.response, 401) if headers['Authorization'].blank?

          # Validate if token is valid
          error!(Errors::InvalidToken.response, 401) unless is_token_valid?(@uid, @token)

          # Validate if signature is valid
          error!(Errors::InvalidSignature.response, 401) unless is_signature_authentic?(@signature)
        end

        def is_token_valid?(uid, token)
          return true if Rails.env.development? || Rails.env.test?
          user = User.approved.find_by!(uid: uid)
          user.api_keys.valid.detect{|a| ApiKey.secure_compare(token, a.encrypted_access_token) }
        end

        def is_signature_authentic?(sig)
          return true if Rails.env.development? || Rails.env.test?
          generated_sig = HmacHandler.signature_from(request.path, params, 2)
          Rails.logger.debug "#{request.path} expected: #{generated_sig}; actual: #{sig}" if Rails.env.staging?
          Rails.logger.debug "params: #{params}" if Rails.env.staging?
          HmacHandler.secure_compare(generated_sig, sig)
        end

        def current_user
          User.approved.find_by!(uid: @uid)
        end
      end

      before do
        authenticate!
      end

      # Mount all endpoints that require authentication
      mount API::V2::Users
      mount API::V2::Events
      mount API::V2::Categories
      mount API::V2::Destinations
      mount API::V2::PromoCodes
      mount API::V2::Devices
      mount API::V2::Notifications
      mount API::V2::Location
      mount API::V2::Referrals
    end
  end
end
