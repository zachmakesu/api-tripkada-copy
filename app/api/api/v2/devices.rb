module API
  module V2
    class Devices < Grape::API

      resource :devices do

        desc "Create a new device"
        post do
          response = DeviceHandler.new(params,current_user).create
          response.blank? ? { messages: 'Successfully created device' } : error!({ messages: response }, 400)
        end

        desc "Unlink device"
        delete do
          current_user.devices.where(token: params[:token], udid: params[:udid]).destroy_all
          { messages: 'Successfully unlink device' }
        end

      end

    end
  end
end
