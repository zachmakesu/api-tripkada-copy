module API
  module V2
    class Base < Grape::API
      version 'v2'
      format :json

      rescue_from :all, backtrace: true do |e|
        ExceptionNotifier.notify_exception(e)
        Rails.logger.debug e if Rails.env.development?
        error!({ error: 'Internal server error', backtrace: e.backtrace }, 500 )
      end
      rescue_from ActiveRecord::RecordNotFound do |e|
        rack_response('{ "status": 404, "message": "Requested resource not found" }', 404)
      end
      rescue_from Errors::AuthorizationError do |e|
        hash_response = { status: 401, message: Errors::AuthorizationError.response }
        rack_response(hash_response.to_json, 401)
      end

      mount API::V2::AuthLess
      mount API::V2::AuthRequired
    end
  end
end
