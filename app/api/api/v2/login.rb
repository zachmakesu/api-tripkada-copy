module API
  module V2
    class Login < Grape::API
      resource :login do
        desc "Facebook Login"
        post '/:type' do
          options = {
            platform: validate_platform(user_agent: request.user_agent,
                                        type: "endpoint")
          }
          handler = FacebookSessionHandler.new(params[:type],
                                               params[:access_token],
                                               options).find_or_create
          if handler.response[:type]
            user = User.find_by(uid: handler.response[:details][:uid])
            GoogleAnalytics::UserHandler.new(user).track!(request)

            { messages: handler.response[:details] }
          else
            error!({messages: handler.response[:details]},401)
          end
        end
      end
    end
  end
end
