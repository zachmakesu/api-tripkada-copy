module API
  module V2
    class AppVersions < Grape::API
      resources :app_versions do
        desc "Get latest app version based on platform"
        get "/:platform/:category/latest" do
          version = AppVersion.latest(platform: params[:platform], category: params[:category])&.decorate
          if version
            present version, with: Entities::V2::AppVersions::Details
          else
            error!( { messages: "App Version not found" } , 400)
          end
        end
      end
    end
  end
end
