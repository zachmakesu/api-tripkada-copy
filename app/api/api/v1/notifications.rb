module API
  module V1
    class Notifications < Grape::API

      resource :notifications do

        desc 'List of not deleted notifications'
        get do
          data = { data: current_user.notifications.not_deleted }
          present  data , with: Entities::V1::Notification::Index
        end

        desc 'Specific not deleted notification'
        get '/:id' do
          notification = current_user.notifications.not_deleted.find(params[:id])
          data = { data: notification}
          present data, with: Entities::V1::Notification::Index
        end

        desc 'Seen a not deleted notification'
        post '/:id' do
          notification = current_user.notifications.unread.find(params[:id])
          notification.see
          data = { data: current_user.notifications.not_deleted }
          present data, with: Entities::V1::Notification::Index
        end

        desc 'Remove a notification'
        delete '/:id' do
          notification = current_user.notifications.find(params[:id])
          notification.update(deleted_at: DateTime.now)
          data = { data: current_user.notifications.not_deleted }
          present data, with: Entities::V1::Notification::Index
        end
      end
    end
  end
end
