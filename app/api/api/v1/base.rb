module API
  module V1
    class Base < Grape::API
      version 'v1'
      format :json

      rescue_from :all, backtrace: true do |e|
        ExceptionNotifier.notify_exception(e)
        Rails.logger.debug e if Rails.env.development?
        error!({ error: 'Internal server error', backtrace: e.backtrace }, 500 )
      end
      rescue_from ActiveRecord::RecordNotFound do |e|
        rack_response('{ "status": 404, "message": "Requested resource not found" }', 404)
      end

      mount API::V1::AuthLess
      mount API::V1::AuthRequired
    end
  end
end
