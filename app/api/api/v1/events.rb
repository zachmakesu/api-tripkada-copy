module API
  module V1
    class Events < Grape::API

      resource :events do

        desc 'List of all upcoming events'
        get '/upcoming' do
          present EventDecorator, with: Entities::V1::Event::Upcoming, current_user: current_user
        end

        desc 'List of all past events'
        get '/history' do
          present EventDecorator, with: Entities::V1::Event::History, current_user: current_user
        end

        desc 'List of upcoming events for organizer'
        get "/organizer/upcoming" do
          present current_user.decorate, with: Entities::V1::Event::OrganizerUpcoming, current_user: current_user
        end

        desc 'Specific detailed payment upcoming events for organizer'
        get "/organizer/upcoming/:id" do
          event = current_user.decorate.upcoming_organized_events.find(params[:id])
          present event.decorate, with: Entities::V1::Event::EventDetailsAndMembers, current_user: current_user
        end

        desc 'List of past events organizer'
        get "/organizer/history" do
          present current_user.decorate, with: Entities::V1::Event::OrganizerHistory, current_user: current_user
        end

        desc 'Specific detailed payment past events organizer'
        get "/organizer/history/:id" do
          event = current_user.decorate.past_organized_events.find(params[:id])
          present event.decorate, with: Entities::V1::Event::EventDetailsAndMembers, current_user: current_user
        end

        desc 'List of upcoming events joiner'
        get "/joiner/upcoming" do
          present current_user.decorate, with: Entities::V1::Event::JoinerUpcoming, current_user: current_user
        end

        desc 'List of past events joiner'
        get "/joiner/history" do
          present current_user.decorate, with: Entities::V1::Event::JoinerHistory, current_user: current_user
        end

        resource :bookmarks do
          desc 'Add event to bookmarks'
          post do
            bookmark = current_user.bookmarks.build(event_id: params[:event_id])
            if bookmark.save
              BookmarkNotification.new(bookmark.id).deliver              
              { messages: "Successfully added to bookmarks" }
            else
              error!({messages: "Failed to add event on your bookmarks"},400)
            end
          end

          desc 'remove event from bookmarks'
          delete do
            bookmark = current_user.bookmarks.find_by!(event_id: params[:event_id])
            if bookmark && bookmark.destroy
              { messages: "Successfully removed event from bookmarks" }
            else
              error!({messages: "Failed to remove event on your bookmarks"},400)
            end
          end

          desc 'get list of bookmarked events'
          get do
            present current_user.decorate, with: Entities::V1::Event::Bookmarks, current_user: current_user
          end
        end

        desc "Create event"
        post do
          handler = EventHandler.new(current_user,params).create
          if handler.response[:type]
            GoogleAnalyticsApiHandler.new('Trips', 'Create', current_user.try(:uid), 1).track_event!
            present handler.response[:details], with: Entities::V1::Event::Get, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "Get event"
        get '/:id' do
          event = current_user.organized_events.find(params[:id]) || current_user.joined_events.find(params[:id])
          if event
            GoogleAnalyticsApiHandler.new('Trips', "Show - #{event.try(:id)}", current_user.try(:iud), 1).track_event!
            present event.decorate, with: Entities::V1::Event::Get, current_user: current_user
          else
            error!({messages: "Event not found"},404)
          end
        end

        desc "Update event"
        put "/:id/update" do
          handler = EventHandler.new(current_user,params).update
          if handler.response[:type]
            present handler.response[:details], with: Entities::V1::Event::Get, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "Delete event"
        delete '/:id/delete' do
          event = Event.not_deleted.find(params[:id]).decorate
          if event.soft_delete
            {messages: "Successfully deleted."}
          else
            error!({messages: event.errors.full_messages.join(", ")},400)
          end
        end

        desc "Duplicate event"
        post "/:id/duplicate" do
          handler = EventHandler.new(current_user,params).duplicate
          if handler.response[:type]
            present handler.response[:details], with: Entities::V1::Event::Get, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "validate joiner"
        get "/:id/validate_joiner" do
          if current_user.decorate.mobile.present?
            { message: "Valid joiner." }
          else
            error!({messages: "Invalid joiner."},400)
          end
        end

        desc "Join event"
        post "/:id/join" do
          handler = EventHandler.new(current_user,params).join
          if handler.response[:type]
            event = Event.upcoming.find(params[:id])
            GoogleAnalyticsApiHandler.new('Trips', "Join - #{event.try(:id)}", current_user.try(:uid), 1).track_event!
            present handler.response[:details], with: Entities::V1::Event::Get, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "Create a review"
        post "/:id/reviews" do
          event = Event.not_deleted.find(params[:id])
          review = event.received_reviews.build(reviewer: current_user, message: params[:message], rating: params[:rating])
          if review.save
            present event.decorate, with: Entities::V1::Event::Get, current_user: current_user
          else
            error!({messages: review.errors.full_messages.join(", ")},400)
          end
        end

        desc "Get reviews"
        get "/:id/reviews" do
          present Event.not_deleted.find(params[:id]).decorate, with: Entities::V1::Review::Get
        end

      end

    end
  end
end
