module API
  module V1
    class Users < Grape::API

      resource :profile do

        desc "User Profile"
        get do
          present current_user.decorate, with: Entities::V1::User::Profile, current_user: current_user
        end

        desc "Update Profile"
        put "/update" do
          handler = UserHandler.new(current_user,params).update_profile
          if handler.response[:type]
            present handler.response[:details], with: Entities::V1::User::Profile, current_user: current_user
          else
            error!({messages: handler.response[:details]},404)
          end
        end

        desc "Update photos base on category"
        put  "/update/:category" do
          handler = UserHandler.new(current_user,params).update_photos
          if handler.response[:type]
            present handler.response[:details], with: Entities::V1::Photo::List, category: params[:category]
          else
            error!({messages: handler.response[:details]},404)
          end
        end

        desc "Upload photos base on category"
        post  "/upload/:category" do
          handler = UserHandler.new(current_user,params).upload_photos
          if handler.response[:type]
            present handler.response[:details], with: Entities::V1::Photo::List, category: params[:category]
          else
            error!({messages: handler.response[:details]},404)
          end
        end

        desc "Delete photos base on category"
        delete  "/remove/:category" do
          handler = UserHandler.new(current_user,params).delete_photos
          if handler.response[:type]
            present handler.response[:details], with: Entities::V1::Photo::List, category: params[:category]
          else
            error!({messages: handler.response[:details]},404)
          end
        end

      end

      resource :user do
        desc "Get your followed organizers"
        get "/following" do
          present current_user.decorate.following, with: Entities::V1::User::Index, current_user: current_user
        end

        desc "Get joiners who followed you"
        get "/followers" do
          present current_user.decorate.followers, with: Entities::V1::User::Index, current_user: current_user
        end

        desc "Get User Profile"
        get "/:uid" do
          user = User.find_by(uid: params[:uid])
          if user
            GoogleAnalyticsApiHandler.new('Users', "Show - #{user.try(:uid)}", current_user.try(:uid), 1).track_event!
            present user.decorate, with: Entities::V1::User::Profile, current_user: current_user
          else
            error!({messages: "User not found."},400)
          end
        end

        desc "Follow an organizer"
        post "/:uid/follows" do
          handler = FollowHandler.new(current_user.uid,params[:uid]).follow
          if handler.response[:success]
            present handler.response[:details].decorate, with: Entities::V1::User::Profile, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "Unfollow an organizer"
        delete "/:uid/follows" do
          handler = FollowHandler.new(current_user.uid,params[:uid]).unfollow
          if handler.response[:success]
            present handler.response[:details].decorate, with: Entities::V1::User::Profile, current_user: current_user
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "List of user's joined events"
        get "/:uid/joined_events" do
          user = User.find_by!(uid: params[:uid])
          present user.decorate, with: Entities::V1::Event::JoinedEvents, current_user: current_user
        end
      end


      resource :joiner do
        desc "Joiner Active Trips"
        get "/:uid/trips" do
          user = User.find_by(uid: params[:uid])
          if user
            present user.decorate, with: Entities::V1::Event::JoinerUpcoming, current_user: current_user
          else
            error!({messages: "User not found."},400)
          end
        end
      end

      resource :organizer do
        desc "Reviewable Trips"
        get "/:uid/reviewables" do
          user = User.find_by(uid: params[:uid])
          if user
            present user.decorate, with: Entities::V1::Event::Reviewable, current_user: current_user
          else
            error!({messages: "User not found."},400)
          end
        end

        desc "Organizer Active Trips"
        get "/:uid/trips" do
          user = User.find_by(uid: params[:uid])
          if user
            present user.decorate, with: Entities::V1::Event::OrganizerUpcoming, current_user: current_user
          else
            error!({messages: "User not found."},400)
          end
        end

        desc "Organizer Reviews from trips"
        get "/:uid/reviews" do
          user = User.find_by(uid: params[:uid])
          if user
            present user.decorate, with: Entities::V1::Review::OrganizerGet, current_user: current_user
          else
            error!({messages: "User not found."},400)
          end
        end
      end

    end
  end
end
