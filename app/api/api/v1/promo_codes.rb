module API
  module V1
    class PromoCodes < Grape::API

      resource :promo_codes do

        desc 'Validate promocode'
        get '/validate' do
          handler = PromoCode::ValidateByCategory.call(code: params[:code], user: current_user)
          if handler.success?
            present handler.promo_code.decorate, with: Entities::V2::PromoCode::Get, current_user: current_user
          else
            error!( { messages: handler.message }, 400)
          end
        end

      end

    end
  end
end
