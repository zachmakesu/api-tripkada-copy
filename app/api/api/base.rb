module API
  class Base < Grape::API
    prefix 'api'
    helpers ApiHelper
    mount API::V1::Base
    mount API::V2::Base
  end
end
