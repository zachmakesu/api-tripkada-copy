module Entities
  module V2
    module RequestDifferentTripDate
      
      class Response < Grape::Entity
        expose :data do
          expose :response
        end

        private

        def response
          object
        end
      end

    end
  end
end
