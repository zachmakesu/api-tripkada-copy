module Entities
  module V2
    module User

      class ShortDetail < Grape::Entity
        expose :links do
          expose :avatar, format_with: :original_photo_url
          expose :avatar_versions, using: V2::Photo::WithVersions do |user, options|
            user.profile_avatar
          end
          expose :cover_photo_image, as: :cover_photo, format_with: :original_photo_url
          expose :cover_photo_versions, using: V2::Photo::WithVersions do |user, options|
            user.cover_photo
          end
        end
        expose :data do
          expose :type, :uid
          expose :attributes do
            expose :full_name, :mobile ,:email, :total_rating
          end
        end
      end

      class Profile < ShortDetail
        expose :data do
          expose :attributes do
            expose :first_name, :last_name, :gender, :country, :state, :city, :address, :mobile, :email, :total_rating, :hobbies, :website, :is_new_user, :tell_us_about_yourself, :emergency_contact_name, :emergency_contact_number, :tagcash_token, :calendar_sync
            expose :birthdate, format_with: :utc
          end
          expose :relationships do
            expose :government_photos, using: V2::Photo::Details
            expose :profile_photos, using: V2::Photo::Details
            expose :followers_count, :following_count, :followed
            expose :bank_account, using: V2::BankAccount::Details
            expose :social_accounts, using: V2::Providers::SocialAccounts
            expose :certificates, using: V2::Photo::Details
            expose :credit_promo_code, using: V2::PromoCode::Get
          end
        end

        private
        def followers_count
          object.followers.count
        end

        def following_count
          object.following.count
        end
        def followed
          options[:current_user].decorate.following?(object)
        end
        def social_accounts
          object.model.providers.social_accounts
        end
      end

      class Index < Grape::Entity
        expose :data, using: Profile
        def data
          object.decorate
        end
      end

      class InvitedJoinersListing < Grape::Entity
        expose :data, using: ShortDetail
        def data
          object.decorate
        end
      end
      
      class AppSettings < Grape::Entity
        expose :data do
          expose :type
          expose :attributes do
            expose :calendar_sync
          end
        end

        private

        def type
          "app settings"
        end

      end

      class WaitlistMembers < ShortDetail
        expose :data do
          expose :attributes do
            expose :waitlist_status do |object, options|
              object.user.waitlist_for(options[:event])&.status
            end
          end
        end
      end

    end
  end
end
