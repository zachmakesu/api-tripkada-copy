module Entities
  module V2
    module CustomNotificationMessage

      class Details < Grape::Entity
        expose :links do
          expose :image, using: V2::Photo::WithURLandVersions do |photo, options|
            photo
          end
        end
        expose :data do
          expose :id, :type, :message
          expose :created_at, format_with: :utc
        end
      end

      class Get < Grape::Entity
        expose :data, using: Details
        def data
          object[:data]
        end
      end

    end
  end
end
