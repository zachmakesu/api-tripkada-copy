module Entities
  module V2
    module Location
      class CountriesListing < Grape::Entity
        expose :data do
          expose :countries
        end

        private
        def countries
          @countries = CS.countries.except!(:COUNTRY_ISO_CODE).sort_by(&:last)
          @countries.map do |k, v|
            hash = {}
            hash[:id] = k
            hash[:name] = v
            hash
          end
        end
      end

      class StatesListing < Grape::Entity
        expose :data do
          expose :states
        end

        private
        def states
          @country = options[:country]
          @states = CS.states(@country).sort_by(&:last)
          @states.map do |k, v|
            hash = {}
            hash[:id] = k
            hash[:name] = v
            hash
          end
        end
      end

      class CitiesListing < Grape::Entity
        expose :data do
          expose :cities
        end

        private
        def cities
          @state = options[:params][:state]
          @country = options[:params][:country]
          CS.cities(@state, @country)
        end
      end
    end
  end
end