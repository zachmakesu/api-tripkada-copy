module Entities
  module V2
    module Itinerary

      class Details < Grape::Entity
        expose :data do
          expose :type, :id
          expose :attributes do
            expose :description
          end
        end
      end

    end
  end
end
