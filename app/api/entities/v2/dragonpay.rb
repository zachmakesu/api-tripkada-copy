module Entities
  module V2
    module Dragonpay
      # Presenter for Dragonpay Client URl Generation Response
      class ClientRequestGenerationResponse < Grape::Entity
        expose :type
        expose :attributes do
          expose :total_amount, :generated_transaction_id, :with_invited_joiners
        end
        expose :link do
          expose :generated_request_url
        end
        def type
          "Dragonpay"
        end
      end

      # Presenter for Dragonpay Client URl Generation Response
      class Get < Grape::Entity
        expose :data, using: ClientRequestGenerationResponse

        private

        def data
          object
        end
      end
    end
  end
end
