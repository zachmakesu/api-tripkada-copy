module Entities
  module V2
    module BankAccount

      class Details < Grape::Entity

        expose :data do
          expose :id
          expose :attributes do
            expose :bank_name, :account_number
          end
        end
      end

    end
  end
end
