module Entities
  module V2
    module Destination 
      class DestinationListing < Grape::Entity
        expose :type, :id
        expose :attributes do
          expose :id, :name, :active
        end
      end

      class Get < Grape::Entity
        expose :data, using: DestinationListing
        private
        def data
          object.all.decorate
        end
      end
    end
  end
end