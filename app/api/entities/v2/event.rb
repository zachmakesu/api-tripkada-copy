module Entities
  module V2
    module Event

      class UnsopedDataShortDetail < Grape::Entity
        expose :type, :id
        expose :attributes do
          expose :name
        end
      end

      class ShortDetail < Grape::Entity
        expose :data do
          expose :type, :id
          expose :attributes do
            expose :name
            expose :start_at, :end_at, :published_at ,format_with: :utc
          end
        end
      end

      class ShortDetailWithOrganizer < Grape::Entity
        expose :links do
          expose :cover_photo, using: V2::Photo::WithURLandVersions do |event, photo|
            event.object.cover_photo || event.photos.first
          end
        end
        expose :data do
          expose :type, :id
          expose :attributes do
            expose :name
            expose :start_at, :end_at,format_with: :utc
          end
          expose :relationships do
            expose :owner, using: V2::User::ShortDetail
          end
        end

      end

      class WaitlistMembers < Grape::Entity
        expose :data do
          expose :type, :id
          expose :attributes do
            expose :name
          end
          expose :relationships do
            expose :waitlist_members, using: V2::User::WaitlistMembers
          end
        end
      end

      class EventDetailsAndMembers < Grape::Entity
        expose :type,:id
        expose :links do
          expose :cover_photo, using: V2::Photo::WithURLandVersions do
            cover_photo
          end
        end
        expose :attributes do
          expose :name, :description, :slots_left, :min_pax, :max_pax, :rate, :days_before_trip_ends, :meeting_place, :inclusion_lists,
            :highlight_lists, :tag_lists, :things_to_bring_lists, :already_a_member, :lat, :lng, :bookmarked, :is_reviewed, :website, :booking_fee,
            :downpayment_rate, :payment_made, :duplicated_from_id, :duplicated_from_event, :number_of_days, :waitlisted
          expose :start_at, :end_at, :published_at ,format_with: :utc
        end
        expose :relationships do
          expose :owner,                                using: V2::User::ShortDetail
          expose :categories,                           using: V2::Category::CategoryListing
          expose :destinations,                         using: V2::Destination::DestinationListing
          expose :photos,                               using: V2::Photo::Details
          expose :sponsors,                             using: V2::Sponsor::Details
          expose :sorted_itineraries, as: :itineraries, using: V2::Itinerary::Details
          expose :valid_memberships, as: :members,      using: V2::EventMembership::MembersAndPayments
        end
        expose :funds do
          expose :total_booking_fee, :total_payments, :to_remit
          expose :remittance_date, format_with: :utc
        end
        expose :options do
          expose :tag_options, :highlight_options, :inclusion_options, :things_to_bring_options
        end

        private
        def bookmarked
          options[:current_user].bookmarked_events.include?(object)
        end
        def already_a_member
          user = options[:current_user] || object.owner
          object.has_membership_of(user)
        end
        def is_reviewed
          user = options[:current_user] || object.owner
          object.is_reviewed_by(user)
        end
        def payment_made
          user = options[:current_user] || object.owner
          object.payment_made_by(user)
        end
        def duplicated_from_event
          object.duplicated_from_event.present? ? object.duplicated_from_event.name : nil
        end
        def remittance_date
          object.end_at
        end
        def total_booking_fee
          object.valid_joiner_members_booking_fee_total
        end
        def total_payments
          object.calculate_total_payments
        end
        def to_remit
          object.calculate_total_remittance
        end
        def cover_photo
          object.object.cover_photo || object.photos.first
        end
        def waitlisted
          object.waitlisted?(options[:current_user])
        end
        def valid_memberships
          object.approved_event_memberships&.limit(10)&.decorate
        end
      end

      class JoinerListing < EventDetailsAndMembers
        expose :relationships do
          expose :invited_joiners, using: V2::User::InvitedJoinersListing do
            invited_joiners
          end
          expose :waitlist_members, using: V2::User::WaitlistMembers
        end

        private

        def invited_joiners
          options[:membership].invited_members
        end
      end

      class Bookmarks < Grape::Entity
        expose :data, using: EventDetailsAndMembers
        private
        def data
          object[:data]
        end
      end

      class Get < Grape::Entity
        expose :data, using: EventDetailsAndMembers
        private
        def data
          object
        end
      end

      class Reviewable < Grape::Entity
        expose :data, using: UnsopedDataShortDetail
        private
        def data
          object.reviewable_events(options[:current_user])
        end
      end

      class FilteredEvents < Grape::Entity
        expose :data, using: EventDetailsAndMembers
        expose :included do
          expose :last_upcoming_event, using: ShortDetail
          expose :last_scoped_event, using: ShortDetail
        end
        private
        def data
          object[:data]
        end
        def last_upcoming_event
          ::Event.last_upcoming_event&.decorate
        end
        def last_scoped_event
          options[:events]&.order_by_start&.last&.decorate
        end
      end

      class Join < Grape::Entity
        expose :data, using: JoinerListing
        private
        def data
          object
        end
      end

      class EventDetailsAndWaitlistMembers < EventDetailsAndMembers
        expose :relationships do
          expose :waitlist_members, using: V2::User::WaitlistMembers
        end
      end

      class EventAndWaitlistDetails < Grape::Entity
        expose :data, using: EventDetailsAndWaitlistMembers

        private

        def data
          object
        end
      end

      class Drafts < Grape::Entity
        expose :data, using: UnsopedDataShortDetail

        private

        def data
          object[:data]
        end
      end

      class Members < Grape::Entity
        expose :data, using: V2::EventMembership::MembersAndPayments
        private

        def data
          object[:data]
        end
      end

      class ReviewableEventAndOrganizerListing < Grape::Entity
        expose :data, using: ShortDetailWithOrganizer

        private

        def data
          object[:data]
        end
      end

    end
  end
end
