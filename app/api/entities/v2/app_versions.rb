module Entities
  module V2
    module AppVersions
      
      class Details < Grape::Entity
        expose :data do
          expose :id, :type
          expose :attributes do
            expose :platform, :category, :version_code, :created_by
          end
          expose :relationships do
            expose :features, using: Entities::V2::AppVersionFeatures::Details
          end
        end
      end
    end
  end
end
