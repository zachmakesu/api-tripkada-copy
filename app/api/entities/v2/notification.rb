module Entities
  module V2
    module Notification

      class NotificationListing < Grape::Entity
        expose :id, :category_key, :message, :seen, :read
        expose :created_at, format_with: :utc
        expose :included do
          expose :waitlist_status, if: ->(notif, _options) {
            notif.waitlist_status?
          }
        end
        expose :relationships do
          expose :notificationable, as: :custom_notification, 
            using: V2::CustomNotificationMessage::Get, 
            if: ->(status, options) { 
              status.notificationable_type == "CustomNotificationMessage" 
            }
          expose :notificationable, as: :event, 
            using: V2::Event::ReviewableEventAndOrganizerListing, if: ->(status, options) { 
              status.notificationable_type == "Event" 
            }
        end

        def notificationable
          { data: object.notificationable }
        end

      end

      class Index < Grape::Entity
        expose :data, using: NotificationListing
        expose :included do
          expose :unseen_notification_count
        end

        def data
          object[:data].decorate
        end

        def unseen_notification_count
          options[:current_user]&.notifications&.not_seen&.count
        end
      end

    end
  end
end
