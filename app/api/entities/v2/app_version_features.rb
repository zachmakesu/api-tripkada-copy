module Entities
  module V2
    module AppVersionFeatures
      
      class Details < Grape::Entity
        expose :data do
          expose :id, :type
          expose :attributes do
            expose :title, :details
          end
        end
      end
    end
  end
end
