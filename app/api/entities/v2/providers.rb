module Entities
  module V2
    module Providers

      class SocialAccounts < Grape::Entity
        expose :links do
          expose :url
        end
        expose :data do
          expose :provider, :uid
          expose :token, if: lambda { |provider, opts| provider.can_view_token? }
        end
      end

      class SocialAccountsDataGrp < Grape::Entity
        expose :data, using: SocialAccounts
        private
        def data
          object.providers.social_accounts
        end
      end
    end
  end
end
