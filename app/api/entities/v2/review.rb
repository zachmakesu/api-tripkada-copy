module Entities
  module V2
    module Review

      class Details < Grape::Entity
        expose :type, :id
        expose :attributes do
          expose :message, :rating
          expose :created_at, format_with: :utc
        end
        expose :relationships do
          expose :reviewer, using: V2::User::ShortDetail
        end
      end

      class DetailsWithTrip < Details
        expose :relationships do
          expose :event, using: V2::Event::Get
        end
      end

      class Get < Grape::Entity
        expose :data, using: Details

        private
        def data
          object.reviews
        end
      end

      class SentReviews < Grape::Entity
        expose :data, using: Details

        private
        def data
          object.sent_reviews
        end
      end

      class ReceivedReviews < Grape::Entity
        expose :data, using: Details

        private
        def data
          object.received_reviews
        end
      end


      class EventReviews < Grape::Entity
        expose :data, using: Details

        private
        def data
          object.event_reviews
        end
      end

      class OrganizerGet < Grape::Entity
        expose :data, using: DetailsWithTrip

        private
        def data
          object.organizer_reviews
        end
      end

    end
  end
end
