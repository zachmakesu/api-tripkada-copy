module Entities
  module V2
    module Referrals
      
      class InvitationResponse < Grape::Entity
        expose :data do
          expose :message
        end

        private

        def message
          object
        end
      end

    end
  end
end
