module Entities
  module V2
    module PromoCode

      class Get < Grape::Entity
        expose :data do
          expose :type, :id
          expose :attributes do
            expose :code, :category, :active, :value, :usage, :usage_limit, :is_available?
            expose :available_credits, if: -> (instance, options) { instance.credit? } 
            expose :start_at, :end_at, format_with: :utc
          end
        end

        private

        def is_available?
          object.corporate? ? object.usable? : object.belongs_to_owner?(user: options[:current_user])
        end
      end

    end
  end
end
