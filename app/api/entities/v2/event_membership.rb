module Entities
  module V2
    module EventMembership

      class PaymentDetails < Grape::Entity
        expose :promo_code, :promo_code_value, :mode, :made, :amount_paid, :status
        expose :downpayment_rate, :balance

        private
        def downpayment_rate
          object.downpayment_rate_with_promo_code_deduction
        end
      end

      class MembersAndPayments < Grape::Entity
        expose :member, merge: true, using: V2::User::ShortDetail
        expose :membership_details do
          expose :id, :no_show
        end
        expose :payment, as: :payment_made do |object, options|
          object.payment.made
        end
        expose :payment_details, using: PaymentDetails

        private
        def payment_details
          object.payment
        end
      end
    end
  end
end
