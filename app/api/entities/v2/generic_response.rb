module Entities
  module V2
    module GenericResponse
      
      class Message < Grape::Entity
        expose :data do
          expose :message
        end
        private

        def message
          object
        end
      end

    end
  end
end
