module Entities
  module V2
    module Category 
      class CategoryListing < Grape::Entity
        expose :type, :id
        expose :attributes do
          expose :id, :name, :active
        end
      end

      class Get < Grape::Entity
        expose :data, using: CategoryListing
        private
        def data
          object.all.decorate
        end
      end
    end
  end
end