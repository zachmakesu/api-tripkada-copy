module Entities
  module V2
    module Photo

      class WithVersions < Grape::Entity
        expose :image, as: :thumb, format_with: :thumb_photo_url
        expose :image, as: :medium, format_with: :medium_photo_url
        expose :image, as: :large, format_with: :large_photo_url
        expose :image, as: :xlarge, format_with: :xlarge_photo_url
        expose :image, as: :xxlarge, format_with: :xxlarge_photo_url
      end

      class WithURLandVersions < Grape::Entity
        expose :image, as: :url, format_with: :original_photo_url
        expose :versions, using: WithVersions do |photo, options|
          photo
        end
      end

      class Details < Grape::Entity
        expose :links do
          expose :image, format_with: :original_photo_url
          expose :image_versions, using: WithVersions do |photo, options|
            photo
          end
        end
        expose :data do
          expose :type, :id
          expose :attributes do
            expose :label, :primary, :category
          end
        end
      end

      class List < Grape::Entity
        expose :data, using: Details

        private
        def data
          object.model.send(options[:category]).order(updated_at: :desc).decorate
        end
      end

    end
  end
end
