module Entities
  module V1
    module Review

      class Details < Grape::Entity
        expose :type, :id
        expose :attributes do
          expose :message, :rating
          expose :created_at, format_with: :utc
        end
        expose :relationships do
          expose :reviewer, using: V1::User::ShortDetail
        end
      end

      class DetailsWithTrip < Details
        expose :relationships do
          expose :event, using: V1::Event::Get
        end
      end

      class Get < Grape::Entity
        expose :data, using: Details

        private
        def data
          object.decorate.received_reviews
        end
      end

      class OrganizerGet < Grape::Entity
        expose :data, using: DetailsWithTrip

        private
        def data
          object.organizer_reviews
        end
      end

    end
  end
end
