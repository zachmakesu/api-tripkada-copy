module Entities
  module V1
    module Photo

      class Details < Grape::Entity
        expose :links do
          expose :image, format_with: :large_photo_url
        end
        expose :data do
          expose :type, :id
          expose :attributes do
            expose :label, :primary, :category
          end
        end
      end

      class List < Grape::Entity
        expose :data, using: Details

        private
        def data
          object.model.send(options[:category]).order(updated_at: :desc).decorate
        end
      end

    end
  end
end
