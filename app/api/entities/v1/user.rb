module Entities
  module V1
    module User

      class ShortDetail < Grape::Entity
        expose :links do
          expose :avatar, format_with: :large_photo_url
        end
        expose :data do
          expose :type, :uid
          expose :attributes do
            expose :full_name, :mobile 
          end
        end
      end

      class Profile < ShortDetail
        expose :data do
          expose :attributes do
            expose :first_name, :last_name, :gender, :country, :city, :address, :mobile, :email, :total_rating, :hobbies, :website
            expose :birthdate, format_with: :utc
          end
          expose :relationships do
            expose :government_photos, using: V1::Photo::Details
            expose :profile_photos, using: V1::Photo::Details
            expose :followers_count, :following_count, :followed
          end
        end

        private
        def followers_count
          object.followers.count
        end

        def following_count
          object.following.count
        end
        def followed
          options[:current_user].decorate.following?(object)
        end
      end

      class Index < Grape::Entity
        expose :data, using: Profile
        def data
          object.decorate
        end
      end

    end
  end
end
