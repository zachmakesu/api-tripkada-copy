module Entities
  module V1
    module EventMembership

      class PaymentDetails < Grape::Entity
        expose :promo_code, :promo_code_value, :mode, :made
        expose :downpayment_rate

        private
        def downpayment_rate
          object.downpayment_rate_with_promo_code_deduction
        end
      end

      class MembersAndPayments < Grape::Entity
        expose :member, merge: true, using: V1::User::ShortDetail
        expose :payment, as: :payment_made do |object, options|
          object.payment.made
        end
        expose :payment_details, using: PaymentDetails

        private
        def payment_details
          object.payment
        end
      end
    end
  end
end
