module Entities
  module V1
    module Notification

      class NotificationListing < Grape::Entity
        expose :id, :notification_type, :message, :seen, :read

        def notification_type
          object.category_key
        end
      end

      class Index < Grape::Entity
        expose :data, using: NotificationListing
        def data
          object[:data]
        end
      end

    end
  end
end
