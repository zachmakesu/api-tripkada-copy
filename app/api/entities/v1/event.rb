module Entities
  module V1
    module Event

      class UnsopedDataShortDetail < Grape::Entity
        expose :type, :id
        expose :attributes do
          expose :name
        end
      end

      class ShortDetail < Grape::Entity
        expose :data do
          expose :type, :id
          expose :attributes do
            expose :name
          end
        end
      end

      class EventDetailsAndMembers < Grape::Entity
        expose :type,:id
        expose :attributes do
          expose :name, :description, :slots_left, :min_pax, :max_pax, :rate, :days_before_trip_ends, :meeting_place, :inclusion_lists, :highlight_lists, :tag_lists, :things_to_bring_lists, :already_a_member, :lat, :lng, :bookmarked, :is_reviewed, :website, :booking_fee, :downpayment_rate, :payment_made, :duplicated_from_id, :duplicated_from_event
          expose :start_at, :end_at, format_with: :utc
        end
        expose :relationships do
          expose :owner,        using: V1::User::ShortDetail
          expose :photos,       using: V1::Photo::Details
          expose :itineraries,  using: V1::Itinerary::Details
          expose :valid_memberships, as: :members, using: V1::EventMembership::MembersAndPayments
        end
        expose :funds do
          expose :total_booking_fee, :total_payments, :to_remit
          expose :remittance_date, format_with: :utc
        end
        expose :options do
          expose :tag_options, :highlight_options, :inclusion_options, :things_to_bring_options
        end

        private
        def bookmarked
          options[:current_user].bookmarked_events.include?(object) 
        end
        def already_a_member
          user = options[:current_user] || object.owner
          object.has_membership_of(user)
        end
        def is_reviewed
          user = options[:current_user] || object.owner
          object.is_reviewed_by(user)
        end
        def payment_made
          user = options[:current_user] || object.owner
          object.payment_made_by(user)
        end
        def duplicated_from_event
          object.duplicated_from_event.present? ? object.duplicated_from_event.name : nil
        end
        def remittance_date
          object.end_at
        end
        def total_booking_fee
          -(object.valid_joiner_memberships.count * 100)
        end
        def total_payments
          object.calculate_total_remittance
        end
        def to_remit
          object.calculate_total_remittance
        end
      end

      class Bookmarks < Grape::Entity
        expose :data, using: EventDetailsAndMembers
        private
        def data
          object.bookmarked_events
        end
      end

      class Upcoming < Grape::Entity
        expose :data, using: EventDetailsAndMembers
        private
        def data
          object.not_deleted.upcoming.order_by_start.decorate
        end
      end

      class History < Grape::Entity
        expose :data, using: EventDetailsAndMembers
        private
        def data
          object.not_deleted.past.order_by_start.decorate
        end
      end

      class OrganizerUpcoming < Grape::Entity
        expose :data, using: EventDetailsAndMembers
        private
        def data
          object.upcoming_organized_events
        end
      end

      class OrganizerHistory < Grape::Entity
        expose :data, using: EventDetailsAndMembers
        private
        def data
          object.past_organized_events
        end
      end

      class JoinerUpcoming < Grape::Entity
        expose :data, using: EventDetailsAndMembers
        private
        def data
          object.upcoming_joined_events
        end
      end

      class JoinerHistory < Grape::Entity
        expose :data, using: EventDetailsAndMembers
        private
        def data
          object.past_joined_events
        end
      end

      class JoinedEvents < Grape::Entity
        expose :data, using: EventDetailsAndMembers
        private
        def data
          object.sorted_joined_events
        end
      end

      class Get < Grape::Entity
        expose :data, using: EventDetailsAndMembers
        private
        def data
          object
        end
      end

      class Reviewable < Grape::Entity
        expose :data, using: UnsopedDataShortDetail
        private
        def data
          object.reviewable_events(options[:current_user])
        end
      end

    end
  end
end
