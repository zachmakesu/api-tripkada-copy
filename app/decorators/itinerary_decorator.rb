class ItineraryDecorator < Draper::Decorator
  delegate_all

  decorates_association :event

  def type
    "itineraries"
  end

end
