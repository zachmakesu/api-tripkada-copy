class NotificationDecorator < Draper::Decorator
  delegate_all
  decorates_association :notificationable

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def type
    if self.uncategorized?
      "UNCATEGORIZED"
    elsif Notification::REMINDERS.include?(self.category_key)
      "REMINDER"
    elsif self.revised_terms_and_condition?
      "Terms and Condition Update"
    else
      "ANNOUNCEMENT"
    end
  end

  def truncated_message
    self.message.truncate(100)
  end

  def truncated_message_mobile
    self.message.truncate(50)
  end

  def cover_photo_url(style=:original)
    self.custom? ? self.notificationable&.image&.url(style) : self.notificationable&.cover_photo_url(style)
  end

  def cover_photo(style=:original)
    cover_photo_url(style) || ActionController::Base.helpers.asset_path("notification-logo.png")
  end

  def waitlist_status
    waitlist = notificationable.waitlists.object.find_by(user_id: user.id)
    waitlist&.status
  end
end
