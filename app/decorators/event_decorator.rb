class EventDecorator < Draper::Decorator
  delegate_all

  decorates_association :owner
  decorates_association :categorizations
  decorates_association :categories
  decorates_association :sorting_destinations
  decorates_association :destinations
  decorates_association :members
  decorates_association :photos
  decorates_association :itineraries
  decorates_association :photos
  decorates_association :received_reviews
  decorates_association :event_memberships
  decorates_association :waitlists
  decorates_association :waitlist_members
  decorates_association :sponsors

  def type
    "events"
  end

  def cover_photo
    object.cover_photo.try(:image) || object.photos.first.try(:image)
  end

  def cover_photo_url(style=:original)
    cover_photo.try{|c| c.url(style)}
  end

  def brand_sponsor_logo_url(style=:original)
    self.sponsors.last&.image&.url(style)
  end

  def cover_photo_complete_url(style=:original)
    URI.join(ActionController::Base.asset_host, cover_photo_url(style)).to_s if cover_photo_url(style)
  end

  def slots_left
    self.waitlist_and_joiners_less_than_max_pax? ? max_pax - self.waitlists_and_joiners_count : "0"
  end

  def days_before_trip_ends
    return 0 unless start_at
    (start_at.to_date - DateTime.now.to_date).to_i
  end

  def owner_name
    Event.includes(:owner).find(model).owner.decorate.full_name
  end

  def total_rating
    event = Event.includes(:received_reviews).find_by(id: self.id).decorate
    event.received_reviews.blank? ? 0 : event.received_reviews.map(&:rating).sum / event.received_reviews.size
  end

  def inclusion_lists
    inclusions.to_s.split(";")
  end

  def highlight_lists
    highlights.to_s.split(";").map{|t| t.gsub(',,','')}
  end

  def tag_lists
    tags.to_s.split(";").map{|t| t.gsub(',,','')}
  end

  def things_to_bring_lists
    things_to_bring.to_s.split(";")
  end

  def tag_options
    EventTag.tags.map(&:name)
  end

  def highlight_options
    EventTag.highlights.map(&:name)
  end

  def inclusion_options
    EventTag.inclusions.map(&:name)
  end

  def things_to_bring_options
    EventTag.things.map(&:name)
  end

  def first_three_members
    valid_members.take(3).map(&:decorate)
  end

  def valid_memberships
    self.approved_event_memberships.order(:created_at).decorate
  end

  def valid_members
    valid_memberships.map(&:member).map(&:decorate)
  end

  def valid_joiner_memberships
    EventMembership.where('event_id = ? and member_id != ?',object.id, object.user_id).approved.order(:created_at).decorate
  end

  def calculate_total_remittance
    if valid_joiner_memberships.present?
      total = valid_joiner_memberships.map { |member| member.payment.promo_code_plus_amount_paid }.sum
      total - valid_joiner_members_booking_fee_total
    else
      0
    end
  end

  def calculate_total_payments
    if valid_joiner_memberships.present?
      valid_joiner_memberships.map { |member| member.payment.promo_code_plus_amount_paid }.sum
    else
      0
    end
  end

  def valid_joiner_members_booking_fee_total
    valid_joiner_memberships.map{|em| em.payment.try(:booking_fee_value) }.sum
  end

  def website
    return unless slug
    URI.join(ActionController::Base.asset_host, 'trips/'+slug).to_s
  end

  def is_reviewed_by(user)
    Event.includes(:reviewers).find(object.id).reviewers.include?(user)
  end

  def has_membership_of(user)
    Event.includes(:members).find(object.id).members.include?(user)
  end

  def payment_made_by(user)
    membership = object.event_memberships.find_by(member: user)
    membership.payment.try(:made) unless membership.nil?
  end

  def rate_with_fee
    rate + booking_fee
  end

  def self.upcoming
    Event.includes(:members,:photos).not_deleted.upcoming.order_by_start.decorate
  end

  def self.past
    Event.includes(:members,:photos).not_deleted.past.order_by_start.decorate
  end

  def self.search(q = nil)
    query = "%#{q}%"
    not_deleted.where('name ILIKE ?', query).sorted_event.decorate
  end

  def start_month_day
    object.start_at.strftime("%B %d").upcase
  end

  def end_month_day
    object.end_at.strftime("%B %d").upcase
  end

  def short_start_month_day
    object.start_at.strftime("%b %d").upcase
  end

  def self.newest_upcoming_trips
    upcoming.reverse.take(5)
  end

  def short_start_month_day_year
    object.start_at.strftime("%b %d, %Y")
  end

  def short_end_month_day_year
    object.end_at.strftime("%b %d, %Y")
  end

  def digit_start_date
    object.start_at.strftime("%m/%d/%Y").upcase
  end

  def sorted_itineraries
    object.itineraries.order(id: :asc).decorate
  end

  def start_month_day_year
    object.start_at.strftime("%B %d, %Y")
  end

  def end_month_day_year
    object.end_at.strftime("%B %d, %Y")
  end

  def start_time
    object.start_at.strftime("%I:%M %p")
  end

  def remittance_date
    (object.start_at - 1.day).strftime("%B %d, %Y")
  end

  def status_in_words
    if self.is_canceled?
      "Canceled"
    else
      if self.is_upcoming?
        "Upcoming"
      elsif self.is_ongoing?
        "On-Going"
      else
        "Done"
      end
    end
  end

  def generate_invite_joiner_message(invited_joiner_name)
    trip_url = "#{ActionController::Base.default_url_options[:host]}" +
      "#{Rails.application.routes.url_helpers.trip_view_path(self)}" +
      "/checkout/invited_payment_details"

    paymaya_url =  trip_url + "?code=&mode=paymaya"
    bank_url = trip_url + "?code=&mode=bank"

    invited_by = "Hey Explorer #{invited_joiner_name}!\n" +
      "Tripkada trip organizer #{self.owner.decorate.full_name} invited you to join in #{self.owner.Male? ? "his" : "her"} upcoming trip '#{self.name}'.\n"

    instruction = "Hop in! to explore exciting adventures and meeting new friends!\n" +
      "Choose prefered link to submit your proof of payment:\n" +
      "#{paymaya_url}\n" +
      "#{bank_url}\n" +
      "For more info visit us on www.tripkada.com\n" +
      "Follow us on facebook! Share the fun!\n"
    invited_by + instruction
  end

  def start_month_day_year
    self.start_at.strftime("%b %d, %Y").upcase
  end

  def downpayment_rate_with_fee
    self.downpayment_rate.to_f + booking_fee
  end

  def booking_fee
    BookingFeeDecorator.latest_fee
  end

  def og_event_photo_url
    self.cover_photo.url(:xlarge)
  end

  def truncated_trip_name
    self.name.truncate(80)
  end

  def travel_dates
    start_date = self.start_at.strftime('%B %d')
    if happening_on_same_day?
      start_date
    elsif happening_on_same_month?
      start_date+" - "+self.end_at.strftime('%d')
    else
      start_date+" - "+self.end_at.strftime('%B %d')
    end
  end

  def self.collection_decorator_class
    PaginatingDecorator
  end

  def generate_checkout_link
    trip_link = ActionController::Base.asset_host+'/trips/'+self.slug
    checkout_link = Rails.application.routes.url_helpers.user_facebook_omniauth_authorize_path(role: 'joiner', url: trip_link, checkout: true)
    ActionController::Base.asset_host+checkout_link
  end
end
