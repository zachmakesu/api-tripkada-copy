class PhotoDecorator < Draper::Decorator
  delegate_all

  def type
    "photos"
  end

  def image_url(style=:original)
    object.image.try{|i| i.url(style)}
  end

  def cover_photo?
    object.category == "cover_photo" && object.primary
  end

  def avatar?
    object.category == "avatar" && object.primary
  end
end
