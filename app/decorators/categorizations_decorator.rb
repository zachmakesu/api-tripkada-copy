class CategorizationsDecorator < Draper::Decorator
  delegate_all
  decorates_association :category
  decorates_association :event

  def type
    "categorizations"
  end
end
