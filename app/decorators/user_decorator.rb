class UserDecorator < Draper::Decorator
  delegate_all

  decorates_association :joined_events
  decorates_association :providers
  decorates_association :profile_photos
  decorates_association :government_photos
  decorates_association :organized_events
  decorates_association :bookmarked_events
  decorates_association :received_reviews
  decorates_association :sent_reviews
  decorates_association :event_reviews
  decorates_association :certificates
  decorates_association :waitlists
  decorates_association :credit_promo_code

  def self.search_in_active_joiners(q = nil)
    query = q.present? ? "%#{q}%" : nil
    return self.most_active_joiners unless query
    where('first_name ILIKE ? OR last_name ILIKE ?',query, query).order(:created_at)
  end

  def type
    "users"
  end

  def avatar
    User.includes(:profile_avatar).find(self.id).profile_avatar.try(:image)
  end

  def cover_photo_image
    self.cover_photo.try(:image)
  end

  def cover_photo_url
    cover_photo_image.try(:url)
  end

  def avatar_url(style=:original)
    avatar.try{|a| a.url(style)}
  end

  def avatar_complete_url(style=:original)
    URI.join(ActionController::Base.asset_host, avatar_url(style)).to_s if avatar_url(style)
  end

  def facebook_url
    object.providers.social_account('facebook')
  end

  def approve!
    self.update(approved_at: DateTime.now) if self.approved_at.nil?
  end

  def delete_previous_api_keys
    api_keys.first.delete if api_keys.count > 5
  end

  def upcoming_organized_events
    object.organized_events.upcoming.order_by_start.decorate
  end

  def past_organized_events
    object.organized_events.past.sorted_event.decorate
  end

  def upcoming_joined_events
    object.joined_events.upcoming.order_by_start.decorate
  end

  def past_joined_events
    object.joined_events.past.order_by_start.decorate
  end

  def sorted_joined_events
    object.joined_events.order_by_start.decorate
  end

  def total_rating
    dividend = self.received_reviews.sum(&:rating)
    divisor = self.received_reviews.count

    divisor == 0 ? 0 : dividend / divisor
  end

  def organizer_reviews
    object.reviewed_events
  end

  def have_valid_ids?
    !government_photos.blank?
  end

  def reviewable_events(current_user)
    object.organized_events.past.where(id: current_user.reviewed_events.pluck(:id)).order_by_start.decorate
  end

  def generate_avatar
    self.profile_photos.object.create(primary: true, category: "avatar", image: URI.parse(self.image_url)) if self.avatar.nil? && !Rails.env.test?
  end

  def formatted_role
    role.gsub(/_/,' ')
  end

  def self.search(q = nil)
    query = "%#{q}%"
    where('first_name ILIKE ? OR last_name ILIKE ?',query, query).order(:created_at)
  end

  # Follows a user.
  def follow(other_user)
    active_relationships.create(followed_id: other_user.id)
  end

  # Unfollows a user.
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    following.include?(other_user)
  end

  def active_joiners
    object.organized_events_active_members.decorate
  end

  def list_of_organized_events
    object.all_organized_events.published.order_by_start.decorate.reverse
  end

  def organized_event_joiners
    object.organized_events.includes(event_memberships: [:member, payment: :promo_code]).collect(&:event_memberships).compact.flatten
  end

  def is_new_user
    object.sign_in_count == 1
  end

  def has_completed_required_info?
    object.mobile.present? && object.email.exclude?("@facebook.com")
  end

  def tagcash_token
    object.providers.tagcash.last.try(:token)
  end

  def most_booked_trips
    object.organized_events.upcoming.joins(:event_memberships).group(:id).order("count(*) desc").limit(5).decorate
  end

  def most_viewed_trips
    object.organized_events.upcoming.order_by_views.limit(5)
  end

  def self.role_options
    self.roles.except(:admin).keys.map{|role| [role.titleize, role]}
  end

  def approved_memberships_count
    self.approved_joiner_event_memberships.count
  end

  def booking_count_definition
    membership_count = self.approved_memberships_count
    case membership_count
    when 0 then "Lead"
    when 1 then "One Off"
    when 2 then "Repeat"
    else
      "Loyal"
    end
  end

  def booking_status
    last_booking_date = self.last_booking_date

    return "Lead" unless last_booking_date.is_a?(Date)

    today = Date.today
    two_months_from_now = Date.today - 2.months
    four_months_from_now = Date.today - 4.months

    if last_booking_date.between?(two_months_from_now, today)
      "Active"
    elsif last_booking_date >= two_months_from_now && last_booking_date <= four_months_from_now
      "At Risk"
    else
      "Lapsed"
    end
  end

  def instagram_provider_id
    object.instagram_provider&.id
  end
end
