class WaitlistDecorator < Draper::Decorator
  delegate_all

  decorates_association :user
  decorates_association :event

  def type
    "waitlists"
  end
end
