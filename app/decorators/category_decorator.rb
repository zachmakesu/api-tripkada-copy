class CategoryDecorator < Draper::Decorator
  delegate_all
  decorates_association :categorizations
  decorates_association :events

  def type
    "categories"
  end

  def self.search(q = nil)
    query = "%#{q}%"
    where('name ILIKE ?',query).order(:created_at)
  end

  def status_and_class
    if self.active?
      { status: "Active", class: "success" }
    else
      { status: "Inactive", class: "alert" }
    end
  end
end