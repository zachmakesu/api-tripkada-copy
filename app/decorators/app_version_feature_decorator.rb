class AppVersionFeatureDecorator < Draper::Decorator
  delegate_all

  def type
    "app_version_features"
  end
end
