class AppVersionDecorator < Draper::Decorator
  delegate_all
  decorates_association :features

  def self.search(q = nil)
    query = "%#{q}%"
    where('version_code ILIKE ? OR created_by ILIKE ?',query, query).order(created_at: :desc)
  end

  def type
    "app_versions"
  end
end
