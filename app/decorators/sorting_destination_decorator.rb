class SortingDestinationDecorator < Draper::Decorator
  delegate_all
  decorates_association :destination
  decorates_association :event

  def type
    "sorting_destinations"
  end

end
