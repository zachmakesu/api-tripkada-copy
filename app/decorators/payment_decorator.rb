class PaymentDecorator < Draper::Decorator
  delegate_all

  decorates_association :member
  decorates_association :event

  def type
    "payments"
  end

  def rate_with_promo_code_deduction
    object.event_membership.event.rate - promo_code_value
  end

  def self.search(q = nil)
    query = "%#{q}%"
    joins(:member).where('first_name ILIKE ? OR last_name ILIKE ?',query, query).order(created_at: :desc)
  end

  def downpayment_rate_with_promo_code_deduction
    downpayment_rate = object.event.downpayment_rate.to_i
    promo_code_value >= downpayment_rate ? 0 : (downpayment_rate - promo_code_value)
  end

  def promo_code_plus_amount_paid
    amount_paid = object.event_membership.event.downpayment_rate
    promo_code_value  + amount_paid
  end

  def balance
    object.fullpayment? ? 0 : rate_with_promo_code_deduction - object.amount_paid
  end

  def promo_code_value
    object.promo_code.try(:value) || 0
  end

  def promo_code
    object.promo_code.try(:code)
  end

  def event_truncated_name
    object.event.name.truncate_words(3)
  end

  def remaining_balance
    if self.fullpayment?
      0
    else
      event = self.event
      rate_with_fee = event.rate + BookingFeeDecorator.latest_fee
      downpayment_with_fee = event.downpayment_rate_with_fee.to_f
      invited_joiners = self.event_membership.invited_members.count
      members = 1 + invited_joiners
      sub_total_rate = rate_with_fee * members
      sub_total_downpayment = downpayment_with_fee * members
      promo_code = self.promo_code_value
      (sub_total_rate - sub_total_downpayment) - promo_code
    end
  end

  def promo_code_in_words
    object.promo_code.present? ? object.promo_code.code.upcase : "NONE"
  end

  def booking_fee_value
    object.booking_fee.value
  end

  def calculate_payment_made
    membership = object.event_membership
    return 0.0 unless membership.valid?
    if paypal?
      amount_paid
    else
      payment = event.downpayment_rate_with_fee
      members = 1 + membership.invited_members.count
      payment * members
    end
  end

  def dragonpay_transaction_id
    self.dragonpay_transaction_logs&.last&.transaction_id
  end

  def paypal_transaction_id
    express_payment&.payment_transaction_id
  end

  def transaction_id
    if self.paypal?
      paypal_transaction_id
    elsif self.dragonpay?
      dragonpay_transaction_id
    else
      nil
    end
  end
end
