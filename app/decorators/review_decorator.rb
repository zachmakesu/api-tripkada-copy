class ReviewDecorator < Draper::Decorator
  delegate_all

  decorates_association :reviewer
  decorates_association :event

  def type
    "reviews"
  end

  def reviewer_avatar
    reviewer.avatar
  end

  def reviewer_name
    reviewer.full_name
  end

end
