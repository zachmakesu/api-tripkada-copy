class SponsorDecorator < Draper::Decorator
  delegate_all

  def type
    "sponsors"
  end
end
