class BookingFeeDecorator < Draper::Decorator
  delegate_all

  def self.latest_fee
    last.try(:value) || 0
  end
end
