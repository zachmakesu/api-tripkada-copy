class PromoCodeDecorator < Draper::Decorator
  delegate_all

  decorates_association :event_memberships
  decorates_association :payments

  def type
    "promo_codes"
  end

  def status
    h.content_tag(:span, (object.active? ? "Active" : "Inactive"), class: [(object.active? ? "success" : "alert"),"label"])
  end

  def usage_over_limit
    "#{object.payments.count} / #{object.usage_limit}"
  end

  def usage
    object.payments.count
  end

  def self.search(q = nil)
    query = "%#{q}%"
    where('code ILIKE ?',query).order(:created_at)
  end
end
