class ProviderDecorator < Draper::Decorator
  delegate_all

  decorates_association :user

  def type
    "providers"
  end

end
