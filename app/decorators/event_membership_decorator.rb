class EventMembershipDecorator < Draper::Decorator
  delegate_all

  decorates_association :member
  decorates_association :event
  decorates_association :payment

  def type
    "event_memberships"
  end

  def self.organizer_all_valid_joiners(organizer)
    event_ids = organizer.organized_events.past.ids
    EventMembership.where("event_id IN (?) AND member_id != ?", event_ids, organizer.id).approved
  end

  def join_date_short
    self.created_at.strftime("%b %d, %Y")
  end
end
