class CustomNotificationMessageDecorator < Draper::Decorator
  delegate_all

  def self.search(q = nil)
    query = "%#{q}%"
    where('message ILIKE ?',query).order(:created_at)
  end

  def image_url(style=:original)
    self.image.url(style)
  end

  def type
    "CustomNotificationMessage"
  end
end
