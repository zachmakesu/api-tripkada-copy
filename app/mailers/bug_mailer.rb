class BugMailer < ApplicationMailer
  default from: 'tripkada.bug.reporter@gmail.com'

  def invalid_koala_api_call(provider, token, err_msg)
    @provider       = provider
    @token          = token
    @error_message  = err_msg
    mail(to: TRIPKADA_DEV, subject: "Koala failure authentication")
  end

  def paypal_rescue_payment(payment, params, client, event, err_msg)
    @payment        = payment
    @params         = params
    @client         = client
    @event          = event
    @error_message  = err_msg
    mail(to: TRIPKADA_DEV, subject: "Paypal: Invalid Payment")
  end
end
