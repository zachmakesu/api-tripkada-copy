class ReferralMailer < ApplicationMailer
  default from: 'tripkada.app@gmail.com'
  add_template_helper(FormatHelper)

  def send_to_staff(referrer_id, joiner_id, event_id)
    @referrer = User.find(referrer_id)
    @referrer_role = @referrer.decorate.formatted_role
    @referrer_full_name = @referrer.full_name
    @joiner = User.find(joiner_id)
    @joiner_full_name = @joiner.full_name
    @event = Event.find(event_id)&.decorate
    membership = @event.object.event_memberships.find_by(member_id: joiner_id)&.decorate
    @joiner_membership_date = membership&.join_date_short&.upcase

    @referral_incentive = ReferralIncentive.latest

    mail(to: mail_recipients, subject: "Referral Incentive")
  end

  def send_to_referrer(referrer_id, joiner_id, event_id)
    @referrer = User.find(referrer_id)
    @referrer_full_name = @referrer.full_name
    @joiner = User.find(joiner_id)
    @joiner_full_name = @joiner.full_name
    @event = Event.find(event_id)
    membership = @event.event_memberships.find_by(member_id: joiner_id)&.decorate
    @joiner_membership_date = membership&.join_date_short&.upcase
    @referral_incentive = ReferralIncentive.latest

    mail(to: @referrer.email, subject: "You Earn Referral Incentive")
  end
end
