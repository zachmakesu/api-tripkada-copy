class UserMailer < ApplicationMailer
  helper :mailer

  def sign_me_up_email(user)
    @user = user
    mail(to: mail_recipients, subject: 'Sign up email')
  end

  def apply_as_organiser(params)
    @params = params
    mail(to: mail_recipients, subject: "Apply for trip organiser")
  end

  def trip_request(params)
    @params = params
    mail(to: mail_recipients, subject: "New trip request")
  end

  def question(params)
    @params = params
    mail(to: mail_recipients, subject: "New question")
  end

  def join_event(membership)
    @joiner = membership.member.decorate
    @event = membership.event.decorate
    @payment = membership.payment
    @other_recipients = %W{ #{@event.owner.email} }
    mail(to: mail_recipients, subject: "New Joiner of trip '#{@event.name}'")
  end

  def apply_as_organizer_from_organizer_app(params)
    @params = params
    mail(to: mail_recipients, subject: "Apply for trip organizer from Organizer App")
  end

  def referral_invite(email, params)
    @params = params
    mail(to: email, subject: "You have been invited to join Tripkada!")
  end
end
