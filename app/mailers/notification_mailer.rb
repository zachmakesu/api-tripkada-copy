class NotificationMailer < ApplicationMailer
  add_template_helper(EventHelper)
  add_template_helper(FormatHelper)
  add_template_helper(MailerHelper)

  def signup_welcome_email(user_email)
    mail to: user_email, subject: "Welcome to Tripkada!"
  end

  def booking_confirmation_joiner(membership)
    @joiner = membership.member.decorate
    @event = membership.event.decorate
    @payment = membership.payment.decorate
    @joiner_email = %W[ #{@joiner.email} ]
    mail(to: @joiner_email, subject: "TRIPKADA BOOKING CONFIRMATION: '#{@event.name}'")
  end

  def booking_notification_organizer(membership)
    @joiner = membership.member.decorate
    @event = membership.event.decorate
    @organizer = membership.event.owner
    @payment = membership.payment.decorate
    mail(to: @organizer.email, subject: "TRIPKADA You just got a booking for '#{@event.name}'")
  end

  def invite_joiner_via_email(params)
    @event = Event.find(params["id"])
    @recipient_name = params["full_name"]
    @recipient_email = params["email"]
    @organizer_email = @event.owner.email
    @organizer_name = @event.owner.decorate.full_name
    mail(to: @recipient_email, subject: "#{@organizer_name} has invited you to join on his/her Trip '#{@event.name}'")
  end

  def trip_invitation(inviter, event, recipient)
    @inviter_name = inviter.decorate.full_name
    @event = event
    @recipient_name = recipient.decorate.full_name
    @uid = recipient.uid
    recipient_email = recipient.email
    mail(to: recipient_email, subject: "TRIPKADA TRIP MEMBERSHIP")
  end

  def request_trip_different_date(requester, event, params)
    @event_name = event.name.titleize
    @organizer = event.owner.decorate
    @requested_start_date = DateTime.parse params[:start_date_time]
    @requested_end_date = DateTime.parse params[:end_date_time]
    @requested_pax = params[:requested_pax]

    mail(to: @organizer.email, subject: "#{@event_name} Different Trip Date Request")
  end

  def review_trip_reminder(event, member)
    @event = event
    @member = member

    mail(to: member.email, subject: "REVIEW TRIP")
  end

  def waitlist_membership_status(event:, user:, status:)
    @event = event
    @user = user.decorate
    @status = status
    mail(to: @user.email, subject: "WAITLIST MEMBERSHIP STATUS")
  end
end
