class ApplicationMailer < ActionMailer::Base
  default from: "join@tripkada.com"
  layout 'mailer'

  TRIPKADA_DEV = %w[
    kevinc@gorated.ph mcdave@gorated.ph rommel@gorated.com redjoker011@gmail.com
    caasi.mirabueno@gmail.com zhiena@gorated.ph jaime@gorated.ph
    cjbm.martinez@yahoo.com manalaysaykent@yahoo.com
  ]
  TRIPKADA_STAFF = %w{ community@tripkada.com april@fliptrip.ph ragde@gorated.ph jointripkada@gmail.com rhave@tripkada.com gia@tripkada.com jp@tripkada.com }

  private
  def mail_recipients(*opts)
    @recipients = if Rails.env.production?
                    TRIPKADA_DEV + TRIPKADA_STAFF + opts
                  else
                    TRIPKADA_DEV
                  end
    @recipients.flatten.uniq
  end
end
