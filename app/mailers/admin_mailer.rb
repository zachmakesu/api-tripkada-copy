class AdminMailer < ApplicationMailer
  add_template_helper(EventHelper)
  add_template_helper(FormatHelper)

  def new_trip(event)
    @event = event.decorate
    @organizer = event.owner.try(:decorate)
    mail(to: mail_recipients, subject: "NEW TRIP")
  end

  def canceled_trip(event)
    @event = event.decorate
    @organizer = event.owner.try(:decorate)
    @memberships = event.approved_event_memberships
    mail(to: mail_recipients, subject: "CANCELED TRIP")
  end

  def review_organizer(review)
    @organizer = review.reviewable.try(:decorate)
    @reviewer = review.reviewer.try(:decorate)
    @review = review

    mail(to: mail_recipients, subject: "REVIEW FOR ORGANIZER")
  end

  def photo_verification(user, photos)
    # Paperclip attachment reader
    # ref: https://stackoverflow.com/a/23208454
    @user = user.decorate
    photos.each.with_index(1) do |photo, index|
      attachments.inline[
        "#{@user.full_name.downcase.gsub!(' ', '-')}-#{photo.category}-#{index}"\
        ".#{photo.image_content_type.split('/').last}"
      ] = Paperclip.io_adapters.for(photo.image).read
    end
    mail(to: mail_recipients, subject: "PHOTO VERIFICATION")
  end

  def request_trip_different_date(requester, event, params)
    @requester = requester.decorate
    @event_name = event.name.titleize
    @organizer = event.owner.decorate
    @requested_start_date = DateTime.parse params[:start_date_time]
    @requested_end_date = DateTime.parse params[:end_date_time]
    @requested_pax = params[:requested_pax]

    mail(to: mail_recipients, subject: "#{@event_name} Different Trip Date Request")
  end

  def invalid_request
    @timestamp = DateTime.now
    attachments["invalid_request_logs.txt"] = File.read(Rails.root.join('public', 'invalid_request_logs.txt'))
    mail(to: mail_recipients, subject: "Invalid Request Log")
  end
end
