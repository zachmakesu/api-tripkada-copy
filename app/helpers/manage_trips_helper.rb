module ManageTripsHelper
  def validate_status_of(event)
    status = event.status_in_words
    content_tag :p, status, class: status.downcase
  end

  def generate_cancel_trip_options_for(event)
    if event.deleted_at.present?
      (content_tag :p, event.trip_cancel_reason, class: "options reason")
    elsif event.is_past?
      content_tag :p, nil, class: "options" do
        content_tag(:select, id: "option", name: "option") do
          content_tag(:option, value:"no"){ "NO" }
        end
      end
    else
      content_tag :p, nil, class: "options" do
        content_tag(:select, "data-trip-slug" => event.slug, id: "option", name: "option") do
          content_tag(:option, value:"no"){ "NO" } + content_tag(:option, value:"cancel"){ "CANCEL TRIP" }
        end
      end
    end
  end

  def amount_remitted_for(event)
    @rate = event.rate
    @members = event.decorate.valid_joiner_memberships.count
    @amount_remitted = @rate * @members
    currency_format(@amount_remitted)
  end

  def date_of_remittance_for(event)
    @remittance_date = event.start_at - 1.day
    month_day_year_format(@remittance_date)
  end

  def render_template_for(user)
    partial = user.decorate.upcoming_organized_events.any? ? "dashboard" : "empty"
    render partial: partial
  end

  def list_destinations(destinations)
    "".tap do |html|
      destinations.each { |destination|  html << "#{content_tag(:span, destination.name)} <br>"}
    end.html_safe
  end
end
