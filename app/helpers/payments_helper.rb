module PaymentsHelper
  def payment_status_options
    Payment.statuses.keys.tap do |status|
      status.delete("expired")
    end
  end
end
