module EventHelper

  def highlight_inclusion_builder(event)
    event = Event.find(event).decorate
    object_counter = []
    object_on_loop = []

    h = event.highlight_lists
    i = event.inclusion_lists
    t = event.things_to_bring_lists
    row_count = [h.count,i.count,t.count].max
    (0..row_count-1).map do |x|
      {highlight: h[x], inclusion: i[x], things_to_bring: t[x]}
    end

  end

  def header_content(action_name)
    case action_name
    when "checkout"
      content_tag :h4, "Checkout"
    when "payment_details"
      content_tag :h4, "Payment"
    when "my_trips"
      link_to("#{Event::EVENT_SCHEME[:upcoming].capitalize}", my_trips_path(type: "#{Event::EVENT_SCHEME[:upcoming]}"), class: ("active" if type_is_upcoming?(@event_type) )) +
        link_to("#{Event::EVENT_SCHEME[:previous].capitalize}", my_trips_path(type: "#{Event::EVENT_SCHEME[:previous]}"), class: ("active" if @event_type.previous?)) +
        link_to("#{Event::EVENT_SCHEME[:bookmarks].capitalize}", my_trips_path(type: "#{Event::EVENT_SCHEME[:bookmarks]}"), class: ("active" if @event_type.bookmarks?))

    when "new"
      content_tag(:h4, "Create Trip")
    else
      content_tag(:h4, "Trips")
    end
  end

  def is_check_out?
    current_page?(controller: 'event', action: 'checkout')
  end

  def checkout_progress
    content_tag(:div, class: "progress-container") do
      content_tag(:div, content_tag(:p, "REVIEW", class: "active"), class: "progress large-4 columns active") +
        content_tag(:div, content_tag(:p, "SELECT", class: (is_check_out? ? "" : "active" )), class: (is_check_out? ? "progress large-4 columns" : "progress large-4 columns active")) +
        content_tag(:div, content_tag(:p, "CONFIRM", class: (is_check_out? ? "" : "active" )), class: (is_check_out? ? "progress large-4 columns" : "progress large-4 columns active"))
    end
  end

  def join_and_pay
    user_signed_in? ? dummy_or_payment_form? : render_dummy_proof_payment("FBLogin")
  end

  def dummy_or_payment_form?
    current_user.mobile.present? ? (render partial: 'proof_payment_form') : render_dummy_proof_payment("editDetailModal")
  end

  def render_dummy_proof_payment(dummy_view)
    render partial: 'dummy_proof_payment', locals: {dummy_view: dummy_view}
  end

  def inclusions
    EventTag.inclusions.pluck(:name)
  end

  def things
    EventTag.things.pluck(:name)
  end

  def generate_input_file_tag_for(event)
    content_tag :ul, class: "file-picker-with-view" do
      4.times.collect do |t|
        photo = event.photos[t]
        concat content_tag :li, (content_tag(:div, content_tag(:label, nil, class: "ion-ios-camera", for: "photo#{t}")) + file_field_tag("event[photos][]", onchange: "event_readURL(this)", id: "photo#{t}") + text_field_tag("event[photo_ids][]", (photo.id if photo.present?))), style: "background-image:url('#{photo.try(:image)}')", class: "#{'has-image' if photo.present? }", onclick: "#{'make_photo_focus(this)' if photo.present? }"
      end
    end
  end

  def trips_per_slider(trips)
    if trips.count <= 12
      6
    elsif trips.count > 12 && trips.count <= 16
      8
    else
      (trips.count / 2.0).round
    end
  end

  def tagcash_link
    if current_user.providers.tagcash.exists?
      link_to image_tag("tagcash-logo.png"), "#", onclick: "checkoutUsing('tagcash','#{payment_details_path}')"
    else
      state = Base64.urlsafe_encode64(current_user.uid+";"+params[:slug])
      link_to image_tag("tagcash-logo.png"), "https://api.tagcash.com/oauth?client_id=#{ENV["TAGCASH_ID"]}&redirect_uri=#{tagcash_callback_url}&response_type=code&scope=user.wallets&state=#{state}"
    end
  end

  def months_listing
    Hash[Date::MONTHNAMES.map.with_index.to_a.drop(1)]
  end

  def generate_event_payment_url(event, code=nil, mode=nil)
    if mode == "paypal"
      handler = PaypalHandler.new({code: code, mode: mode}, event).create_instant_payment
      handler.response[:details].links.find{|v| v.method == "REDIRECT" }.href
    else
      ActionController::Base.default_url_options[:host] + invited_payment_details_path(event, {code: code, mode: mode})
    end
  end

  def generate_booking_button_for event
    book_trip = 'BOOK TRIP'
    request_trip = 'REQUEST THIS TRIP'
    share_trip = 'SHARE TRIP'
    formatted_price = content_tag(:span, "₱") + content_tag(:span, "#{ currency_format event.rate_with_fee }") + content_tag(:span, " / Person")
    content_tag(:p, class: 'price') do
      if !user_signed_in?
        options = Hash.new
        if mobile_device?
          options[:rev_class] = 'reveal tiny login-modal tripkada-login'
        else
          options[:rev_class] = 'reveal tiny tripkada-modal tripkada-login'
        end
        options[:rev_id] = 'BookFBLogin'
        options[:content] = content_tag(:div, class: 'tripkada-login--flex') do
          header = content_tag(:div, "LOGIN", class: 'tripkada-login__header')

          joiner_btn = link_to("<span class='login-joiner ion-social-facebook'></span>Login with Facebook".html_safe, user_facebook_omniauth_authorize_path(role: "joiner", url: request.url, return_to: trip_checkout_path(event), checkout: true, slug: event.slug), class: "tripkada-login__login-joiner")
          joiner_signup = generate_login_signup_link(label: "Sign up", role: "joiner", options:{ return_to: trip_checkout_path(event), checkout: true, slug: event.slug })

          line = generate_login_signup_line(name: "login")
          header + joiner_btn + joiner_signup
        end + content_tag(:div, nil, class: 'tripkada-login__modal_image')
        if event.is_upcoming?
          formatted_price + link_to(book_trip, '#', class: 'join-btn', 'data-open' => 'BookFBLogin')
        else
          link_to(request_trip, '#', class: 'join-btn', 'data-open' => 'BookFBLogin')
        end + foundation_reveal_tag(options)
      elsif event.is_past?
        link_to request_trip , trip_requests_path({event: @event.name}), class: 'join-btn'
      elsif event.members.include?(current_user)
        if event.expired_event_memberships.exclude?(current_user.event_memberships.last) ||
          event.expired_event_memberships.exclude?(current_user.expired_event_memberships.last)
          formatted_price + link_to(share_trip, "#", "data-share-trip" => trip_view_url(event), class: "join-btn")
        else
          formatted_price + link_to(book_trip, trip_checkout_path, "data-page-redirect" => '', class: "join-btn")
        end
      elsif !current_user.decorate.has_completed_required_info?
        formatted_price + link_to(book_trip, "#", "data-open" => "editDetailModal", class: "join-btn")
      elsif event.full?
        options = Hash.new
        options[:rev_class] = 'reveal waitlist-modal custom-modal-with-header'
        options[:rev_id] = 'waitlist-modal'
        options[:content] = content_tag(:p, 'Uh oh! Looks like this trip is already full would you like to') +
          waitlist_button(event) +
            link_to('Request for a private trip', trip_requests_path, class: 'waitlist-request-private-btn')
        button_for_full_event(event, formatted_price) + foundation_reveal_tag(options)
      else
        formatted_price + link_to(book_trip, trip_checkout_path, "data-page-redirect" => '', class: "join-btn")
      end
    end
  end

  def button_for_full_event(event, formatted_price)
    if current_user.waitlist_for(event).present?
      link_to("WAITLIST REQUEST SENT", "#", "data-open" => "waitlist-modal", class: "join-btn")
    else
      formatted_price + link_to("BOOK TRIP", "#", "data-open" => "waitlist-modal", class: "join-btn")
    end
  end

  def waitlist_button(event)
    if current_user.waitlist_for(event).present?
      link_to("SHARE TRIP", "#", "data-share-trip" => trip_view_url(event), class: "waitlist-join-btn")
    else
      link_to('Join the waitlist', join_waitlist_path(event), class: 'waitlist-join-btn', method: :post)
    end
  end

  def foundation_reveal_tag opts={}
    content_tag :div, class: opts[:rev_class], id: opts[:rev_id], 'data-reveal' => '' do
      content_tag(:header, class: opts[:header_class], id: opts[:header_id]) do
        (opts.fetch(:header_content,'') + content_tag(:button, nil, class: 'close-button', 'data-close' => '', 'aria-label' => 'close modal', type: 'button')).html_safe
      end + opts.fetch(:content, '')
    end
  end

  def generate_opengraph_image_url(event_image_url)
    image_url(event_image_url, ActionController::Base.default_url_options)
  end

  def generate_duplicate_button_for event
    tag         = event.is_past? ? "reactivate" : "duplicate"
    icon        = event.is_past? ? "ion-loop" : "ion-ios-browsers"
    title       = "#{tag.titleize} Event"
    class_name  = "#{tag}-event #{icon}"
    link_to organizer_trip_duplicate_path(event), title: title do
      content_tag(:span, nil, class: class_name)
    end
  end

  def generate_facebook_link_for(member)
    el_class = 'has-tip top'
    image = "background-image: url(#{avatar_url_for(member)})"
    name = member.full_name
    data_attr = { 'tool-tip' => '', 'disabled-hover' => false}
    members_list = content_tag :div, nil,
      data: data_attr, class: el_class, 'aria-haspopup' => true,
      tabindex: 3, title: name, style: image

    if member.providers.blank?
      members_list
    else
      link_to profile_path(member.slug), target: "_blank" do
        members_list
      end
    end
  end

  def generate_event_details(event)
    if event.is_upcoming?
      fee_per_person = "#{number_to_currency(event.rate_with_fee,  unit: '')} / Person"
      content_tag(:div, content_tag(:p, content_tag(:span, "#{event.slots_left} slots left!"), class: "slots-left")) +
        content_tag(:div, content_tag(:p, fee_per_person), class: "notice rate")
    else
      content_tag :div do
        content_tag :p, "#{event.valid_members.count} Joined", class: "slots-left"
      end
    end
  end

  def generate_my_trips_page(events)
    events.empty? ? "empty_my_trips" : "paginated_my_trips"
  end

  def generate_empty_message(event_type)
    partial_message = if event_type.bookmarks?
                        "bookmarked trips yet"
                      elsif event_type.previous?
                        "trips yet"
                      else
                        "trips coming up yet"
                      end
    header_type = if !type_is_upcoming?(event_type)
                    event_type.bookmarks? ? "bookmarked" : event_type.type
                  else
                    "upcoming"
                  end
    content_tag(:h5, "No #{header_type} trips") +
      content_tag(:p, "Oops! Looks like you don't have any #{partial_message}."\
                  "<br>Time to find your next adventure!".html_safe)
  end

  def avatar_url_for(member)
    member_avatar = member.avatar_url(:thumb)
    custom_avatar = letter_avatar_url(member.full_name, 200)

    member_avatar || custom_avatar
  end

  def current_user_is_owner_of_event? event
    event.try(:owner) && user_signed_in? ? event.owner == current_user : false
  end

  def paginated_events_count(events)
    events.count <= 3 ? 3 : 6
  end

  def pluralize_downcase text
    text.downcase.pluralize
  end

  def pluralize_capitalize text
    text.capitalize.pluralize
  end

  def category_fields
    {
      association_collection: @categories,
      associated_collection: @event.categories,
      title: "Category",
    }
  end

  def destination_fields
    {
      association_collection: @destinations,
      associated_collection: @event.destinations,
      title: "Destination",
    }
  end

  def type_is_upcoming?(type)
    type.params_type_not_present? || type.upcoming? || type.invalid_params_type?
  end

  def valid_for_duplicate(orig_event, event)
    !orig_event.new_record? && event.new_record?
  end

  def brand_sponsor_logo_uploader(user, event)
    content_tag :div, id: "brand_logo_container",
      class: "focus-photo #{"toggle-focus-hover" if event.sponsors.blank?}",
      style: "background-image:url('#{event.brand_sponsor_logo_url(:large)}')" do
      concat(content_tag :div,
             content_tag(:label, nil, class: "ion-ios-camera", for: "brand_sponsor_logo") +
             content_tag(:button, nil, type: "button",class: "ion-trash-b",
                         onclick: "removePhotoURL(brand_sponsor_logo, brand_logo_container)")
      )
      concat (content_tag :input, nil, class: "hide", name: "event[brand_sponsor_logo]",
              id: "brand_sponsor_logo", type: "file", onchange: "readPhotoURL(this, brand_logo_container)")
      concat (content_tag :input, nil, value: event.sponsors.present? ? event.sponsors.last.id : nil, name: "event[brand_sponsor_id]",
              id: "brand_sponsor_id", type: "hidden")
    end
  end

  def brand_sponsor_logo(event)
    text = content_tag :p, "SPONSORED BY", class: "brand-sponsor__container__text"
    img = image_tag event.brand_sponsor_logo_url, class: "brand-sponsor__container__logo"
    content_tag :section, class: "brand-sponsor" do
      content_tag :div, class: "brand-sponsor__container" do
        text + img
      end
    end if event.sponsors.present?
  end


  def generate_download_button
    content_tag(:div, class: 'download-app-btn download-app-btn--mobile-trips') do
       # where 'download-app-btn' is component class and 'download-app-btn--mobile' is component/container modifier class
      content_tag(:span, "GET THE APP ", class: 'download-app-btn download-app-btn__center') +
        # where 'download-app-btn__center is the element class since it doesnt modify any class
      link_to("<i class='ion-play download-app-btn__playstore_icon'> </i>".html_safe, "https://play.google.com/store/apps/details?id=ph.fliptrip.tripkada.joiner") +
      link_to("<i class='ion-social-apple download-app-btn__apple_icon'> </i>".html_safe, "https://itunes.apple.com/ph/app/tripkada-trip-pooling-app/id1333012684?mt=8")
          # where 'download-app-btn__playstore' and 'download-app-btn__apple' are both element of component/container 'download-app-btn'
    end
  end

  def generate_messenger_notif_content
    content_tag :div do
      check_email_message = "You're all set for your next adventure. Don't forget to check your email " \
      "for your booking confirmation and more details to help you get ready for your trip."
      click_button_message = "Never miss an update or announcement! Click on the button below " \
      "to get the latest updates through your FB Messenger."
      share_travel_button = (link_to "SHARE TRAVEL", "#", "data-share-trip" => trip_view_url(@event))
      content_tag(:h5, "Congratulations!") +
      content_tag(:p, check_email_message) + share_travel_button +
      content_tag(:p, click_button_message)

    end
  end
end
