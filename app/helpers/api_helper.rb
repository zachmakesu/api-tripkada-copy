module ApiHelper
  extend Grape::API::Helpers

  def build_attachment(image)
    attachment =  {
      :filename => image[:filename],
      :type => image[:type],
      :headers => image[:head],
      :tempfile => image[:tempfile]
    }
    ActionDispatch::Http::UploadedFile.new(attachment)
  end

  # TODO: Change references to utc
  # Why name format :utc if it returns :iso8601?
  Grape::Entity.format_with :utc do |date|
    DateTime.parse(date.to_s).iso8601 unless date.blank?
  end

  ['original','thumb','medium','large','xlarge','xxlarge'].each do |size|
    Grape::Entity.format_with "#{size}_photo_url".to_sym do |photo|
      #photo = photo || Photo.default
      URI.join(ActionController::Base.asset_host, photo.url(size)).to_s if photo
    end
  end

  def validate_platform(user_agent:, type:)
    @mod_params = {}
    @mod_params[:user_agent] = user_agent
    @mod_params[:type] = type
    handler = Organizers::DevicePlatform.call(params: @mod_params)
    handler.device_platform
  end
end
