module LayoutHelper
  # Used to achieve nested layouts without content_for. This helper relies on
  # Rails internals, so beware that it make break with future major versions
  # of Rails. Inspired by http://stackoverflow.com/a/18214036
  #
  # Usage: For example, suppose "child" layout extends "parent" layout.
  # Use <%= yield %> as you would with non-nested layouts, as usual. Then on
  # the very last line of layouts/child.html.erb, include this:
  #
  #     <% parent_layout "parent" %>
  #
  def parent_layout(layout)
    @view_flow.set(:layout, output_buffer)
    output = render(:file => "layouts/#{layout}")
    self.output_buffer = ActionView::OutputBuffer.new(output)
  end

  def dynamic_layout(controller_name, action)
    admin_controllers = %w(
      admin 
      categories 
      destinations 
      promo_codes 
      payments 
      users 
      booking_fees 
      referral_incentives 
      app_versions 
      mailerlite 
      custom_notification_messages
    )

    layout =  if %w(sign_in_as_organizer).include?(action) || (controller_name == "sessions" && action == "new")
                "blank"
              elsif for_user_profile?(controller_name, action)
                "base"
              elsif admin_controllers.include?(controller_name)
                "admin"
              elsif %w(sessions home).include?(controller_name)
                "main"
              elsif %w(event notifications organizer).include?(controller_name)
                "base"
              elsif %w(messenger_webview).include?(controller_name)
                "messenger_webview"
              else
                "blank"
              end
    @view_flow.set(:layout, output_buffer)
    output = render(:file => "layouts/#{layout}")
    self.output_buffer = ActionView::OutputBuffer.new(output)
  end

  def default_title
    "Tripkada - Join Trips Planned By Travelers Like You"
  end

  def custom_title_tag
    if params[:controller] == "event" && params[:action] == "page"
      (strip_tags(content_for(:title)) if content_for?(:title)) + " | " + default_title
    else
      default_title + " | " + strip_tags(content_for(:title)) if content_for?(:title)
    end
  end

  def for_user_profile?(controller_name, action)
    invalid_actions = %w(index update_role segmentations export_segmentations current_user_uid organizer_applicants)
    controller_name == "users" && !invalid_actions.include?(action)
  end
end
