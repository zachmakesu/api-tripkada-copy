module ApplicationHelper
  include LetterAvatar::AvatarHelper
  def default_ogp
    "<meta property='fb:app_id' content='#{ENV['FB_APP_ID']}' />
    <meta property='og:title' content='Tripkada : Joining trips in the Philippines ' />
    <meta property='og:description' content='A mobile app for joining trips with fellow travellers in the Philippines. Learn more Now!' />
    <meta name='description' content='A mobile app for joining trips with fellow travellers in the Philippines. Learn more Now!'>
    <meta property='og:image' content='#{asset_url('tripkada-og-logo.png')}' />".html_safe
  end

  def generate_fb_app_id
    ENV.fetch("FB_APP_ID")
  end

  def generate_fb_page_id
    ENV.fetch("FB_PAGE_ID")
  end

  def is_mobile?
    request.user_agent =~ /Mobile|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/
  end

  def truncated_custom_description(d)
    d.truncate(100) + " Join This Trip Now!"
  end

  def generate_login_signup_link(label: , role:, options: {})
    organizer_copy = content_tag(:div, "Apply as organizer", class: 'tripkada-login__footer')

    link = link_to("Apply Now",user_facebook_omniauth_authorize_path(role:
        "#{role}", url: request.url),class: 'tripkada-login__link-text')
    signup_link = link_to(label ,user_facebook_omniauth_authorize_path(role:
        "#{role}", url: request.url, return_to: options[:return_to], checkout: options[:checkout], slug: options[:slug]), class: 'tripkada-login__link-text')
    login_link = link_to("Login" ,user_facebook_omniauth_authorize_path(role:
        "#{role}", url: request.url, return_to: options[:return_to], checkout: options[:checkout], slug: options[:slug]),class: 'tripkada-login__link-text')

    organizer_login_copy = if label == "Sign up"
      content_tag(:div) do
        content_tag(:div, "Sign up as organizer", class: 'tripkada-login__footer') + signup_link
      end + organizer_copy
    else
      content_tag(:div) do
        content_tag(:div, "Login as organizer", class: 'tripkada-login__footer') + login_link
      end + organizer_copy
    end + link

    joiner_copy = content_tag(:div, "Don't have an account?", class: 'tripkada-login__footer') + signup_link

    copy = role == "joiner" ? joiner_copy : organizer_login_copy
    content_tag(:div, class: "tripkada-login--flex__copy__link #{"tripkada-login--flex__copy__link--margin-less" if options[:copy_for] == "LOGIN"}") do
      copy
    end
  end

  def generate_login_signup_line(name:)
    hr = content_tag(:hr, " " , class: 'tripkada-login__line-style')
    content_tag :div, class: "tripkada-login__#{name.delete(' ')}-line" do
      hr + " or " + hr
    end
  end

  def generate_login_signup_btn(name: , role:)
    link_to("#{name}
     ".html_safe, user_facebook_omniauth_authorize_path(role:
     "#{role}", url: request.url), class: "tripkada-login__login-#{role}")
  end

  def generate_login_signup_link_home(label:)
    body = content_tag(:div, class: 'tripkada-login--flex') do
        header = content_tag(:div, label, class: 'tripkada-login__header')
        joiner_btn = generate_login_signup_btn(name: "<span class='login-joiner ion-social-facebook'></span>
          #{label.capitalize} with Facebook", role: "joiner")

        joiner_signup = generate_login_signup_link(label: "Sign up", role: "joiner")
        organizer_signup = generate_login_signup_link(label: "Sign up", role: "organizer")
        organizer_login = generate_login_signup_link(label: "Apply Now", role: "organizer", options: { copy_for: label })

        line = generate_login_signup_line(name: "#{label.downcase}")

      if (label == "LOGIN")
        button = (content_tag(:div, nil, class: "tripkada-login--flex__copy--login"){ joiner_btn + joiner_signup })
        header + button + line + organizer_login
      else
        button = (content_tag(:div, nil, class: "tripkada-login--flex__copy"){ joiner_btn + organizer_signup })
        header + button
      end
    end
    image = content_tag(:div, " ", class:'tripkada-login__modal_image')
  body + image
  end

  def identify_user_platform
    browser.name
  end

  def identify_device_platform
    @mod_params = {}
    @mod_params[:user_agent] = request.user_agent
    @mod_params[:type] = "controller"
    handler = Organizers::DevicePlatform.call(params: @mod_params)
    handler.device_platform
  end
end
