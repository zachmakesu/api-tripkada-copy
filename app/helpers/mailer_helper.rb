module MailerHelper
  def display_invited_joiners(membership)
    return unless membership.invited_members.any?
    invited_joiners = membership.invited_members

    joiners_list = []
    joiners_list.tap{ |list|
      invited_joiners.collect{ |joiner|
        hash = {}
        hash[:Name] = joiner.decorate.full_name
        hash[:Email] = joiner.email
        list << hash
      }
    }

    table_label = content_tag :h1, "Invited Joiners"

    columns = ["Name", "Email"]
    head = content_tag :thead do
      columns.collect{ |col| concat content_tag(:th, col) }.join().html_safe
    end

    body = content_tag :tbody do
      data = joiners_list.collect do |joiner|
        content_tag :tr do
          columns.collect{ |col|
            concat content_tag(:td, joiner[col.to_sym])
          }.to_s
        end
      end
      data.join().html_safe
    end
    table_label + (content_tag :table, head.concat(body))
  end

  def with_invited_joiners(payment) 
    invited_members = payment.event_membership&.invited_members
    if invited_members.present?
      content_tag :span, "(with #{invited_members.count} invited joiner(s))", class: "highlight-bold highlight-text"
    end
  end

  def with_promo_code(payment) 
    promo_code = payment.promo_code
    if promo_code
      content_tag :span, "PROMO CODE: #{promo_code}", class: "highlight-bold highlight-text"
    end
  end
end
