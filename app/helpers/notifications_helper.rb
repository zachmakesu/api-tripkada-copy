module NotificationsHelper

  def notification_class(notification)
    !notification.read ? "type-of-notification active" : "type-of-notification"
  end

  def notification_data(notification)
    notification_data = {}
    notification_data.tap do |data|
      data[:image_url] =  notification.cover_photo(:large)
      data[:message] =  notification.message
      data[:has_read] =  notification.read?
      data[:notification_link] =  user_notification_path(notification)
      data[:open] = "notificationModal"
      if notification.notificationable_type == "Event"
        trip_data = data[:trip_data] = {}
        trip_data[:trip_url] = trip_view_path(notification.notificationable)
        trip_data[:organizer_avatar] = notification.notificationable.owner&.avatar_url
        trip_data[:trip_name] = notification.notificationable&.name
        if notification.waitlist_status?
          trip_data[:waitlist_checkout_url] = get_waitlist_checkout_url(
            event: notification.notificationable, user_id: notification.user_id
          )
        end
      end
    end
  end

  def get_waitlist_checkout_url(event:, user_id:)
    waitlist = event.waitlists.object.find_by(user_id: user_id)
    waitlist_checkout_path(waitlist.id) if waitlist&.approved?
  end
end
