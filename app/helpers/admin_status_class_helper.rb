module AdminStatusClassHelper
  def validate_status_and_class(object)
    state = object.status_and_class
    content_tag :p, state[:status], class: state[:class]
  end
end
