module HomeHelper

  def generate_title(type)
    case type
      when "featured" then "FEATURED TRIPS"
      when "upcoming" then "UPCOMING TRIPS"
      else
        "TRIPS THIS MONTH"
    end
  end

  def generate_button_for(type)
    case type
      when "featured"
        link_to "SEE ALL FEATURED TRIPS", trips_path(featured: true), class: 'button'
      when "upcoming"
        link_to "SEE ALL UPCOMING TRIPS", trips_path, class: 'button'
      else
        link_to "SEE ALL TRIPS THIS MONTH", trips_path(start_month: month_year_format(DateTime.current), end_month: month_year_format(DateTime.current)), class: 'button'
    end
  end

  def generate_organizer_application_form_submit_btn
    if user_signed_in?
      button_tag "SUBMIT"
    else
      content_tag :button, "SUBMIT", type:"button", data: { open: "loginModal" }
    end
  end

  def generate_user_info(field)
    current_user.try(field) || params[field]
  end
end
