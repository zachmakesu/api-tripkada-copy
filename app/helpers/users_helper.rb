module UsersHelper

  def status_with_icon(membership)
    downpayment = membership.payment.downpayment?
    icon = content_tag :span, nil, class: "icon " +(downpayment ? "ion-ios-redo" : "ion-checkmark")
    content = icon + content_tag(:span, downpayment ? " Downpayment Paid" : "Fully Paid")
    content_tag :p, content, class: (downpayment ? "downpayment" : "fullpayment")
  end

  def has_completed_data_of(current_user)
    current_user.mobile.blank?
  end

  def generate_profile_photos_input_file_tag_for(user)
    content_tag :ul, class: "file-picker-with-view" do
      6.times.collect do |t|
        photo = user.profile_photos[t]
        concat content_tag :li, (content_tag(:div,
          content_tag(:label, nil, class: "ion-ios-camera", for: "profile_photo#{t}")
        ) +
          file_field_tag("user[profile_photos][][photo]", onchange: "profilePhotoReadURL(this)", id: "profile_photo#{t}") +
          text_field_tag("user[profile_photos][][label]", "#{photo.try(:label)}", id: "profile_photo_label#{t}") +
          text_field_tag("user[profile_photos][][photo]", "#{photo.try(:id)}")
        ),
          style: "background-image:url('#{photo.try(:image)}')",
          class: "#{'has-image' if photo.present? }",
          onclick: "#{'changeProfilePhotoModal(this)' if photo.present?}"
      end
    end
  end

  def generate_profile_photos_show_for(user)
    if user.profile_photos.empty?
      content_tag :div, class: "empty-photos-container" do
        image_tag("no-photo.png", class: "no-photos-image") +
          content_tag(:p, "No photos", class: "title") +
          (link_to "UPLOAD PHOTOS", profile_edit_path(user), class: "empty-button" if current_user_is?(user))
      end
    else
      content_tag :ul, class: "file-picker-with-view show" do
        user.profile_photos.each do |photo|
          concat content_tag :li, (),
          style: "background-image:url('#{photo.try(:image)}')", class: "#{'has-image' if photo.present? }",
          onclick: "showProfilePhotoModal('#{photo.try(:image).url}', '#{photo.label}')"
        end
      end
    end
  end

  def generate_social_accounts_icons_for(user)
    unless user.joiner_and_organizer? && current_user&.joiner?
      content_tag :div, class: "social-icons" do
        concat link_to icon("facebook-official"), user.facebook_provider.url, class: "facebook" if user.facebook_provider.present? && current_user.present?
      end
    end
  end

  def users_notification_count
    user_signed_in? ? current_user.notifications.not_seen.count : 0
  end

  def users_notification_count_string
    (count = current_user.notifications.not_seen.count > 99 ? "99+" : current_user.notifications.not_seen.count) if user_signed_in?
  end

  def users_notification_empty?
    users_notification_count <= 0
  end

  def credit_code
    current_user.credit_code
  end

  def credit_promo_code_available_credits
    current_user&.credit_promo_code&.available_credits.to_f
  end

  def generate_review_details(user)
    body = (image_tag("Review.png") + content_tag(:div, "#{user.received_reviews.count} reviews"))
    content = if is_mobile?
                body
              else
                content_tag :div, class: "count" do
                  body
                end
              end
    content unless user == current_user
  end

  def generate_follow_button(user)
    unless user == current_user || user.joiner?
      if user.followers.include?(current_user)
        link_to "<i class='icon ion-ios-minus-empty'></i> Unfollow".html_safe, unfollow_user_path(user.slug), class: "unfollow-button", method: :delete if current_user.present?
      else
        link_to "<i class='icon ion-ios-plus-empty'></i> Follow".html_safe, follow_user_path(user.slug), class: "follow-button", method: :post if current_user.present?
      end
    end
  end

  def generate_instagram_photos(client, user)
    if current_user_is?(user) || user.profile_photos.present?
      content_tag :div, class: "photos-container#{" instagram" if is_mobile?}" do
        content_tag(:p, "INSTAGRAM PHOTOS") +
        instagram_photos(client, user)
      end
    end
  end

  def instagram_photos(client, user)
    if client.nil?
      instagram_connect_button = content_tag(:p, link_to( icon("instagram") + "Link Instagram Photos", instagram_connect_path, class: "button instagram"), class: "right")
      user == current_user ? instagram_connect_button : "No Instagram Photos"
    else
      begin
        content_tag :div, class: "multiple-instagram-photos-slider#{'-mobile' if mobile_device?}" do
          if client.user_recent_media.empty?
            "No Instagram Photos"
          else
            client.user_recent_media.each do |photo|
              if photo.type == "image"
                concat content_tag :div, (), class: (mobile_device? ? "instagram-photos" : "medium-2 large-2 columns has-image"),
                  style: "background-image:url('#{photo.images.standard_resolution.url}')",
                  onclick: "showProfilePhotoModal('#{photo.images.standard_resolution.url}', '#{photo.caption.text unless photo.caption.nil?}')"
              end
            end
          end
        end
      rescue Instagram::BadRequest => e
        if e.message =~ /access_token provided is invalid/
          content_tag(:p, link_to( icon("instagram") + "Link Instagram Photos", instagram_connect_path(user.instagram_provider_id), class: "button instagram"), class: "right")
        end
      end
    end
  end

  def generate_link_to_trips(user, type)
    if user == current_user
      if type == "past"
        path = my_trips_path(app_role, type: Event::EVENT_SCHEME[:previous])
        partial_text = "PAST TRIPS"
      elsif type == "upcoming"
        path = my_trips_path(app_role, type: Event::EVENT_SCHEME[:upcoming])
        partial_text = "UPCOMING TRIPS"
      else
        path = my_trips_path(app_role, type: Event::EVENT_SCHEME[:upcoming])
        partial_text = "TRIPS JOINED"
      end
    else
      path = profile_trips_path(user, type: type) if %w{ joined organized upcoming }.include?(type)
      partial_text = if type == "organized"
                       partial_text = "PAST TRIPS"
                     elsif type == "joined"
                       partial_text = "TRIPS JOINED"
                     else
                       partial_text = "UPCOMING TRIPS"
                     end
    end
    link_to "SEE ALL #{partial_text}", path, class: 'button'
  end

  def generate_title_for(type)
    if type == "joined"
      "TRIPS JOINED"
    elsif type == "upcoming"
      "UPCOMING TRIPS"
    elsif type == "organized"
      "PAST TRIPS ORGANIZED"
    end
  end

  def generate_tab_title_for(user, type)
    partial_text = if type == "organized"
                     "Organized Trips"
                   elsif type == "upcoming"
                     "Upcoming Trips"
                   elsif type == "joined"
                     "Joined Trips"
                   end
    "#{user.full_name}'s #{partial_text}"
  end

  def generate_title_for_empty(type)
    case type
      when "upcoming" then "No upcoming trips"
      when "past" then "No past trips organized"
      when "joined" then "No trips joined"
    end
  end

  def generate_content_for_empty(type)
    partial_message = type == "past" ? "trips yet" : "trips coming up yet"

    "Oops! Looks like you don't have any #{partial_message}."\
      "<br>Time to find your next adventure!".html_safe
  end

  def generate_button_for_empty(type)
    content = type == "joined" ? "JOIN TRIPS" : "CREATE TRIP"
    path = type == "joined" ? trips_path : create_trip_path

    link_to content, path, class: "empty-button"
  end

  def generate_reviews_navigation(reviews, user)
    if reviews.empty?
      btn_review = link_to '<span class="review-btn no-reviews ion-plus-round f-right"></span>'.html_safe, "#",
                     'aria-controls': "addReviewModal", 'aria-haspopup': "true", 'tabindex': 0
      content_tag(:p, "No reviews yet", class: "no-reviews-text #{'remove-margin' if current_user_is?(user) || current_user.nil?}") +
      (btn_review unless current_user_is?(user) || current_user.nil?)
    end
  end

  def current_user_is?(user)
    current_user == user
  end

  def valid_email?(email:)
    EmailAddress.valid?(email) && email.exclude?("@facebook.com")
  end

  def check_messenger_user_for(user)
    MessengerUser.fetch_by(
      first_name: user.first_name,
      last_name: user.last_name
    )
  end
end
