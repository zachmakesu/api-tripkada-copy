module OrganizerHelper
  def destination_photo(image:, label:)
    image_url = asset_url(image)
    content_tag :div, class: "landing-page__wrapper__content__container__destinations-slider__image-container__image", style: "background-image: url(#{image_url})" do
      content_tag :h4, label, class: "landing-page__wrapper__content__container__destinations-slider__image-container__image__label"
    end
  end

  def generate_link_organizer_application(label:)
    link_to label, be_a_trip_organizer_path, class: "landing-page__wrapper__content__container__join-btn-container__link button"
  end

  def generate_page_title(title:)
    provide(:title, title)
  end

  def generate_stylesheet_content(stylesheet:)
    content_for(:head){ stylesheet_link_tag stylesheet }
  end
end
