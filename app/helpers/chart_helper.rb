module ChartHelper
  def trip_views_chart
    generate_line_chart(url: organized_trip_views_path, label: "No. of views")
  end

  def booked_trips_chart
    generate_line_chart(url: organized_trip_bookings_path, label: "No. of Bookings")
  end

  def monthly_trips_chart
    generate_line_chart(url: monthly_organized_trips_path, label: "No. of Trips")
  end

  def monthly_book_count_chart
    generate_line_chart(url: monthly_organized_trips_booking_count_path, label: "Booking Count")
  end

  private

  def generate_line_chart (url: , label: , colors: ["#FF518D"] )
    line_chart url, colors: colors, discrete: true,
    library: {
      xAxis: {
        type: 'datetime'
      },
      tooltip: {
        pointFormat: "#{label}: <b>{point.y}</b>",
        xDateFormat: '%B'
      }
    }
  end
end
