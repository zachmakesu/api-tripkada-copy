include Facebook::Messenger

# BOT REACTIONS
Bot.on :message do |message_obj|
  puts "Message Reaction"
  Chatbot::ProcessMessageObj.call(message_obj: message_obj)
end

Bot.on :postback do |message_obj|
  puts "Postback Reaction"
  Chatbot::ProcessPostbackObj.call(message_obj: message_obj)
end

Bot.on :delivery do |message_obj|
  puts "Delivery Reaction"
  # puts "Delivered message(s) #{message_obj.ids}"
  # postback.reply( PostbackPayloadHandler.postback_reply(postback.payload) )
end

Bot.on :optin do |message_obj|
  puts "Optin Reaction"
  Chatbot::ProcessOptinObj.call(message_obj: message_obj)
end

Bot.on :referral do |message_obj|
  puts "Referral Reaction"
end

Bot.on :message_echo do |message_obj|
  puts "Message Echo Reaction"
end

Bot.on :account_linking do |message_obj|
  puts "Account Linking Reaction"
end

Bot.on :read do |message_obj|
  puts "Read Reaction"
end
