class CreateEventMemberships < ActiveRecord::Migration
  def change
    create_table :event_memberships do |t|
      t.references :event, index: true, foreign_key: true
      t.integer :member_id
      t.integer :status, null: false, default: 0
      t.text    :message

      t.timestamps null: false
    end
  end
end
