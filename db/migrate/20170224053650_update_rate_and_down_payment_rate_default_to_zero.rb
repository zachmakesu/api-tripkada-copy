class UpdateRateAndDownPaymentRateDefaultToZero < ActiveRecord::Migration
  def change
    change_column_default :events, :booking_fee, 0
    change_column_default :events, :downpayment_rate, 0
  end
end
