class CreateProviders < ActiveRecord::Migration
  def change
    create_table :providers do |t|
      t.references :user, index: true, foreign_key: true
      t.string :role
      t.string :provider
      t.string :uid
      t.string :facebook_url
      t.string :token

      t.timestamps null: false
    end
  end
end
