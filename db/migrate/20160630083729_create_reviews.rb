class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.references :event, index: true, foreign_key: true, unique: true
      t.integer :reviewer_id, index: true, foreign_key: true, unique: true
      t.text :message
      t.float :rating

      t.timestamps null: false
    end
  end
end
