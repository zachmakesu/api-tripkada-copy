class CreateCustomNotificationMessages < ActiveRecord::Migration
  def change
    create_table :custom_notification_messages do |t|
      t.text       :message
      t.string     :created_by
      t.timestamps null: false
    end
  end
end
