class CreateAppVersionFeatures < ActiveRecord::Migration
  def change
    create_table :app_version_features do |t|
      t.references :app_version
      t.string :title
      t.text :details
      t.timestamps null: false
    end
  end
end
