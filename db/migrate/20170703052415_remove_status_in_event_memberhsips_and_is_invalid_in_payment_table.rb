class RemoveStatusInEventMemberhsipsAndIsInvalidInPaymentTable < ActiveRecord::Migration
  def self.up
    remove_column :event_memberships, :status
    remove_column :payments, :is_invalid
  end

  def self.down
    add_column :event_memberships, :status, :integer, default: 0, null: false
    add_column :payments ,:is_invalid, :boolean, default: false
  end
end
