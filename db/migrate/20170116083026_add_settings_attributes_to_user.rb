class AddSettingsAttributesToUser < ActiveRecord::Migration
  def change
    add_column :users, :emergency_contact_name, :string
    add_column :users, :emergency_contact_number, :string
    add_column :users, :tell_us_about_yourself, :string
  end
end
