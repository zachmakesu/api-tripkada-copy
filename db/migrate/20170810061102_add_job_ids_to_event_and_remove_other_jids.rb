class AddJobIdsToEventAndRemoveOtherJids < ActiveRecord::Migration
  def up
    add_column :events, :job_ids, :text, default: '', null: false
    remove_column :events, :incoming_notification_jid
    remove_column :events, :review_notification_jid
  end

  def down
    remove_column :events, :job_ids, :string
    add_column :events, :incoming_notification_jid, :string
    add_column :events, :review_notification_jid, :string
  end
end
