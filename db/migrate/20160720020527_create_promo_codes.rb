class CreatePromoCodes < ActiveRecord::Migration
  def change
    create_table :promo_codes do |t|
      t.string :code
      t.float :value
      t.boolean :active, default: true
      t.datetime :start_at
      t.datetime :end_at

      t.timestamps null: false
    end
  end
end
