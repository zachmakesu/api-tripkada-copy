class AddCalendarSyncColumnInUserTable < ActiveRecord::Migration
  def change
    add_column :users, :calendar_sync, :boolean, default: false
    add_index :users, :calendar_sync
  end
end
