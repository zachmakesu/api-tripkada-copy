class AddEmailAndQuestionAnswersToMessengerUser < ActiveRecord::Migration
  def change
    add_column :messenger_users, :email, :string
    add_column :messenger_users, :concern_answers, :text
  end
end
