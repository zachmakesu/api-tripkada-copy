class AddNumberOfDaysInEvent < ActiveRecord::Migration
  def change
    add_column :events, :number_of_days, :integer
  end
end
