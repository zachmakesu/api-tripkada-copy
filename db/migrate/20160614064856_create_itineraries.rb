class CreateItineraries < ActiveRecord::Migration
  def change
    create_table :itineraries do |t|
      t.references :event, index: true, foreign_key: true
      t.string :name
      t.text :description
      t.time :arrival

      t.timestamps null: false
    end
  end
end
