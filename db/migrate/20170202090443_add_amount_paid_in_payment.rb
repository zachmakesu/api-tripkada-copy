class AddAmountPaidInPayment < ActiveRecord::Migration
  def change
    add_column :payments, :amount_paid, :float, default: 0, null: false;
  end
end
