class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :user
      t.text       :message
      t.integer    :notification_type, null: false
      t.boolean    :seen, default: false, null: false
      t.datetime   :deleted_at
      t.timestamps null: false
    end
  end
end
