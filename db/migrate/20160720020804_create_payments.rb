class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :event_membership, index: true, foreign_key: true
      t.references :promo_code, index: true, foreign_key: true
      t.integer :mode

      t.timestamps null: false
    end
  end
end
