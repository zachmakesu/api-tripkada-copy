class CreateMailerliteGroups < ActiveRecord::Migration
  def change
    create_table :mailerlite_groups do |t|
      t.string :category
      t.integer :group_id
      t.string  :name
      t.timestamps null: false
    end
  end
end
