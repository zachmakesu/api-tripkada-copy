class CreateBookingFees < ActiveRecord::Migration
  def change
    create_table :booking_fees do |t|
      t.float :value, default: 0.0
      t.string :created_by

      t.timestamps null: false
    end
  end
end
