class AddStatusColumnInPaymentModel < ActiveRecord::Migration
  def change
    add_column :payments, :status, :integer, null: false, default: 0
  end
end
