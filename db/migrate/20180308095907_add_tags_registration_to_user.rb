class AddTagsRegistrationToUser < ActiveRecord::Migration
  def change
    add_column :users, :registration_platform, :integer
  end
end
