class RemoveDestinationInEvent < ActiveRecord::Migration
  def self.up
    remove_column :events, :destination
  end

  def self.down
    add_column :events, :destination
  end
end
