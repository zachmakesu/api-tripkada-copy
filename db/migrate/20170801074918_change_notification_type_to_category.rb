class ChangeNotificationTypeToCategory < ActiveRecord::Migration
  def self.up
    rename_column :notifications, :notification_type, :category
    change_column :notifications, :category, :integer, default: 0
  end

  def self.down
    rename_column :notifications, :category, :notification_type
    change_column :notifications, :notification_type, :integer, default: nil
  end
end
