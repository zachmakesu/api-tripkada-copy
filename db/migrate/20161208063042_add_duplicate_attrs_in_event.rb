class AddDuplicateAttrsInEvent < ActiveRecord::Migration
  def change
    add_column :events, :duplicated_from_id, :integer
    add_column :events, :created_duplicates, :integer, default: 0
  end
end
