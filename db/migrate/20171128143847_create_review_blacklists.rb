class CreateReviewBlacklists < ActiveRecord::Migration
  def change
    create_table :review_blacklists do |t|
      t.references :user, null: false, index: true
      t.references :event, null: false, index: true
      t.timestamps null: false
    end
  end
end
