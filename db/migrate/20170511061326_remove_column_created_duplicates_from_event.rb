class RemoveColumnCreatedDuplicatesFromEvent < ActiveRecord::Migration
  def change
    remove_column :events, :created_duplicates, :integer, default: 0
  end
end
