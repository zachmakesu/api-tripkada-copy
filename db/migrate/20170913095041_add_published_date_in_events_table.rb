class AddPublishedDateInEventsTable < ActiveRecord::Migration
  def change
    add_column :events, :published_at, :datetime
    add_index :events, :published_at
  end
end
