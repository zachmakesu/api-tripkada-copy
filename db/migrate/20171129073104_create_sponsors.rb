class CreateSponsors < ActiveRecord::Migration
  def change
    create_table :sponsors do |t|
      t.references :event, index: true, null: false
      t.attachment :image
      t.timestamps null: false
    end
  end
end
