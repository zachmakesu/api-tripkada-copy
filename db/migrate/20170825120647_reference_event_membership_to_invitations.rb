class ReferenceEventMembershipToInvitations < ActiveRecord::Migration
  def change
    add_column :invitations, :event_membership_id, :integer
    add_index :invitations, :event_membership_id
  end
end
