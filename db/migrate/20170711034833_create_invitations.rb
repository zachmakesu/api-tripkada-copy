class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.references :event, index: true, null: false
      t.references :user, index: true, null: false, foreign_key: true
      t.integer :invitee_id, index: true, null: false
      t.timestamps null: false
    end
  end
end
