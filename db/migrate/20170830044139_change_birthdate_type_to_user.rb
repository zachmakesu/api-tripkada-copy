class ChangeBirthdateTypeToUser < ActiveRecord::Migration
  def up
    rename_column :users, :birthdate, :old_birthdate
    add_column :users, :birthdate, :date
  end

  def down
    remove_column :users, :birthdate, :date
    rename_column :users, :old_birthdate, :birthdate
  end
end
