class AddBookingFeeToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :booking_fee, :float, default: 0.0
  end
end
