class RemoveReferencedEventInInvitatinTable < ActiveRecord::Migration
  def self.up
    remove_column :invitations, :event_id
  end

  def self.down
    add_column :invitations, :event_id, :integer, null: false
  end
end
