class AddReviewJidInEvent < ActiveRecord::Migration
  def change
    add_column :events, :review_notification_jid, :string
  end
end
