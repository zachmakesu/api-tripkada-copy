class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.integer    :imageable_id, index: true, foreign_key: true
      t.boolean    :primary, default: false
      t.string     :label
      t.string     :category, default: "uncategorized"
      t.string     :imageable_type
      t.attachment :image

      t.timestamps null: false
    end
  end
end
