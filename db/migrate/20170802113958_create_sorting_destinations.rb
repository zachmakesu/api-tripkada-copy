class CreateSortingDestinations < ActiveRecord::Migration
  def change
    create_table :sorting_destinations do |t|
      t.references :event, index: true
      t.references :destination, index: true
      t.timestamps null: false
    end
  end
end
