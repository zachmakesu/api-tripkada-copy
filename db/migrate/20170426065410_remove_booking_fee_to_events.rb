class RemoveBookingFeeToEvents < ActiveRecord::Migration
  def change
    remove_column :events, :booking_fee
  end
end
