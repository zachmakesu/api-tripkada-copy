class AddNoShowToEventMembership < ActiveRecord::Migration
  def change
    add_column :event_memberships, :no_show, :boolean, :default => false
  end
end
