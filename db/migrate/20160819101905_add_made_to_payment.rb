class AddMadeToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :made, :integer
  end
end
