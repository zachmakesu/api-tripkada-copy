class CreateTransactionFees < ActiveRecord::Migration
  def change
    create_table :transaction_fees do |t|
      t.float :amount, null: false
      t.integer :category, null: false, defaul: 0
      t.timestamps null: false
    end
  end
end
