class CreateAppVersions < ActiveRecord::Migration
  def change
    create_table :app_versions do |t|
      t.string :created_by, null: false
      t.string :version_code, null: false
      t.string :features, null: false
      t.integer :platform, null: false
      t.timestamps null: false
    end
  end
end
