class AddOrganizerDateToUsers < ActiveRecord::Migration
  def change
    add_column :users, :approved_organizer_at, :datetime
  end
end
