class AddNumberOfViewsColumnInEventsTable < ActiveRecord::Migration
  def change
    add_column :events, :number_of_views, :integer, default: 0
  end
end
