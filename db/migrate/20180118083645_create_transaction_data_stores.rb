class CreateTransactionDataStores < ActiveRecord::Migration
  def change
    create_table :transaction_data_stores do |t|
      t.string :transaction_id
      t.text :data
      t.timestamps null: false
    end
  end
end
