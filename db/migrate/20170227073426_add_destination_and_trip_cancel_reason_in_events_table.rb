class AddDestinationAndTripCancelReasonInEventsTable < ActiveRecord::Migration
  def change
    add_column :events, :destination, :string
    add_column :events, :trip_cancel_reason, :string
  end
end
