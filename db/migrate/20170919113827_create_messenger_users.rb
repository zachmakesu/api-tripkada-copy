class CreateMessengerUsers < ActiveRecord::Migration
  def change
    create_table :messenger_users do |t|
      t.integer   :user_fb_id, limit: 5, null: false
      t.string    :first_name
      t.string    :last_name
      t.string    :job_id
      t.datetime  :last_reply
      t.boolean   :accepts_email, default: false
      t.timestamps null: false
    end
  end
end
