class CreateDragonpayTransactionLogs < ActiveRecord::Migration
  def change
    create_table :dragonpay_transaction_logs do |t|
      t.references :payment, index: true
      t.string :transaction_id, null: false
      t.string :reference_number, null: false
      t.string :status, null: false
      t.string :message, null: false
      t.text :response, null: false
      t.timestamps null: false
    end
  end
end
