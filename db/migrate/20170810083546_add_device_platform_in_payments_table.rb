class AddDevicePlatformInPaymentsTable < ActiveRecord::Migration
  def change
    add_column :payments, :device_platform, :integer, default: 0
  end
end
