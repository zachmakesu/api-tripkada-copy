class AddTransactionIdsToExpressPayments < ActiveRecord::Migration
  def change
    add_column :express_payments, :payment_transaction_id, :string
  end
end
