class CreateWaitlists < ActiveRecord::Migration
  def change
    create_table :waitlists do |t|
      t.references :user,  index: true, foreign_key: true
      t.references :event, index: true, foreign_key: true
      t.integer    :status, default: 0, null: false
      t.timestamps null: false
    end
  end
end
