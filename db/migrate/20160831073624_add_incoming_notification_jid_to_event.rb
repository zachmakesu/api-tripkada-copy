class AddIncomingNotificationJidToEvent < ActiveRecord::Migration
  def change
    add_column :events, :incoming_notification_jid, :string
  end
end
