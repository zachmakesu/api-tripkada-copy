class AddCategoryInPromoCodeTable < ActiveRecord::Migration
  def change
    add_column :promo_codes, :category, :integer, default: 0, null: false
  end
end
