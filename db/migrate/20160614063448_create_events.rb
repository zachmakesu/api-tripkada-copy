class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.references :user, index: true, foreign_key: true
      t.string :slug, unique: true
      t.string :name
      t.datetime :start_at
      t.datetime :end_at
      t.text :description
      t.text :meeting_place
      t.decimal :rate
      t.integer :min_pax
      t.integer :max_pax
      t.text :tags
      t.text :highlights
      t.text :inclusions
      t.text :things_to_bring
      t.float :lat
      t.float :lng
      t.datetime :deleted_at

      t.timestamps null: false
    end
  end
end
