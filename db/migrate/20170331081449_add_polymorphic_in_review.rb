class AddPolymorphicInReview < ActiveRecord::Migration
  def change
    add_column :reviews, :reviewable_id, :integer, index: true, foreign_key: true
    add_column :reviews, :reviewable_type, :string
    add_column :reviews, :reviewer_type, :integer
    remove_column :reviews, :event_id, :integer
  end
end
