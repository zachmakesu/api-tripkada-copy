class RenameProvidersFacebookUrl < ActiveRecord::Migration
  def change
    rename_column :providers, :facebook_url, :url
  end
end
