class CreateReferrals < ActiveRecord::Migration
  def change
    create_table :referrals do |t|
      t.references :user, index: true, null: false, foreign_key: true
      t.integer :joiner_id, index: true, null: false
      t.timestamps null: false
    end
  end
end
