class CreateEventTags < ActiveRecord::Migration
  def change
    create_table :event_tags do |t|
      t.integer :tag_type
      t.string :name

      t.timestamps null: false
    end
  end
end
