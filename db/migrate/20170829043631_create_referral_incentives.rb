class CreateReferralIncentives < ActiveRecord::Migration
  def change
    create_table :referral_incentives do |t|
      t.float :value, default: 0.0
      t.string :created_by
      t.timestamps null: false
    end
  end
end
