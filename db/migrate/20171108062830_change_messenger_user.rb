class ChangeMessengerUser < ActiveRecord::Migration
  def change
    remove_column :messenger_users, :accepts_email
    add_column :messenger_users, :message_concern, :string
    add_column :messenger_users, :sendable, :boolean, default: true
  end
end
