class AddBookingFeeAndDownpaymentRate < ActiveRecord::Migration
  def change
    add_column :events, :booking_fee, :float
    add_column :events, :downpayment_rate, :float
  end
end
