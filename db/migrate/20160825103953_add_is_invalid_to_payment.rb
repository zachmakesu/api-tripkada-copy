class AddIsInvalidToPayment < ActiveRecord::Migration
  def change
    add_column :payments, :is_invalid, :boolean, default: false
  end
end
