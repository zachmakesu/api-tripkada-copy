class AddCategoryInAppVersion < ActiveRecord::Migration
  def up
    add_column :app_versions, :category, :integer
    remove_column :app_versions, :features
  end

  def down
    add_column :app_versions, :features, :string
    remove_column :app_versions, :category
  end
end
