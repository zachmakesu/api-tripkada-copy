class CreateExpressPayments < ActiveRecord::Migration
  def change
    create_table :express_payments do |t|
      t.integer :payment_id, null: false
      t.string  :payer_id
      t.string  :payment_tracking_id
      t.string  :payment_status
      t.timestamps null: false
    end
    add_index :express_payments, :payment_id
  end
end
