class CreateCreditLogs < ActiveRecord::Migration
  def change
    create_table :credit_logs do |t|
      t.references :promo_code, null: false
      t.references :payment
      t.decimal :amount, null: false
      t.integer :category, default: 0
      t.timestamps null: false
    end
      add_index :credit_logs, :category
  end
end
