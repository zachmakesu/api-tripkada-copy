class AddApplyAsOrganizerInUserModel < ActiveRecord::Migration
  def change
    add_column :users, :applied_as_organizer_at, :datetime
  end
end
