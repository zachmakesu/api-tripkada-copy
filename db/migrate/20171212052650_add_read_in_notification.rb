class AddReadInNotification < ActiveRecord::Migration
  def change
    add_column :notifications, :read, :datetime
  end
end
