class AddUsageLimitToPromoCode < ActiveRecord::Migration
  def change
    add_column :promo_codes, :usage_limit, :integer
  end
end
