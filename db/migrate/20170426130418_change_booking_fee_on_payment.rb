class ChangeBookingFeeOnPayment < ActiveRecord::Migration
  def change
    remove_column :payments, :booking_fee
    add_reference :payments, :booking_fee, index: true 
  end
end
