# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180411080356) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "api_keys", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "encrypted_access_token", default: "", null: false
    t.datetime "expires_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "api_keys", ["user_id"], name: "index_api_keys_on_user_id", using: :btree

  create_table "app_version_features", force: :cascade do |t|
    t.integer  "app_version_id"
    t.string   "title"
    t.text     "details"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "app_versions", force: :cascade do |t|
    t.string   "created_by",   null: false
    t.string   "version_code", null: false
    t.integer  "platform",     null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "category"
  end

  create_table "bank_accounts", force: :cascade do |t|
    t.string   "bank_name"
    t.string   "account_number"
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "booking_fees", force: :cascade do |t|
    t.float    "value",      default: 0.0
    t.string   "created_by"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "bookmarks", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "bookmarks", ["event_id"], name: "index_bookmarks_on_event_id", using: :btree
  add_index "bookmarks", ["user_id"], name: "index_bookmarks_on_user_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name",                      null: false
    t.boolean  "active",     default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "categorizations", force: :cascade do |t|
    t.integer  "event_id"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "categorizations", ["category_id"], name: "index_categorizations_on_category_id", using: :btree
  add_index "categorizations", ["event_id"], name: "index_categorizations_on_event_id", using: :btree

  create_table "credit_logs", force: :cascade do |t|
    t.integer  "promo_code_id",             null: false
    t.integer  "payment_id"
    t.decimal  "amount",                    null: false
    t.integer  "category",      default: 0
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "credit_logs", ["category"], name: "index_credit_logs_on_category", using: :btree

  create_table "custom_notification_messages", force: :cascade do |t|
    t.text     "message"
    t.string   "created_by"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "destinations", force: :cascade do |t|
    t.string   "name",                      null: false
    t.boolean  "active",     default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "devices", force: :cascade do |t|
    t.integer  "user_id"
    t.boolean  "is_enabled", default: true
    t.string   "name"
    t.string   "platform"
    t.string   "token"
    t.string   "udid"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "role"
  end

  add_index "devices", ["user_id"], name: "index_devices_on_user_id", using: :btree

  create_table "dragonpay_transaction_logs", force: :cascade do |t|
    t.integer  "payment_id"
    t.string   "transaction_id",   null: false
    t.string   "reference_number", null: false
    t.string   "status",           null: false
    t.string   "message",          null: false
    t.text     "response",         null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "dragonpay_transaction_logs", ["payment_id"], name: "index_dragonpay_transaction_logs_on_payment_id", using: :btree

  create_table "event_memberships", force: :cascade do |t|
    t.integer  "event_id"
    t.integer  "member_id"
    t.text     "message"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "no_show",    default: false
  end

  add_index "event_memberships", ["event_id"], name: "index_event_memberships_on_event_id", using: :btree

  create_table "event_tags", force: :cascade do |t|
    t.integer  "tag_type"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "slug"
    t.string   "name"
    t.datetime "start_at"
    t.datetime "end_at"
    t.text     "description"
    t.text     "meeting_place"
    t.decimal  "rate"
    t.integer  "min_pax"
    t.integer  "max_pax"
    t.text     "tags"
    t.text     "highlights"
    t.text     "inclusions"
    t.text     "things_to_bring"
    t.float    "lat"
    t.float    "lng"
    t.datetime "deleted_at"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.float    "downpayment_rate",   default: 0.0
    t.integer  "duplicated_from_id"
    t.integer  "number_of_views",    default: 0
    t.string   "trip_cancel_reason"
    t.integer  "number_of_days"
    t.text     "job_ids",            default: "",  null: false
    t.datetime "featured_at"
    t.datetime "published_at"
  end

  add_index "events", ["published_at"], name: "index_events_on_published_at", using: :btree
  add_index "events", ["user_id"], name: "index_events_on_user_id", using: :btree

  create_table "express_payments", force: :cascade do |t|
    t.integer  "payment_id",             null: false
    t.string   "payer_id"
    t.string   "payment_tracking_id"
    t.string   "payment_status"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "payment_transaction_id"
  end

  add_index "express_payments", ["payment_id"], name: "index_express_payments_on_payment_id", using: :btree

  create_table "follows", force: :cascade do |t|
    t.integer  "follower_id", null: false
    t.integer  "followed_id", null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "follows", ["followed_id"], name: "index_follows_on_followed_id", using: :btree
  add_index "follows", ["follower_id", "followed_id"], name: "index_follows_on_follower_id_and_followed_id", unique: true, using: :btree
  add_index "follows", ["follower_id"], name: "index_follows_on_follower_id", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "highlights", force: :cascade do |t|
    t.integer  "event_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "highlights", ["event_id"], name: "index_highlights_on_event_id", using: :btree

  create_table "inclusions", force: :cascade do |t|
    t.integer  "event_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "inclusions", ["event_id"], name: "index_inclusions_on_event_id", using: :btree

  create_table "invitations", force: :cascade do |t|
    t.integer  "user_id",             null: false
    t.integer  "invitee_id",          null: false
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "event_membership_id"
  end

  add_index "invitations", ["event_membership_id"], name: "index_invitations_on_event_membership_id", using: :btree
  add_index "invitations", ["invitee_id"], name: "index_invitations_on_invitee_id", using: :btree
  add_index "invitations", ["user_id"], name: "index_invitations_on_user_id", using: :btree

  create_table "itineraries", force: :cascade do |t|
    t.integer  "event_id"
    t.string   "name"
    t.text     "description"
    t.time     "arrival"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "itineraries", ["event_id"], name: "index_itineraries_on_event_id", using: :btree

  create_table "mailerlite_groups", force: :cascade do |t|
    t.string   "category"
    t.integer  "group_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messenger_users", force: :cascade do |t|
    t.integer  "user_fb_id",      limit: 8,                null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "job_id"
    t.datetime "last_reply"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "message_concern"
    t.boolean  "sendable",                  default: true
    t.string   "email"
    t.text     "concern_answers"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "message"
    t.integer  "category",              default: 0,     null: false
    t.boolean  "seen",                  default: false, null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "notificationable_id"
    t.string   "notificationable_type"
    t.datetime "read"
  end

  create_table "payments", force: :cascade do |t|
    t.integer  "event_membership_id"
    t.integer  "promo_code_id"
    t.integer  "mode"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "made"
    t.float    "amount_paid",         default: 0.0, null: false
    t.integer  "booking_fee_id"
    t.integer  "status",              default: 0,   null: false
    t.integer  "device_platform",     default: 0
  end

  add_index "payments", ["booking_fee_id"], name: "index_payments_on_booking_fee_id", using: :btree
  add_index "payments", ["event_membership_id"], name: "index_payments_on_event_membership_id", using: :btree
  add_index "payments", ["promo_code_id"], name: "index_payments_on_promo_code_id", using: :btree

  create_table "photos", force: :cascade do |t|
    t.integer  "imageable_id"
    t.boolean  "primary",            default: false
    t.string   "label"
    t.string   "category",           default: "uncategorized"
    t.string   "imageable_type"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  add_index "photos", ["imageable_id"], name: "index_photos_on_imageable_id", using: :btree

  create_table "promo_codes", force: :cascade do |t|
    t.string   "code"
    t.float    "value"
    t.boolean  "active",      default: true
    t.datetime "start_at"
    t.datetime "end_at"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "usage_limit"
    t.integer  "user_id"
    t.integer  "category",    default: 0,    null: false
  end

  add_index "promo_codes", ["user_id"], name: "index_promo_codes_on_user_id", using: :btree

  create_table "providers", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "role"
    t.string   "provider"
    t.string   "uid"
    t.string   "url"
    t.string   "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "providers", ["user_id"], name: "index_providers_on_user_id", using: :btree

  create_table "referral_incentives", force: :cascade do |t|
    t.float    "value",      default: 0.0
    t.string   "created_by"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "referrals", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.integer  "joiner_id",  null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "referrals", ["joiner_id"], name: "index_referrals_on_joiner_id", using: :btree
  add_index "referrals", ["user_id"], name: "index_referrals_on_user_id", using: :btree

  create_table "review_blacklists", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.integer  "event_id",   null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "review_blacklists", ["event_id"], name: "index_review_blacklists_on_event_id", using: :btree
  add_index "review_blacklists", ["user_id"], name: "index_review_blacklists_on_user_id", using: :btree

  create_table "reviews", force: :cascade do |t|
    t.integer  "reviewer_id"
    t.text     "message"
    t.float    "rating"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "reviewable_id"
    t.string   "reviewable_type"
    t.integer  "reviewer_type"
    t.string   "platform"
  end

  add_index "reviews", ["reviewer_id"], name: "index_reviews_on_reviewer_id", using: :btree

  create_table "sorting_destinations", force: :cascade do |t|
    t.integer  "event_id"
    t.integer  "destination_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "sorting_destinations", ["destination_id"], name: "index_sorting_destinations_on_destination_id", using: :btree
  add_index "sorting_destinations", ["event_id"], name: "index_sorting_destinations_on_event_id", using: :btree

  create_table "sponsors", force: :cascade do |t|
    t.integer  "event_id",           null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "sponsors", ["event_id"], name: "index_sponsors_on_event_id", using: :btree

  create_table "transaction_data_stores", force: :cascade do |t|
    t.string   "transaction_id"
    t.text     "data"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "transaction_fees", force: :cascade do |t|
    t.float    "amount",     null: false
    t.integer  "category",   null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                    default: "",    null: false
    t.string   "encrypted_password",       default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",            default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "uid"
    t.string   "image_url"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "old_birthdate"
    t.integer  "role",                     default: 0
    t.integer  "gender",                   default: 0
    t.string   "country"
    t.string   "city"
    t.string   "address"
    t.string   "mobile"
    t.datetime "approved_at"
    t.string   "rating"
    t.text     "hobbies"
    t.string   "website"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "emergency_contact_name"
    t.string   "emergency_contact_number"
    t.string   "tell_us_about_yourself"
    t.string   "state"
    t.date     "birthdate"
    t.boolean  "calendar_sync",            default: false
    t.string   "slug"
    t.datetime "applied_as_organizer_at"
    t.datetime "approved_organizer_at"
    t.integer  "registration_platform"
  end

  add_index "users", ["calendar_sync"], name: "index_users_on_calendar_sync", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "waitlists", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "event_id"
    t.integer  "status",     default: 0, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "waitlists", ["event_id"], name: "index_waitlists_on_event_id", using: :btree
  add_index "waitlists", ["user_id"], name: "index_waitlists_on_user_id", using: :btree

  add_foreign_key "api_keys", "users"
  add_foreign_key "bookmarks", "events"
  add_foreign_key "bookmarks", "users"
  add_foreign_key "devices", "users"
  add_foreign_key "event_memberships", "events"
  add_foreign_key "events", "users"
  add_foreign_key "invitations", "users"
  add_foreign_key "itineraries", "events"
  add_foreign_key "payments", "event_memberships"
  add_foreign_key "payments", "promo_codes"
  add_foreign_key "providers", "users"
  add_foreign_key "referrals", "users"
  add_foreign_key "waitlists", "events"
  add_foreign_key "waitlists", "users"
end
