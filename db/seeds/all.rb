# Add seed file for all envs here
models = %w{categories destinations users events event_tags promo_codes referral_incentives transaction_fee}

models.each do |model|
  puts '#' * 80
  puts "Table: #{model.upcase}"
  require_relative "models/#{model}.rb"
end
