categories = [
  {
    name: "Surfing"
  },
  {
    name: "Hiking"
  },
  {
    name: "Zipline"
  },
  {
    name: "Trekking"
  },
  {
    name: "Kayak"
  },
  {
    name: "Biking"
  },
  {
    name: "Others"
  }
]
ActiveRecord::Base.transaction do
  if Category.all.empty?
    categories.each do |category|
      new_category = Category.new
      new_category.name = category[:name]
      if new_category.save
        print '✓'
      else
        puts new_category.errors.inspect
        break
      end
    end
    print "\nTotal : #{Category.all.count}\n"
  else
    print "Skipped seeding category table.\n"
  end
end