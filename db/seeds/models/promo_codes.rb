ActiveRecord::Base.transaction do
  if User.any?
    User.find_each do |user|
      next if user.credit_promo_code.present?
      credit_promo_code = user.build_credit_promo_code(
        code: PromoCode.generate_unique_code, value: 0.0
      )
      if credit_promo_code.save
        print '✓'
      else
        puts credit_promo_code.errors.inspect
        break
      end
    end
  else
    print "Skipped seeding user's credit promo code.\n"
  end
end
