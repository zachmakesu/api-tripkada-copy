event_tag_list = [
  {
    name: "Surf",
    type: 0
  },
  {
    name: "Hike",
    type: 0
  },
  {
    name: "Beach",
    type: 0
  },
  {
    name: "Baler",
    type: 0
  },
  {
    name: "SurfLesson",
    type: 1
  },
  {
    name: "Town Tour",
    type: 1
  },
  {
    name: "The Circle Hostel",
    type: 1
  },
  {
    name: "Boat Transfer",
    type: 2
  },
  {
    name: "Food",
    type: 2
  },
  {
    name: "Entrance Fee",
    type: 2
  },
  {
    name: "Guide",
    type: 2
  },
  {
    name: "Transportation",
    type: 2
  },
  {
    name: "Sunblock",
    type: 3
  },
  {
    name: "Swimsuit",
    type: 3
  },
  {
    name: "Snacks",
    type: 3
  }
]
ActiveRecord::Base.transaction do
  if EventTag.all.empty?
    event_tag_list.each do |tag|
      event_tag = EventTag.new
      event_tag.name     = tag[:name]
      event_tag.tag_type = tag[:type]
      if event_tag.save
        print '✓'
      else
        puts EventTag.errors.inspect
        break
      end
    end
    print "\nTotal : #{EventTag.all.count}\n"
  else
    print "Skipped seeding events table.\n"
  end
end
