ActiveRecord::Base.transaction do
  if ReferralIncentive.all.empty?
    incentive = ReferralIncentive.new
    incentive.value       = 100
    incentive.created_by  = "ADMIN" 
    if incentive.save
      print '✓'
    else
      puts incentive.errors.inspect
      break
    end
    print "\nTotal : #{ReferralIncentive.all.count}\n"
  else
    print "Skipped seeding referral_incentives table.\n"
  end
end
