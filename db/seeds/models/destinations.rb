destinations = [
  {
    name: "Mountains"
  },
  {
    name: "Beaches"
  },
  {
    name: "Caves"
  },
  {
    name: "Rivers"
  }
]
ActiveRecord::Base.transaction do
  if Destination.all.empty?
    destinations.each do |destination|
      new_destination = Destination.new
      new_destination.name = destination[:name]
      if new_destination.save
        print '✓'
      else
        puts new_destination.errors.inspect
        break
      end
    end
    print "\nTotal : #{Destination.all.count}\n"
  else
    print "Skipped seeding destination table.\n"
  end
end