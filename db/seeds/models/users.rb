ActiveRecord::Base.transaction do
  if User.all.empty?
    (0..5).each do |user|
      new_user = User.new
      new_user.email                = Faker::Internet.email
      new_user.first_name           = Faker::Name.first_name
      new_user.last_name            = Faker::Name.last_name
      new_user.gender               = Faker::Number.between(1,2)
      new_user.password             = 'password'
      new_user.image_url            = Faker::Placeholdit.image
      new_user.birthdate            = 20.years.ago
      new_user.country              = Faker::Address.country
      new_user.city                 = Faker::Address.city
      new_user.address              = Faker::Address.street_address
      new_user.mobile               = Faker::PhoneNumber.cell_phone
      if new_user.save
        print '✓'
      else
        puts new_user.errors.inspect
        break
      end
    end
    print "\nTotal : #{User.all.count}\n"
  else
    print "Skipped seeding users table.\n"
  end
end
