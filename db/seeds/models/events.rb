ActiveRecord::Base.transaction do
  if Event.all.empty? && User.count >= 2
    User.all.limit(2).each do |user|
      event = user.organized_events.new
      event.name            = "Travel to #{Faker::Address.city}"
      event.start_at        = (DateTime.now + Faker::Number.between(1, 5).days)
      event.end_at          = (DateTime.now + Faker::Number.between(6, 10).days)
      event.description     = Faker::Lorem.sentence
      event.meeting_place   = Faker::Address.street_address
      event.min_pax         = Faker::Number.between(1, 5) 
      event.max_pax         = Faker::Number.between(5, 10) 
      event.rate            = Faker::Commerce.price
      event.highlights      = "#{Faker::Book.title};#{Faker::Book.title}"
      event.inclusions      = "#{Faker::Superhero.power};#{Faker::Superhero.power}"
      event.tags            = "tag1;tag2"
      event.things_to_bring = "bag;phone"
      event.lat             = Faker::Address.latitude
      event.lng             = Faker::Address.longitude
      if event.save
        event.photos.create(image: URI.parse(Faker::Placeholdit.image))
        event.photos.create(image: URI.parse(Faker::Placeholdit.image))
        event.itineraries.create(description: Faker::Address.street_address)
        event.itineraries.create(description: Faker::Address.street_address)
        event.members << user
        print '✓'
      else
        puts event.errors.inspect
        break
      end
    end
    print "\nTotal : #{Event.all.count}\n"
  else
    print "Skipped seeding events table.\n"
  end
end
