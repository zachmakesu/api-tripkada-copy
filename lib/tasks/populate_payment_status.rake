desc "Populate Payment.status using Data from Payment.is_invalid"
task populate_payment_status: :environment do
  Payment.find_each do |payment|
    status = payment.is_invalid? ? 'declined' : 'approved'
    payment.status = status
    payment.save(validate: false)
  end
  puts "Payment Status Successfully Populated..."
end
