namespace :users do
  desc "add organizer_approved_date_for_current_organizers"
  task approved_organizer_at: :environment do
    success_count = 0
    fail_count = 0
    User.joiner_and_organizer.find_each do |user|
      next if user.approved_organizer_at.present?
      if user.update(approved_organizer_at: user.created_at)
        print '✓'
        success_count += 1;
      else
        print '✗'
        fail_count += 1;
      end
    end
    puts "\n\nSuccess[✓]:#{success_count}   Failure[✗]:#{fail_count}"
  end
end
