desc "Execute Service For Sending Invalid Request Logs Mailer"
task execute_invalid_request_notifier: :environment do
  Logger::SendInvalidRequestMailer.call
end
