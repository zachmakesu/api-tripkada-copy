# https://gist.github.com/hopsoft/56ba6f55fe48ad7f8b90

namespace :backup do
  date_time         = Time.current
  date              = date_time.strftime("%Y-%m-%d")
  time              = date_time.strftime("%H%M%S")

  desc "Backup Assets"
  task assets: :environment do
    assets_dir        = Rails.root.join("public", "assets")
    backup_dir        = backup_directory(Rails.env, create: true, date: date)
    assets_backup_dir = "#{backup_dir}/assets-#{time}"
    assets_backup_cmd = "cp -R #{assets_dir} #{assets_backup_dir}"

    system assets_backup_cmd
    puts "\n Backup Assets to: #{assets_backup_dir}\n"
  end

  desc "Backup Database"
  task database: :environment do
    dump_fmt   = ensure_format("p") # sql format
    dump_sfx   = suffix_for_format(dump_fmt)
    backup_dir = backup_directory(Rails.env, create: true, date: date)
    full_path  = nil
    cmd        = nil

    with_config do |_app, host, db, user, password|
      full_path = "#{backup_dir}/#{time}"\
                  "_#{db}.#{dump_sfx}"
      cmd       = "PGPASSWORD='#{password}' pg_dump -F #{dump_fmt} -v -O "\
                  "-U '#{user}' -h '#{host}' -d '#{db}' -f '#{full_path}'"
    end

    system cmd
    puts "\n Backup Database to file: #{full_path}\n"
  end

  desc "Dumps the database to backups folder"
  task db_assets: :environment do
    require "English"
    backup_dir        = backup_directory

    # Backup Postgresql database using `sql` format
    # Backup Assets
    # Rake::Task["backup:assets"].invoke
    Rake::Task["backup:database"].invoke

    # Check process status
    # https://stackoverflow.com/a/38683399/8680724
    if $CHILD_STATUS.success?
      # Execute Task for removing existing backups
      Rake::Task["clean:backups"].invoke
      # Notify Slack Channel in Production Environment
      send_notif(dir: backup_dir, status: "success") if Rails.env.production?
    else
      send_notif(dir: backup_dir, status: "failed") if Rails.env.production?
    end
  end
end

namespace :clean do
  desc "Remove Existing Assets and Database Backup"
  task :backups do
    today             = Time.zone.now.strftime("%Y-%m-%d")
    yesterday         = Time.zone.yesterday.strftime("%Y-%m-%d")
    backup_today      = Rails.root.join("db", "backups/#{today}").to_s
    backup_yesterday  = Rails.root.join("db", "backups/#{yesterday}").to_s
    whitelist_backups = %W[#{backup_today} #{backup_yesterday}]
    backups = Dir.glob("#{Rails.root.join('db', 'backups')}/**")
    # Remove Existing Backups Except Yesterday and Current Day's Backup
    backups.each do |f|
      next if whitelist_backups.include?(f)
      FileUtils.rm_rf(f)
      puts "Removing Existing Backup #{f}"
    end
  end
end

namespace :restore do
  desc "Backup Assets folder"
  task assets: :environment do
    public_dir          = Rails.root.join("public", "")
    assets_dir          = Rails.root.join("public", "assets")
    backup_dir          = backup_directory
    # list assets backup
    assets_backups       = Dir.glob("#{backup_dir}**/*assets*")
    # sort and get latest assets backup
    newest_assets_backup = assets_backups.sort_by(&File.method(:mtime)).last
    # copy backup assets to assets folder and only overwrite same contents
    assets_restore_cmd = "cp -Rf #{newest_assets_backup} #{assets_dir}"

    system assets_restore_cmd
    puts assets_restore_cmd
    puts "\n Copying Assets to: #{public_dir}\n"
  end

  desc "Restores the database from a backup using PATTERN"
  task db: :environment do
    pattern = "sql"
    file = nil
    cmd  = nil

    with_config do |_app, host, db, user, _password|
      backup_sql_files   = Dir.glob("#{backup_directory}**/*#{pattern}*")
      # Sort Files Ascending
      files              = backup_sql_files.sort_by(&File.method(:mtime))

      case files.size
      when 0
        puts "No backups found for the pattern '#{pattern}'"
      when 1
        file = files.last # latest sql backup
        fmt  = format_for_file file
        case fmt
        when nil
          puts "No recognized dump file suffix: #{file}"
        when "p"
          cmd = "psql -U '#{user}' -h '#{host}' -d '#{db}' -f '#{file}'"
        else
          cmd = "pg_restore -F #{fmt} -v -c -C -U '#{user}'"\
                " -h '#{host}' -d '#{db}' -f '#{file}'"
        end
      else
        puts "Too many files match the pattern '#{pattern}':"
        puts " " + files.join("\n ")
        puts ""
        puts "Try a more specific pattern"
        puts ""
      end
    end
    unless cmd.nil?
      Rake::Task["db:drop"].invoke
      Rake::Task["db:create"].invoke
      puts cmd
      system cmd
      puts "\n Restored from file: #{file}\n"
    end
  end
end
private

def send_notif(dir:, status:)
  webhook_url = ENV.fetch("SLACK_CHANNEL_HOOK")
  icon_url = "http://emojis.slackmojis.com/emojis/images/1488491307/"\
             "1803/datical.jpg?1488491307"
  channel = ENV.fetch("SLACK_CHANNEL")
  name = "Database and Assets Backup Bot #{Rails.env.titleize}"
  notifier = Slack::Notifier.new webhook_url, channel: channel, username: name
  notifier.ping notifier_options(icon_url: icon_url, dir: dir, status: status)
end

def notifier_options(icon_url:, dir:, status:)
  success_message = "Database and Assets Backup Successfully Created in #{dir}"
  error_message = "Failed to Backup Database and Assets, Please check "\
                  "server logs"
  {
    text: status == "success" ? success_message : error_message,
    icon_url: icon_url,
    attachments: attachments_options(status: status)
  }
end

def attachments_options(status:)
  [
    {
      "fallback": "Database and Assets Backup Task",
      "color": status == "success" ? "good" : "danger",
      "fields": [
        {
          "title": "Database and Assets Backup",
          "value": status.titleize
        }
      ]
    }
  ]
end

def ensure_format(format)
  return format if %w[c p t d].include?(format)

  case format
  when "dump" then "c"
  when "sql" then "p"
  when "tar" then "t"
  when "dir" then "d"
  else "p"
  end
end

def suffix_for_format(suffix)
  case suffix
  when "c" then "dump"
  when "p" then "sql"
  when "t" then "tar"
  when "d" then "dir"
  end
end

def format_for_file(file)
  case file
  when /\.dump$/ then "c"
  when /\.sql$/  then "p"
  when /\.dir$/  then "d"
  when /\.tar$/  then "t"
  end
end

def backup_directory(_suffix=nil, create: false, date: nil)
  backup_dir = Rails.root.join("db", "backups/#{date}")
  if create && !Dir.exist?(backup_dir)
    puts "Creating #{backup_dir} .."
    FileUtils.mkdir_p(backup_dir)
  end

  backup_dir
end

def with_config
  yield Rails.application.class.parent_name.underscore,
  ActiveRecord::Base.connection_config[:host],
  ActiveRecord::Base.connection_config[:database],
  ActiveRecord::Base.connection_config[:username],
  ActiveRecord::Base.connection_config[:password]
end
