namespace :users do
  desc "migrate user birthdates from old strings to new date formats"
  task migrate_birthdates: :environment do
    puts "==================================== Converting String Dates ====================================\n\n"

    User.find_each do |user|
      next unless user.old_birthdate.present?
      user.birthdate = Date.parse(user.old_birthdate) rescue nil
      if user.save
        puts "Successfully parsed and save string birthdate : #{user.birthdate}"
      else
        puts "Failed to save user due to #{user.errors.full_messages.join(",")} with date : #{user.old_birthdate}"
      end
    end

    puts "\n==================================== Comparing String Dates ====================================\n\n"

    success_count = 0
    fail_count = 0
    User.find_each do |user|
      next unless user.old_birthdate.present?
      if Date.parse(user.old_birthdate) == user.birthdate
        print '✓'
        success_count += 1;
      else
        print '✗'
        fail_count += 1;
      end
    end

    puts "\n\nSuccess[✓]:#{success_count}   Failure[✗]:#{fail_count}"
  end
end
