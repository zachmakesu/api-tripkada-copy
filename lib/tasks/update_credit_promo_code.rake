namespace :users do
  desc "update user credit promo code"
  task update_credit_promo_code: :environment do
    puts "==================================== Update Credit Promo Code ====================================\n\n"

    PromoCode.credit.find_each do |credit|
      if credit.update(code: PromoCode.generate_unique_code)
        puts "Credit Promo Code Successfully updated"
      else
        puts "Failed to save user due to #{promo_code.errors.full_messages.to_sentence}"
      end
    end
  end
end
