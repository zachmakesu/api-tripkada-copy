namespace :dragonpay do
  desc "mark expired dragonpay payments"
  task :mark_expired => :environment do
    all_pending_dragonpay = Payment.dragonpay.pending
    DragonpayTransactionLog::check_for_expired(payments: all_pending_dragonpay)
  end
end
