namespace :users do
  desc "create user credit promo code"
  task credit_promo_code: :environment do
    puts "==================================== Create User Credit Promo Code ====================================\n\n"

    User.find_each do |user|
      next if user.credit_promo_code.present?
      promo_code = user.build_credit_promo_code(code: PromoCode.generate_unique_code, value: 0.0)
      if promo_code.save
        puts "Credit Promo Code Successfully created"
      else
        puts "Failed to save user due to #{promo_code.errors.full_messages.to_sentence}"
      end
    end

    puts "\n==================================== Check User Promo Codes ====================================\n\n"

    success_count = 0
    fail_count = 0
    User.find_each do |user|
      if user.credit_promo_code.present?
        print '✓'
        success_count += 1;
      else
        print '✗'
        fail_count += 1;
      end
    end
    puts "\n\nSuccess[✓]:#{success_count}   Failure[✗]:#{fail_count}"
  end
end
