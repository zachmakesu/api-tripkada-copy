namespace :users do
  desc "add registration_platform_for_current_users"
  task update_registration_platform: :environment do
    success_count = 0
    fail_count = 0
    User.all.find_each do |user|
      next if user.registration_platform.present?
      platform = identify_device_platform(user.devices.first&.platform)
      if user.update(registration_platform: platform)
        print "✓"
        success_count += 1
      else
        print "✗"
        fail_count += 1
      end
    end
    puts "\n\nSuccess[✓]:#{success_count}   Failure[✗]:#{fail_count}"
  end
end

def identify_device_platform(device)
  case device
  when "android"
    "android_app"
  when "ios"
    "ios_app"
  else
    "desktop"
  end
end
