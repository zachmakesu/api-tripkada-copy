desc "Populate Mailerlite Group"
task populate_mailerlite_group: :environment do
    puts "==================================== Create Mailerlite Group ====================================\n\n"
    data = [
      { group_id: 8718256, name: "Tripkada Official Mailing List", category: "default" },
      { group_id: 8718394, name: "Tripkada One-Off Users (Booked Once)", category: "one_off" },
      { group_id: 8718412, name: "Tripkada Repeat Users (Booked Twice)", category: "repeat" },
      { group_id: 8718422, name: "Tripkada Loyal (Booked 3 or more)", category: "loyal" },
      { group_id: 8760264, name: "Active (Booked not less than two months ago)", category: "active" },
      { group_id: 8760266, name: "At Risk (Booked more than 2 months ago but less than 4 months ago)", category: "at_risk" },
      { group_id: 8760270, name: "Lapsed (Booking more than 4 months ago)", category: "lapsed" }
    ]
    MailerliteGroup.create(data) if MailerliteGroup.all.empty?
  puts "Mailerlite Group  Successfully Populated..."
end
