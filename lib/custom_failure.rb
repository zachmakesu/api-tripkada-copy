class CustomFailure < Devise::FailureApp
  # Redirect to on fail authentication root_path
  def respond
    if http_auth?
      http_auth
    else
      redirect_to root_path
    end
  end
end