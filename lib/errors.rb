module Errors
  class AuthenticationError < StandardError
    def self.response
      {
        code:     self.code,
        details:  self.details
      }
    end
  end

  class MissingAuthorization < AuthenticationError
    def self.code
      100
    end
    def self.details
      "Missing Authorization Header"
    end
  end

  class InvalidToken < AuthenticationError
    def self.code
      101
    end
    def self.details
      "Invalid or Expired Access Token"
    end
  end

  class InvalidSignature < AuthenticationError
    def self.code
      102
    end
    def self.details
      "Invalid or Malformed Signature"
    end
  end

  class AuthorizationError < StandardError
    def self.response
      {
        code:     103,
        details:  "Unauthorized Action"
      }
    end
  end
end
