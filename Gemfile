source "https://rubygems.org"

gem "active_type", ">= 0.3.2"
gem "autoprefixer-rails", ">= 5.0.0.1"
gem "bcrypt", "~> 3.1.7"
gem "coffee-rails", "~> 4.1.0"
gem "dotenv-rails", ">= 2.0.0"
gem "jquery-rails"
gem "mail", ">= 2.6.3"
gem "marco-polo"
gem "pg", "~> 0.15"
gem "rails", "4.2.6"
gem "redis-namespace"
gem "sass-rails", "~> 5.0"
gem "secure_headers", "~> 3.0"
gem "sidekiq"
gem "sinatra", ">= 1.3.0", :require => false
gem 'grape'
gem 'grape-entity'
gem 'instagram'
gem 'devise'
gem 'pusher'
gem "paperclip"
gem "koala"
gem 'exception_notification'
gem 'grape_on_rails_routes'
gem 'rspec-rails'
gem 'ci_reporter_rspec'
gem 'faker'
gem 'draper'
gem 'foundation-rails'
gem 'font-awesome-sass'
gem 'friendly_id', '~> 5.1.0'
gem 'simple_form'
gem 'kaminari'
gem 'simple-navigation', '~> 4.0', '>= 4.0.3'
gem 'tabs_on_rails'
gem 'jquery-ui-rails'
gem 'geocoder'
gem 'fcm'
gem 'omniauth-facebook'
gem 'city-state'
gem 'slack-notifier'
gem 'jquery-slick-rails'
gem 'tagsinput-rails'
gem 'jquery-timepicker-addon-rails'
gem 'deep_cloneable', '~> 2.2.2'
gem 'staccato'
gem 'premailer-rails' #Provide Access to Css For html based emails
gem 'chikka'
gem 'momentjs-rails', '~> 2.11', '>= 2.11.1'
gem 'angularjs-rails', '~> 1.6', '>= 1.6.2'
gem 'ionicons-rails', '~> 2.0'
gem 'paypal-sdk-rest'
gem 'chartkick', '~> 1.3', '>= 1.3.2'
gem 'groupdate', '~> 2.5', '>= 2.5.2'
gem 'uglifier'
gem 'whenever', '~> 0.9.4', require: false
gem 'sitemap_generator', '~> 5.1'
gem 'letter_avatar'
gem 'ejs'
gem 'light-service'
gem "facebook-messenger"
gem 'browser', '~> 1.1'
gem 'api-pagination'
gem 'faraday'
gem 'dragon_pay'
gem "email_address"
gem 'parser', "~> 2.5", ">= 2.5.0.5"
gem "aws-sdk", "< 2.0"

group :production, :staging do
  gem "unicorn"
  gem "unicorn-worker-killer"
end

group :development do
  gem "annotate", ">= 2.5.0"
  gem "awesome_print"
  gem "better_errors"
  gem "binding_of_caller"
  gem "letter_opener"
  gem "listen"
  gem "quiet_assets"
  gem "rack-livereload"
  gem "spring"
  gem "xray-rails", ">= 0.1.18"
end

group :development do
  gem "airbrussh", "~> 1.0", :require => false
  gem "brakeman", :require => false
  gem "bundler-audit", ">= 0.5.0", :require => false
  gem "capistrano", "~> 3.4", :require => false
  gem "capistrano-bundler", :require => false
  gem "capistrano-mb", ">= 0.22.2", :require => false
  gem "capistrano-nc", :require => false
  gem "capistrano-rails", :require => false
  gem "capistrano-db-tasks", require: false
  gem "guard", ">= 2.2.2", :require => false
  gem "guard-livereload", :require => false
  gem "rb-fsevent", :require => false
  gem "simplecov", :require => false
  gem "sshkit", "~> 1.8", :require => false
  gem "terminal-notifier", :require => false
  gem "terminal-notifier-guard", :require => false
  gem "thin", :require => false
  gem 'guard-rspec', require: false
  gem "rubocop", ">= 0.50.0", :require => false
  gem 'overcommit'
  gem "scss_lint", require: false
end

group :test do
  gem "vcr"
  gem "webmock"
  gem 'shoulda-matchers'
  gem "shoulda-context"
  gem "capybara"
  gem "connection_pool"
  gem "launchy"
  gem "mocha"
  gem "poltergeist"
  gem "test_after_commit"
  gem "factory_girl_rails"
  gem 'database_cleaner'
  gem "pusher-fake", "1.7.0"
end

group :development, :test do
  gem "bullet"
  gem "pry"
end
