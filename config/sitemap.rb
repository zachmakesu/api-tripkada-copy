# reference: https://www.cookieshq.co.uk/posts/creating-a-sitemap-with-ruby-on-rails-and-upload-it-to-amazon-s3
SitemapGenerator::Sitemap.default_host = "https://www.tripkada.com/"

SitemapGenerator::Sitemap.create do
  add trips_path, changefreq: 'daily'

  Event.find_each do |event|
    add trip_view_path(event), lastmod: event.updated_at, changefreq: 'daily'
  end

  add who_we_are_path
  add how_it_works_path
  add privacy_policy_path
  add collaborate_on_project_path
  add become_a_trip_organizer_path
  add apply_for_a_job_path
  add trip_requests_path
  add questions_path
end
