require 'capistrano-db-tasks'

set :db_local_clean, true
set :db_remote_clean, true
set :assets_dir, 'public/system'

set :application, "api-tripkada"
set :repo_url, "git@bitbucket.org:gorated/api-tripkada.git"

after 'deploy:published', 'deploy:update_crontab'
# Project-specific overrides go here.
# For list of variables that can be customized, see:
# https://github.com/mattbrictson/capistrano-mb/blob/master/lib/capistrano/tasks/defaults.rake

fetch(:mb_recipes) << "sidekiq"
fetch(:mb_aptitude_packages).merge!(
  "ppa:chris-lea/redis-server" => :redis
)

set :mb_dotenv_keys, %w(
  rails_secret_key_base
  sidekiq_web_username
  sidekiq_web_password
  gmail_username
  gmail_password
  facebook_key
  facebook_secret
  hmac_secret
  hmac_secret_2
  google_map_api
  fb_pixel_id
  google_analytics_tracking_id
  fb_app_id
  tagcash_id
  tagcash_secret
  tripkada_merchant_id
  tripkada_type
  paypal_client_id
  paypal_client_secret
  paypal_app_id
  paypal_username
  paypal_password
  paypal_signature
  paypal_sandbox_email
  paypal_client_id_sandbox
  paypal_client_secret_sandbox
  paypal_app_id_sandbox
  paypal_username_sandbox
  paypal_password_sandbox
  paypal_signature_sandbox
  paypal_merchant_email
  pusher_app_id
  pusher_key
  pusher_secret
  pusher_host
  pusher_port
  pusher_ws_host
  pusher_wss_port
  pusher_ws_port
  fcm_key
  instagram_client_id
  instagram_secret
  instagram_redirect_uri
  mailerlite_api_key
  facebook_app_id
  dragonpay_merchant_id
  dragonpay_secret_key
  slack_channel_hook
  slack_channel
  aws_bucket_name
  aws_access_key
  aws_access_key_secret
)

set :whenever_roles,        ->{ :app }

namespace :deploy do
  desc "Update the crontab sitemap file"
  task :update_crontab do
    on roles :app do
      execute "cd #{release_path} && bundle exec whenever --update-crontab"
    end
  end
end
