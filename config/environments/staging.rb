# Staging configuration is identical to production, with some overrides
# for hostname, etc.

require_relative "./production"

Rails.application.configure do
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    address:              'smtp.gmail.com',
    port:                 587,
    domain:               'gorated.com',
    user_name:            ENV["GMAIL_USERNAME"],
    password:             ENV["GMAIL_PASSWORD"],
    authentication:       'plain',
  enable_starttls_auto: true  }

  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.default_url_options = {
    :host => "staging.tripkada.com",
    :protocol => "https"
  }

  config.action_mailer.default_options = {
    :from => "join@tripkada.com"
  }

  config.action_mailer.asset_host = "https://staging.example.com"
  config.action_controller.asset_host = 'https://staging.tripkada.com'

  config.action_controller.default_url_options = {
    :host => "staging.tripkada.com",
    :protocol => "https"
  }
  # compress assets
  config.assets.js_compressor = :uglifier
  config.assets.css_compressor = :sass
  # for routes.url_helpers
  Rails.application.routes.default_url_options[:host] = 'staging.tripkada.com'
end
