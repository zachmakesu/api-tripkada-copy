SimpleNavigation::Configuration.run do |navigation|
  navigation.renderer = FoundationRenderer
  navigation.selected_class = 'active'
  navigation.autogenerate_item_ids = false

  navigation.items do |primary|
    primary.item :events, "#{image_tag('location White.png')} Events".html_safe, root_path
    primary.item :categories, "#{image_tag('tag White.png')} Categories".html_safe, categories_path
    primary.item :destinations, "#{image_tag('tag White.png')} Destinations".html_safe, destinations_path
    primary.item :users, "#{image_tag('user White.png')} Users".html_safe, users_path
    primary.item :promo_codes, "#{image_tag('tag White.png')} Promo Codes".html_safe, promo_codes_path
    primary.item :payments, "#{image_tag('banknote White.png')} Payments".html_safe, payments_path
    primary.dom_class = 'vertical menu sidebar'
  end
end
