Rails.application.routes.draw do

  @web = { domain: ["tripkada.com","localhost", "13.250.138.152", "13.229.186.20"] } unless Rails.env.development? || Rails.env.test?
  @api = { subdomain: ["staging-api","api"] } unless Rails.env.development? || Rails.env.test?

  mount Facebook::Messenger::Server, at: 'bot'
  mount Sidekiq::Web => "/sidekiq" # monitoring console
  constraints @api do
    mount API::Base => '/'
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"

  # You can have the root of your site routed with "root"

  constraints @web do
    authenticate :user do

      scope :profile do
        post "/:slug/reviews",    to: "users#review",                   as: "profile_review"
        get "/:slug/edit",        to: "users#edit",                     as: "profile_edit"
        put "/:slug",             to: "users#update_profile_photos",    as: "profile_update"
        post "/:slug/follow",     to: "users#follow",                   as: "follow_user"
        delete "/:slug/unfollow", to: "users#unfollow",                 as: "unfollow_user"
      end

      scope :trips do
        get "/:slug/checkout",                                to: "event#checkout",                       as: "trip_checkout"
        get "/waitlists/:id/checkout",                        to: "event#waitlist_checkout",              as: "waitlist_checkout"
        get "/:slug/checkout/payment_details",                to: "event#payment_details",                as: "payment_details"
        get "/:slug/checkout/paypal_payment_verification",    to: "payments#paypal_payment_verification", as: "paypal_payment_verification"
        post "/:slug/join",                                   to: "event#join",                           as: "join"
        post "/:slug/join_using_promo_code",                  to: "event#join_using_promo_code",          as: "join_using_promo_code"
        post "/promo_code/validate",                          to: "promo_codes#validate",                 as: "validate_promo_code"
        post "/:slug/join_waitlist",                          to: "event#join_waitlist",                  as: "join_waitlist"
        get  "/:slug/checkout/dragonpay",                     to: "event#dragonpay",                      as: "dragonpay"
      end

      scope :organizer do
        get "/trips/:slug/edit",                      to: "event#edit",               as: "organizer_trip_edit"
        get "/trips/:slug/duplicate",                 to: "event#duplicate",          as: "organizer_trip_duplicate"
        put "/trips/:slug",                           to: "event#update",             as: "organizer_trips"
        delete "/trips/:slug",                        to: "event#destroy"
        get "/create_trip",                           to: "event#new"
        post "/create_trip",                          to: "event#create"
        put "/trip_no_show",                          to: "event#no_show",            as: "trip_no_show"
        scope :manage_trips do
          get "/",                                    to: "manage_trips#index",        as: "manage_trips"
          get "/my_trips",                            to: "manage_trips#trips",        as: "manage_trips_my_trips"
          get "/my_trips/:slug/waitlists",            to: "manage_trips#waitlists",    as: "manage_trips_waitlist"
          put "/my_trips/:slug/cancel",               to: "manage_trips#cancel_trip",  as: "organizer_trip_cancel"
          get "/joiner",                              to: "manage_trips#joiner",       as: "manage_trips_joiner"
          get "/payments",                            to: "manage_trips#payments",     as: "manage_trips_payments"
          get "/funds",                               to: "manage_trips#funds",        as: "manage_trips_funds"
          post "/invite_joiner",                      to: "manage_trips#invite_joiner", as: "organizer_invite_joiner"
          post "/invite_joiner_via_email",            to: "manage_trips#invite_joiner_via_email", as: "organizer_invite_joiner_via_email"
          get "organized_trip_views",                 to: "manage_trips#organized_trip_views"
          get "organized_trip_bookings",              to: "manage_trips#organized_trip_bookings"
          get "monthly_organized_trips",                      to: "manage_trips#monthly_organized_trips"
          get "monthly_organized_trips_booking_count",        to: "manage_trips#monthly_organized_trips_booking_count"

          post 'my_trips/waitlists/update_waitlist',  to: "manage_trips#update_waitlist", as: "update_waitlist"
        end
      end

      scope :admin do
        get "/",                                            to: "admin#index",                                     as: "admin"
        get "/event/:slug",                                 to: "admin#event",                                     as: "get_event"
        get "/users",                                       to: "users#index"
        get "/users/organizer_applicants",                  to: "users#organizer_applicants",                      as: "organizer_applicants"
        get "/users/export_all_organizers",                 to: "users#export_all_organizers",                     as: "export_all_organizers"
        get "/users/export_stats_organizer",                to: "users#export_stats_organizer",                    as: "export_stats_organizer"
        get "/users/cities",                                to: "users#cities"
        get "/users/export",                                to: "users#export",                                    as: "export_users"
        get "/users/segmentations",                         to: "users#segmentations",                             as: "user_segmentations"
        get "/users/export_segmentations",                  to: "users#export_segmentations",                      as: "export_users_segmentation"
        get "/payments/export_all_payments",                to: "payments#export_all_payments",                    as: "export_all_payments"
        get "/users/user_info",                             to: "users#user_info",                                 as: "user_info"
        get "/payments/mark_expired",                       to: "payments#mark_dragonpay_as_expired",              as: "mark_expired_payments"

        delete "/remove_joiner",                            to: "admin#remove_joiner"
        put "/users/:id/update_role",                       to: "users#update_role",                               as: "update_role"
        put "/event/featured",                              to: "admin#make_featured",                             as: "make_featured"
        get "/mailerlite/groups",                           to: "mailerlite#index",                                as: "mailerlite_groups"
        post "/mailerlite/groups",                          to: "mailerlite#create",                               as: "mailerlite_new_group"
        post "/mailerlite/groups/:id/import_subscribers",   to: "mailerlite#import_subscribers",                   as: "mailerlite_import_subscribers"
        delete "/mailerlite/groups/:id",                    to: "mailerlite#delete",                               as: "mailerlite_delete_group"
        post "/notifications/:id/deliver",                  to: "custom_notification_messages#deliver",            as: "deliver_notification"
        post "/promo_code/import",                          to: "promo_codes#import",                              as: "import_promo_code"

        resources :payments, only: [:index,:update]
        resources :app_versions, except: [:new]
        resources :promo_codes
        resources :categories
        resources :destinations
        resources :custom_notification_messages
        resources :booking_fees, only: [:index,:create]
        resources :referral_incentives, only: [:index,:create]
      end

      scope :payment do
        post "paypal_transaction",  to: "payments#paypal_transaction"
      end

      scope :user do
        get "/notifications",           to: "notifications#index"
        put "/notifications/:id",       to: "notifications#show",                             as: "user_notification"
        get "/notification_count",      to: "notifications#count_users_unseen_notifications", as: "count_unseen_notifications"
        put "/profile_update",          to: "users#update"
      end

      scope :referral do
        post "send_email_invitations",                  to: "referrals#send_invitations",      as: "send_referral_email_invitations"
      end

      scope :instagram do
        get "(/:provider_id)/connect",  to: "instagram#connect",      as: "instagram_connect"
        get "/callback",                to: "instagram#callback",     as: "instagram_callback"
      end

      get "/:role/my_trips",            to: "event#my_trips",       as: "my_trips"
      post "/become_a_trip_organizer",      to: "home#create_form_organiser"
    end

    root "home#index"
    get "profile/:slug/trips",                            to: "users#trips",                   as: "profile_trips"
    get "profile/:slug/reviews",                          to: "users#reviews",                 as: "profile_reviews"
    get "/trips_per_page",                                to: "event#paginated_page"
    get "/paginated_trips",                               to: "users#paginated_trips"
    get "/paginated_reviews",                             to: "users#paginated_reviews"
    get "/trips/:slug",                                   to: "event#page",                    as: "trip_view"
    get "/trips/:slug/joiners",                           to: "event#joiners",                 as: "trip_joiners"
    get "/trips/:slug/checkout/invited_payment_details",  to: "event#invited_payment_details", as: "invited_payment_details"
    get "/who_we_are",                                    to: "home#who_we_are"
    get "/how_it_works",                                  to: "home#how_it_works"
    get "/privacy_policy",                                to: "home#privacy_policy"
    get "/collaborate_on_project",                        to: "home#collaborate"
    post "/sign_me_up",                                   to: "home#sign_me_up"
    get "/trips",                                         to: "event#index"
    get "/users/sign_in/organizer",                       to: "home#sign_in_as_organizer",     as: "organizer_sign_in"
    get "/apply_for_a_job",                               to: "home#apply_for_a_job"
    get "/trip_requests",                                 to: "home#requests"
    post "/trip_requests",                                to: "home#create_requests"
    get "/questions",                                     to: "home#questions"
    post "/questions",                                    to: "home#create_questions"
    get "/tagcash_callback",                              to: "home#tagcash_callback"
    get "/profile/:slug",                                 to: "users#show",                    as: "profile"
    match "/404",                                         to: "custom_errors#page_not_found", via: :all
    get 'users/uid',                                      to: "users#current_user_uid",        as: "users_uid"
    get 'organizer/landing_page',                         to: "organizer#index",               as: "organizer_landing_page"
    get "/be_a_trip_organizer",                           to: "home#apply_as_organiser",       as: "be_a_trip_organizer"

    get "/messenger_webview/set_profile",                                to: "messenger_webview#set_profile"
    get "/messenger_webview/unset_profile",                              to: "messenger_webview#unset_profile"
    get "/messenger_webview/subscribe",                                  to: "messenger_webview#subscribe"
    get "/messenger_webview/unsubscribe",                                to: "messenger_webview#unsubscribe"
    get "/messenger_webview/specific_date_trips/:fbuser_obj_id",         to: "messenger_webview#specific_date_trips"
    get "/messenger_webview/destination_based_trips/:fbuser_obj_id",     to: "messenger_webview#destination_based_trips"
    get "/messenger_webview/activity_based_trips/:fbuser_obj_id",        to: "messenger_webview#activity_based_trips"
    get "/messenger_webview/event_filters/:fbuser_obj_id",               to: "messenger_webview#event_filters"
    get "/messenger_webview/filtered_events",                            to: "messenger_webview#filtered_events"
    get "/messenger_webview/fill_up_form/:fbuser_obj_id",                to: "messenger_webview#fill_up_form"
    get "/messenger_webview/checkout_notif/:membership_id/:fbuser_obj_id", to: "messenger_webview#checkout_notif"
    get "/invitee/validate_email/"                                       , to: "event#validate_invitee_email"
    scope :payment do
      get  "/dragonpay/callback", to: "payments#dragonpay_callback"
      post  "/dragonpay/postback", to: "payments#dragonpay_postback"
    end
    devise_for :users, controllers: { sessions: 'users/sessions', omniauth_callbacks: "users/omniauth_callbacks" }, :skip => [:registrations, :password]
  end

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
