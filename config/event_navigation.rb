SimpleNavigation::Configuration.run do |navigation|
  navigation.renderer = FoundationRenderer
  navigation.selected_class = 'active'
  navigation.autogenerate_item_ids = false

  navigation.items do |primary|
    if user_signed_in?
      if current_user.joiner_and_organizer?
        if current_user.has_completed_required_data?
          primary.item :create_trip, 'CREATE TRIPS', create_trip_path
        else
          primary.item :create_trip, 'CREATE TRIPS', "#", html: { data: { open: "editDetailModal" } }
        end
      end
      primary.item :event_trip, 'TRIPS', '#',  html: {class: 'dropdown-arrow'} do |event_trip|
        event_trip.item :trips_page, 'Trips Page', trips_path
        event_trip.item :my_trips, 'My Trips', my_trips_path(app_role)
        event_trip.dom_class = 'menu'
        event_trip.item :manage_trips, "Manage Trips", manage_trips_path if current_user.joiner_and_organizer?
      end
    else
      primary.item :event_trip, 'TRIPS', trips_path
    end

    primary.item :about, 'ABOUT TRIPKADA', '#',  html: {class: 'dropdown-arrow'} do |about|
      about.item :who_we_are, 'Who we are', who_we_are_path
      about.item :how_it_works, 'How it works', how_it_works_path
      about.item :press, 'Press', '#'
      about.dom_class = 'menu'
    end
    primary.item :blog, 'BLOG', 'http://blog.tripkada.com/'
    primary.item :join, 'JOIN US' , '#', html: {class: 'dropdown-arrow'} do |join|
      join.item :trip_organiser_landing_page, 'Organizer Page', organizer_landing_page_path
      join.item :trip_organiser, "Be a trip organizer", be_a_trip_organizer_path
      join.item :collaborate, 'Collaborate on project', collaborate_on_project_path
      join.item :apply, 'Apply for a job', apply_for_a_job_path
      join.dom_class = 'menu'
    end
    primary.item :contact, 'CONTACT US', '#', html: {class: 'dropdown-arrow'} do |contact|
      contact.item :requests, 'Trip requests', trip_requests_path
      contact.item :questions, 'Questions', questions_path
      contact.dom_class = 'menu'
    end
    if user_signed_in?
      primary.item :notifications, content_tag(:span, users_notification_count_string), notifications_path, html: {class: 'notification-alert', id: "notification-count"} unless users_notification_empty? || mobile_device?
      primary.item :signed_in_user, mobile_device? && !users_notification_empty? ? "#{current_user.first_name}" : "Hi #{current_user.first_name}! <div style='background-image:url(#{current_user.decorate.avatar_url(:thumb)})'></div>".html_safe,'#', link_html: { :class => "user-logout logged-user dropdown-arrow" } do |signed_in_user|
        signed_in_user.item :notifications,  mobile_device? ? "Notifications <span id='span-number-of-notifications'>#{users_notification_count_string}</span>".html_safe : "Notifications", notifications_path
        signed_in_user.item :credits, 'My Credits', '#', link_html: { :'data-open' => "referralModal" }
        signed_in_user.item :profile, 'My Profile', profile_path(current_user.slug)
        signed_in_user.item :settings, 'Settings', '#', link_html: { :'data-open' => "editDetailModal" }
        signed_in_user.item :logout, 'Logout', destroy_user_session_path, method: :delete
        signed_in_user.dom_class = 'menu'
      end
    else
      primary.item :login, 'SIGN UP', '#', link_html: { :'data-open' => "sign-upModal" }
      primary.item :login, 'LOGIN', '#', link_html: { :'data-open' => "loginModal" }
      primary.item :download, "#{"<span class='download-app-btn download-app-btn__center'>
        GET THE APP</span>"}
        #{link_to "<i class='ion-play download-app-btn__playstore_icon'> </i>".html_safe,
        "https://play.google.com/store/apps/details?id=ph.fliptrip.tripkada.joiner",
        target: "_blank" } #{ link_to "<i class='ion-social-apple download-app-btn__apple_icon'>
        </i>".html_safe, "https://itunes.apple.com/ph/app/tripkada-trip-pooling-app/id1333012684?mt=8",
        target: "_blank"}",
        nil ,html: { class: 'dropdown-download-app' }, link_html: { class: "download-app-btn" }
    end
    primary.dom_class = 'dropdown menu'
  end
end
