# Use this file to easily define all of your cron jobs.
every :day, at: '5:00 am', roles: [:app] do
  rake "-s sitemap:refresh"
end

every :day, at: "12:00am", roles: [:app] do
  rake "execute_invalid_request_notifier"
end

every :day, at: "12:00am", roles: [:app] do
  rake "backup:db_assets"
end
