set :branch, ENV.fetch("CAPISTRANO_BRANCH", "master")

set :mb_sidekiq_concurrency, 1

set :mb_privileged_user, "deployer"

set :deploy_to, "/home/deployer/apps/api-tripkada"

set :stage, "production"

server "deployer@ec2-13-229-186-20.ap-southeast-1.compute.amazonaws.com",
  :user => "deployer",
  :roles => %w(app backup cron db redis sidekiq web)
