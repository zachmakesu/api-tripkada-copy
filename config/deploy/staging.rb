set :branch, ENV.fetch("CAPISTRANO_BRANCH", "development")

set :mb_sidekiq_concurrency, 1

set :mb_privileged_user, "deployer"

set :deploy_to, "/home/deployer/apps/staging-api-tripkada"

set :stage, "staging"

server "ec2-13-250-138-152.ap-southeast-1.compute.amazonaws.com",

  :user => "deployer",
  :roles => %w(app backup cron db redis sidekiq web)
