SimpleNavigation::Configuration.run do |navigation|
  navigation.renderer = FoundationRenderer
  navigation.selected_class = 'active'
  navigation.autogenerate_item_ids = false

  navigation.items do |primary|

    primary.item :who_we_are, 'Who we are', who_we_are_path
    primary.item :how_it_works, 'How it works', how_it_works_path
    primary.item :press, 'Press', '#'
    primary.item :blog, 'BLOG', '#'
    primary.item :trip_organiser, "Be a trip organizer", be_a_trip_organizer_path if current_user.blank? || (current_user.present? && current_user.joiner?)
    primary.item :collaborate, 'Collaborate on project', collaborate_on_project_path
    primary.item :apply, 'Apply for a job', apply_for_a_job_path
    primary.item :requests, 'Trip requests', trip_requests_path
    primary.item :questions, 'Questions', questions_path
    primary.dom_class = 'menu'
   end
end
