# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

Rails.application.config.assets.precompile += %w( main.css event.js main.js base.js admin.css manage_trips.css media_query.css messenger_view.css notification.css notification_mailer.css custom_errors.css admin.js invite_joiner.js update_payment_status.js organizer/landing_page.css)
