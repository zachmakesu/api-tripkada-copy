require "instagram"
IG_CALLBACK_URL = ENV.fetch("INSTAGRAM_REDIRECT_URI")
Instagram.configure do |config|
  config.client_id = ENV.fetch("INSTAGRAM_CLIENT_ID")
  config.client_secret = ENV.fetch("INSTAGRAM_SECRET")
end
