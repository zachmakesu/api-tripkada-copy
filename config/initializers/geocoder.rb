#prevent geocoder from running during test
Geocoder.configure(:lookup => :test)
Geocoder::Lookup::Test.set_default_stub([{
  latitude:  14.603057,
  longitude: 121.091960,
  address: '1800, Philippines',
  state: 'Metro Manila',
  city: 'Pasig',
  state_code: 'Metro Manila',
  postal_code: '1800',
  country: 'Philippines',
  country_code: '63'
}])
