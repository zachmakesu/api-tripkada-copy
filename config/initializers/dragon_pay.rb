DragonPay.configure do |config|
  config.test_mode   = Rails.env.development? || Rails.env.staging?
  config.merchant_id = ENV.fetch("DRAGONPAY_MERCHANT_ID")
  config.secret_key  = ENV.fetch("DRAGONPAY_SECRET_KEY")
end
