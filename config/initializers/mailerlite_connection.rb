class MailerliteConnection
  CLIENT_URL = "https://api.mailerlite.com/api/v2/".freeze

  def self.initialize
    conn_opts = {
      headers: { 
        "X-MailerLite-ApiKey": ENV.fetch("MAILERLITE_API_KEY"),
        "Content-Type": "application/json"
      },
      url: CLIENT_URL
    }

    Faraday.new(conn_opts)
  end
end
