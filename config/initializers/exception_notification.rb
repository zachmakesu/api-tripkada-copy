require 'exception_notification/rails'

ExceptionNotification.configure do |config|
  unless Rails.env.development? || Rails.env.test?
    config.add_notifier :email, {
      :email_prefix         => "[ERROR] Tripkada (#{Rails.env})",
      :sender_address       => %{"Notifier" <notifier@tripkada.com>},
      :exception_recipients => %w{ kevinc@gorated.ph mcdave@gorated.ph redjoker011@gmail.com cassi.mirabueno@gmail.com jaime@gorated.ph }
    }

    config.add_notifier :slack, {
      :webhook_url => "https://hooks.slack.com/services/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",
      :username => "Tripkada Bot (#{Rails.env})",
      :channel => '#tripkada-web-and-api',
      :additional_parameters => {
        :icon_emoji => ':boom:'
      }
    }
  end
end
