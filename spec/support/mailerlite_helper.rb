module MailerliteHelper
  def create_mailerlite_group(name:)
    VCR.use_cassette 'mailerlite/actions/create_group' do
      service = Actions::Mailerlite::Groups::CreateGroup.execute(name: name, body_params: {})
      client = MailerliteConnection.initialize
      client.send(service.request_type.to_sym) do |request|
        request.url(service.path, {})
        request.body = service.body_params.to_json
      end
    end
  end
  def delete_mailerlite_group(group_id:)
    VCR.use_cassette 'mailerlite/actions/delete_group' do
      service = Actions::Mailerlite::Groups::DeleteGroup.execute(group_id: group_id)
      client = MailerliteConnection.initialize
      client.send(service.request_type.to_sym) do |request|
        request.url(service.path, {})
        request.body = "{}"
      end
    end
  end
  def fetch_mailerlite_groups
    VCR.use_cassette 'mailerlite/actions/fetch_groups' do
      service = Actions::Mailerlite::Groups::GetGroups.execute()
      client = MailerliteConnection.initialize
      client.send(service.request_type.to_sym) do |request|
        request.url(service.path, {})
        request.body = "{}"
      end
    end
  end
  def import_mailerlite_group_subscribers(group_id:, subscribers:)
    VCR.use_cassette 'mailerlite/actions/import_group_subscribers' do
      subscribers_builder = Actions::Mailerlite::Groups::BuildSubscribers.execute(
        subscribers: subscribers, 
        body_params: {}
      )
      service = Actions::Mailerlite::Groups::ImportSubscribers.execute(
        group_id: group_id, 
        subscribers: subscribers_builder.subscribers,
        body_params: {}
      )
      client = MailerliteConnection.initialize
      client.send(service.request_type.to_sym) do |request|
        request.url(service.path, {})
        request.body = service.body_params.to_json
      end
    end
  end
  def delete_mailerlite_group_invalid(group_id:)
    VCR.use_cassette 'mailerlite/actions/delete_group_invalid' do
      service = Actions::Mailerlite::Groups::DeleteGroup.execute(group_id: group_id)
      client = MailerliteConnection.initialize
      client.send(service.request_type.to_sym) do |request|
        request.url(service.path, {})
        request.body = "{}"
      end
    end
  end
  def fetch_mailerlite_groups_organizer
    VCR.use_cassette 'mailerlite/organizer/fetch_groups' do
      Mailerlite::Groups::GetGroups.call
    end
  end
  def create_mailerlite_group_organizer(name:)
    VCR.use_cassette 'mailerlite/organizer/create_group' do
      Mailerlite::Groups::CreateGroup.call(name: name)
    end
  end
  def delete_mailerlite_group_organizer(group_id:)
    VCR.use_cassette 'mailerlite/organizer/delete_group' do
      Mailerlite::Groups::DeleteGroup.call(group_id: group_id)
    end
  end
  def delete_mailerlite_group_invalid_organizer(group_id:)
    VCR.use_cassette 'mailerlite/organizer/delete_group_invalid' do
      Mailerlite::Groups::DeleteGroup.call(group_id: group_id)
    end
  end
  def import_mailerlite_group_subscribers_organizer(group_id:, subscribers:)
    VCR.use_cassette 'mailerlite/organizer/import_group_subscribers' do
      subscribers_builder = Actions::Mailerlite::Groups::BuildSubscribers.execute(
        subscribers: subscribers, 
        body_params: {}
      )
      service = Actions::Mailerlite::Groups::ImportSubscribers.execute(
        group_id: group_id, 
        subscribers: subscribers_builder.subscribers,
        body_params: {}
      )
      client = MailerliteConnection.initialize
      client.send(service.request_type.to_sym) do |request|
        request.url(service.path, {})
        request.body = service.body_params.to_json
      end
    end
  end
  def add_subscriber_to_group(subscriber:, booking_count:)
    VCR.use_cassette 'mailerlite/organizer/add_to_group' do
      Mailerlite::Groups::AddToGroup.call(subscriber: subscriber, booking_count: booking_count)
    end
  end
  def add_subscriber_to_group_invalid(subscriber:, booking_count:)
    VCR.use_cassette 'mailerlite/organizer/add_to_group_invalid' do
      Mailerlite::Groups::AddToGroup.call(subscriber: subscriber, booking_count: booking_count)
    end
  end
  def remove_from_group(group_id:, email:)
    VCR.use_cassette 'mailerlite/organizer/remove_subscriber' do
      Mailerlite::Groups::RemoveSubscriber.call(group_id: group_id, subscriber_email: email)
    end
  end
  def remove_from_group_invalid(group_id:, email:)
    VCR.use_cassette 'mailerlite/organizer/remove_subscriber_invalid' do
      Mailerlite::Groups::RemoveSubscriber.call(group_id: group_id, subscriber_email: email)
    end
  end
end
