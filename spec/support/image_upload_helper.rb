module ImageUploadHelper
  def build_attachment(image:)
    {
      filename: image.original_filename,
      type: image.content_type,
      tempfile: image.tempfile,
      head: "#{Faker::Lorem.word}"
    }
  end
end
