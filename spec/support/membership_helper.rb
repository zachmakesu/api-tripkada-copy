module MembershipHelper
  def build_memberships(event:, user:, times:, params:)
    (1..times).each do
      membership = event.event_memberships.new(member_id: user.id)
      membership.save(validate: false)
      valid_payment_params = params
      valid_payment_params[:event_membership_id] = membership.id
      Payment.approved.create(valid_payment_params)
    end
  end
end
