module BuildParametersHelper
  def build_categorizations_params(event:, categorizations_ids:)
    new_categories = []
    event_categories = event.categorizations
    categorizations_ids.each do |category_id|
      new_categories << category_id unless event_categories.where(category_id: category_id).present?
    end
    Category.where(
      id: new_categories
    ).map{|category| { category_id: category.id } }
  end

  def build_destinations_params(event:, destinations_ids:)
    new_destinations = []
    event_destinations = event.sorting_destinations
    destinations_ids.each do |destination_id|
      new_destinations << destination_id unless event_destinations.where(destination_id: destination_id).present?
    end
    Destination.where(
      id: new_destinations
    ).map{|destination| { destination_id: destination.id } }
  end

  def build_photos_params(photos:)
    photos.each_with_index.map do |x,i|
      {
        image:  photos[i],
        primary: nil
      }
    end
  end
end
