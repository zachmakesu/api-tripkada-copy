module PaypalHandlerHelper
  def initialize_payment(params, event)
    VCR.use_cassette 'paypal/initialize_valid_payment', record: :new_episodes do
      PaypalHandler.new(params, event).create_instant_payment
    end
  end

  def initialize_payment_invalid(params, event)
    VCR.use_cassette 'paypal/initialize_valid_payment_invalid', record: :new_episodes do
      PaypalHandler.new(params, event).create_instant_payment
    end
  end

  def join_event_through_paypal_valid(params, event)
    VCR.use_cassette 'paypal/join_event_valid_payment' do
      PaypalHandler.new(params, event).execute_payment
    end
  end

  def join_event_through_paypal_invalid(params, event)
    VCR.use_cassette 'paypal/join_event_invalid_payment' do
      PaypalHandler.new(params, event).execute_payment
    end
  end

  def initialize_api_payment(params, event)
    VCR.use_cassette 'paypal/initialize_api_payment' do
      PaypalHandler.new(params, event).create_instant_payment
    end
  end

  def initialize_api_payment_once(params, event)
    VCR.use_cassette 'paypal/initialize_api_payment_once' do
      PaypalHandler.new(params, event).create_instant_payment
    end
  end

  def execute_api_payment(params)
    VCR.use_cassette 'paypal/execute_api_payment' do
      payment = PayPal::SDK::REST::Payment.find(params[:paymentId])
      payment.execute(payer_id: params[:PayerID])
      payment
    end
  end

  def execute_api_payment_invalid(params)
    VCR.use_cassette 'paypal/execute_api_payment_invalid' do
      payment = PayPal::SDK::REST::Payment.find(params[:paymentId])
      payment.execute(payer_id: params[:PayerID])
      payment
    end
  end

  def validate_api_payment(params, event)
    VCR.use_cassette 'paypal/validate_api_payment' do
      PaypalHandler.new(params, event).validate_payment_and_join
    end
  end

  def validate_api_payment_invalid(params, event)
    VCR.use_cassette 'paypal/validate_api_payment_invalid' do
      PaypalHandler.new(params, event).validate_payment_and_join
    end
  end

  def initialize_payment_with_transaction_fee(params, event)
    VCR.use_cassette 'paypal/initialize_payment_with_transaction_fee' do
      PaypalHandler.new(params, event).create_instant_payment
    end
  end

  def execute_payment_with_transaction_fee(params, event)
    VCR.use_cassette 'paypal/execute_payment_with_transaction_fee' do
      PaypalHandler.new(params, event).execute_payment
    end
  end


  def initialize_payment_with_invited_joiners(params, event)
    VCR.use_cassette 'paypal/initialize_payment_with_invited_joiners' do
      PaypalHandler.new(params, event).create_instant_payment
    end
  end

  def execute_payment_with_invited_joiners(params, event)
    VCR.use_cassette 'paypal/execute_payment_with_invited_joiners' do
      PaypalHandler.new(params, event).execute_payment
    end
  end


  def initialize_payment_with_invited_joiners_unparsed(params, event)
    VCR.use_cassette 'paypal/initialize_payment_with_invited_joiners_unparsed' do
      PaypalHandler.new(params, event).create_instant_payment
    end
  end

  def execute_payment_with_invited_joiners_unparsed(params, event)
    VCR.use_cassette 'paypal/execute_payment_with_invited_joiners_unparsed' do
      PaypalHandler.new(params, event).execute_payment
    end
  end

  def initialize_payment_with_waitlist(params, event)
    VCR.use_cassette 'paypal/initialize_payment_with_waitlist' do
      PaypalHandler.new(params, event).create_instant_payment
    end
  end

  def execute_payment_with_waitlist(params, event)
    VCR.use_cassette 'paypal/execute_payment_with_waitlist' do
      PaypalHandler.new(params, event).execute_payment
    end
  end

  def initialize_payment_with_promo_code(params, event)
    VCR.use_cassette 'paypal/initialize_payment_with_promo_code' do
      PaypalHandler.new(params, event).create_instant_payment
    end
  end

  def initialize_payment_without_promo_code(params, event)
    VCR.use_cassette 'paypal/initialize_payment_without_promo_code' do
      PaypalHandler.new(params, event).create_instant_payment
    end
  end
end
