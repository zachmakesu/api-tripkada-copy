module EventFilterHelper
  def filtered_events (collection, params)
    @collection      = collection
    @search          = params.fetch(:search){ "" }
    @sort            = params[:sort] == "desc"

    first_event       = Event.first_past_event.start_at.beginning_of_month
    @start_month      = params[:start_month].nil? ? first_event : Time.strptime(params[:start_month], "%m/%Y").beginning_of_month rescue first_event
    last_event        = Event.last_upcoming_event.start_at.end_of_month
    @end_month        = params[:end_month].nil? ? last_event : Time.strptime(params[:end_month], "%m/%Y").end_of_month rescue last_event

    first_event_date  = Event.first_past_event.start_at
    @start_date       = params[:start_date].nil? ? first_event_date : DateTime.strptime(params[:start_date], "%m/%d/%Y") rescue first_event
    last_event_date   = Event.last_upcoming_event.start_at
    @end_date         = params[:end_date].nil? ? last_event_date : DateTime.strptime(params[:end_date], "%m/%d/%Y") rescue last_event

    @min_cost        = params.fetch(:min_cost){ Event::MINCOST }.to_f
    @max_cost        = params.fetch(:max_cost){ Event::MAXCOST }.to_f
    @min_days        = params.fetch(:min_days){ Event::MINDAYS }.to_i
    @max_days        = params.fetch(:max_days){ Event::MAXDAYS }.to_i
    @category_ids    = params.fetch(:category_ids){ [] }
    @category_id     = params.fetch(:category_id, nil)
    @destination_ids = params.fetch(:destination_ids){ [] }
    @destination_id  = params.fetch(:destination_id, nil)
    @featured        = params.fetch(:featured){ }


    @collection = @collection.filtered_search(@search)
    @collection = @sort ? @collection.sorted_event : @collection.order_by_start
    @max_cost   = Event.maximum(:rate) + BookingFeeDecorator.latest_fee if @max_cost >= Event::MAXCOST
    @collection = @collection.cost(@min_cost, @max_cost)
    @collection = @collection.from_months(@start_month, @end_month)
    @collection = @collection.from_date(@start_date, @end_date)
    @max_days   = Event.maximum(:number_of_days) if @max_days >= Event::MAXDAYS
    @collection = @collection.days(@min_days, @max_days)
    @collection = @category_ids.any? ?  @collection.activity(@category_ids.map{ |n| n.to_i }.uniq.compact) : @collection
    @collection = @category_id ?  @collection.activity(@category_id) : @collection
    @collection = @destination_ids.any? ? @collection.common_places(@destination_ids.map{ |n| n.to_i }.uniq.compact) : @collection
    @collection = @destination_id ? @collection.common_places(@destination_id) : @collection
    @collection = @featured ? @collection.featured : @collection
    @collection.uniq
  end
end
