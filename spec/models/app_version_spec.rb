require 'rails_helper'

RSpec.describe AppVersion, type: :model do
  let(:app_version_params){
    {
      created_by: "#{Faker::Name.name}",
      version_code: "#{Faker::Lorem.word}",
      platform: Faker::Number.between(0, 1)
    }
  }
  let(:ios_joiner_app_version){ create(:ios_joiner_version) }

  context "with valid params" do
    it "should success" do
      app_version = described_class.new(app_version_params)
      expect{app_version.save}.to change{described_class.count}.from(0).to(1)
    end
  end
  context "with invalid params" do
    before(:each) do
      @invalid_params = app_version_params
    end
    it "should not be valid without creator" do
      @invalid_params[:created_by] = nil
      app_version = described_class.new(@invalid_params)
      expect(app_version).not_to be_valid
    end
    it "should not be valid without version_code" do
      @invalid_params[:version_code] = nil
      app_version = described_class.new(@invalid_params)
      expect(app_version).not_to be_valid
    end
    it "should not be valid without platform" do
      @invalid_params[:platform] = nil
      app_version = described_class.new(@invalid_params)
      expect(app_version).not_to be_valid
    end
  end

  context ".latest" do
    it "should return latest app_version" do
      latest_ios_joiner_app_version = ios_joiner_app_version
      expect(described_class.latest(platform: "ios", category: "joiner")).to eq(latest_ios_joiner_app_version)
    end
    it "should return nil platform or category is invalid" do
      expect(described_class.latest(platform: Faker::Lorem.word, category: Faker::Lorem.word)).to be_nil
    end
  end
end
