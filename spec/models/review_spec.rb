require 'rails_helper'

RSpec.describe Review, type: :model do

  let!(:current_user) { create(:user) }
  let!(:dummy_user)   { create(:user) }
  let!(:category)     { create(:category) }
  let!(:destination)     { create(:destination) }

  def event_params
    {
      name: Faker::Address.street_name,
      start_at: DateTime.now + 1.days,
      end_at: DateTime.now + 3.days,
      rate: 500,
      downpayment_rate: 100,
      min_pax: 5,
      max_pax: 10,
      things_to_bring: "bag, phone, etc.",
      inclusions_name: [Faker::StarWars.vehicle,Faker::StarWars.vehicle],
      highlights_name: [Faker::StarWars.droid,Faker::StarWars.droid],
      itineraries_description: [Faker::Address.street_address,Faker::Address.street_address],
      photos: {},
      category_ids: Category.ids,
      destination_ids: Destination.ids,
    }
  end

  context "when create review" do
    it "should not save even if reviewer is a member of event because event is ongoing" do
      EventHandler.new(current_user,event_params).create
      Event.last.received_reviews.create(reviewer: current_user,rating:2)

      expect(Event.last.received_reviews.count).to eq(0)
    end

    it "should not save if reviewer is a member of event" do
      EventHandler.new(dummy_user,event_params).create

      expect(Event.last.received_reviews.count).to eq(0)
    end
  end

end
