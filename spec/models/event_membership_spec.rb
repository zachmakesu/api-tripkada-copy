require 'rails_helper'

RSpec.describe EventMembership, type: :model do
  let!(:event){ create(:event) }
  let!(:user){ create(:user) }
  let(:promo_code){ create(:promo_code) }
  let(:booking_fee){ create(:booking_fee) }
  let(:payment_params){
    {
      mode:           "free_downpayment",
      made:           "downpayment",
      promo_code_id:  promo_code.id,
      booking_fee_id: booking_fee.id
    }
  }

  describe "Creating an event membership" do
    it "should successfully save" do
      event_membership = EventMembership.create(member_id: user.id, event_id: event.id)

      expect(event_membership.persisted?).to be_truthy
    end

    it "should not save if full slots" do
      event.update_attribute(:max_pax, 0)
      event_membership = EventMembership.create(member_id: user.id, event_id: event.id)

      expect(event_membership.persisted?).to be_falsey
      expect(event_membership.errors.full_messages.to_sentence).to eq("Too much members in specific event.")
    end

    it "exclude membership validation if event is draft" do
      event.update(max_pax: 0, published_at: nil)
      event_membership = EventMembership.create(member_id: user.id, event_id: event.id)

      expect(event_membership.persisted?).to be_truthy
    end

    it "should save if user's first booking has expired" do
      membership = EventMembership.pending.create!(member_id: user.id, event_id: event.id)
      payment = Payment.new(payment_params)
      payment.event_membership = membership
      payment.save!
      payment.expired!
      event_membership = EventMembership.pending.create(member_id: user.id, event_id: event.id)
      expect(event_membership.persisted?).to be_truthy
    end

    it "should not save if user is already a member" do
      EventMembership.create(member_id: user.id, event_id: event.id)
      event_membership = EventMembership.create(member_id: user.id, event_id: event.id)
      expect(event_membership.persisted?).to be_falsey
      expect(event_membership.errors.full_messages.to_sentence).to eq("Member has already been taken")
    end

    it "should not save if user params is not present" do
      event_membership = EventMembership.create(event_id: event.id)

      expect(event_membership.persisted?).to be_falsey
      expect(event_membership.errors.full_messages.to_sentence).to eq("Member can't be blank")
    end
  end

  describe "#mark_as_no_show!" do
    it "should successfully set no_show to true" do
      event_membership = EventMembership.create(member_id: user.id, event_id: event.id)
      expect(event_membership.mark_as_no_show!).to be_truthy
    end
  end

  describe ".approved_joiners_except_invitee" do
    it "returns joiner memberships except invitee" do
      # create joiner membership
      build_memberships(event: event, user: user, times: 3, params: payment_params)
      # create invitee membership
      invitee_params = payment_params
      invitee_params[:mode] = "through_invite"
      build_memberships(event: event, user: user, times: 3, params: invitee_params)
      joiner_memberships_count = event.event_memberships
                                      .approved_joiners_except_invitee.count
      expect(joiner_memberships_count).to eq(3)
    end
  end
end
