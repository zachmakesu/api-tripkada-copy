require 'rails_helper'

RSpec.describe User, type: :model do
  let(:credit_params){
    {
      code: Faker::Name.first_name,
      value: 0.0
    }
  }
  let!(:current_user)   { create(:user) }
  let(:event2){ build(:event, user_id: current_user.id) }
  let(:past_event2){ build(:past_event, user_id: current_user.id) }
  let(:user){ create(:user) }
  let(:event){ create(:event) }
  let(:past_event){ build(:past_event, user_id: user.id) }
  let(:joiner){ create(:joiner) }
  let(:booking_fee){ create(:booking_fee) }
  let(:promo_code){ create(:promo_code, value: 1000) }
  let(:payment_params){
    {
      mode:           "free_downpayment",
      made:           "downpayment",
      promo_code_id:  promo_code.id,
      booking_fee_id: booking_fee.id
    }
  }
  let(:review_params){
    {
      reviewer: joiner,
      rating:   Faker::Number.between(0, 5),
      message:  Faker::Lorem.word
    }
  }
  let(:device_params) {
    {
      name: "test",
      platform: "android",
      token: "#{SecureRandom.hex(8)}",
      udid: "#{SecureRandom.hex(8)}",
      role: "organizer"
    }
  }


  describe "credit_promo_code" do
    context "when generating credit promo code" do
      it "should create credit promo code associated to the user on create" do
        expect(user.credit_promo_code).not_to be_nil
      end
      it "should not create another credit promo code" do
        another_promo_code = user.promo_codes.credit.create(credit_params)
        expect(another_promo_code.errors.full_messages.to_sentence).to eq("Credit Promo Code Already Exist")
      end
      it "should only return credit promo code" do
        corporate_params = credit_params
        corporate_params[:category] = 0
        corporate_promo_code = user.promo_codes.create(corporate_params)
        expect(user.credit_promo_code).not_to eq(corporate_promo_code)
      end
    end
  end

  describe "total bookings" do
    it "total bookings not to include organizer" do
      expect(user.total_bookings).to eq 0
    end
  end

  describe "device used when creating trips" do
    it "get device from organizer" do
      user.devices.create!(device_params)
      user.devices.create!(
        name: "test",
        platform: "android",
        token: "#{SecureRandom.hex(8)}",
        udid: "#{SecureRandom.hex(8)}",
        role: "organizer"
      )
      expect(user.device_used_for_creating_trips).to eq ("android")
    end
  end

  describe "publish trips at" do
    it "get day when organizer post their trips" do
      event2.update(created_at: Time.zone.today)
      past_event2.update(created_at: Time.zone.today)
      expect(current_user.publish_trips_at).to eq Time.zone.today.strftime("%A")
    end
  end

  describe "#credit_code" do
    it "should return credit promo code" do
      expect(user.credit_code).not_to be_nil
    end
  end

  describe "#credit_value" do
    it "should return credit promo code" do
      expect(user.credit_value).not_to be_nil
    end
  end

  describe ".approved event joiner payments" do
    before(:each) do
      past_event.save(validate: false)
      membership = past_event.event_memberships.new(member_id: joiner.id)
      membership.save
      @valid_payment_params = payment_params
      @valid_payment_params[:event_membership_id] = membership.id
    end
    context "when fetching approved joiner payments" do
      it "should include payment to the list of approved joiner payments" do
        payment = Payment.approved.create(@valid_payment_params)
        expect(joiner.approved_event_joiner_payments).to include(payment)
      end
      it "should include payment to the list of approved joiner payments" do
        payment = Payment.declined.create(@valid_payment_params)
        expect(joiner.approved_event_joiner_payments).not_to include(payment)
      end
    end
  end

  describe ".approved past joined events" do
    before(:each) do
      past_event.save(validate: false)
      membership = past_event.event_memberships.new(member_id: joiner.id)
      membership.save
      @valid_payment_params = payment_params
      @valid_payment_params[:event_membership_id] = membership.id
    end
    context "when fetching approved past event" do
      it "should include joined event to the list of approved past joined events" do
        Payment.approved.create(@valid_payment_params)
        expect(joiner.approved_past_joined_events).to include(past_event)
      end
      it "should include joined event to the list of approved past joined events" do
        Payment.declined.create(@valid_payment_params)
        expect(joiner.approved_past_joined_events).not_to include(past_event)
      end
    end
  end

  describe "#reviewable events" do
    before(:each) do
      past_event.save(validate: false)
      membership = past_event.event_memberships.new(member_id: joiner.id)
      membership.save
      valid_payment_params = payment_params
      valid_payment_params[:event_membership_id] = membership.id
      Payment.approved.create(valid_payment_params)
    end
    context "when fetching reviewable events" do
      it "should include joined event organizer to the list of reviewable organizers" do
        expect(joiner.reviewable_events).to include(past_event)
      end
    end
    context "when organizer is already reviewed" do
      it "should not include joined event to the list of reviewable events" do
        user.received_reviews.as_joiner.create(review_params)
        expect(joiner.reviewable_events).not_to include(past_event)
      end
    end
  end

  describe "#reviewable_events_exclude_blacklist" do
    before(:each) do
      past_event.save(validate: false)
      membership = past_event.event_memberships.new(member_id: joiner.id)
      membership.save
      valid_payment_params = payment_params
      valid_payment_params[:event_membership_id] = membership.id
      Payment.approved.create(valid_payment_params)
    end
    context "when fetching excluded blacklist reviewable events" do
      it "should include joined event to the list of reviewable events" do
        expect(joiner.reviewable_events_exclude_blacklist).to include(past_event)
      end
      it "should not include joined event to the list of reviewable events" do
        joiner.review_blacklists.create(event_id: past_event.id)
        expect(joiner.reviewable_events_exclude_blacklist).not_to include(past_event)
      end
    end
  end

  describe ".lead" do
    context "when fetching users without memberships" do
      it "should include user in the list" do
        expect(described_class.lead).to include(joiner)
      end
      it "should not include user in the list" do
        past_event.save(validate: false)
        build_memberships(
          event: past_event, user: joiner, times: 1, params: payment_params
        )
        expect(described_class.lead).not_to include(joiner)
      end
    end
  end
  describe ".one_off" do
    before(:each) do
      # create membership
      past_event.save(validate: false)
      build_memberships(
        event: past_event, user: joiner, times: 1, params: payment_params
      )
      @query = described_class.one_off
    end
    context "when fetching users with only one membership" do
      it "should include user in the list" do
        expect(@query).to include(joiner)
      end
      it "should not include user in the list" do
        # create another membership
        build_memberships(
          event: event, user: joiner, times: 1, params: payment_params
        )
        expect(@query).not_to include(joiner)
      end
    end
  end
  describe ".repeat" do
    context "when fetching users with only two membership" do
      it "should include user in the list" do
        build_memberships(
          event: event, user: joiner, times: 2, params: payment_params
        )
        expect(described_class.repeat).to include(joiner)
      end
      it "should not include user in the list" do
        expect(described_class.repeat).not_to include(joiner)
      end
    end
  end
  describe ".loyal" do
    context "when fetching users with three or more membership" do
      it "should include user in the list" do
        build_memberships(
          event: event, user: joiner, times: 3, params: payment_params
        )
        expect(described_class.loyal).to include(joiner)
      end
      it "should not include user in the list" do
        expect(described_class.loyal).not_to include(joiner)
      end
    end
  end

  describe ".active" do
    context "when fetching users with last booking not more than 2 months" do
      it "should include user in the list" do
        # create new membership
        build_memberships(
          event: event, user: joiner, times: 1, params: payment_params
        )
        expect(described_class.active).to include(joiner)
      end
      it "should not include user in the list" do
        expect(described_class.active).not_to include(joiner)
      end
    end
  end
  describe ".at_risk" do
    context "when fetching users with last booking 2 months ago" do
      it "should include user in the list" do
        # create membership dated 2 months ago
        membership = event.event_memberships.new(
          member_id: joiner.id,
          created_at: 2.months.ago
        )
        membership.save(validate: false)
        valid_payment_params = payment_params
        valid_payment_params[:event_membership_id] = membership.id
        Payment.approved.create(valid_payment_params)

        #expect(described_class.at_risk).to include(joiner)
      end
      it "should not include user in the list" do
        # create new membership
        build_memberships(
          event: event, user: joiner, times: 1, params: payment_params
        )
        expect(described_class.at_risk).not_to include(joiner)
      end
    end
  end
  describe ".lapsed" do
    context "when fetching users with last booking 4 months ago or more" do
      it "should include user in the list" do
        # create membership dated 6 months ago
        membership = event.event_memberships.new(
          member_id: joiner.id,
          created_at: 6.months.ago
        )
        membership.save(validate: false)
        valid_payment_params = payment_params
        valid_payment_params[:event_membership_id] = membership.id
        Payment.approved.create(valid_payment_params)

        expect(described_class.lapsed).to include(joiner)
      end
      it "should not include user in the list" do
        # create new membership
        build_memberships(
          event: event, user: joiner, times: 1, params: payment_params
        )
        expect(described_class.lapsed).not_to include(joiner)
      end
    end
  end
  describe ".valid_event_joiners" do
    context "when fetching users with joiner memberships" do
      it "should include user in the list" do
        # create membership
        build_memberships(
          event: event, user: joiner, times: 1, params: payment_params
        )
        expect(described_class.valid_event_joiners).to include(joiner)
      end
      it "should not include user in the list" do
        # create new organizer mode membership
        membership = event.event_memberships.new(member_id: joiner.id)
        membership.save(validate: false)
        valid_payment_params = payment_params
        valid_payment_params[:event_membership_id] = membership.id
        valid_payment_params[:mode] = Payment.modes[:organizer]
        Payment.approved.create(valid_payment_params)
        expect(described_class.valid_event_joiners).not_to include(joiner)
      end
    end
  end
  describe "#apply_as_organizer!" do
    context "when updating applied_as_organizer_at column" do
      it "should success" do
        user.apply_as_organizer!
        expect(user.applied_as_organizer_at).to be_present
      end
    end
  end
  describe ".organizer_applicants" do
    context "when fetching users applying for organizer" do
      before(:each) { user.update(role: "joiner") }

      it "should include user" do
        user.apply_as_organizer!
        expect(User.organizer_applicants).to include(user)
      end
      it "should not include user" do
        expect(User.organizer_applicants).not_to include(user)
      end
    end
  end
  describe ".search" do
    context "when searching users in scope" do
      before(:each) { user.update(role: "joiner") }

      it "should include user" do
        user.apply_as_organizer!
        expect(User.search(collection: User.organizer_applicants)).to include(user)
      end
      it "should not include user" do
        expect(User.search(collection: User.organizer_applicants)).not_to include(user)
      end
    end
  end
  describe "#latest_device_platform" do
    context "when getting latest device platform" do
      it "should set web as default platform" do
        platform = user.latest_device_platform
        expect(platform).to eq("desktop")
      end
      it "should set platform as android" do
        user.devices.new(platform: "android").save(validate: false)
        platform = user.latest_device_platform
        expect(platform).to eq("android")
      end
      it "should set platform as ios" do
        user.devices.new(platform: "ios").save(validate: false)
        platform = user.latest_device_platform
        expect(platform).to eq("ios")
      end
    end
  end
end
