require 'rails_helper'

RSpec.describe TransactionFee, type: :model do
  describe "when validating transaction fee" do
    let!(:params) {
      {
        amount: Faker::Number.number(10),
        category: "dragonpay"
      }
    }
    context "with valid params" do
      it "it should be valid" do
        expect(described_class.new(params)).to be_valid
      end
    end
    context "with invalid params" do
      before(:each) { @invalid_params = params }
      it "it should be invalid if amount is empty" do
        @invalid_params[:amount] = nil
        expect(described_class.new(params)).not_to be_valid
      end
      it "it should raise ArgumentError if category is invalid" do
        @invalid_params[:category] = Faker::Lorem.word
        expect{ described_class.new(params) }.to raise_error(ArgumentError)
      end
    end
  end
  describe ".latest" do
    let!(:dragonpay_transaction_fee){ create(:dragonpay_transaction_fee) }
    let!(:uncategorized_transaction_fee){ create(:uncategorized_transaction_fee) }
    context "when featching latest transaction fee" do
      it "should return uncategorized transaction fee" do
        transaction_fee = described_class.latest(category: "uncategorized")
        expect(transaction_fee).to eq(uncategorized_transaction_fee)
      end
      it "should return dragonpay transaction fee" do
        transaction_fee = described_class.latest(category: "dragonpay")
        expect(transaction_fee).to eq(dragonpay_transaction_fee)
      end
      it "should return nil for invalid category" do
        transaction_fee = described_class.latest(category: Faker::Lorem.word)
        expect(transaction_fee).to be_nil
      end
    end
  end
end
