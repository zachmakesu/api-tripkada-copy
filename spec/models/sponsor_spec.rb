require 'rails_helper'

RSpec.describe Sponsor, type: :model do
  let(:event){ create(:event) }

  before :each do
    @valid_image = Rack::Test::UploadedFile.new("spec/fixtures/files/test.jpg", "image/jpeg")
    @invalid_image = Rack::Test::UploadedFile.new("spec/fixtures/files/sample.pdf", "application/pdf")
  end

  context "when creating sponsors" do
    before(:each) do
      @sponsor = described_class.new
      @sponsor.event_id = event.id
    end
    it "should be valid with valid photo" do
      @sponsor.image = @valid_image
      expect(@sponsor).to be_valid
    end

    it "should be invalid with invalid photo" do
      @sponsor.image = @invalid_image
      expect(@sponsor).not_to be_valid
    end

    it "should be invalid without event" do
      @sponsor.image = @valid_image
      @sponsor.event_id = nil
      expect(@sponsor).not_to be_valid
    end
  end
end
