require 'rails_helper'

RSpec.describe DragonpayTransactionLog, type: :model do
  let!(:current_user) { create(:user) }
  let!(:booking_fee)  { create(:booking_fee) }
  let!(:organizer)    { create(:user) }
  let!(:membership) do
    create(:event_membership, member: current_user,
    event: organizer.organized_events.last)
  end

  let!(:payment_two_days) do
    create(:payment, booking_fee: booking_fee,
    event_membership: membership)
  end

  let!(:payment_one_hour) do
    create(:payment_one_hour, booking_fee: booking_fee,
    event_membership: membership)
  end

  let!(:payment_five_minutes) do
    create(:payment_five_minutes,
    booking_fee: booking_fee, event_membership: membership)
  end

  let!(:two_days_params) do
    {
      payment_id: payment_two_days.id,
      transaction_id: "111111111111",
      status: "pending",
      response: { "procid" => "MLH" },
      reference_number: "1234567",
      message: "Waiting for payment",
      created_at: Time.zone.now - 2.days
    }
  end

  let!(:one_hour_params) do
    {
      payment_id: payment_one_hour.id,
      transaction_id: "111111111122",
      status: "pending",
      response: { "procid" => "BPI" },
      reference_number: "1234565",
      message: "Waiting for payment",
      created_at: Time.zone.now - 60.minutes
    }
  end

  let!(:five_minute_params) do
    {
      payment_id: payment_five_minutes.id,
      transaction_id: "111111111133",
      status: "pending",
      response: { "procid" => "GCSH" },
      reference_number: "1234565",
      message: "Waiting for payment",
      created_at: Time.zone.now - 5.minutes
    }
  end

  before(:each) do
    @two_days = DragonpayTransactionLog.create!(two_days_params)
    @one_hour = DragonpayTransactionLog.create!(one_hour_params)
    @five_minutes = DragonpayTransactionLog.create!(five_minute_params)
  end

  describe ".check_for_expired" do
    it "should mark expired for Payment.dragonpay TWO_DAY_DEADLINE" do
      pending = Payment.dragonpay.pending
      DragonpayTransactionLog.check_for_expired(payments: pending)
      expect(@two_days.payment.expired?).to be true
    end
    it "should mark expired for Payment.dragonpay ONE_HOUR_DEADLINE" do
      pending = Payment.dragonpay.pending
      DragonpayTransactionLog.check_for_expired(payments: pending)
      expect(@one_hour.payment.expired?).to be true
    end
    it "should mark expired for Payment.dragonpay FIVE_MINUTES_DEADLINE" do
      pending = Payment.dragonpay.pending
      DragonpayTransactionLog.check_for_expired(payments: pending)
      expect(@five_minutes.payment.expired?).to be true
    end
  end
end
