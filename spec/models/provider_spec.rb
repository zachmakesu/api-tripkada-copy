require 'rails_helper'

RSpec.describe Provider, type: :model do
  let!(:user) { create(:user) }

  it "is valid with valid attributes" do
    provider = Provider.new(role: 'joiner', provider: 'facebook')
    expect(provider.valid?).to be_truthy
  end

  it "is invalid without role" do
    provider = Provider.new(provider: 'facebook')
    expect(provider.valid?).to be_falsey
  end

  it "is invalid without provider" do
    provider = Provider.new(role: 'joiner')
    expect(provider.valid?).to be_falsey
  end

  it "is invalid user with same uid for providers in uniq user scope" do
    providers = Provider::PROVIDERS_WITH_UNIQ_USER
    Provider.create(role: 'joiner', provider: providers.last, user: user)
    provider = Provider.new(role: 'joiner', provider: providers.last, user: user)
    expect(provider.valid?).to be_falsey
  end

  it "is invalid to create organizer provider using joiner and facebook auth" do
    user.joiner!
    provider = Provider.new(role: 'organizer', provider: 'facebook', user: user)
    expect(provider.valid?).to be_falsey
  end

  it "is valid to create organizer provider using organizer and facebook auth" do
    provider = Provider.new(role: 'organizer', provider: 'facebook', user: user)
    expect(provider.valid?).to be_truthy
  end

  it "should be valid for viewing token if in providers with viewable token" do
    provider = Provider.new(role: 'joiner', provider: Provider::PROVIDERS_WITH_VIEWABLE_TOKEN.last, user: user)
    expect(provider.can_view_token?).to be_truthy
  end

  it "should be invalid for viewing token if not in providers with viewable token" do
    providers = Provider::PROVIDERS - Provider::PROVIDERS_WITH_VIEWABLE_TOKEN
    provider = Provider.new(role: 'joiner', provider: providers.last, user: user)
    expect(provider.can_view_token?).to be_falsey
  end
end
