require 'rails_helper'

RSpec.describe MailerliteGroup, type: :model do
  let(:valid_params){ 
    {
      name: Faker::Name.name,
      group_id: Faker::Number::number(6),
      category: "default"
    }
  }

  context "when creating mailerlite group" do
    it "should be valid" do
      expect(described_class.new(valid_params)).to be_valid
    end
    it "should be invalid without name" do
      invalid_params = valid_params
      invalid_params[:name] = nil
      expect(described_class.new(invalid_params)).not_to be_valid
    end
    it "should be invalid without group id" do
      invalid_params = valid_params
      invalid_params[:group_id] = nil
      expect(described_class.new(invalid_params)).not_to be_valid
    end
    it "should be invalid if category is invalid" do
      invalid_params = valid_params
      invalid_params[:category] = Faker::Name.name
      expect(described_class.new(invalid_params)).not_to be_valid
    end
  end
end
