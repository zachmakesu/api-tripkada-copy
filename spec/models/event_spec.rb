require 'rails_helper'

RSpec.describe Event, type: :model do
  let(:organizer) { create(:user) }
  let(:joiner) { create(:joiner) }
  let(:event){ create(:event, user_id: organizer.id) }
  let(:past_event){ build(:past_event, user_id: organizer.id) }
  let(:canceled_event){ build(:canceled_event, user_id: organizer.id) }
  let(:new_event){ FactoryGirl.build(:event) }
  let!(:booking_fee) { create(:booking_fee) }
  let!(:membership) do
    create(:event_membership, member: joiner,
    event: organizer.organized_events.last)
  end
  let!(:payment_two_days) do
    create(:payment, booking_fee: booking_fee,
    event_membership: membership)
  end

  describe "#soft_delete" do
    context "when deleting an event" do
      it "should update Event deleted_at column" do
        expect(event.soft_delete).to be_truthy
      end
    end
  end

  describe ".has_expired_membership_for" do
    it "return true if user.expired_membership matches wth event_expired_membership" do
      payment_two_days.expired!
      event = organizer.organized_events.last
      expect(event.has_expired_membership_for?(joiner)).to be_truthy
    end
  end

  describe ".search" do
    it "should include event in upcoming events" do
      expect(Event.upcoming.search(query: event.name)).to include(event)
    end
    it "should not include past events" do
      past = past_event.name
      expect(Event.upcoming.search(query: past)).not_to include(past_event)
    end
  end

  describe "Validate date range" do
    context "Save Event" do
      it "should successfully save event" do
        expect(event.save).to be_truthy
      end

      it "should not save event with invalid start_at" do
        event.start_at = 2.days.ago
        expect(event.save).to be false
      end

      it "should not save event with invalid end date" do
        event.start_at = 4.days.from_now
        event.end_at = 2.days.from_now
        expect(event.save).to be false
      end
    end
  end

  describe "Model Validations" do
    context "with valid params" do
      it "returns valid" do
        expect(new_event).to be_valid
      end

      it "returns max pax is greater than min pax" do
        expect(new_event.max_pax).to be > (event.min_pax)
      end

      it "returns rate is greater than downpayment rate" do
        expect(new_event.rate).to be > (event.downpayment_rate)
      end

      it "returns valid when rate is equal downpayment" do
        new_event.rate = 100
        new_event.downpayment_rate = 100
        expect(new_event).to be_valid
      end
    end

    context "with invalid params" do
      before do
        new_event.min_pax = 5
        new_event.max_pax = 2
        new_event.rate = 100
        new_event.downpayment_rate = 200
        new_event.min_pax = 0
        new_event.max_pax = 0
      end

      it "returns invalid" do
        expect(new_event).not_to be_valid
      end

      it "returns downpayment rate is greater than rate" do
        expect(new_event.downpayment_rate).to be > (new_event.rate)
      end

      it "returns max pax equal to 0" do
        expect(new_event.max_pax).to eq(0)
      end

      it "returns min pax equal to 0" do
        expect(new_event.min_pax).to eq(0)
      end
    end
  end

  describe "when updating event" do
    before { @approved_joiners_count = event.approved_event_memberships.count }
    context "when max pax is greater than or equal approved joiners count" do
      it "successfully updated" do
        expect(event.update(max_pax: @approved_joiners_count + 1, min_pax: 1))
          .to be_truthy
      end
    end
    context "when is max pax less than approved joiners count" do
      it "returns false response" do
        expect(event.update(max_pax: @approved_joiners_count - 1, min_pax: 1)).to be_falsey
      end
    end
  end

  describe "#organized_by?" do
    context "organizer validation" do
      it "should return true for valid organizer" do
        expect(event.organized_by?(user: organizer)).to be_truthy
      end

      it "should return false for invalid organizer" do
        expect(event.organized_by?(user: joiner)).to be_falsey
      end
    end
  end

  describe "#waitlisted?" do
    it "should return true if user is in the waitlist" do
      Waitlist.create(user_id: joiner.id, event_id: event.id)
      expect(event.waitlisted?(joiner)).to be_truthy
    end

    it "should return false if user is not in the waitlist" do
      expect(event.waitlisted?(joiner)).to be_falsey
    end

    it "should return false if user is a joiner of the event" do
      EventMembership.create(member_id: joiner.id, event_id: event.id)
      expect(event.waitlisted?(joiner)).to be_falsey
    end
  end

  describe ".past_not_deleted" do
    before(:each) do
      past_event.save(validate: false)
      canceled_event.save(validate: false)
      @past_not_deleted_event = Event.past_not_deleted
    end
    it "should not return canceled events" do
      expect(@past_not_deleted_event).not_to include(canceled_event)
    end
    it "should only return past events" do
      new_event.save(validate: false)
      expect(@past_not_deleted_event).not_to include(new_event)
    end
  end

  describe ".from_date" do
    before(:each) do
      past_event.save(validate: false)
    end

    it "should include event in the list" do
      query = described_class.from_date(DateTime.now - 10.days, DateTime.now - 5.days)
      expect(query).to include(past_event)
    end
    it "should not include event in the list" do
      query = described_class.from_date(DateTime.now - 2.days, DateTime.now - 1.days)
      expect(query).not_to include(past_event)
    end
  end

end
