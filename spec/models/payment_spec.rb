require 'rails_helper'

RSpec.describe Payment, type: :model do
  let!(:current_user) { create(:user) }
  let!(:organizer)    { create(:user) }
  let!(:booking_fee)  { create(:booking_fee) }
  let!(:promo_code)   { create(:promo_code) }
  let!(:image)        { Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg') }
  let!(:join_params) {{
                        member: current_user,
                        event: organizer.organized_events.last,
  }}

  let!(:payment_params) {{
                           promo_code_id: nil,
                           mode: 1,
                           made: 1,
                           booking_fee: booking_fee,
                           status: 1
  }}
  
  let!(:payment_with_promo_code_params) {{
    promo_code_id: current_user.credit_promo_code.id,
                           mode: 1,
                           made: 1,
                           booking_fee: booking_fee
  }}


  context "when creating payment" do
    before(:each) do
      @membership = EventMembership.create(join_params)
      @payment = Payment.new
      @payment.attributes = payment_params
    end

    it "should save if valid prerequisites" do
      @payment.event_membership = @membership
      @payment.payment_photos.build({image: image})
      expect{@payment.save}.to change{Payment.count}.from(0).to(1)
    end

    it "should not save if membership missing" do
      @payment.payment_photos.build()
      expect{@payment.save}.to_not change{Payment.count}
    end

    it "should not save if payment_photos missing" do
      @payment.event_membership = @membership
      expect{@payment.save}.to_not change{Payment.count}
    end
  end

  context "when creating payment with promo code" do
    before(:each) do
      @membership = EventMembership.create(join_params)
      @payment = Payment.new
      @payment.attributes = payment_with_promo_code_params
    end
    it "should create credit_usage_log for every use of credit_promo_code" do
      @payment.event_membership = @membership
      @payment.payment_photos.build({image: image})
      expect{@payment.save}.to change{@payment.promo_code.credit_logs.count}.from(0).to(1)
    end
    it "should not create credit_usage_log if promo code category is corporate" do
      @payment.event_membership = @membership
      @payment.payment_photos.build({image: image})
      @payment.promo_code_id = promo_code.id
      expect{@payment.save}.not_to change{@payment.promo_code.credit_logs.count}
    end
  end

  describe ".approved_joiner_payment" do
    before(:each) do
      @membership = EventMembership.create(join_params)
      @payment = described_class.new
    end
    it "should include payment to the list of approved joiner payment" do
      @payment.attributes = payment_params
      @payment.event_membership = @membership
      @payment.payment_photos.build({image: image})
      @payment.save
      expect(described_class.approved_joiner).to include(@payment)
    end
    it "should not include organizer payment to the list of approved joiner payment" do
      @payment.attributes = payment_params
      @payment.event_membership = @membership
      @payment.payment_photos.build({image: image})
      @payment.mode = described_class.modes[:organizer]
      @payment.save
      expect(described_class.approved_joiner).not_to include(@payment)
    end
    it "should not include declined payment to the list of approved joiner payment" do
      @payment.attributes = payment_params
      @payment.event_membership = @membership
      @payment.payment_photos.build({image: image})
      @payment.status = described_class.statuses[:declined]
      @payment.save
      expect(described_class.approved_joiner).not_to include(@payment)
    end
  end

end
