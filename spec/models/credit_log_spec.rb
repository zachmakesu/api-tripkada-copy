require 'rails_helper'

RSpec.describe CreditLog, type: :model do
  let(:referrer)              { create(:user) }
  let!(:current_user)         { create(:user) }
  let!(:booking_fee)          { create(:booking_fee) }
  let!(:promo_code)           { create(:promo_code) }
  let!(:event)                { build(:event) }
  let!(:image)        { 
    Rack::Test::UploadedFile.new(
      Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg'
    ) 
  }
  let!(:payment_with_promo_code_params) {{
    promo_code_id: current_user.credit_promo_code.id,
    mode: 1,
    made: 1,
    booking_fee: booking_fee
  }}
  describe "when creating credit log" do
    before(:each) do
      event.downpayment_rate = 500
      event.rate = 1000
      event.save
      @membership = EventMembership.create(
        event_id: event.id, member_id: current_user.id
      )
      @payment = Payment.new
      @payment.attributes = payment_with_promo_code_params
      booking_fee.update(value: 100)
    end
    context "credits deductions" do
      it "should create credit log with amount equal to deducted amount if credit is less than downpayment with fee "do
        current_user.credit_promo_code.credit_logs.earned_incentive.create(amount: 200)
        existing_credits = current_user.credit_promo_code.credit_logs.earned_incentive.sum(:amount)
        @payment.event_membership = @membership
        @payment.payment_photos.build({image: image})
        @payment.save
        
        credit_usage_amount = current_user.credit_promo_code.credit_logs.deduction.last.amount

        expect(credit_usage_amount).to eq(existing_credits)
      end
      it "should create credit log with amount equal to downpayment with fee if credit is greater than downpayment with fee but less than rate with fee"do
        current_user.credit_promo_code.credit_logs.earned_incentive.create(amount: 800)
        @payment.event_membership = @membership
        @payment.payment_photos.build({image: image})
        @payment.save
        
        credit_usage_amount = current_user.credit_promo_code.credit_logs.deduction.last.amount

        expect(credit_usage_amount).to eq(event.decorate.downpayment_rate_with_fee)
      end
      it "should create credit log with amount equal to rate with fee if credit is greater than or equal to rate with fee"do
        current_user.credit_promo_code.credit_logs.earned_incentive.create(amount: 1500)
        @payment.event_membership = @membership
        @payment.payment_photos.build({image: image})
        @payment.save
        
        credit_usage_amount = current_user.credit_promo_code.credit_logs.deduction.last.amount

        expect(credit_usage_amount).to eq(event.decorate.rate_with_fee)
      end
    end
    context "credits earned incentives" do
      it "should create credit log for earned incentives"do
        referrer.referrals.create(joiner_id: current_user.id) 
        incentive_log = Actions::User::EarnReferralIncentive.execute(referrer: referrer).incentive_log
        referrer_incentive_log = referrer.credit_promo_code.credit_logs.earned_incentive.last

        expect(incentive_log).to eq(referrer_incentive_log)
      end
    end
  end
end
