require 'rails_helper'

RSpec.describe ReviewBlacklist, type: :model do
  let(:event) { create(:event) }
  let(:user) { create(:user) }
  
  context "when creating review blacklist" do
    it "should successfully created" do
      blacklist = user.review_blacklists.new(event_id: event.id)
      expect{blacklist.save}.to change{described_class.count}.from(0).to(1)
    end
    it "should not successfully created" do
      blacklist = user.review_blacklists.new(event_id: "")
      expect{blacklist.save}.not_to change{described_class.count}
    end
    it "should be invalid if event is already in users review blacklist" do
      user.review_blacklists.create(event_id: event.id)
      blacklist = user.review_blacklists.new(event_id: event.id)
      expect(blacklist).not_to be_valid
    end
  end
end
