require 'rails_helper'

RSpec.describe Photo, type: :model do
  let(:event){ create(:event) }
  let(:user){ create(:user) }
  before :each do
    @valid_image = Rack::Test::UploadedFile.new("spec/fixtures/files/test.jpg", "image/jpeg")
    @invalid_image = Rack::Test::UploadedFile.new("spec/fixtures/files/200x200.png", "image/jpeg")
  end
  describe "#ensure_valid_dimensions" do
    context "validate photo dimensions" do
      it "should be valid with valid photo dimensions" do
        @photo = Photo.new
        @photo.image = @valid_image
        expect(@photo).to be_valid
      end

      it "should be invalid with invalid photo dimnsions" do
        @photo = Photo.new
        @photo.image = @invalid_image
        expect(@photo).not_to be_valid
      end
    end
  end
  describe "#primary_photo" do
    context "cover photo validation" do
      it "should not create two cover photos for event" do
        2.times do
          @event_photo = event.photos.new(category: "cover_photo",primary: true)
          @event_photo.image = @valid_image
        end
        event.photos.map(&:save)
        expect(event.photos.count).not_to eq(2)
      end
    end

    context "avatar photo validation" do
      it "should not create two avatar photos for user" do
        2.times do
          user.photos.create(category: "avatar", primary: true, image: @valid_image)
        end
        expect(user.photos.count).to eq(1)
      end
    end
  end
end
