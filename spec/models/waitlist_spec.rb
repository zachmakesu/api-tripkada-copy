require 'rails_helper'

RSpec.describe Waitlist, type: :model do
  let!(:event){ create(:event) }
  let!(:user){ create(:user) }
  let(:waitlist_params){
    {
     user_id: user.id,
     event_id: event.id
    }
  }

  describe "Joining the waitlist" do
    it "should successfully save waitlist" do
      waitlist = Waitlist.new(waitlist_params)
      expect(waitlist.save).to be_truthy
    end

    it "should not save if user is already in the waitlist" do
      Waitlist.create(waitlist_params)
      user_waitlist = Waitlist.new(waitlist_params)
      expect(user_waitlist.save).to be_falsey
    end

    it "should not save if empty user" do
      waitlist = Waitlist.create(user_id: user.id)
      expect(waitlist.errors.full_messages.to_sentence).to eq("Event can't be blank")
    end

    it "should not save if empty event" do
      waitlist = Waitlist.create(event_id: event.id)
      expect(waitlist.errors.full_messages.to_sentence).to eq("User can't be blank")
    end
  end
end
