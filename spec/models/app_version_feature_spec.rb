require 'rails_helper'

RSpec.describe AppVersionFeature, type: :model do
  let(:app_version_features_params){
    {
      title: "#{Faker::Name.name}",
      details: "#{Faker::Lorem.paragraph}",
    }
  }
  context "with valid params" do
    it "should success" do
      app_version = described_class.new(app_version_features_params)
      expect{app_version.save}.to change{AppVersionFeature.count}.from(0).to(1)
    end
  end
  context "with invalid params" do
    before(:each) do
      @invalid_params = app_version_features_params
    end
    it "should not be valid without title" do
      @invalid_params[:title] = nil
      app_version = described_class.new(@invalid_params)
      expect(app_version).not_to be_valid
    end
    it "should not be valid without details" do
      @invalid_params[:details] = nil
      app_version = described_class.new(@invalid_params)
      expect(app_version).not_to be_valid
    end
  end
end
