require 'rails_helper'

RSpec.describe PromoCode, type: :model do
  let!(:promo_code) { create(:promo_code) }
  let!(:user) { create(:user) }
  let!(:joiner) { create(:joiner) }
  let!(:booking_fee) { create(:booking_fee) }
  let!(:event) { create(:event) }
  let!(:valid_params){
    {
      user_id:      user.id,
      code:         Faker::Code.ean,
      value:        0.0,
      category:     "credit",
      usage_limit:  999
    }
  }
  let!(:payment_params){
    {
      event_membership_id: Faker::Number.number(2).to_i,
      mode: "free_trip",
      made: "downpayment",
      booking_fee_id: booking_fee.id,
      status: 1,
      promo_code_id: promo_code.id,
      device_platform: 0
    }
  }
  let!(:payment_with_promo_code_params) {{
    promo_code_id: user.credit_promo_code.id,
    mode: 1,
    made: 1,
    booking_fee: booking_fee
  }}
  let!(:image)        { 
    Rack::Test::UploadedFile.new(
      Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg'
    ) 
  }

  describe "Delete" do
    context "when deleting a promo code" do
      it "should destroy PromoCode" do
        expect(promo_code.destroy).to be_truthy
      end
    end
  end

  describe "Validate PromoCode" do
    context "Save PromoCode" do
      it "should successfully save promo code" do
        expect(promo_code.save).to be_truthy
      end

      it "should not save promo code with blank code" do
        promo_code.code = ''
        expect(promo_code.save).to be false
      end

      it "should not save promo code with blank value" do
        promo_code.value = ''
        expect(promo_code.save).to be false
      end

      it "should not save promo code with blank usage_limit" do
        promo_code.usage_limit = ''
        expect(promo_code.save).to be false
      end
    end

    it "should not be case sentitive when searching for the promo_code" do
      service = PromoCode::ValidateByCategory.call(code: promo_code.code, user: user)
      expect(service.success?).to be_truthy
    end
  end

  describe "Credit Promo Code" do
    context "when creating promo code for user's credits" do
      it "should not create credit promo code if it already exist" do
        PromoCode.create(valid_params)
        existing_promo_code = user.promo_codes.create(valid_params)
        expect(existing_promo_code.errors.full_messages).to include(
          "Credit Promo Code Already Exist"
        )
      end
    end
  end

  describe ".usable?" do
    context "when validating corporate promo code" do
      it "should return valid response for usable promo code" do
        validator = promo_code.usable?
        expect(validator).to be_truthy
      end
      it "should return false response when promo code exceeds usage limit" do
        promo_code.update(usage_limit: 1)
        membership = EventMembership.create(
          event_id: event.id, member_id: user.id
        )        
        payment = membership.build_payment(payment_params)
        payment.save!

        validator = promo_code.usable?
        expect(validator).to be_falsey
      end
    end
  end
  describe ".belongs_to_owner?" do
    context "when using credit and private_use promo code" do
      it "should return valid response for valid promo code" do
        joiner_credit_promo_code = joiner.credit_promo_code
        validator = joiner_credit_promo_code.belongs_to_owner?(user: joiner)
        expect(validator).to be_truthy
      end
      it "should return false response for invalid promo code" do
        private_use_promo_code = user.build_private_use_promo_code(
          code: PromoCode.generate_unique_code, usage_limit: 10, value: 100
        )
        private_use_promo_code.save!

        validator = private_use_promo_code.belongs_to_owner?(user: joiner)
        expect(validator).to be_falsey
      end
    end
  end
  describe "#available_credits"do
    context "when validating available credits" do
      before(:each) do
        event.downpayment_rate = 500
        event.rate = 1000
        event.save
        @membership = EventMembership.create(
          event_id: event.id, member_id: user.id
        )
        @payment = Payment.new
        @payment.attributes = payment_with_promo_code_params
        booking_fee.update(value: 100)
      end
      it "ensure that used credits are duducted" do
        user.credit_promo_code.credit_logs.earned_incentive.create(amount: 1500)
        @payment.event_membership = @membership
        @payment.payment_photos.build({image: image})
        @payment.save
        user.credit_promo_code.reload
        used_credits = user.credit_promo_code.credit_logs.deduction.sum(:amount)
        incentives = user.credit_promo_code.credit_logs.earned_incentive.sum(:amount)
        calculated_available_credits = incentives - used_credits

        expect(user.credit_promo_code.available_credits).to eq(calculated_available_credits)
      end
    end
  end
end
