require 'rails_helper'

RSpec.describe NotificationsController, type: :controller do

  let(:current_user) { create(:user) }
  let(:valid_session) { {} }

  before(:each) do
    sign_in :user, current_user # sign_in(scope, resource)
    @notification = FactoryGirl.create(:notification, user_id: current_user.id)
  end

  describe "GET #index" do
    it "returns all users notification as @notifications" do
      get :index
      expect(assigns(:notifications)).to eq([@notification])
    end
  end

  describe "GET #show" do
    it "returns the requested notification as @notification" do
      get :show, {:id => @notification.id}
      expect(assigns(:notification)).to eq(@notification)
    end
  end

  describe "GET #count_users_unseen_notifications" do
    it "count users unseen notifications and return it to count_unseen_notifications" do
      count_unseen_notifications = current_user.notifications.not_seen.count
      get :count_users_unseen_notifications
      expect(assigns(:count_unseen_notifications)).to eq(count_unseen_notifications)
    end
  end
end
