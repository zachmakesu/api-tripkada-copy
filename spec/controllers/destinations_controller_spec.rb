require 'rails_helper'

RSpec.describe DestinationsController, type: :controller do
  # This should return the minimal set of attributes required to create a valid
  # Destination. As you add validations to Destination, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    {   
      name: Faker::Address.city,
      active: true
    }
  }

  let(:invalid_attributes) {
    {   
      name: nil,
      active: true
    }
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # DestinationsController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  let(:admin) { create(:user, role: "admin") }

  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:admin]
    sign_in :user, admin # sign_in(scope, resource)
  end

  describe "GET #index" do
    it "assigns all destinations as @destinations" do
      destinations = Destination.all
      get :index, {}, valid_session
      expect(assigns(:destinations).map{|e| e }).to eq(destinations.map{ |e| e })
    end
  end

  describe "GET #show" do
    it "assigns the requested destination as @destination" do
      destination = Destination.create! valid_attributes
      get :show, {:id => destination.to_param}, valid_session
      expect(assigns(:destination)).to eq(destination)
    end
  end

  describe "GET #new" do
    it "assigns a new destination as @destination" do
      get :new, {}, valid_session
      expect(assigns(:destination)).to be_a_new(Destination)
    end
  end

  describe "GET #edit" do
    it "assigns the requested destination as @destination" do
      destination = Destination.create! valid_attributes
      get :edit, {:id => destination.to_param}, valid_session
      expect(assigns(:destination)).to eq(destination)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Destination" do
        expect {
          post :create, {:destination => valid_attributes}, valid_session
        }.to change(Destination, :count).by(1)
      end

      it "assigns a newly created destination as @destination" do
        post :create, {:destination => valid_attributes}, valid_session
        expect(assigns(:destination)).to be_a(Destination)
        expect(assigns(:destination)).to be_persisted
      end

      it "redirects to the created destination" do
        post :create, {:destination => valid_attributes}, valid_session
        expect(response).to redirect_to(Destination.last)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved destination as @destination" do
        post :create, {:destination => invalid_attributes}, valid_session
        expect(assigns(:destination)).to be_a_new(Destination)
      end

      it "re-renders the 'new' template" do
        post :create, {:destination => invalid_attributes}, valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {   
          code: Faker::Address.city,
          active: false
        }
      }

      it "updates the requested destination" do
        destination = Destination.create! valid_attributes
        put :update, {:id => destination.to_param, :destination => new_attributes}, valid_session
        destination.reload
        expect(assigns(:destination)).to eq(destination)
      end

      it "assigns the requested destination as @destination" do
        destination = Destination.create! valid_attributes
        put :update, {:id => destination.to_param, :destination => valid_attributes}, valid_session
        expect(assigns(:destination)).to eq(destination)
      end

      it "redirects to the destination" do
        destination = Destination.create! valid_attributes
        put :update, {:id => destination.to_param, :destination => valid_attributes}, valid_session
        expect(response).to redirect_to(destination)
      end
    end

    context "with invalid params" do
      it "assigns the destination as @destination" do
        destination = Destination.create! valid_attributes
        put :update, {:id => destination.to_param, :destination => invalid_attributes}, valid_session
        expect(assigns(:destination)).to eq(destination)
      end

      it "re-renders the 'edit' template" do
        destination = Destination.create! valid_attributes
        put :update, {:id => destination.to_param, :destination => invalid_attributes}, valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested destination" do
      destination = Destination.create! valid_attributes
      expect {
        delete :destroy, {:id => destination.to_param}, valid_session
      }.to change(Destination, :count).by(-1)
    end

    it "redirects to the destinations list" do
      destination = Destination.create! valid_attributes
      delete :destroy, {:id => destination.to_param}, valid_session
      expect(response).to redirect_to(destinations_url)
    end
  end
end
