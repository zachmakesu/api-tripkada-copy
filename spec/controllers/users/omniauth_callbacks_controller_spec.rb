require 'rails_helper'

RSpec.describe Users::OmniauthCallbacksController, type: :controller do
  include Rails.application.routes.url_helpers

  let!(:event) { create(:event) }

  class HandlerTest
    attr_accessor :response

    def initialize
      @response = Hash.new 
    end

    def success_login
      @response = {
        failed_facebook_auth: false,
        details: { uid: User.last.uid }
      }
      self
    end

    def user_not_found
      @response = {
        failed_facebook_auth: false,
        details: { uid: 99999999 }
      }
      self
    end

    def failed_facebook_auth
      @response = {
        failed_facebook_auth: true
      }
      self
    end
  end


  context "logged in via facebook" do
    before(:each) do
      create(:user)
      OmniAuth.config.test_mode = true
      OmniAuth.config.mock_auth[:facebook] = OmniAuth::AuthHash.new({
        :provider => 'facebook',
        :uid => '123545'
      })
      OmniAuth.config.add_mock(:facebook, { credentials: { token: '' }})
      request.env["devise.mapping"] = Devise.mappings[:user] # If using Devise
      request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:facebook] 
      request.env["omniauth.params"] = { 'url' => trip_view_path(event), 'role' => 'joiner' }
      request.env['HTTP_REFERER'] = trip_view_path(event)
    end

    it "should return successful with valid credentials" do
      allow_any_instance_of(FacebookSessionHandler).to receive(:find_or_create).and_return(HandlerTest.new.success_login)
      get :facebook

      expect(response).to redirect_to(trip_view_path(event))
      expect(session[:app_role]).to eq('joiner')
      expect(flash[:notice]).to be_truthy
    end

    it "should return false for user not found" do
      allow_any_instance_of(FacebookSessionHandler).to receive(:find_or_create).and_return(HandlerTest.new.user_not_found)
      get :facebook

      expect(response).to redirect_to(root_path)
      expect(flash[:alert]).to be_truthy
      expect(flash[:alert]).to eq('User account not found.')
    end

    it "should return false for invalid facebook authentication from facebook session handler" do
      allow_any_instance_of(FacebookSessionHandler).to receive(:find_or_create).and_return(HandlerTest.new.failed_facebook_auth)
      get :facebook

      expect(response).to redirect_to(trip_view_path(event))
      expect(flash[:alert]).to be_truthy
      expect(flash[:alert]).to eq('Could not authenticate you from Facebook')
    end
  end

end
