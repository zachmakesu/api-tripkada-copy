require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  let(:current_user){ create(:user, email: "testuser@tripkada.com") }
  let(:valid_session) { {} }

  let(:user_params){
    {
      fname: current_user.first_name,
      lname: current_user.last_name,
      number: current_user.mobile,
      email: current_user.email
    }
  }

  before(:each) do
    request.env["HTTP_REFERER"] = "/"
  end

  describe "POST #create_requests" do
    context "with valid email" do
      it "successfully sends request" do
        post :create_requests, user_params, valid_session
        expect(controller).to set_flash[:notice]
      end
    end

    context "with invalid email" do
      before(:each) { current_user.update(email: "123@facebook.com") }
      it "doesn't send request" do
        post :create_requests, user_params, valid_session
        expect(controller).to set_flash[:alert]
      end
    end
  end

  describe "POST #create_questions" do
    context "with valid email" do
      it "successfully sends question" do
        post :create_questions, user_params, valid_session
        expect(controller).to set_flash[:notice]
      end
    end

    context "with invalid email" do
      before(:each) { current_user.update(email: "123@facebook.com") }
      it "doesn't send question" do
        post :create_questions, user_params, valid_session
        expect(controller).to set_flash[:alert]
      end
    end
  end

  describe "POST #create_form_organiser" do
    before(:each) { allow(controller).to receive(:current_user) { current_user } }

    context "with valid email" do
      it "successfully sends request" do
        post :create_form_organiser, user_params, valid_session
        expect(current_user.applied_as_organizer_at).to be_present
        expect(response).to redirect_to(organizer_landing_page_path)
        expect(controller).to set_flash[:notice]
      end
    end

    context "with invalid email" do
      before(:each) do
        current_user.update(email: "123@facebook.com")
        request.env["HTTP_REFERER"] = be_a_trip_organizer_path
      end

      it "doesn't send request" do
        post :create_form_organiser, user_params, valid_session
        expect(current_user.applied_as_organizer_at).to be_nil
        expect(response).to redirect_to(be_a_trip_organizer_path)
        expect(controller).to set_flash[:alert]
      end
    end
  end
end
