require 'rails_helper'

RSpec.describe ManageTripsController, type: :controller do
  let(:valid_session) { {} }

  let(:organizer) { FactoryGirl.create(:user) }

  before(:each) do
    sign_in :user, organizer # sign_in(scope, resource)
  end

  describe "PUT #cancel_trip" do
    it "cancel upcoming trip" do
      @event = FactoryGirl.create(:event)
      params = {}
      params[:reason] = "Weather Condition"
      params[:slug] = @event.slug

      put :cancel_trip, params, valid_session
      @event.reload
      expect(@event.trip_cancel_reason).to eq(params[:reason])
    end
  end
end
