require 'rails_helper'

RSpec.describe InstagramController, type: :controller do

  let(:current_user) { FactoryGirl.create(:user) }
  let(:instagram_provider_params) {
    {
      role: "joiner",
      uid: current_user.id,
    }
  }
  before(:each) do
    sign_in :user, current_user # sign_in(scope, resource)
  end

  describe "GET #callback" do
    it "should redirect to users profile path" do
      VCR.use_cassette '/controllers/instagram_controller' do
        get :callback, {code: "f37e888e2a674334ba94cea23e6ff7bb"}, {}
      end
      instagram_provider = current_user.providers.instagram.new(instagram_provider_params)
      expect(instagram_provider.save).to be_truthy
      expect(subject).to redirect_to(profile_path(current_user.slug))
    end


  end
end
