require 'rails_helper'

describe PaypalHandler do
  let(:organizer){ FactoryGirl.create(:user) }

  let!(:event){ create(:event, user_id: organizer.id) }

  let(:joiner){ FactoryGirl.create(:joiner) }

  let(:booking_fee){ FactoryGirl.create(:booking_fee) }

  let(:mod_event){ FactoryGirl.create(:mod_event, user_id: organizer.id) }

  let(:incentive) { create(:referral_incentive) }

  let(:promo_code) { create(:promo_code) }

  let(:credit_promo_code_params){
    {
      code: SecureRandom.hex(6),
      value: 0.0,
      usage_limit: 999,
      category: "credit"
    }
  }

  let(:referral_params){
    {
      joiner_id: joiner.id
    }
  }

  let(:express_payment_params){
    {
      payment_id: 1,
      payer_id: "WG9CJ3EHLRP84",
      payment_tracking_id: "PAY-5FJ5931086547184FLETIMUA",
      payment_status: "approved"
    }
  }

  let(:paypal_payment_params){
    {
      user_id: joiner.id
    }
  }

  let(:join_event_params){
    {
      user_id: joiner.id,
      mode: "paypal",
      made: "downpayment",
      from_controller: true,
      paymentId: "PAY-3EL12227HG063932KLETIAWA",
      PayerID: "WG9CJ3EHLRP84",
      platform: "desktop"
    }
  }

  let(:join_event_params_invalid){
    {
      user_id: joiner.id,
      mode: "paypal",
      made: "downpayment",
      express_payment: true,
      paymentId: "PAY-9NC464703Y063525MLETHV5Q",
      PayerID: "",
    }
  }

  let(:api_valid_payment_details){
    {
      paymentId: "PAY-5FJ5931086547184FLETIMUA",
      PayerID: "WG9CJ3EHLRP84"
    }
  }

  let(:api_valid_payment_details_invalid){
    {
      paymentId: "PAY-4XV22790L5888553MLESZKJY",
      PayerID: "WG9CJ3EHLRPdasdasd",
    }
  }

  let(:api_valid_payment_with_fee){
    {
      paymentId: "PAY-5AE56918FJ7264347LFUG5QA",
      PayerID: "WG9CJ3EHLRP84",
      user_id: joiner.id,
      platform: "desktop"
    }
  }

  let(:invited_joiner_params){
    [
      {
        name: "Roldan Cruz",
        email: "testemail_roldan@hotmail.com"
      },
      {
        name: "Juan Tamad",
        email: "testemail_juan@hotmail.com"
      }
    ]
  }

  let(:invited_joiner_names){
    [
      Faker::Name.name,
      Faker::Name.name
    ]
  }

  let(:invited_joiner_emails){
    [
      Faker::Internet.email,
      Faker::Internet.email
    ]
  }

  let(:payment_details_with_invited_joiners){
    {
      paymentId: "PAY-0W817972LP709992FLGQU4PQ",
      PayerID: "WG9CJ3EHLRP84",
      user_id: joiner.id,
      platform: "desktop"
    }
  }

  let(:payment_details_with_invited_joiners_unparsed){
    {
      paymentId: "PAY-74Y249780X144653ALG5W6RY",
      PayerID: "WG9CJ3EHLRP84",
      user_id: joiner.id,
      platform: "desktop"
    }
  }

  let(:payment_details_with_waitlist){
    {
      paymentId: "PAY-69T26413TD687535LLHBF3HQ",
      PayerID: "WG9CJ3EHLRP84",
      user_id: joiner.id,
      platform: "desktop"
    }
  }

  let(:waitlist_params){
    {
      user_id: joiner.id,
      event_id: event.id,
      status: Waitlist.statuses[:pending]
    }
  }

   describe "#create_instant_payment" do
    context "Initialize Payment" do
      it "successfully initialize payment with valid params" do
        handler = initialize_payment(paypal_payment_params, event)
        expect(handler.response[:type]).to be_truthy
      end

      it "should not initialize payment with invalid params" do
        trip = event
        trip.update(downpayment_rate: 0)
        handler = initialize_payment_invalid(paypal_payment_params, "")
        expect(handler.response[:type]).to be_falsey
      end
    end

    context "when rate is equal to downpayment and promo code is present" do
      it "should less promo code value in total amount" do
        valid_params = paypal_payment_params
        promo_code.update(value: 100)
        valid_params[:code] = promo_code.code
        default_rate = 500
        booking_fee.update(value: 100)
        event.update(rate: default_rate, downpayment_rate: default_rate)
        rate_with_booking_fee  = default_rate + booking_fee.value
        handler = initialize_payment_with_promo_code(valid_params, event)
        response_amount_with_fee = handler.response[:details]
                                          .transactions.last.amount.total

        fixed_rate = PaypalHandler::PAYPAL_FIXED_RATE
        transaction_rate = PaypalHandler::PAYPAL_TRANSACTION_RATE
        rate_less_promo_code = rate_with_booking_fee - promo_code.value
        with_fixed_rate = (rate_less_promo_code + fixed_rate) 
        total = with_fixed_rate / (1 - transaction_rate)

        expect(response_amount_with_fee.to_f).to eq(total.round(2))
      end
    end

    context "when rate is not equal to downpayment and promo code is not present" do
      it "should not less promo code value in total amount" do
        promo_code.update(value: 100)
        booking_fee.update(value: 100)
        event.update(rate: 500, downpayment_rate: 100)
        downpayment_with_fee = event.decorate.downpayment_rate_with_fee
        handler = initialize_payment_without_promo_code(
          paypal_payment_params, event
        )
        response_amount_with_fee = handler.response[:details].transactions
                                          .last.amount.total

        fixed_rate = PaypalHandler::PAYPAL_FIXED_RATE
        transaction_rate = PaypalHandler::PAYPAL_TRANSACTION_RATE
        with_fixed_rate = (downpayment_with_fee + fixed_rate) 
        total = with_fixed_rate / (1 - transaction_rate)

        expect(response_amount_with_fee.to_f).to eq(total.round(2))
      end
    end
  end

  describe "join event through paypal transaction" do

    before(:each) do
      booking_fee
    end

    context "when joining event through paypal" do
      it "successfully join event after success paypal transaction" do
        join_event_through_paypal_valid(join_event_params, event)
        expect(event.event_memberships.count).to eq(1)
      end
      it "should not join event if paypal transaction is unsuccessfull" do
        join_event_through_paypal_invalid(join_event_params_invalid, event)
        expect(event.event_memberships).to be_blank
      end
    end

    context "updating waitlist status if success paypal transaction" do
      it "Updates waitlist status to paid" do
        waitlist_params[:status] = Waitlist.statuses[:approved]
        Waitlist.create(waitlist_params)
        initialize_payment_with_waitlist(paypal_payment_params, event)
        execute_payment_with_waitlist(payment_details_with_waitlist, event)

        expect(event.event_memberships.count).to eq(1)
        expect(joiner.waitlist_for(event).status).to eq("paid")
      end

      it "should not join event if waitlist is denied" do
        waitlist_params[:status] = Waitlist.statuses[:denied]
        Waitlist.create(waitlist_params)
        initialize_payment_with_waitlist(paypal_payment_params, event)
        execute_payment_with_waitlist(payment_details_with_waitlist, event)

        expect(event.event_memberships).to be_blank
        expect(joiner.waitlist_for(event).status).to eq("denied")
      end
    end
  end

  describe "#validate_payment" do
    context "Validate paypal tracking id" do
      it "should join successfully to event with valid tracking id" do
        initialize_api_payment(paypal_payment_params, mod_event)
        execute = execute_api_payment(api_valid_payment_details)

        booking_fee
        params = {}
        params[:user_id] = joiner.id

        params[:paymentId] = execute.id
        params[:platform] = "desktop"

        validate = validate_api_payment(params, mod_event)

        expect(validate.response[:type]).to be_truthy
      end

      it "should avoid joining of user for malformed/invalid tracking id" do
        initialize_api_payment(paypal_payment_params, mod_event)
        execute = execute_api_payment(api_valid_payment_details)
        booking_fee
        params = {}
        params[:user_id] = joiner.id
        params[:paymentId] = execute.id

        validate = validate_api_payment(params, event)

        expect(validate.response[:type]).to be_falsey
      end
    end

    context "validate paypal payment errors" do
      it "error should include Payment is already used" do
        initialize_api_payment(paypal_payment_params, mod_event)
        execute = execute_api_payment(api_valid_payment_details)
        booking_fee
        params = {}
        params[:user_id] = joiner.id
        params[:paymentId] = execute.id
        ExpressPayment.create(express_payment_params)
        validate = validate_api_payment(params, mod_event)

        expect(validate.response[:details]).to include("Payment is already used")
      end

      it "error should include Payment item doesn't match the event" do
        initialize_api_payment(paypal_payment_params, mod_event)
        execute = execute_api_payment(api_valid_payment_details)
        booking_fee
        params = {}
        params[:user_id] = joiner.id
        params[:paymentId] = execute.id

        validate = validate_api_payment(params, event)

        expect(validate.response[:details]).to include("Payment item doesn't match the event")
      end

      it "error should include Payment state is not valid" do
        initialize_payment = initialize_api_payment_once(paypal_payment_params, mod_event)

        booking_fee
        params = {}
        params[:user_id] = joiner.id
        params[:paymentId] = initialize_payment.response[:details].id
        params[:platform] = "desktop"

        validate = validate_api_payment_invalid(params, mod_event)

        expect(validate.response[:details]).to include("Payment state is not valid")
      end
    end
  end

  context "payment with transaction fee" do
    before(:each) do
      booking_fee.update(value: 100)
    end

    it "add transaction_fee for each transactions" do
      initialize_payment_with_transaction_fee(paypal_payment_params, event)

      execute = execute_payment_with_transaction_fee(api_valid_payment_with_fee, event)

      fixed_rate = PaypalHandler::PAYPAL_FIXED_RATE
      transaction_rate = PaypalHandler::PAYPAL_TRANSACTION_RATE

      response_amount_with_fee = execute.response[:details].transactions.first.item_list.items.first.price
      actual_amount_with_fee = (event.decorate.downpayment_rate_with_fee + fixed_rate) / (1 - transaction_rate)

      expect(response_amount_with_fee.to_f).to eq(actual_amount_with_fee.round(2))
    end

    it "equals to the expected amount to be received" do
      initialize_payment = initialize_payment_with_transaction_fee(paypal_payment_params, event)
      execute = execute_payment_with_transaction_fee(api_valid_payment_with_fee, event)

      fixed_rate = PaypalHandler::PAYPAL_FIXED_RATE
      transaction_rate = PaypalHandler::PAYPAL_TRANSACTION_RATE

      response_amount_with_fee = execute.response[:details].transactions.first.item_list.items.first.price
      actual_fee = (response_amount_with_fee.to_f * transaction_rate) + fixed_rate
      net = response_amount_with_fee.to_f - actual_fee

      expect(net.round(2)).to eq(event.decorate.downpayment_rate_with_fee)
    end
  end

  context "payment with invited joiners" do
    before(:each) do
      booking_fee.update(value: 100)
    end

    it "should include invited joiners count in amount calculation" do
      mod_params = paypal_payment_params
      mod_params[:invited_joiners] = invited_joiner_params
      initialize_payment_with_invited_joiners(mod_params, event)

      execute = execute_payment_with_invited_joiners(payment_details_with_invited_joiners, event)
      fixed_rate = PaypalHandler::PAYPAL_FIXED_RATE
      transaction_rate = PaypalHandler::PAYPAL_TRANSACTION_RATE

      joiners = invited_joiner_params.count
      response_amount_with_fee = execute.response[:details].transactions.first.item_list.items.first.price
      total = (joiners + 1) * event.decorate.downpayment_rate_with_fee

      actual_amount_with_fee = (total + fixed_rate) / (1 - transaction_rate)

      expect(response_amount_with_fee.to_f).to eq(actual_amount_with_fee.round(2))
    end

    it "should include invited joiners count in amount calculation before data is parse" do
      mod_params = paypal_payment_params
      mod_params[:invited_joiner_names] = invited_joiner_names
      mod_params[:invited_joiner_emails] = invited_joiner_emails
      initialize_payment_with_invited_joiners_unparsed(mod_params, event)

      execute = execute_payment_with_invited_joiners_unparsed(payment_details_with_invited_joiners_unparsed, event)
      fixed_rate = PaypalHandler::PAYPAL_FIXED_RATE
      transaction_rate = PaypalHandler::PAYPAL_TRANSACTION_RATE

      joiners = mod_params[:invited_joiner_names].split(',').flatten.count
      response_amount_with_fee = execute.response[:details].transactions.first.item_list.items.first.price
      total = (joiners + 1) * event.decorate.downpayment_rate_with_fee

      actual_amount_with_fee = (total + fixed_rate) / (1 - transaction_rate)

      expect(response_amount_with_fee.to_f).to eq(actual_amount_with_fee.round(2))
    end

    it "should create memberships for invited joiners" do
      mod_params = paypal_payment_params
      mod_params[:invited_joiners] = invited_joiner_params
      initialize_payment_with_invited_joiners(mod_params, event)

      execute = execute_payment_with_invited_joiners(payment_details_with_invited_joiners, event)

      invited_joiner_emails = invited_joiner_params.map{ |u| u[:email] }
      invited_joiners = User.where(email: invited_joiner_emails)
      expect(joiner.invited_joiners).to include(*invited_joiners)
    end
  end

  context "booking trips with referral" do
    before(:each) do
      organizer.referrals.create(referral_params)
      organizer.promo_codes.create(credit_promo_code_params)
      booking_fee
    end

    it "should update referrer's credit for every referred joiners success booking" do
      organizer.referrals.create(referral_params)
      existing_credits = organizer.credit_promo_code.value
      total_credits = existing_credits + incentive.value
      join_event_through_paypal_valid(join_event_params, event)
      organizer.credit_promo_code.reload

      expect(event.event_memberships.count).to eq(1)
      expect(organizer.credit_promo_code.credit_logs.earned_incentive.last.amount).to eq(total_credits)
    end
  end
end
