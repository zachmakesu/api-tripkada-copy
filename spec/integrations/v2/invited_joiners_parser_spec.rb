require 'rails_helper'

describe InvitedJoinersParser do

  context "when parsing invited joiners data" do
    it " should parse Array of emails and names to Array of Hashes of Name and Email" do
      names = %W{ #{Faker::Name.name} #{Faker::Name.name}}
      emails = %W{ #{Faker::Internet.email} #{Faker::Internet.email}}

      data = []
      names.each_with_index do |name, i|
        hash = {}
        hash[:name] = name
        hash[:email] = emails[i]
        data.push(hash)
      end
      
      parser = InvitedJoinersParser.parse(names: names, emails: emails) 
      expect(parser.data).to eq(data)
    end
  end
end
