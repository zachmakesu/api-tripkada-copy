require 'rails_helper'

describe InviteJoinerHandler do
  let(:inviter){ FactoryGirl.create(:joiner) }

  let(:event){ FactoryGirl.create(:event) }

  let(:existing_joiner){ FactoryGirl.create(:user) }

  let(:booking_fee){ FactoryGirl.create(:booking_fee) }

  let(:incentive) { create(:referral_incentive) }

  let(:credit_promo_code_params){
    {
      code: SecureRandom.hex(6),
      value: 0.0,
       usage_limit: 999,
      category: "credit"
    }
  }

  let(:registered_invitee_params){
    [
      {
        name: existing_joiner.decorate.full_name,
        email: existing_joiner.email
       }
    ]
  }

  let(:unregistered_invitee_params_valid){
    [
      {
        name: Faker::Name.name,
        email: Faker::Internet.email
       },
       {
         name: Faker::Name.name,
         email: Faker::Internet.email
       }
    ]
  }


  let(:unregistered_invitee_params_invalid){
    [

      {
        name: Faker::Name.name,
        email: "testemail_01.hotmail.com"
       },
       {
         name: Faker::Name.name,
        email: "testemail_02.hotmail.com"
       }
    ]
  }

  describe "#find_or_create" do
    before(:each) do
      booking_fee
      @membership = inviter.event_memberships.create(event_id: event.id)
      inviter.promo_codes.create(credit_promo_code_params)
      incentive
    end
    context "create account and membership for unregistered users" do
      it "should create account and membership with valid params" do
        handler = InviteJoinerHandler.new(inviter, event.id, unregistered_invitee_params_valid, @membership.id).find_or_create 
        expect(inviter.invited_joiners).to include(*handler.response[:details])
      end
      it "should not create account and membership with invalid params" do
        handler = InviteJoinerHandler.new(inviter, event.id, unregistered_invitee_params_invalid, @membership.id).find_or_create 
        expect(inviter.invited_joiners).to be_empty
      end
    end
    context "find joiner by email and create membership" do
      it "should find existing user and create membership for event" do
        handler = InviteJoinerHandler.new(inviter, event.id, registered_invitee_params, @membership.id).find_or_create 
        expect(existing_joiner.joined_events).to include(event)
      end
      it "should set payment mode as through_invite" do
        handler = InviteJoinerHandler.new(inviter, event.id, registered_invitee_params, @membership.id).find_or_create 
        expect(existing_joiner.event_memberships.last.payment.mode).to eq("through_invite")
      end
    end

    context "with referral" do
      it "should assign inviter as referrer for new users" do
        handler = InviteJoinerHandler.new(inviter, event.id, unregistered_invitee_params_valid, @membership.id).find_or_create 
        expect(inviter.invited_joiners.last.accepted_referral.referrer).to eq(inviter)
      end
      it "should update referrer's credit for every referred joiners success booking" do
        existing_credits = inviter.credit_promo_code.available_credits
        invited_joiners_count = unregistered_invitee_params_valid.count
        incentive_with_invited_joiners = invited_joiners_count * incentive.value
        total_credits = existing_credits + incentive_with_invited_joiners
        
        handler = InviteJoinerHandler.new(inviter, event.id, unregistered_invitee_params_valid, @membership.id).find_or_create 
        inviter.credit_promo_code.reload
        
        expect(handler.response[:type]).to be_truthy
        expect(inviter.credit_promo_code.available_credits).to eq(total_credits)
      end
    end
  end

end
