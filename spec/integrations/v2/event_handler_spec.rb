require 'rails_helper'

describe EventHandler do
  let(:organizer) { create(:user) }
  let(:categories) { create_list(:category, 2) }
  let(:destinations) { create_list(:destination, 2) }
  let(:generated_event) { create(:event, user_id: organizer.id) }
  let(:joiner) { create(:joiner) }
  let(:booking_fee) { create(:booking_fee) }
  let!(:promo_code) { create(:promo_code) }
  let(:incentive) { create(:referral_incentive) }
  let(:referrer) { create(:user) }
  let(:device_params){
    {
      name: "test",
      platform: "test_platform",
      token: "#{SecureRandom.hex(8)}",
      udid: "#{SecureRandom.hex(8)}"
    }
  }

  let(:referral_params){
    {
      joiner_id: joiner.id
    }
  }

  let(:valid_membership_params){
    {
      id: Event.last.id, mode: "bank",
      made: "downpayment", from_controller: true,
      promo_code_id: promo_code.id, free_downpayment: true
    }
  }

  let(:credit_promo_code_params){
    {
      code: SecureRandom.hex(6),
      value: 0.0,
       usage_limit: 999,
      category: "credit"
    }
  }
  let(:invalid_membership_params){
    {
      id: Event.last.id, mode: "bank",
      made: "downpayment",
      promo_code_id: promo_code.id,
      free_downpayment: true
    }
  }

  let(:valid_image) {
    Rack::Test::UploadedFile.new(
      Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg'
    )
  }

  describe "#join" do
    before(:each) do
      FactoryGirl.create(:event)
      Event.last.update(user_id: organizer.id)
      booking_fee
    end

    context "validates required info upon booking" do
      it "should not join event when mobile is nil" do
        joiner.update!(mobile: nil)
        event = EventHandler.new(joiner, valid_membership_params).join
        expect(event.response[:type]).to be_falsey
      end
      it "should join event when required info is completed" do
        event = EventHandler.new(joiner, valid_membership_params).join
        expect(event.response[:type]).to be_truthy
      end
    end

    context "validate device platform upon joining event" do
      it "should be success if device platform is present and valid" do

        event = EventHandler.new(joiner, valid_membership_params).join
        expect(event.response[:type]).to be_truthy
      end

      it "should set desktop as default device platform" do
        invalid_params = {
          id: Event.last.id, mode: "bank",
          made: "downpayment",
          promo_code_id: promo_code.id, free_downpayment: true
        }

        EventHandler.new(joiner, invalid_params).join
        expect(Event.last.event_memberships.last.payment.device_platform).to eq(Payment.device_platforms.key(0))
      end
      it "should return error for invalid device platform" do
        #set invalid device for current_user
        invalid_params = invalid_membership_params
        invalid_params[:platform] = Faker::Lorem.word
        event = EventHandler.new(joiner, invalid_params).join
        expect(event.response[:details]).to eq("'#{invalid_params[:platform]}' is not a valid device_platform")
      end
    end
  end

  context "booking trips with referral" do
    before(:each) do
      referrer.referrals.create(referral_params)
      referrer.promo_codes.create(credit_promo_code_params)
      booking_fee
    end

    it "should update referrer's credit for every referred joiners success booking" do
      existing_credits = referrer.credit_promo_code.value
      total_credits = existing_credits + incentive.value
      event = EventHandler.new(joiner, valid_membership_params).join
      referrer.credit_promo_code.reload

      expect(event.response[:type]).to be_truthy
      expect(referrer.credit_promo_code.credit_logs.earned_incentive.last.amount).to eq(total_credits)
    end
  end

  context "adding brand sponsor logo in event" do
    before(:each) do
      @admin = joiner
      @admin.update(role: 2)
      @event_attr = generated_event.attributes
      @event_attr[:brand_sponsor_logo] = valid_image
      @event_attr[:category_ids] = categories.map(&:id)
      @event_attr[:destination_ids] = destinations.map(&:id)
    end
    it "should successfully create brand sponsor logo if user is admin" do
      handler = described_class.new(@admin, @event_attr.symbolize_keys).update
      expect(generated_event.sponsors).to be_present
    end
    it "should not create brand sponsor logo if user is not admin" do
      handler = described_class.new(organizer, @event_attr.symbolize_keys).update
      expect(generated_event.sponsors).not_to be_present
    end
  end
end
