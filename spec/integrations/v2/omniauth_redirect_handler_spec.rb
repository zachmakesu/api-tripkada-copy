require "rails_helper"

describe OmniauthRedirectHandler do
  include Rails.application.routes.url_helpers

  class Request
    include Rails.application.routes.url_helpers
    attr_accessor :referer, :event

    def initialize
      @referer = trip_view_path(Event.last)
      @event = Event.last
    end
  end

  class InvalidRequest < Request
    def initialize
      @referer = nil
      @event = nil
    end
  end

  let!(:user) { create(:user) }
  let!(:event) { create(:event) }
  let!(:path) { Rails.application.routes.recognize_path(trip_view_path(event)) }

  let!(:invalid_facebook_params) {
    {
      type: false,
      details: {
        message: "User is not allowed to login as organizer"
      }
    }
  }

  let!(:valid_facebook_params) {
    {
      type: true,
      details: {
        message: ""
      }
    }
  }

  let!(:options) {
    {
      path: path,
      event: event.decorate,
      user: user,
      request: Request.new,
      handler_response: valid_facebook_params,
      app_role: 'organizer',
      kind: 'Facebook',
      proceed_checkout: false
    }
  }

  describe "#get_path" do
    context "with valid params for admin" do
      it "should redirect to admin path" do
        user.admin!
        redirect_handler = OmniauthRedirectHandler.new(options).get_path

        expect(redirect_handler.return_to).to eq(admin_path)
      end
    end

    context "with valid params for important notifs" do
      it "should redirect to notification page" do
        user.notifications.create(message: "Test", category: 7, notificationable_id: event.id, notificationable_type: "Event")
        redirect_handler = OmniauthRedirectHandler.new(options).get_path

        expect(redirect_handler.return_to).to eq(user_notification_path(user.notifications.last.id))
      end
    end

    context "with valid params fit to make facebook handler false" do
      it "should redirect to apply as organizer path" do
        options[:handler_response]  = invalid_facebook_params
        redirect_handler = OmniauthRedirectHandler.new(options).get_path

        expect(redirect_handler.return_to).to eq(be_a_trip_organizer_path)
      end
    end

    context "with valid params for past event" do
      it "should redirect to trip request path" do
        event.update_columns(start_at: 2.days.ago,end_at: 1.day.ago)
        redirect_handler = OmniauthRedirectHandler.new(options).get_path

        expect(redirect_handler.return_to).to eq(trip_requests_path)
      end
    end

    context "with valid params for full event" do
      it "should redirect to trip page with flash full trip true" do
        event.update(min_pax: 0, max_pax: 0)
        redirect_handler = OmniauthRedirectHandler.new(options).get_path

        expect(redirect_handler.return_to).to eq(trip_view_path(event))
        expect(redirect_handler.flash[:full_trip]).to be_truthy
      end
    end

    context "with valid params to proceed on checkout" do
      it "should redirect to checkout page" do
        options[:proceed_checkout]  = true
        redirect_handler = OmniauthRedirectHandler.new(options).get_path

        expect(redirect_handler.return_to).to eq(trip_checkout_path(event))
      end
    end

    context "with valid params redirect using referer url" do
      it "should redirect to request referer" do
        redirect_handler = OmniauthRedirectHandler.new(options).get_path

        expect(redirect_handler.return_to).to eq(Request.new.referer)
      end
    end

    context "with valid params using dynamic path" do
      it "should redirect to admin path for admin user" do
        request = InvalidRequest.new
        user.admin!
        options[:request] = request
        redirect_handler  = OmniauthRedirectHandler.new(options).get_path

        expect(redirect_handler.return_to).to eq(admin_path)
      end

      it "should redirect to trips path for organizer in joiner mode" do
        request = InvalidRequest.new
        user.joiner_and_organizer!
        options[:request]   = request
        options[:app_role]  = 'joiner'
        redirect_handler    = OmniauthRedirectHandler.new(options).get_path

        expect(redirect_handler.return_to).to eq(trips_path)
      end

      it "should redirect to my trips path for organizer mode" do
        request = InvalidRequest.new
        user.joiner_and_organizer!
        options[:request] = request
        redirect_handler  = OmniauthRedirectHandler.new(options).get_path

        expect(redirect_handler.return_to).to eq(my_trips_path(options[:app_role]))
      end

      it "should redirect to root path for first time login joiner" do
        request = InvalidRequest.new
        user.update(sign_in_count: 1, role: 0)
        options[:request]   = request
        options[:app_role]  = 'joiner'
        redirect_handler    = OmniauthRedirectHandler.new(options).get_path

        expect(redirect_handler.return_to).to eq(root_path)
      end

      it "should redirect to trips path for joiner" do
        request = InvalidRequest.new
        user.update(sign_in_count: 10, role: 0)
        options[:request]   = request
        options[:app_role]  = 'joiner'
        redirect_handler    = OmniauthRedirectHandler.new(options).get_path

        expect(redirect_handler.return_to).to eq(trips_path)
      end

      it "should redirect to specified trip's page for invited joiners" do
        request = InvalidRequest.new
        user.update(sign_in_count: 0, role: 0)
        trip_url = trip_view_path(event)
        options[:url]       = trip_url
        options[:request]   = request
        options[:app_role]  = 'invitee'
        redirect_handler    = OmniauthRedirectHandler.new(options).get_path

        expect(redirect_handler.return_to).to eq(trip_url)
      end
    end
  end

end
