require "rails_helper"

describe FacebookSessionHandler do

  let(:user) do
    VCR.use_cassette 'facebook/test_user' do
      Koala::Facebook::TestUsers.new(app_id: ENV["FACEBOOK_KEY"], secret: ENV["FACEBOOK_SECRET"]).create(true)
    end
  end
  let(:access_token) { user["access_token"] }
  let(:handler) do
    VCR.use_cassette 'facebook/me_object' do
      FacebookSessionHandler.new("joiner", access_token, options)
    end
  end

  let(:existing_invitee){ create(:joiner) }
  let(:options) do
    {
      uid: existing_invitee.uid,
      referrer_uid: existing_invitee.uid,
      platform: "desktop"
    }
  end


  let(:invitee) do
    VCR.use_cassette 'facebook/test_invitee' do
      Koala::Facebook::TestUsers.new(app_id: ENV["FACEBOOK_KEY"], secret: ENV["FACEBOOK_SECRET"]).create(true)
    end
  end

  let(:invitee_handler) do
    VCR.use_cassette 'facebook/invitee' do
      FacebookSessionHandler.new("invitee", invitee["access_token"], options)
    end
  end

  let(:new_user_handler) do
    VCR.use_cassette 'facebook/new_user' do
      FacebookSessionHandler.new("invitee", invitee["access_token"], options)
    end
  end

  let(:referred_joiner) do
    VCR.use_cassette 'facebook/test_referred_joiner' do
      Koala::Facebook::TestUsers.new(app_id: ENV["FACEBOOK_KEY"], secret: ENV["FACEBOOK_SECRET"]).create(true)
    end
  end

  let(:referral) do
    VCR.use_cassette 'facebook/referral' do
      FacebookSessionHandler.new("from_referral", referred_joiner["access_token"], options)
    end
  end

  describe :create do
    before { handler }

    context "with valid credentials" do

      it "saves a record of the created user" do
        expect { handler.create }.to change(User, :count)
      end

    end

    context "with invalid email" do
      before do
        allow(handler).to receive(:auth) { { "email" => "", "id" => "123" } }
      end

      it "generates a tmp email for user" do
        auth_user = handler.create

        expect(auth_user.email).to eq "123@facebook.com"
      end
    end
  end

  describe :find do
    it "return user based on uid" do
      handler.find_or_create
      auth_user = handler.find
      expect(auth_user.providers.last.uid).to eq(user['id'])
      expect(auth_user.providers.last.provider).to eq('facebook')
    end
  end

  describe "Login through invites" do
    it "return user based on uid" do
      allow(invitee_handler).to receive(:auth) { { "email" => existing_invitee.email } }
      invitee_handler.find_or_create
      current_user = invitee_handler.find

      expect(current_user).to eq(existing_invitee)
      expect(User.count).to eq(1)
    end

    it "create user if uid is not found" do
      allow(new_user_handler).to receive(:auth) { { "email" => "", "id" => "123" } }
      new_user_handler.find_or_create
      user = new_user_handler.find

      expect(user).to eq(User.last)
    end
  end

  describe "Login with referral" do
    it "should create user account with referrer" do
      referral.find_or_create
      current_user = referral.find
      expect(current_user.accepted_referral.referrer).to eq(existing_invitee)
    end
  end

end
