require "rails_helper"

describe EventHandler do

  let!(:current_user) { create(:user) }
  let!(:category) { create(:category, name: "new") }
  let!(:destination) { create(:destination, name: "new") }
  let!(:joiner) { create(:user) }
  let!(:event) { create(:event) }
  let!(:promo_code) { create(:promo_code) }
  let!(:booking_fee) { create(:booking_fee) }

  let!(:event_params) {
    {
      name: Faker::Address.street_name,
      start_at: DateTime.now + 1.days,
      end_at: DateTime.now + 3.days,
      rate: 500,
      downpayment_rate: 100,
      min_pax: 5,
      max_pax: 10,
      highlights: "#{Faker::StarWars.character};#{Faker::StarWars.character}",
      inclusions: "#{Faker::Superhero.power};#{Faker::Superhero.power}",
      tags: "tag1;tag2",
      things_to_bring: "bag, phone, etc.",
      meeting_place: Faker::Address.street_name,
      itineraries_description: [Faker::Address.street_address,Faker::Address.street_address],
      lat: Faker::Address.latitude,
      lng: Faker::Address.longitude,
      photos: {},
      category_ids: Category.ids,
      destination_ids: Destination.ids,
      published_at: DateTime.now
    }
  }

  describe "#create" do

    context "with valid parameters" do

      it "should create events with its associations" do
        @event = EventHandler.new(current_user, event_params)

        expect{@event.create}.to change{current_user.organized_events.count}.from(20).to(21)
      end

    end

    context "with missing parameters" do

      it "should not create events without category" do
        event_params[:category_ids] = []
        @event = EventHandler.new(current_user, event_params).create
        expect(@event.response[:type]).to be false
      end

      it "should not create events without destination" do
        event_params[:destination_ids] = []
        @event = EventHandler.new(current_user, event_params).create
        expect(@event.response[:type]).to be false
      end

    end

    context "with duplicate action" do

      it "should duplicate event with its associations" do
        event.update(owner: current_user)
        event.photos.create(image: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg'))
        @new_event = event.deep_clone(include: [:itineraries])

        params = {
          event_id: event.id,
          name: @new_event.name,
          start_at: DateTime.now + 1.days,
          end_at: DateTime.now + 3.days,
          rate: @new_event.rate,
          downpayment_rate: @new_event.downpayment_rate,
          min_pax: @new_event.min_pax,
          max_pax: @new_event.max_pax,
          highlights: @new_event.highlights,
          inclusions: @new_event.inclusions,
          tags: @new_event.tags,
          things_to_bring: @new_event.things_to_bring,
          meeting_place: @new_event.meeting_place,
          itineraries_description: @new_event.itineraries.map(&:description),
          lat: @new_event.lat,
          lng: @new_event.lng,
          photo_ids: event.photos.ids,
          category_ids: Category.ids,
          destination_ids: Destination.ids,
          published_at: DateTime.now
        }

        @new_event = EventHandler.new(current_user, params).create
        expect(@new_event.response[:type]).to be true
        expect(File.exist?(@new_event.response[:details].object.photos.last.image.path)).to be true
      end

    end

    it "should not duplicate event with invalid parent event" do
      @new_event = event.deep_clone(include: [:itineraries])

      params = {
        event_id: 99999999,
        name: @new_event.name,
        start_at: DateTime.now + 1.days,
        end_at: DateTime.now + 3.days,
        rate: @new_event.rate,
        downpayment_rate: 100,
        min_pax: @new_event.min_pax,
        max_pax: @new_event.max_pax,
        highlights: @new_event.highlights,
        inclusions: @new_event.inclusions,
        tags: @new_event.tags,
        things_to_bring: @new_event.things_to_bring,
        meeting_place: @new_event.meeting_place,
        itineraries_description: @new_event.itineraries.map(&:description),
        lat: @new_event.lat,
        lng: @new_event.lng,
        photo_ids: event.photos.ids,
        category_ids: Category.ids,
        destination_ids: Destination.ids,
        published_at: DateTime.now
      }

      @new_event = EventHandler.new(current_user, params).create

      expect(@new_event.response[:type]).to be false
    end

  end

  describe "#update" do

    before(:each) do
      @event = EventHandler.new(current_user, event_params).create
    end

    def update_params
      {
        id: @event.response[:details].id,
        name: Faker::Address.street_name,
        start_at: DateTime.now,
        end_at: DateTime.now + 3.days,
        rate: 500,
        downpayment_rate: 100,
        min_pax: 5,
        max_pax: 10,
        highlights: "#{Faker::StarWars.character};#{Faker::StarWars.character}",
        inclusions: "#{Faker::Superhero.power};#{Faker::Superhero.power}",
        tags: "tag1;tag2",
        things_to_bring: "bag, phone, etc.",
        meeting_place: Faker::Address.street_name,
        itinerary_ids: [@event.response[:details].itineraries.first.id, @event.response[:details].itineraries.last.id],
        itineraries_description: [Faker::Address.street_address,Faker::Address.street_address],
        photos: {},
        category_ids: Category.ids,
        destination_ids: Destination.ids,
        published_at: DateTime.now
      }
    end

    context "with valid parameters" do

      it "should update created events including associations" do

        @update_event = EventHandler.new(current_user,update_params)

        expect(@update_event.update).to be_truthy

      end

    end

    context "with missing parameters" do

      it "should not update events without category" do
        update_params[:category_ids] = []
        @update_event = EventHandler.new(current_user,update_params).update

        expect(@update_event.response[:type]).to be false
      end

      it "should not update events without destination" do
        update_params[:destination_ids] = []
        @update_event = EventHandler.new(current_user, update_params).update

        expect(@update_event.response[:type]).to be false
      end

    end

  end

  describe "#join" do

    before(:each) do
      EventHandler.new(current_user, event_params).create
      event.update(user_id: User.first.id)
    end

    context "validates required info upon booking" do
      before(:each) do
        @joiner_params = { id: event.id, mode: "bank", made: "downpayment",
          promo_code_id: promo_code.id, code: promo_code.code,
          free_downpayment: true, from_controller: true}
      end
      it "should not join event when mobile is nil" do
        joiner.update!(mobile: nil)
        event = EventHandler.new(joiner, @joiner_params).join
        expect(event.response[:type]).to be_falsey
      end
      it "should join event required info is completed" do
        event = EventHandler.new(joiner, @joiner_params).join
        expect(event.response[:type]).to be_truthy
      end
    end

    context "with valid parameters" do

      it "should not create and require payment proof if not the owner of event" do
        params = { id: Event.last.id, mode: "bank", made: "downpayment" }
        @event_membership = EventHandler.new(joiner, params).join

        expect(Event.last.members.count).to eq(1)
      end

    end

    context "Join Event using Promo Code as Payment" do
      it "should join the event if promo code is valid and equal to or greater than downpayment rate" do
        params = { id: event.id, mode: "bank", made: "downpayment", promo_code_id: promo_code.id, code: promo_code.code, free_downpayment: true, from_controller: true}
        handler = EventHandler.new(joiner, params).join

        expect(handler.response[:type]).to be_truthy
      end


      it "should join the event and set payment mode as free_trip if promo code is valid and equal to or greater than rate" do
        params = { id: event.id, mode: "bank", made: "downpayment", promo_code_id: promo_code.id, code: promo_code.code, free_trip: true, from_controller: true}
        handler = EventHandler.new(joiner, params).join

        expect(joiner.event_memberships.last.payment.mode).to eq("free_trip")
      end

      it "should join the event and set payment made as fullpayment if promo code is valid and equal to or greater than rate" do
        params = { id: event.id, mode: "bank", made: "downpayment", promo_code_id: promo_code.id, code: promo_code.code, free_trip: true, from_controller: true }
        handler = EventHandler.new(joiner, params).join

        expect(joiner.event_memberships.last.payment.made).to eq("fullpayment")
      end

      it "return insufficient promo code value" do
        promo_code.update(value: 0)
        params = { id: event.id, mode: "bank", made: "downpayment", promo_code_id: promo_code.id, code: promo_code.code, free_downpayment: true }
        handler = EventHandler.new(joiner, params).join

        expect(handler.response[:details]).to include("Insufficient Promo Code Value")
      end

    end

  end

  # describe "#invite_joiner" do

  #   context "with valid parameters" do

  #     it "should return selected event" do
  #       params = { id: current_user.decorate.upcoming_organized_events.last.id, full_name: "Test Developer", mobile_number: "9275619747" }
  #       handler = EventHandler.new(current_user, params).invite_joiner
  #       expect(handler.response[:details]).to eq(current_user.decorate.upcoming_organized_events.last)
  #     end

  #   end

  #   context "with invalid parameters" do

  #     it "should return required error message" do
  #       params = { id: current_user.decorate.upcoming_organized_events.last.id, mobile_number: "09223954187" }
  #       handler = EventHandler.new(current_user, params).invite_joiner
  #       expect(handler.response[:details]).to eq("Full name can't be blank, Please Provide a 10 digit Mobile Number with format 9221234567")
  #     end

  #   end

  # end


  describe "#invite_joiner_via_email" do

    context "with valid parameters" do

      it "should return selected event" do
        params = { id: current_user.decorate.upcoming_organized_events.last.id, full_name: "Test Developer", email: "test@email.com" }
        handler = EventHandler.new(current_user, params).invite_joiner_via_email
        expect(handler.response[:details]).to eq(current_user.decorate.upcoming_organized_events.last)
      end

    end

    context "with invalid parameters" do

      it "should return required error message" do
        params = { id: current_user.decorate.upcoming_organized_events.last.id, email: "notanemail" }
        handler = EventHandler.new(current_user, params).invite_joiner_via_email
        expect(handler.response[:details]).to eq("Full name can't be blank, Please provide a valid email")
      end

    end

  end

end
