require "rails_helper"

describe GoogleAnalyticsApiHandler, :vcr => true  do

  let(:tracker) { Staccato.tracker(ENV["GOOGLE_ANALYTICS_TRACKING_ID"], nil, ssl: true) }

  let(:category) { 'Registration' }
  let(:action)   { 'Submit' }
  let(:label)    { 'New User' }
  let(:value)    { 555 }

  let(:event)   { tracker.build_event(category: category, action: action, label: label, value: value) }

  let(:handler) do
    VCR.use_cassette 'google_analytics_api/staccato', :record => :new_episodes do
      GoogleAnalyticsApiHandler.new(category, action, label, value)
    end
  end

  context "#track_event" do
    it "calls Staccato build and track event" do
      expect_any_instance_of(Staccato::Tracker).to receive(:build_event).with({
        category: category,
        action: action,
        label: label,
        value: value
      }).and_return(event)

      handler.track_event!
    end
  end

end
