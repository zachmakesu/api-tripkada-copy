require 'rails_helper'

RSpec.describe UserHandler, type: :service do

  let!(:user) { create(:user) }
  let!(:params) {
    {
      email: "testuser@tripkada.com",
      gender: "Male",
      birthdate: 20.years.ago,
      country: Faker::Address.country,
      city: Faker::Address.city,
      address: Faker::Address.street_address,
      mobile: Faker::PhoneNumber.cell_phone
    }
  }

  let!(:invalid_params) {
    {
      email: "invalid@facebook.com",
      gender: "Invalid Gender",
      birthdate: 20.years.ago,
      country: Faker::Address.country,
      city: Faker::Address.city,
      address: Faker::Address.street_address,
      mobile: Faker::PhoneNumber.cell_phone
    }
  }

  context "Update User" do
    it "with valid params should update" do
      handler = UserHandler.new(user, params).update_profile

      expect(handler.response[:type]).to be_truthy
    end

    it "will be false with invalid params" do
      handler = UserHandler.new(user, invalid_params).update_profile

      expect(handler.response[:type]).to be false
    end
  end

end
