require 'rails_helper'

describe EventDecorator do
  let(:upcoming_event){ FactoryGirl.create(:event) }
  let(:canceled_event){ FactoryGirl.create(:canceled_event) }
  let(:past_event){
    event = FactoryGirl.build(:event)
    event[:start_at] = DateTime.now - 2.days
    event[:end_at] = DateTime.now - 1.days
    event.save(validation: false)
    event
  }
  let(:ongoing_event){
    event = FactoryGirl.build(:event)
    event[:start_at] = DateTime.now
    event[:end_at] = DateTime.now + 1.days
    event.save(validation: false)
    event
  }
  describe "#status_in_words" do
    context "when validating Status of Event" do
      it "return Canceled for Canceled event" do
        expect(canceled_event.decorate.status_in_words).to include("Canceled")
      end
      it "return Upcoming for Upcoming event" do
        expect(upcoming_event.decorate.status_in_words).to include("Upcoming")
      end
      it "return On-Going for On-Going event" do
        expect(ongoing_event.decorate.status_in_words).to include("On-Going")
      end
      it "return Done for Past event" do
        expect(past_event.decorate.status_in_words).to include("Done")
      end
    end
  end
end
