# Preview all emails at http://localhost:3000/rails/mailers/notification_mailer
class NotificationMailerPreview < ActionMailer::Preview
  def signup_welcome_email
    user = User.first
    NotificationMailer.signup_welcome_email(user)
  end
  def booking_confirmation
    membership = EventMembership.last
    NotificationMailer.booking_confirmation_joiner(membership)
  end

  def booking_notification
    membership = EventMembership.last
    NotificationMailer.booking_notification_organizer(membership)
  end

  def trip_invitation
    event = Event.last
    user = User.last
    invitee = User.first
    NotificationMailer.trip_invitation(user, event, invitee)
  end

  def request_different_trip_date
    requester = User.first
    event = Event.last
    params = {}
    params[:start_date_time] = "#{event.start_at}"
    params[:end_date_time] = "#{event.end_at}"
    params[:requested_pax] = event.max_pax
    NotificationMailer.request_trip_different_date(requester, event, params)
  end

  def review_trip
    event = Event.last
    member = event.members.last
    NotificationMailer.review_trip_reminder(event, member)
  end

  def waitlist_status
    event = Event.last
    user = User.last
    NotificationMailer.waitlist_membership_status(event: event, user: user, status: "approved")
  end
end
