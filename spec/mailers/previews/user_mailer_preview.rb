# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  def join_event
    UserMailer.join_event(EventMembership.last)
  end

  def apply_as_organiser
    UserMailer.apply_as_organiser(User.last)
  end
end
