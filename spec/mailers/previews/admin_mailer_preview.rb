class AdminMailerPreview < ActionMailer::Preview
  def new_trip
    AdminMailer.new_trip(Event.upcoming.not_deleted.last)
  end

  def canceled_trip
    AdminMailer.canceled_trip(Event.where.not(deleted_at: nil).last)
  end

  def review_organizer
    AdminMailer.review_organizer(Review.last)
  end

  def photo_verification
    photo = Photo.where(category: "verification_photo").take(2)
    AdminMailer.photo_verification(photo.last.imageable, photo)
  end

  def request_different_trip_date
    requester = User.first
    event = Event.last
    params = {}
    params[:start_date_time] = "#{event.start_at}"
    params[:end_date_time] = "#{event.end_at}"
    params[:requested_pax] = event.max_pax
    AdminMailer.request_trip_different_date(requester, event, params)
  end
end
