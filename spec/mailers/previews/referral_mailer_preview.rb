class ReferralMailerPreview < ActionMailer::Preview
  def referral_mail_notification_staff
    event = Event.last.id
    joiner = Event.last.members.last.id
    referrer = User.first.id
    ReferralMailer.send_to_staff(referrer, joiner, event)
  end

  def referral_mail_notification_referrer
    event = Event.last.id
    joiner = Event.last.members.last.id
    referrer = User.first.id
    ReferralMailer.send_to_referrer(referrer, joiner, event)
  end
end
