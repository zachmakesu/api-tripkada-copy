require 'rails_helper'

RSpec.describe API::V2::Devices do

  let!(:current_user)   { create(:user) }
  let!(:device_params)  {
    {
      udid:     Faker::Code.ean,
      token:    Faker::Bitcoin.address,
      platform: 'ios',
      name:     'iPhone',
      role:     'joiner'
    }
  }

  before(:each) do
    http_login(current_user)
  end

  context 'Create device' do
    it 'should create device and return success' do
      post '/api/v2/devices', device_params, @env

      expect(Device.find_by(token: device_params[:token])).to be_truthy
      expect(response).to be_success
    end
  end

  context "Unlink Device" do
    it 'should remove device and return success' do
      current_user.devices.create(device_params)
      delete '/api/v2/devices', { token: device_params[:token], udid: device_params[:udid] }, @env

      expect(Device.find_by(token: device_params[:token])).to be_falsey
      expect(response).to be_success
    end
  end

end
