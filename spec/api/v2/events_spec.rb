require 'rails_helper'

describe API::V2::Events do

  let!(:current_user)   { create(:user) }
  let!(:event)          { create(:event, user_id: current_user.id) }
  let!(:categories)     { create_list(:category, 5) }
  let!(:destinations)   { create_list(:destination, 5) }
  let!(:joiner)         { create(:joiner) }
  let!(:image)          { Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg') }
  let(:booking_fee)     { create(:booking_fee) }

  let!(:event_params){
    {
      name: Faker::Address.street_name,
      start_at: DateTime.now + 1.days,
      end_at: DateTime.now + 3.days,
      rate: 500,
      downpayment_rate: 100,
      min_pax: 5,
      max_pax: 10,
      highlights: "#{Faker::StarWars.character};#{Faker::StarWars.character}",
      inclusions: "#{Faker::Superhero.power};#{Faker::Superhero.power}",
      tags: "tag1;tag2",
      things_to_bring: "bag, phone, etc.",
      meeting_place: Faker::Address.street_name,
      itineraries_description: [Faker::Address.street_address,Faker::Address.street_address],
      lat: Faker::Address.latitude,
      lng: Faker::Address.longitude,
      photos: {},
      category_ids: categories.map(&:id),
      destination_ids: destinations.map(&:id)
    }
  }

  let(:joiner_params){
    {
      mode: "bank",
      made: "downpayment"
    }
  }


  let(:invited_joiner_params){
    [
      {
        name: Faker::Name.name,
        email: Faker::Internet.email
      },
      {
        name: Faker::Name.name,
        email: Faker::Internet.email
      }
    ]
  }


  let(:device_params){
    {
      name: "test",
      platform: "ios_app",
      token: "#{SecureRandom.hex(8)}",
      udid: "#{SecureRandom.hex(8)}"
    }
  }

  let(:trip_request_params){
    {
      start_date_time: "#{DateTime.now + 2.days}",
      end_date_time: "#{DateTime.now + 3.days}",
      requested_pax: Faker::Number.number(2)
    }
  }

  let(:payment_params){
    {
      mode:           "dragonpay",
      made:           "downpayment",
      booking_fee_id: booking_fee.id
    }
  }

  let(:waitlist){ Waitlist.create(user_id: joiner.id, event_id: event.id) }

  before(:each) do
    http_login(current_user)
  end

  context "Create event" do
    it "should save if parameters is valid" do
      post "/api/v2/events", event_params, @env
      expect(response).to be_success
    end

    it "should return bad request for invalid parameters" do
      invalid_params = event_params
      invalid_params[:rate] = 200
      invalid_params[:downpayment_rate] = 500
      post "/api/v2/events", invalid_params, @env
      expect(response.status).to eq(400)
    end

    it "should return 401 status for invalid authorization" do
      http_login(joiner)
      post "/api/v2/events", {}, @env
      expect(response.status).to eq(401)
    end

    it "should return authorization error for invalid authorization" do
      http_login(joiner)
      post "/api/v2/events", {}, @env
      @json_response = JSON.parse(response.body).symbolize_keys
      expect(@json_response[:message]["details"]).to eq("Unauthorized Action")
    end
  end

  describe "Created event" do

    before(:each) do
      post "/api/v2/events", event_params, @env
    end

    context "Get event" do
      it "should display event with valid id" do
        get "/api/v2/events/#{event.id}",{}, @env

        expect(response).to be_success
      end

      it "should not display event that doesnt belongs to user" do
        get "/api/v2/events/200", {}, @env

        expect(response.status).to eq(404)
      end
    end

    context "Update event" do
      it "should update event with valid parameters and id" do
        put "/api/v2/events/#{event.id}/update", event_params, @env

        expect(response).to be_success
      end

      it "should not update event that doesnt belongs to user" do
        put "/api/v2/events/200/update", event_params, @env

        expect(response.status).to eq(404)
      end
    end

    context "Upload photo for event" do
      it "should create with valid response" do
        post "/api/v2/events/#{event.id}/photos", {
          photo: image
        }, @env

        expect(response.status).to eq(201)
      end

      it "should create with valid response for cover photo" do
        post "/api/v2/events/#{event.id}/photos", {
          primary: true,
            photo: image,
            category: 'cover_photo'
        }, @env

        expect(response.status).to eq(201)
        expect(event.cover_photo).to be_truthy
      end

      it "should create with valid response for cover photo even if cover photo is existing" do
        new_photo = event.photos.create(primary: true, image: image, category: "cover_photo")
        previous_cover_photo = event.cover_photo
        post "/api/v2/events/#{event.id}/photos", {
          primary: true,
            photo: image,
            category: 'cover_photo'
        }, @env

        expect(response.status).to eq(201)
        expect(event.cover_photo).to be_truthy
        expect(new_photo.cover_photo?).to be_truthy
        expect(Photo.find(previous_cover_photo.id).cover_photo?).to be_falsey
      end

      it "should create with valid response for cover photo" do
        event.photos.create(primary: true, image: image, category: "cover_photo")
        put "/api/v2/events/#{event.id}/photos/#{event.cover_photo.id}", {
          primary: true,
            photo: image,
            category: 'cover_photo'
        }, @env

        expect(response.status).to eq(200)
        expect(event.cover_photo).to be_truthy
      end

      it "should create with valid response for cover photo and unset existing cover photo" do
        previous_cover_photo = event.photos.create(primary: true, image: image, category: "cover_photo")
        photo = event.photos.create(image: image)
        put "/api/v2/events/#{event.id}/photos/#{photo.id}", {
          primary: true,
            photo: image,
            category: 'cover_photo'
        }, @env

        expect(response.status).to eq(200)
        expect(event.cover_photo).to be_truthy
        expect(Photo.find(photo.id).cover_photo?).to be_truthy
        expect(Photo.find(previous_cover_photo.id).cover_photo?).to be_falsey
      end

      it "should update cover photo with valid response for cover photo" do
        photo = event.photos.create(primary: true, image: image, category: "cover_photo")
        put "/api/v2/events/#{event.id}/photos/#{photo.id}", {
          primary: true,
            photo: image,
            category: 'cover_photo'
        }, @env

        expect(response.status).to eq(200)
        expect(event.cover_photo).to be_truthy
        expect(Photo.find(photo.id).cover_photo?).to be_truthy
      end

      it "should update existing photo with valid response" do
        event.photos.create(image: image)
        put "/api/v2/events/#{event.id}/photos/#{event.photos.last.id}", {
          photo: image
        }, @env

        expect(response.status).to eq(200)
      end

      it "should not save with invalid photo id" do
        put "/api/v2/events/#{event.id}/photos/99999", {
          photo: image
        }, @env

        expect(response.status).to eq(404)
      end

      it "should delete uploaded photo with id" do
        event.photos.create(image: image)
        delete "/api/v2/events/#{event.id}/photos/#{event.photos.last.id}", {}, @env

        expect(response.status).to eq(200)
      end
    end

    context "Join Event" do
      before (:each) do
        http_login(joiner)
        joiner.devices.create(device_params) # set user device type
        booking_fee
      end

      it "should not join another user without payment proof" do
        post "/api/v2/events/#{event.id}/join", { }, @env

        expect(response.status).to eq(400)
      end


      it "should create membership with valid params" do
        photos = []
        photos << image
        mod_params = joiner_params
        mod_params[:photos] = photos
        post "/api/v2/events/#{event.id}/join", mod_params, @env
        expect(response.status).to eq(201)
      end

      it "should not join the event and create membership if no slots available" do
        photos = []
        photos << image
        mod_params = joiner_params
        mod_params[:photos] = photos
        event.update_attribute(:max_pax, 0)
        post "/api/v2/events/#{event.id}/join", mod_params, @env
        expect(response.status).to eq(400)
      end

      it "should join the event and create membership for invited joiners " do
        photos = []
        photos << image
        mod_params = joiner_params
        mod_params[:photos] = photos
        mod_params[:invited_joiners] = invited_joiner_params
        post "/api/v2/events/#{event.id}/join", mod_params, @env
        invited_joiner_emails = invited_joiner_params.map{ |u| u[:email] }
        invited_joiners = User.where(email: invited_joiner_emails)
        expect(joiner.invited_joiners).to include(*invited_joiners)
      end
    end

    context "Delete Event" do
      it "should soft delete an Event" do
        delete "/api/v2/events/#{event.id}/delete", {}, @env

        expect(response).to be_success
        expect(current_user.organized_events.size).to eq(21) #got 20 because of factory girl create events upon creation of user
      end
    end

    context "Invite Joiner via Email" do
      it "should return event upon success" do
        http_login(current_user)
        event = current_user.organized_events.upcoming.first
        post "/api/v2/events/organizer/upcoming/#{event.id}/invite_joiner_via_email", { full_name: "test name", email: "test@email.com" }, @env
        expect(response).to be_success
      end

      it "should return error message upon failure" do
        http_login(current_user)
        event = current_user.organized_events.upcoming.first
        post "/api/v2/events/organizer/upcoming/#{event.id}/invite_joiner_via_email", { }, @env
        expect(json["messages"]).to eq("Full name can't be blank, Please provide a valid email")
      end
    end

  end

  describe "Bookmark Event" do
    before(:each) do
      post "/api/v2/events", event_params, @env
      post "/api/v2/events/bookmarks", { event_id: event.id }, @env
    end

    context "Add Event to your bookmark" do
      it "should save as bookmark for the event" do
        expect(response).to be_success
        expect(current_user.bookmarked_events.include?(event)).to be_truthy
      end

      it "should not save as bookmark" do
        post "/api/v2/events/bookmarks", { event_id: event.id }, @env

        expect(response.status).to eq(400)
      end
    end

    context "Delete Event from your bookmark" do
      it "should delete event from bookmarked events" do
        delete "/api/v2/events/bookmarks", { event_id: event.id }, @env

        expect(response).to be_success
        expect(current_user.bookmarked_events.include?(event)).to be false
      end

      it "should not delete bookmark of invalid event" do
        delete "/api/v2/events/bookmarks", { event_id: 20 }, @env

        expect(response.status).to eq(400)
      end
    end

    context "List of bookmark" do
      it "should show list of events" do
        get "/api/v2/events/bookmarks", {}, @env

        expect(response).to be_success
      end
    end
  end

  describe "Request Different Trip Date" do
    context "when requesting for different trip date" do
      before (:each) do
        http_login(current_user)
      end
      it "should success message for valid request" do
        post "/api/v2/events/#{event.id}/request_different_date", trip_request_params, @env
        expect(response).to be_success
      end

      it "should not success message for invalid request" do
        invalid_request_params = trip_request_params
        invalid_request_params.delete(:start_date_time)

        post "/api/v2/events/#{event.id}/request_different_date", invalid_request_params, @env
        expect(response.status).to eq(400)
      end
    end
  end

  context "Waitlist" do
    it "should show waitlist of the event" do
      get "/api/v2/events/#{event.id}/waitlists", {}, @env
      expect(response).to be_success
    end

    it "should add a new joiner in the waitlist" do
      waitlist.destroy
      post "/api/v2/events/#{event.id}/waitlists", {}, @env
      expect(response).to be_success
    end

    it "should update waitlist status" do
      put "/api/v2/events/#{event.id}/waitlists", { waitlist_action: "approve", waitlist_id: waitlist.id }, @env
      expect(response).to be_success
      expect(Waitlist.last.status).to eq("approved")
    end

    it "should not add a new joiner in the waitlist" do
      waitlist.update_attribute(:user_id, current_user.id)
      post "/api/v2/events/#{event.id}/waitlists", {}, @env
      expect(response.status).to eq(400)
    end
  end

  describe "Drafts" do
    before (:each) do
      event.update(published_at: nil)
    end
    context "Update" do
      it "should update event with valid parameters and id" do
        put "/api/v2/events/organizer/drafts/#{event.id}", event_params, @env

        expect(response).to be_success
      end

      it "should not update event that doesnt belongs to user" do
        put "/api/v2/events/organizer/drafts/200", event_params, @env

        expect(response.status).to eq(404)
      end
    end

    it "should get all drafts" do
      get "/api/v2/events/organizer/drafts", {}, @env
      expect(response).to be_success
    end

    it "should get specific draft" do
      get "/api/v2/events/organizer/drafts/#{event.id}", {}, @env
      expect(response).to be_success
    end

    it "should successfully delete a draft" do
      delete "/api/v2/events/organizer/drafts/#{event.id}", {}, @env
      expect(response).to be_success
    end
  end

  describe "Event Members" do
    it "should success" do
      get "/api/v2/events/#{event.id}/members", {}, @env
      expect(response).to be_success
    end
    it "should fail" do
      get "/api/v2/events/#{Faker::Number.number(8)}/members", {}, @env
      expect(response).not_to be_success
    end
  end

  describe "Update Payment Status" do
    before(:each) do
      build_memberships(
        event: event, user: joiner, times: 1, params: payment_params
      )
      @params = {
        membership_id: event.event_memberships.last.id,
        status: "declined"
      }
    end
    it "should success" do
      post "/api/v2/events/organizer/#{event.id}/update_payment_status", @params, @env
      expect(response).to be_success
    end
    it "should resource not found without membership id" do
      invalid_params = @params
      invalid_params[:membership_id] = nil
      post "/api/v2/events/organizer/#{event.id}/update_payment_status", invalid_params, @env
      expect(response.status).to eq(404)
    end
    it "should return invalid request if status is invalid" do
      invalid_params = @params
      invalid_params[:status] = Faker::Lorem.word
      post "/api/v2/events/organizer/#{event.id}/update_payment_status", invalid_params, @env
      expect(response.status).to eq(400)
    end
  end


  describe "Mark Membership as no show" do
    before(:each) do
      build_memberships(
        event: event, user: joiner, times: 1, params: payment_params
      )
      @params = { membership_id: event.event_memberships.last.id }
    end
    it "should success" do
      post "/api/v2/events/organizer/#{event.id}/member/mark_as_no_show", @params, @env
      expect(response).to be_success
    end
    it "should resource not found without membership id" do
      invalid_params = @params
      invalid_params[:membership_id] = nil
      post "/api/v2/events/organizer/#{event.id}/update_payment_status", invalid_params, @env
      expect(response.status).to eq(404)
    end
  end

  describe "Generate Dragonpay Client URL" do
    before do
      create(:dragonpay_transaction_fee)
      @endpoint = "/api/v2/events/#{event.id}/generate_dragonpay_client_url"
    end

    context "with valid params" do
      before do
        @data_store_count = TransactionDataStore.count
        post @endpoint, {}, @env
      end

      it "returns success response" do
        expect(response).to be_success
      end

      it "creates transaction log" do
        expect(TransactionDataStore.count).not_to eq(@data_store_count)
      end
    end
    context "with invalid params" do
      before do
        params = {
          invited_joiner_emails: %W[#{current_user.email}],
          invited_joiner_names: %W[#{Faker::Name.name}]
        }
        post @endpoint, params, @env
      end
      it "should fail with invalid email" do
        expect(response.status).to eq(400)
      end
      it "does not create transaction log" do
        expect(TransactionDataStore.count).to eq(0)
      end
    end
  end
end
