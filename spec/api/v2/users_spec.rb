require 'rails_helper'

describe API::V2::Users do

  let!(:current_user)         { create(:user) }
  let!(:organizer)            { create(:user) }
  let!(:joiner)               { create(:joiner) }
  let(:bank_account_details)  { FactoryGirl.build(:bank_account) }
  let!(:image)                { Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg') }
  let(:booking_fee)           { FactoryGirl.create(:booking_fee) }
  let(:user_params) do
    { email: "testuser@tripkada.com",
      gender: "Male",
      birthdate: DateTime.now,
      country: Faker::Address.country,
      city: Faker::Address.city,
      address: Faker::Address.street_address,
      mobile: Faker::PhoneNumber.phone_number,
      bank_name: bank_account_details[:bank_name],
      account_number: bank_account_details[:account_number]
      }
  end

  let(:reviewer_params) do
    { rating: 5,
      message: Faker::Lorem.word
      }
  end

  let(:past_events_params) do
    { name: "Past event",
      start_at: 10.days.ago,
      end_at: 5.days.ago,
      min_pax: 5,
      max_pax: 10,
      rate: 100000,
      published_at: 10.days.ago
    }
  end

  let(:payment_params) do
    {
      mode: 5,
      made: 0,
      booking_fee_id: booking_fee.id,
      status: 1
    }
  end

  let(:instagram_params) do
    {
      uid: 1,
      role: 'joiner',
      url: Faker::Avatar.image,
      token: Faker::Code.isbn
    }
  end

  let(:invalid_attachment) { Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/sample.pdf'), 'application/pdf') }
  let(:photo_labels){ Faker::Lorem.words(2)}

  before(:each) do
    http_login(current_user)
  end

  context "Endpoints for current user" do
    it "get current user profile should be success" do
      get "/api/v2/profile", {}, @env

      expect(response).to be_success
    end

    it "should update user profile" do
      put "/api/v2/profile/update", user_params, @env

      expect(response).to be_success
    end

    context "Create or Update Organizer Bank Account Details" do
      it "should create or update if params are present" do
        put "/api/v2/profile/update", user_params, @env
        @user = current_user
        expect(@user.bank_account.account_number).to eq(bank_account_details[:account_number])
      end

      it "should not create or update if params are not present" do
        @params= {}
        put "/api/v2/profile/update", @params, @env
        @user = current_user
        expect(@user.bank_account).to be_nil
      end

      it "should get reviewable organizers" do
        get "/api/v2/profile/reviewable_organizers", {}, @env
        expect(response).to be_success
      end
    end

    context "on following" do
      it "should follow an organizer" do
        post "/api/v2/user/#{organizer.uid}/follows", {}, @env
        expect(response).to be_success
        expect(json["data"]["attributes"]["first_name"]).to eq(organizer.first_name)
      end

      it "should not follow an already followed organizer" do
        FollowHandler.new(current_user.uid,organizer.uid).follow
        post "/api/v2/user/#{organizer.uid}/follows", {}, @env
        expect(json["messages"]).to eq("Followed user already followed.")
      end
    end

    context "on unfollowing" do
      it "should unfollow a followed organizer" do
        FollowHandler.new(current_user.uid,organizer.uid).follow
        delete "/api/v2/user/#{organizer.uid}/follows", {}, @env
        expect(response).to be_success
        expect(json["data"]["attributes"]["first_name"]).to eq(organizer.first_name)
      end

      it "should not unfollow an unfollowed organizer" do
        delete "/api/v2/user/#{organizer.uid}/follows", {}, @env
        expect(json["messages"]).to eq("User not found")
      end
    end
  end

  context "Create or Update current user cover photo" do
    it "should upload cover photo" do
      post "/api/v2/profile/cover_photo", {
        cover_photo: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg')
      }, @env
      expect(response).to be_success
      expect(current_user.cover_photo.present?).to be_truthy
      expect(current_user.cover_photo.image_file_name).to eq('test.jpg')
    end
  end

  context "Delete current user cover photo" do
    it "should delete cover photo" do
      current_user.photos.create(category: "cover_photo")
      delete "/api/v2/profile/cover_photo", {}, @env
      expect(response).to be_success
      expect(current_user.cover_photo.blank?).to be_truthy
    end
  end

  context "Update current user profile avatar" do
    it "should update profile avatar" do
      current_user.create_profile_avatar(image: image)
      put "/api/v2/profile/update/profile_avatar", {
        photo: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg'),
        label: "Best profile Avatar ever!"
      }, @env
      expect(response).to be_success
      keyword = "test.jpg"
      url = json["links"]["avatar"]
      num = /\/#{keyword}/ =~ url
      expect(!!num).to be_truthy
    end
  end

  context "Upload current user profile photos" do
    it "should upload profile photos" do
      post "/api/v2/profile/upload/profile_photos", {
        photos: [Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg')]
      }, @env
      expect(response).to be_success
    end
  end

  context "Update current user profile photos" do
    it "should update profile photos" do
      current_user.profile_photos.create()
      put "/api/v2/profile/update/profile_photos", {
        photo_ids: [current_user.profile_photos.last.id],
        photos: [Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg')]
      }, @env
      expect(response).to be_success
    end
  end

  context "Remove current user profile photos" do
    it "should remove profile photos" do
      current_user.profile_photos.create()
      delete "/api/v2/profile/remove/profile_photos", { photo_ids: [current_user.profile_photos.last.id] }, @env
      expect(response).to be_success
    end
  end

  context "Manage photo" do
    it "should upload photo" do
      expect {
        post "/api/v2/profile/photos", { photo: image, label: Faker::Hacker.say_something_smart }, @env
      }.to change{current_user.profile_photos.count}.from(0).to(1)
      expect(response).to be_success
    end

    it "should not upload nil params" do
      expect {
        post "/api/v2/profile/photos", {}, @env
      }.to_not change(current_user.profile_photos, :count)
      expect(response.status).to eq(400)
    end

    it "should not upload without image" do
      expect {
        post "/api/v2/profile/photos", { label: Faker::Hacker.say_something_smart }, @env
      }.to_not change(current_user.profile_photos, :count)
      expect(response.status).to eq(400)
    end

    it "should update exisiting photo" do
      current_user.profile_photos.create(image: image)
      put "/api/v2/profile/photos/#{current_user.profile_photos.last.id}", { photo: image, label: Faker::Hacker.say_something_smart }, @env
      expect(response).to be_success
    end

    it "should update existing photo with label only" do
      current_user.profile_photos.create(image: image)
      put "/api/v2/profile/photos/#{current_user.profile_photos.last.id}", { label: Faker::Hacker.say_something_smart }, @env
      expect(response).to be_success
    end

    it "should not update non existing photo" do
      put "/api/v2/profile/photos/9999999", { photo: image, label: Faker::Hacker.say_something_smart }, @env
      expect(response.status).to eq(404)
    end

    it "should not update with nil params" do
      current_user.profile_photos.create(image: image)
      put "/api/v2/profile/photos/#{current_user.profile_photos.last.id}", {}, @env
      expect(response.status).to eq(400)
    end

    it "should delete exisiting photo" do
      current_user.profile_photos.create(image: image)
      expect {
        delete "/api/v2/profile/photos/#{current_user.profile_photos.last.id}", {}, @env
      }.to change{current_user.profile_photos.count}.from(1).to(0)
      expect(response).to be_success
    end

    it "should not delete non exisiting photo" do
      expect {
        delete "/api/v2/profile/photos/99999999", {}, @env
      }.to_not change(current_user.profile_photos, :count)
      expect(response.status).to eq(404)
    end
  end

  context "Connect instagram account" do
    it "should return success for valid params" do
      post "/api/v2/profile/connect/instagram", instagram_params, @env

      expect(response.status).to eq(201)
    end

    it "should return false if already have instagram provider" do
      current_user.providers.create(uid: 1, role: 'joiner', url: Faker::Avatar.image, token: Faker::Code.isbn, provider: 'instagram')
      post "/api/v2/profile/connect/instagram", instagram_params, @env

      expect(response.status).to eq(400)
    end

    it "should successfully update instagram with valid params" do
      current_user.providers.create(uid: 1, role: 'joiner', url: Faker::Avatar.image, token: Faker::Code.isbn, provider: 'instagram')
      put "/api/v2/profile/connect/instagram", instagram_params, @env

      expect(response).to be_success
    end

    it "should return false for calling update when there is not instagram yet even if params is valid" do
      put "/api/v2/profile/connect/instagram", instagram_params, @env

      expect(response.status).to eq(404)
    end

    it "should successfully delete instagram accounts" do
      current_user.providers.create(uid: 1, role: 'joiner', url: Faker::Avatar.image, token: Faker::Code.isbn, provider: 'instagram')
      delete "/api/v2/profile/connect/instagram", instagram_params, @env

      expect(response).to be_success
    end

    it "should successfully delete instagram accounts event if there is no existing" do
      delete "/api/v2/profile/connect/instagram", instagram_params, @env

      expect(response).to be_success
    end
  end

  context "Get public user profile" do
    it "should return user" do
      get "/api/v2/user/#{organizer.uid}", {}, @env

      expect(response).to be_success
    end
  end

  context "Get User organized trips" do
    it "should success returning user's upcoming organized trips" do
      get "/api/v2/user/#{joiner.uid}/trips/upcoming", {}, @env
      expect(response).to be_success
    end

    it "should success returning user's past organzied trips" do
      get "/api/v2/user/#{joiner.uid}/trips/previous", {}, @env
      expect(response).to be_success
    end
  end

  context "Joiner review on organizer" do
    before(:each) do
      past_event = organizer.organized_events.new(past_events_params)
      past_event.save(validate: false)
      organizer_membership = past_event.event_memberships.create(member_id: organizer.id)
      organizer_membership.build_payment(payment_params).save
      joiner_membership = past_event.event_memberships.create(member_id: current_user.id)
      joiner_membership.build_payment(payment_params).save
    end

    it "is successful" do
      post "/api/v2/joiner/#{organizer.uid}/review_organizer", reviewer_params, @env
      expect(response).to be_success
    end

    it "should return a suggestion message" do
      http_login(joiner)
      post "/api/v2/joiner/#{organizer.uid}/review_organizer", reviewer_params, @env
      expect(response.status).to eq(400)
      expect(json["messages"]).to eq("You need to join at least 1 trip to this organizer before you can submit a review")
    end

    it "should return already reviewed message" do
      organizer.received_reviews.as_joiner.create({reviewer: current_user, rating: 1, message: "test"})

      post "/api/v2/joiner/#{organizer.uid}/review_organizer", reviewer_params, @env
      expect(response.status).to eq(400)
      expect(json["messages"]).to eq("You already reviewed this organizer")
    end

    it "should return a suggestion message and self review not applicable message" do
      post "/api/v2/joiner/#{current_user.uid}/review_organizer", reviewer_params, @env
      expect(response.status).to eq(400)
      expect(json["messages"]).to eq("You need to join at least 1 trip to this organizer before you can submit a review, You can't review yourself, review other organizers instead")
    end
  end

  context "Organizer review on joiner" do
    before(:each) do
      past_event = current_user.organized_events.new(past_events_params)
      past_event.save(validate: false)
      organizer_membership = past_event.event_memberships.create(member_id: organizer.id)
      organizer_membership.build_payment(payment_params).save
      joiner_membership = past_event.event_memberships.create(member_id: joiner.id)
      joiner_membership.build_payment(payment_params).save
    end
    it "is successful" do
      post "/api/v2/organizer/#{joiner.uid}/review_joiner", reviewer_params, @env
      expect(response).to be_success
    end

    it "should return not a valid joiner message" do
      http_login(organizer)
      post "/api/v2/organizer/#{current_user.uid}/review_joiner", reviewer_params, @env
      expect(response.status).to eq(400)
      expect(json["messages"]).to eq("This joiner hasn't joined a trip from you yet")
    end

    it "should return already reviewed message" do
      joiner.received_reviews.as_organizer.create({reviewer: current_user, rating: 1, message: "test"})
      post "/api/v2/organizer/#{joiner.uid}/review_joiner", reviewer_params, @env
      expect(response.status).to eq(400)
      expect(json["messages"]).to eq("You already reviewed this joiner")
    end

    it "should return a suggestion message and self review not applicable message" do
      post "/api/v2/organizer/#{current_user.uid}/review_joiner", reviewer_params, @env
      expect(response.status).to eq(400)
      expect(json["messages"]).to eq("This joiner hasn't joined a trip from you yet, You can't review yourself, review other joiner instead")
    end
  end

  context "Public Organizer endpoints" do
    it "should successfully return reviewable trips" do
      get "/api/v2/organizer/#{organizer.uid}/reviewables", {}, @env

      expect(response).to be_success
    end

    it "should successfully return reviews" do
      get "/api/v2/organizer/#{organizer.uid}/reviews", {}, @env

      expect(response).to be_success
    end
  end

  context "Get user's sent_reviews" do
    it "should return user' sent_reviews" do
      get "/api/v2/user/#{organizer.uid}/sent_reviews", {}, @env

      expect(response).to be_success
    end
  end

  context "Get user's received_reviews" do
    it "should return user' received_reviews" do
      get "/api/v2/user/#{organizer.uid}/received_reviews", {}, @env

      expect(response).to be_success
    end
  end

  context "Upload Certificates" do
    before (:each) do
      @params = {}
      @params[:photos] = []
      @params[:photos][0] = image
      @params[:photos][1] = image
      @params[:photo_labels] = photo_labels
    end

    it "should upload valid certificates" do
      post "/api/v2/profile/certificate", @params, @env
      expect(response).to be_success
    end
    it "should not upload invalid certificates" do
      invalid_params = @params
      invalid_params[:photos][1] = invalid_attachment
      post "/api/v2/profile/certificate", invalid_params, @env
      expect(response.status).to eq(400)
    end
  end

  context "User Calendar" do
    it "should successfully sync users calendar and update calendar sync to true" do
      put "/api/v2/profile/calendar/sync", {}, @env
      current_user.reload

      expect(response).to be_success
      expect(current_user.calendar_sync).to be_truthy
    end

    it "should successfully unsync users calendar and update calendar sync to false" do
      put "/api/v2/profile/calendar/unsync", {}, @env
      current_user.reload

      expect(response).to be_success
      expect(current_user.calendar_sync).to be_falsey
    end
  end

  context "Reviewable Event Organizers" do
    it "should successfully fetch reviewable events and organizers" do
      get "/api/v2/profile/reviewable_organizers", {}, @env
      expect(response).to be_success
    end
  end
  context "Reviewable Event Organizers" do
    it "should successfully fetch reviewable events and organizers" do
      event = Event.new(past_events_params)
      event.save(validate: false)
      build_memberships(
        event: event, user: current_user, times: 1, params: payment_params
      )
      post "/api/v2/profile/reviewable_organizers/blacklist", {id: event.id}, @env
      expect(response).to be_success
    end
    it "should not success if event is invalid" do
      event = Event.new(past_events_params)
      event.save(validate: false)
      post "/api/v2/profile/reviewable_organizers/blacklist", {id: event.id}, @env
      expect(response).not_to be_success
    end
  end
end
