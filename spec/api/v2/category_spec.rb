require 'rails_helper'

describe API::V2::Category do

  let!(:current_user) { create(:user) }
  let!(:category)     { create(:category) }

  before(:each) do
    http_login(current_user)
  end

  describe "Category" do

    context "Get list category" do
      it "should display all category" do
        get "/api/v2/categories",{}, @env
        expect(response).to be_success
        expect(json["data"].count).to eq(1)
      end
    end

  end
end
