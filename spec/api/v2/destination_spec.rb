require 'rails_helper'

describe API::V2::Destination do

  let!(:current_user) { create(:user) }
  let!(:destination)     { create(:destination) }

  before(:each) do
    http_login(current_user)
  end

  describe "Destination" do

    context "Get list destination" do
      it "should display all destination" do
        get "/api/v2/destinations",{}, @env
        expect(response).to be_success
        expect(json["data"].count).to eq(1)
      end
    end

  end
end
