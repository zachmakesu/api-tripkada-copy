require 'rails_helper'

describe API::V2::Referrals do
  let!(:referrer) { create(:user) }
  let(:email_params){
    {
      emails: "#{Faker::Internet.email}, #{Faker::Internet.email}, #{Faker::Internet.email}"
    }
  }

  before(:each) do
    http_login(referrer)
  end

  context 'when sending referral invitations' do
    it 'should return success response' do
      post '/api/v2/referrals/send_invitations', email_params, @env

      expect(response).to be_success
    end
  end
end
