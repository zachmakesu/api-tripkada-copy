require 'rails_helper'

describe API::V2::AppVersions do

  let!(:current_user)     { create(:user) }
  let!(:ios_joiner_version)      { create(:ios_joiner_version) }
  let!(:android_joiner_version)  { create(:android_joiner_version) }
  let!(:ios_organizer_version)      { create(:ios_organizer_version) }
  let!(:android_organizer_version)  { create(:android_organizer_version) }

  before(:each) do
    http_login(current_user)
  end

  context "Get latest app version" do
    it "should return latest ios version for joiner" do
      get "/api/v2/app_versions/ios/joiner/latest",{}, @env
      expect(response).to be_success
    end
    it "should return latest ios version for organizer" do
      get "/api/v2/app_versions/ios/organizer/latest",{}, @env
      expect(response).to be_success
    end
    it "should return latest android version for joiner" do
      get "/api/v2/app_versions/android/joiner/latest",{}, @env
      expect(response).to be_success
    end
    it "should return latest android version for organizer" do
      get "/api/v2/app_versions/android/organizer/latest",{}, @env
      expect(response).to be_success
    end
    it "should bad request for invalid platform" do
      get "/api/v2/app_versions/#{Faker::Lorem.word}/joiner/latest",{}, @env
      expect(response.status).to eq(400)
    end
  end

end
