require 'rails_helper'

describe API::V2::Location do

  let!(:current_user)   { create(:user) }

  let!(:location_params){
    {
      country: Faker::Address.country_code,
      state:   Faker::Address.state_abbr
    }
  }

  before(:each) do
    http_login(current_user)
  end

  context "Get All Countries" do
    it "should return list of countries" do
      get "/api/v2/location/countries",{}, @env
      expect(response).to be_success
    end
  end

  context "Get States" do
    it "should return list of states for selected country" do
      @country = location_params[:country]
      get "/api/v2/location/countries/@country/states",{}, @env
      expect(response).to be_success
    end
  end

  context "Get Cities" do
    it "should return list of states for selected country" do
      @country = location_params[:country]
      @state = location_params[:state]
      get "/api/v2/location/countries/@country/states/@state/cities",{}, @env
      expect(response).to be_success
    end
  end

end
