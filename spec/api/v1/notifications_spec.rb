require 'rails_helper'

describe API::V1::Notifications do
  let!(:current_user) { create(:user) }
  let(:notification) { build(:notification) }

  before(:each) do
    http_login(current_user)
  end

  context 'List of not deleted notifications' do
    it 'should be successful as valid return with or without notifications' do
      get '/api/v1/notifications', {}, @env

      expect(response).to be_success
    end
  end

  context 'View notification' do
    it 'should be successful for existing notification' do
      current_user.notifications.create(notification.attributes)
      get "/api/v1/notifications/#{current_user.notifications.last.id}", {}, @env

      expect(response).to be_success
    end

    it 'should be not found 404 for notifications not in existing scope' do
      get '/api/v1/notifications/11111', {}, @env

      expect(response.status).to eq(404)
    end
  end

  context 'Seen a not deleted notification' do
    it 'should be successful if valid notification' do
      current_user.notifications.create(notification.attributes)
      post "/api/v1/notifications/#{current_user.notifications.last.id}", {}, @env
      expect(response).to be_success
    end

    it 'should be not found if not in scope of unread or array of records' do
      post '/api/v1/notifications/1111111', {}, @env

      expect(response.status).to eq(404)
    end
  end


  context 'Delete notification' do
    it 'should be successful with valid params' do
      current_user.notifications.create(notification.attributes)
      delete "/api/v1/notifications/#{current_user.notifications.last.id}", {}, @env

      expect(response).to be_success
    end

    it 'should be not found if id is not in scope of records' do
      delete '/api/v1/notifications/111111', {}, @env

      expect(response.status).to eq(404)
    end
  end
end
