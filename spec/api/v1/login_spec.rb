require "rails_helper"

describe API::V1::Login do
  let(:joiners) { Koala::Facebook::TestUsers.new(:app_id => ENV["FACEBOOK_KEY"], :secret => ENV["FACEBOOK_SECRET"]) }

  let(:joiner_token) { joiners.list.first["access_token"] }
  let(:organizer_token) { joiners.list.first["access_token"] }

  describe ".login" do

    context "with valid credentials" do
      it "returns successful for logging in as joiner" do
        post '/api/v1/login/joiner',{ access_token: joiner_token }, @env

        expect(response).to be_success
      end

      it "returns false for logging in as organizer" do
        post '/api/v1/login/organizer',{ access_token: organizer_token }, @env

        expect(response.status).to eq(401)
      end
    end

  end

end
