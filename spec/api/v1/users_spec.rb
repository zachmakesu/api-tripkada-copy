require 'rails_helper'

describe API::V1::Users do

  let!(:current_user) { create(:user) }
  let!(:organizer) { create(:user) }

  before(:each) do
    http_login(current_user)
  end

  context "Endpoints for current user" do
    it "get current user profile should be success" do
      get "/api/v1/profile", {}, @env

      expect(response).to be_success
    end

    it "should update user profile" do
      put "/api/v1/profile/update", {
        email: "TestUser@tripkada.com",
        gender: "Male",
        birthdate: DateTime.now,
        country: "Philippines",
        city: "Pasig",
        address: "Manggahan",
        mobile: "09XX-XXX-XXXX",
      }, @env

      expect(response).to be_success
    end

    context "on following" do
      it "should follow an organizer" do
        post "/api/v1/user/#{organizer.uid}/follows", {}, @env
        expect(response).to be_success
        parsed_body = JSON.parse(response.body)
        expect(parsed_body["data"]["attributes"]["first_name"]).to eq(organizer.first_name)
      end

      it "should not follow an already followed organizer" do
        FollowHandler.new(current_user.uid,organizer.uid).follow
        post "/api/v1/user/#{organizer.uid}/follows", {}, @env
        parsed_body = JSON.parse(response.body)
        expect(parsed_body["messages"]).to eq("Followed user already followed.")
      end
    end

    context "on unfollowing" do
      it "should unfollow a followed organizer" do
        FollowHandler.new(current_user.uid,organizer.uid).follow
        delete "/api/v1/user/#{organizer.uid}/follows", {}, @env
        expect(response).to be_success
        parsed_body = JSON.parse(response.body)
        expect(parsed_body["data"]["attributes"]["first_name"]).to eq(organizer.first_name)
      end

      it "should not unfollow an unfollowed organizer" do
        delete "/api/v1/user/#{organizer.uid}/follows", {}, @env
        parsed_body = JSON.parse(response.body)
        expect(parsed_body["messages"]).to eq("User not found")
      end
    end


  end

  context "Upload, Update and remove current user photos" do
    before(:each) do
      post "/api/v1/profile/upload/profile_photos", {
        photos: [Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg')],
        primary: [true]
      }, @env
    end

    it "should upload profile photos" do
      expect(response).to be_success
    end

    it "should update profile photos" do
      current_user.profile_photos.create()
      put "/api/v1/profile/update/profile_photos", {
        photo_ids: [current_user.profile_photos.last.id],
        photos: [Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg')],
      }, @env

      expect(response).to be_success
    end

    it "should remove profile photos" do
      current_user.profile_photos.create()
      delete "/api/v1/profile/remove/profile_photos", { photo_ids: [current_user.profile_photos.last.id] }, @env

      expect(response).to be_success
    end
  end

  context "Get public user profile" do
    it "should return user" do
      get "/api/v1/user/#{organizer.uid}", {}, @env

      expect(response).to be_success
    end
  end

  context "Get joiner active trips" do
    it "should success returning trip data" do
      get "/api/v1/joiner/#{current_user.uid}/trips", {}, @env
      expect(response).to be_success
    end
  end

  context "Public Organizer endpoints" do
    it "should successfully return reviewable trips" do
      get "/api/v1/organizer/#{organizer.uid}/reviewables", {}, @env

      expect(response).to be_success
    end

    it "should successfully return trips" do
      get "/api/v1/organizer/#{organizer.uid}/trips", {}, @env

      expect(response).to be_success
    end

    it "should successfully return reviews" do
      get "/api/v1/organizer/#{organizer.uid}/reviews", {}, @env

      expect(response).to be_success
    end
  end

end
