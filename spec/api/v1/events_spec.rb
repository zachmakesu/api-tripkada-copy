require 'rails_helper'

describe API::V1::Events do
  let!(:categories)   { create_list(:category, 5) }
  let!(:destinations) { create_list(:destination, 5) }
  let!(:current_user) { create(:user) }
  let!(:joiner)       { create(:user) }

  let!(:event_params){
    {
      name: Faker::Address.street_name,
      start_at: DateTime.now + 1.days,
      end_at: DateTime.now + 3.days,
      rate: 500,
      downpayment_rate: 100,
      min_pax: 5,
      max_pax: 10,
      highlights: "#{Faker::StarWars.character};#{Faker::StarWars.character}",
      inclusions: "#{Faker::Superhero.power};#{Faker::Superhero.power}",
      tags: "tag1;tag2",
      things_to_bring: "bag, phone, etc.",
      meeting_place: Faker::Address.street_name,
      itineraries_description: [Faker::Address.street_address,Faker::Address.street_address],
      lat: Faker::Address.latitude,
      lng: Faker::Address.longitude,
      photos: {},
      category_ids: Category.ids,
      destination_ids: Destination.ids,
      published_at: DateTime.now
    }
  }

  before(:each) do
    http_login(current_user)
  end

  context "Create event" do
    it "should save if parameters is valid" do
      post "/api/v1/events", event_params, @env
      expect(response).to be_success
    end

    it "should return bad request for invalid paramters" do
      invalid_params = event_params
      invalid_params[:rate] = 200
      invalid_params[:downpayment_rate] = 500
      post "/api/v1/events", invalid_params, @env

      expect(response.status).to eq(400)
    end
  end

  describe "Created event" do

    before(:each) do
      post "/api/v1/events", event_params, @env
    end

    context "Get event" do
      it "should display event with valid id" do
        get "/api/v1/events/#{Event.last.id}",{}, @env

        expect(response).to be_success
      end

      it "should not display event that doesnt belongs to user" do
        get "/api/v1/events/200", {}, @env

        expect(response.status).to eq(404)
      end
    end

    context "and make a review for the current event" do
      it "should display invalid creation of review" do
        post "/api/v1/events/#{Event.last.id}/reviews", { message: "Test test", rating: 2 }, @env


        expect(response.status).to eq(400)
        expect(Event.last.received_reviews.count).to eq(0)
      end

      it "should be error if invalid params is pass" do
        post "/api/v1/events/#{Event.last.id}/reviews", { message: "Test test", rating: 5.2 }, @env

        expect(response.status).to eq(400)
      end
    end

    context "Update event" do
      it "should update event with valid parameters and id" do
        put "/api/v1/events/#{Event.last.id}/update", event_params, @env

        expect(response).to be_success
      end

      it "should not update event that doesnt belongs to user" do
        put "/api/v1/events/200/update", event_params, @env

        expect(response.status).to eq(404)
      end
    end

    context "Join Event" do
      it "should not join another user without payment proof" do
        http_login(joiner)
        post "/api/v1/events/#{Event.last.id}/join", { }, @env

        expect(response.status).to eq(400)
      end
    end

    context "Delete Event" do
      it "should soft delete an Event" do
        delete "/api/v1/events/#{Event.last.id}/delete", {}, @env

        expect(response).to be_success
        expect(current_user.organized_events.size).to eq(20) #got 20 because of factory girl create events upon creation of user
      end
    end

  end

  describe "Bookmark Event" do
    before(:each) do
      post "/api/v1/events", event_params, @env
      post "/api/v1/events/bookmarks", { event_id: Event.last.id }, @env
    end

    context "Add Event to your bookmark" do
      it "should save as bookmark for the event" do
        expect(response).to be_success
        expect(current_user.bookmarked_events.include?(Event.last)).to be_truthy
      end

      it "should not save as bookmark" do
        post "/api/v1/events/bookmarks", { event_id: Event.last.id }, @env

        expect(response.status).to eq(400)
      end
    end

    context "Delete Event from your bookmark" do
      it "should delete event from bookmarked events" do
        delete "/api/v1/events/bookmarks", { event_id: Event.last.id }, @env

        expect(response).to be_success
        expect(current_user.bookmarked_events.include?(Event.last)).to be false
      end

      it "should not delete bookmark of invalid event" do
        delete "/api/v1/events/bookmarks", { event_id: 20 }, @env

        expect(response.status).to eq(404)
      end
    end

    context "List of bookmark" do
      it "should show list of events" do
        get "/api/v1/events/bookmarks", {}, @env

        expect(response).to be_success
      end
    end
  end

end
