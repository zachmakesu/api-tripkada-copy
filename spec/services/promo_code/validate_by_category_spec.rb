require 'rails_helper'

RSpec.describe PromoCode::ValidateByCategory do
  let!(:promo_code) { create(:promo_code) }
  let!(:user) { create(:user) }
  let!(:joiner) { create(:joiner) }
  let!(:booking_fee) { create(:booking_fee) }
  let!(:event) { create(:event) }
  let!(:private_use_promo_code){ create(:private_use_promo_code, user_id: user.id)}

  describe ".validate_by_category" do
    context "when validating promo code by category" do
      it "should return valid response for valid corporate promo code" do
        validator = described_class.call(
          code: promo_code.code, user: user
        )
        expect(validator.success?).to be_truthy
      end
      it "should return error message for invalid corporate promo code" do
        validator = described_class.call(
          code: SecureRandom.uuid, user: user
        )
        expect(validator.message).to eq('Promo Code not found')
      end
      it "should return valid response for valid credit promo code" do
        credit_promo_code = user.credit_promo_code.code
        validator = described_class.call(
          code: credit_promo_code, user: user
        )
        expect(validator.success?).to be_truthy
      end
      it "should return valid response for valid private_use promo code" do
        validator = described_class.call(
          code: user.reload.private_use_promo_code.code, user: user
        )
        expect(validator.success?).to be_truthy
      end
      it "should return correct category for validated promo code" do
        credit_promo_code = user.credit_promo_code.code
        validator = described_class.call(
          code: credit_promo_code, user: user
        )
        expect(validator.promo_code.category).to eq(user.credit_promo_code.category)
      end
      it "should return error message if credit promo code is used by another user" do
        user_credit_promo_code = user.credit_promo_code.code
        joiner_credit_promo_code = joiner.credit_promo_code.code
        validator = described_class.call(
          code: joiner_credit_promo_code, user: user
        )
        expect(validator.message).to eq('promo code does not belong to user')
      end
      it "should return error message if private_use promo code is used by another user" do
        validator = described_class.call(
          code: user.reload.private_use_promo_code.code, user: joiner
        )
        expect(validator.message).to eq('promo code does not belong to user')
      end
    end
  end

end
