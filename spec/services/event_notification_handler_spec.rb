require 'rails_helper'

RSpec.describe EventNotificationHandler do

  let!(:organizer)            { create(:user, role: 1) }
  let!(:joiner)               { create(:user, role: 0) }
  let!(:joiner)               { create(:user, role: 0) }
  let!(:booking_fee)          { create(:booking_fee) }
  let!(:event)                { create(:event, owner: organizer, job_ids: "testID;testID2") }
  let!(:joiner_membership)    { create(:event_membership, event_id: event.id, member_id: joiner.id) }
  let!(:organizer_membership) { create(:event_membership, event_id: event.id, member_id: organizer.id) }

  context "#deliver" do
    it "should change job ids of event" do
      organizer_membership
      handler = EventNotificationHandler.new(event: event, membership: nil).deliver

      expect(event.job_ids).to eq(handler.job_ids.join(";"))
    end

    it "should add new job ids create for new member of event" do 
      handler = EventNotificationHandler.new(event: event, membership: joiner_membership).deliver

      expect(event.job_ids).to eq(handler.job_ids.join(";")+";testID;testID2")
    end

    it "should not change job ids for pending membership" do
      joiner_membership.payment.pending!
      EventNotificationHandler.new(event: event, membership: joiner_membership).deliver

      expect(event.job_ids).to eq("testID;testID2")
    end
  end
end
