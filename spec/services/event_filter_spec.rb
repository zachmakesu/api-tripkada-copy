require "rails_helper"

describe EventFilter do

  let!(:current_user) { create(:user, email: "testuser@tripkada.com") }
  let!(:created_categories) { create_list(:category, 10) }
  let!(:created_destinations) { create_list(:destination, 10) }

  let!(:assign_categories){
    current_user.organized_events.each do |event|
      categorizations = Category.limit(5).order("RANDOM()").ids.map{|id| {category_id: id} }
      event.categorizations.create(categorizations)
    end
  }

  let!(:assign_common_places){
    current_user.organized_events.each do |event|
      sorting_destinations = Destination.limit(5).order("RANDOM()").ids.map{|id| {destination_id: id} }
      event.sorting_destinations.create(sorting_destinations)
    end
  }

  before(:each) do
    @filter_params = Hash.new
  end

  describe "#result" do
    context "Upcoming" do
      it "should return searched Events" do
        @filter_params[:search] = "es"
        events = EventFilter.new(Event.upcoming, @filter_params ).result
        count = filtered_events(Event.upcoming, @filter_params).count
        expect(events.count).to eq(count)
        expect(events.first.name).to include(@filter_params[:search])
      end

      it "should return Events sorted by asc" do
        @filter_params[:sort] = "asc"
        events = EventFilter.new(Event.upcoming, @filter_params ).result
        first_event = filtered_events(Event.upcoming, @filter_params).first
        second_event = filtered_events(Event.upcoming, @filter_params).second
        last_event = filtered_events(Event.upcoming, @filter_params).last
        expect(events.first).to eq(first_event)
        expect(events.second).to eq(second_event)
        expect(events.last).to eq(last_event)
      end

      it "should return Events sorted by desc" do
        @filter_params[:sort] = "desc"
        events = EventFilter.new(Event.upcoming, @filter_params ).result
        first_event = filtered_events(Event.upcoming, @filter_params).first
        second_event = filtered_events(Event.upcoming, @filter_params).second
        last_event = filtered_events(Event.upcoming, @filter_params).last
        expect(events.first).to eq(first_event)
        expect(events.second).to eq(second_event)
        expect(events.last).to eq(last_event)
      end

      it "should return Events filtered by min and 10000 below max cost" do
        @filter_params[:min_cost] = 0
        @filter_params[:max_cost] = 9000
        events = EventFilter.new(Event.upcoming, @filter_params ).result
        count = filtered_events(Event.upcoming, @filter_params).count
        expect(events.count).to eq(count)
        expect(events.count).to eq(4) # 4 upcoming events below 10000 rate created on user
        expect(events.first.rate.to_f).to be_between(@filter_params[:min_cost].to_f, @filter_params[:max_cost].to_f)
      end

      it "should return Events filtered by min and 10000 above max cost" do
        @filter_params[:min_cost] = 4000
        @filter_params[:max_cost] = 10000
        events = EventFilter.new(Event.upcoming, @filter_params ).result
        count = filtered_events(Event.upcoming, @filter_params).count
        expect(events.count).to eq(count)
        expect(events.count).to eq(9) # 3 upcoming events between 4000 and 9000 rate + 6 upcoming events above 10000 rate created on user
        expect(events.first.rate.to_f).to be_between(@filter_params[:min_cost].to_f, Event.maximum(:rate))
      end

      it "should return Events filtered by min and 10 below max days" do
        @filter_params[:min_days] = 0
        @filter_params[:max_days] = 9
        events = EventFilter.new(Event.upcoming, @filter_params ).result
        count = filtered_events(Event.upcoming, @filter_params).count
        expect(events.count).to eq(count)
        expect(events.count).to eq(4) # 4 upcoming events below 10 number_of_days created on user
        expect(events.first.number_of_days).to be_between(@filter_params[:min_days], @filter_params[:max_days])
      end

      it "should return Events filtered by min and 10 above max days" do
        @filter_params[:min_days] = 4
        @filter_params[:max_days] = 10
        events = EventFilter.new(Event.upcoming, @filter_params ).result
        count = filtered_events(Event.upcoming, @filter_params).count
        expect(events.count).to eq(count)
        expect(events.count).to eq(9) # 3 upcoming events between 4 and 9 number_of_days + 6 upcoming events above 10 number_of_days created on user
        expect(events.first.number_of_days).to be_between(@filter_params[:min_days], Event.maximum(:number_of_days))
      end

      it "should return Events filtered by category" do
        @filter_params[:category_ids] = Category.limit(5).order("RANDOM()").ids
        events = EventFilter.new(Event.upcoming, @filter_params ).result
        count = filtered_events(Event.upcoming, @filter_params).count
        expect(events.count).to eq(count)
        expect((events.first.categories.ids & @filter_params[:category_ids]).present?).to be_truthy
      end

      it "should return Events filtered by one category" do
        @filter_params[:category_id] = Category.last.id.to_s
        events = EventFilter.new(Event.upcoming,  @filter_params ).result
        count = filtered_events(Event.upcoming, @filter_params).count
        expect(events.count).to eq(count)
        expect((events.first.categories.ids && @filter_params[:category_id]).present?).to be_truthy
      end

      it "should return Events filtered by common places (destinations)" do
        @filter_params[:destination_ids] = Destination.limit(5).order("RANDOM()").ids
        events = EventFilter.new(Event.upcoming, @filter_params ).result
        count = filtered_events(Event.upcoming, @filter_params).count
        expect(events.count).to eq(count)
        expect((events.first.destinations.ids & @filter_params[:destination_ids]).present?).to be_truthy
      end

      it "should return Events filtered by common places (destination)" do
        @filter_params[:destination_id] = Destination.last.id.to_s
        events = EventFilter.new(Event.upcoming, @filter_params ).result
        count = filtered_events(Event.upcoming, @filter_params).count
        expect(events.count).to eq(count)
        expect((events.first.destinations.ids && @filter_params[:destination_id]).present?).to be_truthy
      end

      it "should return Events filtered by common places (destination) and one category" do
        @filter_params[:destination_id] = Destination.last.id.to_s
        @filter_params[:category_id] = Category.last.id.to_s
        events = EventFilter.new(Event.upcoming, @filter_params ).result
        count = filtered_events(Event.upcoming, @filter_params).count
        expect(events.count).to eq(count)
        expect((events.reload.first.destinations.ids && @filter_params[:destination_id]).present?).to be_truthy
        expect((events.first.categories.ids && @filter_params[:category_id]).present?).to be_truthy
      end

      it "should return Events filtered between start_month and end_month" do
        @filter_params[:start_month] = DateTime.now.strftime("%m/%Y").upcase
        @filter_params[:end_month] = (DateTime.now + 2.months).strftime("%m/%Y").upcase

        events = EventFilter.new(Event.upcoming, @filter_params ).result
        count = filtered_events(Event.upcoming, @filter_params).count
        expect(events.count).to eq(count)

        start_at = Time.strptime(@filter_params[:start_month], "%m/%Y").beginning_of_month
        end_month = Time.strptime(@filter_params[:end_month], "%m/%Y").end_of_month

        expect(events.first.start_at).to be_between(start_at,end_month)
      end

      it "should return Events filtered this month" do
        # create event for current month
        current_event = FactoryGirl.build(:event, start_at: DateTime.now, end_at: DateTime.now)
        current_event.save(validate: false)

        @filter_params[:start_month] = DateTime.now.strftime("%m/%Y").upcase
        @filter_params[:end_month] = DateTime.now.strftime("%m/%Y").upcase
        events = EventFilter.new(Event.not_deleted, @filter_params ).result
        count = filtered_events(Event.not_deleted, @filter_params).count
        expect(events.count).to eq(count)

        start_at = Time.strptime(@filter_params[:start_month], "%m/%Y").beginning_of_month
        end_month = Time.strptime(@filter_params[:end_month], "%m/%Y").end_of_month

        expect(events.order_by_start.first.start_at).to be_between(start_at,end_month)
      end

      it "should return Events filtered by possible filters" do
        @filter_params[:search] = "es"
        @filter_params[:start] = "desc"
        @filter_params[:min_cost] = 0
        @filter_params[:max_cost] = 9000
        @filter_params[:min_days] = 0
        @filter_params[:max_days] = 5
        @filter_params[:category_ids] = Category.limit(5).order("RANDOM()").ids
        @filter_params[:destination_ids] = Destination.limit(5).order("RANDOM()").ids

        @filter_params[:start_month] = DateTime.now.strftime("%m/%Y").upcase
        @filter_params[:end_month] = (DateTime.now + 2.months).strftime("%m/%Y").upcase

        events = EventFilter.new(Event.upcoming, @filter_params ).result
        count = filtered_events(Event.upcoming, @filter_params).count
        first_event = filtered_events(Event.upcoming, @filter_params).first
        second_event = filtered_events(Event.upcoming, @filter_params).second
        last_event = filtered_events(Event.upcoming, @filter_params).last
        expect(events.count).to eq(count)
        expect(events.first.name).to include(@filter_params[:search])
        expect(events.first).to eq(first_event)
        expect(events.second).to eq(second_event)
        expect(events.last).to eq(last_event)
        expect(events.first.rate.to_f).to be_between(@filter_params[:min_cost].to_f, @filter_params[:max_cost].to_f)
        expect(events.first.number_of_days).to be_between(@filter_params[:min_days], @filter_params[:max_days])
        expect((events.first.categories.ids & @filter_params[:category_ids]).present?).to be_truthy
        expect((events.first.destinations.ids & @filter_params[:destination_ids]).present?).to be_truthy

        start_at = Time.strptime(@filter_params[:start_month], "%m/%Y").beginning_of_month
        end_month = Time.strptime(@filter_params[:end_month], "%m/%Y").end_of_month

        expect(events.first.start_at).to be_between(start_at,end_month)
      end
    end

    context "History" do
      it "should return searched Events" do
        @filter_params[:search] = "es"
        events = EventFilter.new(Event.past, @filter_params ).result
        count = filtered_events(Event.past, @filter_params).count
        expect(events.count).to eq(count)
        expect(events.first.name).to include(@filter_params[:search])
      end

      it "should return Events sorted by asc" do
        @filter_params[:sort] = "asc"
        events = EventFilter.new(Event.past, @filter_params ).result
        first_event = filtered_events(Event.past, @filter_params).first
        second_event = filtered_events(Event.past, @filter_params).second
        last_event = filtered_events(Event.past, @filter_params).last
        expect(events.first).to eq(first_event)
        expect(events.second).to eq(second_event)
        expect(events.last).to eq(last_event)
      end

      it "should return Events sorted by desc" do
        @filter_params[:sort] = "desc"
        events = EventFilter.new(Event.past, @filter_params ).result
        first_event = filtered_events(Event.past, @filter_params).first
        second_event = filtered_events(Event.past, @filter_params).second
        last_event = filtered_events(Event.past, @filter_params).last
        expect(events.first).to eq(first_event)
        expect(events.second).to eq(second_event)
        expect(events.last).to eq(last_event)
      end

      it "should return Events filtered by min and 10000 below max cost" do
        @filter_params[:min_cost] = 0
        @filter_params[:max_cost] = 9000
        events = EventFilter.new(Event.past, @filter_params ).result
        count = filtered_events(Event.past, @filter_params).count
        expect(events.count).to eq(count)
        expect(events.count).to eq(4) # 4 past events below 10000 rate created on user
        expect(events.first.rate.to_f).to be_between(@filter_params[:min_cost].to_f, @filter_params[:max_cost].to_f)
      end

      it "should return Events filtered by min and 10000 above max cost" do
        @filter_params[:min_cost] = 4000
        @filter_params[:max_cost] = 10000
        events = EventFilter.new(Event.past, @filter_params ).result
        count = filtered_events(Event.past, @filter_params).count
        expect(events.count).to eq(count)
        expect(events.count).to eq(9) # 3 past events between 4000 and 9000 rate + 6 past events above 10000 rate created on user
        expect(events.first.rate.to_f).to be_between(@filter_params[:min_cost].to_f, Event.maximum(:rate))
      end

      it "should return Events filtered by min and 10 below max days" do
        @filter_params[:min_days] = 0
        @filter_params[:max_days] = 9
        events = EventFilter.new(Event.past, @filter_params ).result
        count = filtered_events(Event.past, @filter_params).count
        expect(events.count).to eq(count)
        expect(events.count).to eq(4) # 4 past events below 10 number_of_days created on user
        expect(events.first.number_of_days).to be_between(@filter_params[:min_days], @filter_params[:max_days])
      end

      it "should return Events filtered by min and 10 above max days" do
        @filter_params[:min_days] = 4
        @filter_params[:max_days] = 10
        events = EventFilter.new(Event.past, @filter_params ).result
        count = filtered_events(Event.past, @filter_params).count
        expect(events.count).to eq(count)
        expect(events.count).to eq(9) # 3 past events between 4 and 9 number_of_days + 6 past events above 10 number_of_days created on user
        expect(events.first.number_of_days).to be_between(@filter_params[:min_days], Event.maximum(:number_of_days))
      end

      it "should return Events filtered by category" do
        @filter_params[:category_ids] = Category.limit(5).order("RANDOM()").ids
        events = EventFilter.new(Event.past, @filter_params ).result
        count = filtered_events(Event.past, @filter_params).count
        expect(events.count).to eq(count)
        expect((events.first.categories.ids & @filter_params[:category_ids]).present?).to be_truthy
      end

      it "should return Events filtered by common places (destinations)" do
        @filter_params[:destination_ids] = Destination.limit(5).order("RANDOM()").ids
        events = EventFilter.new(Event.past, @filter_params ).result
        count = filtered_events(Event.past, @filter_params).count
        expect(events.count).to eq(count)
        expect((events.first.destinations.ids & @filter_params[:destination_ids]).present?).to be_truthy
      end

      it "should return Events filtered between start_month and end_month" do
        @filter_params[:start_month] = (DateTime.now - 2.months).strftime("%m/%Y").upcase
        @filter_params[:end_month] = DateTime.now.strftime("%m/%Y").upcase

        events = EventFilter.new(Event.past, @filter_params ).result
        count = filtered_events(Event.past, @filter_params).count
        expect(events.count).to eq(count)

        start_at = Time.strptime(@filter_params[:start_month], "%m/%Y").beginning_of_month
        end_month = Time.strptime(@filter_params[:end_month], "%m/%Y").end_of_month

        expect(events.first.start_at).to be_between(start_at,end_month)
      end

      it "should return featured Events" do
        @filter_params[:featured] = true

        events = EventFilter.new(Event.past, @filter_params ).result
        count = filtered_events(Event.past, @filter_params).count
        expect(events.count).to eq(count)
      end

      it "should return Events filtered by possible filters" do
        @filter_params[:search] = "es"
        @filter_params[:start] = "desc"
        @filter_params[:min_cost] = 0
        @filter_params[:max_cost] = 9000
        @filter_params[:min_days] = 0
        @filter_params[:max_days] = 5
        @filter_params[:category_ids] = Category.limit(5).order("RANDOM()").ids
        @filter_params[:destination_ids] = Destination.limit(5).order("RANDOM()").ids
        @filter_params[:start_month] = (DateTime.now - 2.months).strftime("%m/%Y").upcase
        @filter_params[:end_month] = DateTime.now.strftime("%m/%Y").upcase

        events = EventFilter.new(Event.past, @filter_params ).result
        count = filtered_events(Event.past, @filter_params).count
        first_event = filtered_events(Event.past, @filter_params).first
        second_event = filtered_events(Event.past, @filter_params).second
        last_event = filtered_events(Event.past, @filter_params).last
        expect(events.count).to eq(count)
        expect(events.first.name).to include(@filter_params[:search])
        expect(events.first).to eq(first_event)
        expect(events.second).to eq(second_event)
        expect(events.last).to eq(last_event)
        expect(events.first.rate.to_f).to be_between(@filter_params[:min_cost].to_f, @filter_params[:max_cost].to_f)
        expect(events.first.number_of_days).to be_between(@filter_params[:min_days], @filter_params[:max_days])
        expect((events.first.categories.ids & @filter_params[:category_ids]).present?).to be_truthy
        expect((events.first.destinations.ids & @filter_params[:destination_ids]).present?).to be_truthy

        start_at = Time.strptime(@filter_params[:start_month], "%m/%Y").beginning_of_month
        end_month = Time.strptime(@filter_params[:end_month], "%m/%Y").end_of_month

        expect(events.first.start_at).to be_between(start_at,end_month)
      end
    end
  end
end
