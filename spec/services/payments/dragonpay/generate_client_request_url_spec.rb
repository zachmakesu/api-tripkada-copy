require 'rails_helper'

RSpec.describe Payments::Dragonpay::GenerateClientRequestURL do
  let(:event){ create(:event) }
  let(:joiner){ create(:user) }
  let(:names){ "#{Faker::Name.name},#{Faker::Name.name},#{Faker::Name.name}" }
  let(:emails){
    (1..3).map.with_index(1) { |_x, i| "joiner#{i}@gmail.com" }
  }
  let!(:transaction_fee){ create(:dragonpay_transaction_fee) }

  describe "when generating client request url" do
    before(:each) do
      @params = {}
      @params[:invited_joiner_names] = names.split(",")
      @params[:invited_joiner_emails] = emails
    end
    it "should success" do
      @service = described_class.call(event: event, params: {}, joiner: joiner)
      expect(@service.success?).to be_truthy
    end
    it "should return generated url" do
      @service = described_class.call(event: event, params: {}, joiner: joiner)
      expect(@service.generated_request_url).to be_present
    end
    context "with invited joiners" do
      it "should set with invited joiners to true" do
        @service = described_class.call(event: event, params: @params, joiner: joiner)
        expect(@service.success?).to be_truthy
      end
    end
    context "with invalid invited joiner params" do
      before(:each) do
        @params[:invited_joiner_names] = %W[#{Faker::Name.name}]
        # Append Invalid Email
        @params[:invited_joiner_emails] << "test@example.com"
        @service = described_class.call(event: event, params: @params, joiner: joiner)
      end
      it "should not success" do
        expect(@service.success?).to be_falsey
      end
      it "should set return error message" do
        expect(@service.message).to be_present
      end
    end
  end

end
