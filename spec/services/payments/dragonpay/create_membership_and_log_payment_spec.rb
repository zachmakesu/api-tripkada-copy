require 'rails_helper'

RSpec.describe Payments::Dragonpay::CreateMembershipAndLogPayment do
  let(:user){ create(:user) }
  let(:event){ create(:event, user_id: user.id) }
  let!(:booking_fee){ create(:booking_fee) }
  let(:joiner){ create(:joiner) }
  let(:names){ ["#{Faker::Name.name}", "#{Faker::Name.name}", "#{Faker::Name.name}"] }
  let(:emails) do
    [
      "#{Faker::Internet.email}", "#{Faker::Internet.email}", "#{Faker::Internet.email}"
    ]
  end
  let(:params){
    {
      mode: "dragonpay",
      made: "downpayment",
      status: "S",
      txnid: SecureRandom.hex(3),
      refno: SecureRandom.hex(4),
      message: Faker::Lorem.word,
      type: "controller"
    }
  }

  describe "when creating membership and payment with dragonpay transaction" do
    context "without invited joiners" do
      it "should success with valid params" do
        @service = described_class.call(event: event, params: params, joiner: joiner)
        expect(@service).to be_success
      end
      it "should fail with invalid params" do
        invalid_params = params
        params[:mode] = nil
        @service = described_class.call(event: event, params: invalid_params, joiner: joiner)
        expect(@service).to be_failure
      end
    end
    context "with invited joiners" do
      before(:each) do
        @mod_params = params
        @mod_params[:invited_joiner_names] = names
        @mod_params[:invited_joiner_emails] = emails
        @service = described_class.call(event: event, params: @mod_params, joiner: joiner)
      end
      it "should success" do
        expect(@service.success?).to be_truthy
      end
      it "should set with invited joiners to true" do
        expect(@service.with_invited_joiners).to be_truthy
      end
      it "should return correct invited joiners count" do
        expect(@service.invited_joiners.flatten.count).to eq(names.count)
      end
    end
    context "with invalid invited joiner params" do
      before(:each) do
        @mod_params = params
        @mod_params[:invited_joiner_names] = names
        emails.pop # remove one email in email array
        @mod_params[:invited_joiner_emails] = emails
        @service = described_class.call(event: event, params: @mod_params, joiner: joiner)
      end
      it "should not success" do
        expect(@service.success?).to be_falsey
      end
      it "should set return error message" do
        expect(@service.message).to be_present
      end
    end
  end

end
