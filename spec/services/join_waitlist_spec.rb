require 'rails_helper'
require 'light-service/testing'

describe JoinWaitlist do

  let(:current_user){ create(:joiner) }
  let(:organizer){ create(:user) }
  let(:notification){ create(:notification, user_id: organizer.id) }
  let(:event){ create(:event, user_id: organizer.id) }
  let(:new_waitlist_params){
    {
      user: current_user,
      event: event
    }
  }

  context "Joining the waitlist" do
    it "should be success" do
      result = described_class.call(new_waitlist_params)
      waitlist = Waitlist.last
      expect(result).to be_success
      expect(event.waitlists.count).to eq(1)
      expect(waitlist.user).to eq(current_user)
      expect(notification.user).to eq(event.owner)
    end

    it "should not be success if already in the waitlist" do
      Waitlist.create(user_id: current_user.id, event_id: event.id)
      result = described_class.call(new_waitlist_params)
      expect(result).to_not be_success
      expect(event.waitlists.count).to eq(1)
      expect(result.message).to eq("User is already on the waitlist.")
    end
  end
end
