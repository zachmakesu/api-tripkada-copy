require 'rails_helper'

RSpec.describe Event::Booking::EnsureSufficientSlotsAndValidInviteeEmails do
  let(:event){ create(:event) }
  let(:joiner){ create(:joiner) }
  let(:emails){
    (1..3).map.with_index(1) { |_x, i| "joiner#{i}@gmail.com" }
  }
  let(:names){
    "#{Faker::Name.name},#{Faker::Name.name},"\
    "#{Faker::Name.name}"
  }

  describe "ensure joiners email are valid" do
    before do
      @params = {}
      @params[:invited_joiner_emails] = emails
      @params[:invited_joiner_names] = names.split(",")
    end

    context "without invited joiners" do
      context "with valid email" do
        it "returns success response" do
          service = described_class.call(
            event: event,
            joiner: joiner,
            params: {}
          )
          expect(service).to be_success
        end
      end
      context "with invalid email" do
        before do
          # Update joiner email to an invalid email
          joiner.update(email: "test123@example.com")
          @service = described_class.call(
            event: event,
            joiner: joiner,
            params: {}
          )
        end

        it "returns failure response" do
          expect(@service).to be_failure
        end

        it "returns error message" do
          error_message = "#{joiner.email} is not a valid email!,"\
                          " Please Provide Valid Email"
          expect(@service.message).to eq(error_message)
        end
      end
    end
    context "with invited joiners" do
      context "with valid invited joiners email" do
        it "returns success response" do
          service = described_class.call(
            event: event,
            joiner: joiner,
            params: @params
          )
          expect(service).to be_success
        end
      end
      context "with invalid invited joiners email" do
        context "without duplicate emails" do
          before do
            @params[:invited_joiner_names] << Faker::Name.name
            @params[:invited_joiner_emails] << "test@example.com"
            @service = described_class.call(
              event: event,
              joiner: joiner,
              params: @params
            )
          end

          it "returns failure response" do
            expect(@service).to be_failure
          end

          it "returns error message" do
            error_message = "#{@params[:invited_joiner_emails].last} is not a valid"\
                            " email!, Please Provide Valid Email"
            expect(@service.message).to eq(error_message)
          end
        end
        context "with duplicate emails" do
          before do
            @dup_email = emails.last
            @params[:invited_joiner_emails] << @dup_email
            @params[:invited_joiner_names] << Faker::Name.name
            @service = described_class.call(
              event: event,
              joiner: joiner,
              params: @params
            )
          end

          it "returns failure response" do
            expect(@service).to be_failure
          end

          it "returns error message" do
            error_message = "Please avoid duplicate emails! "\
                            "#{@dup_email}"
            expect(@service.message).to eq(error_message)
          end
        end
      end
    end
  end
  describe "when validating trip slots sufficiency" do
    before do
      @params = {}
      @params[:invited_joiner_emails] = emails
      @params[:invited_joiner_names] = names.split(",")
    end
    context "with sufficient slots" do
      before do
        event.update(min_pax: 5, max_pax: 10)
        @service = described_class.call(
          event: event,
          joiner: joiner,
          params: @params
        )
      end

      it "returns success response" do
        expect(@service).to be_success
      end
      it "set is_sufficient to true" do
        expect(@service.is_sufficient).to be_truthy
      end
    end
    context "with insufficient slots" do
      before do
        event.update(min_pax: 1, max_pax: 1)
        @service = described_class.call(
          event: event,
          joiner: joiner,
          params: @params
        )
      end

      it "should not success" do
        expect(@service).to be_failure
      end

      it "set is_sufficient to false" do
        expect(@service.is_sufficient).to be_falsey
      end
    end
  end
end
