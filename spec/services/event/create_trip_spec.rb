require 'rails_helper'

RSpec.describe Event::CreateTrip do
  let(:organizer){ create(:user) }
  let(:category){ create(:category) }
  let(:destination){ create(:destination) }
  let!(:event_params){
    {
      name: Faker::Address.street_name,
      start_at: DateTime.now + 1.days,
      end_at: DateTime.now + 3.days,
      rate: 500,
      downpayment_rate: 100,
      min_pax: 5,
      max_pax: 10,
      highlights: "#{Faker::StarWars.character};#{Faker::StarWars.character}",
      inclusions: "#{Faker::Superhero.power};#{Faker::Superhero.power}",
      tags: "tag1;tag2",
      things_to_bring: "bag, phone, etc.",
      meeting_place: Faker::Address.street_name,
      itineraries_description: [Faker::Address.street_address,Faker::Address.street_address],
      lat: Faker::Address.latitude,
      lng: Faker::Address.longitude,
      photos: {},
      category_ids: [category.id],
      destination_ids: [destination.id],
      draft: true.to_s
    }
  }

  context "when creating a draft trip" do
    it "should success" do
      result = described_class.call(organizer: organizer, params: event_params)
      expect(result).to be_success
    end

    it "should be save as draft" do
      result = described_class.call(organizer: organizer, params: event_params)
      draft_events = organizer.unpublished_events
      expect(draft_events).to include(result.event)
    end
  end

  context "when creating a published trip" do
    it "should success with valid params" do
      result = described_class.call(organizer: organizer, params: event_params)
      expect(result).to be_success
    end

    it "should be published" do
      mod_params = event_params
      mod_params[:draft] = false.to_s
      result = described_class.call(organizer: organizer, params: mod_params)
      organized_published_events = organizer.organized_events
      expect(organized_published_events).to include(result.event)
    end

    it "should return error message" do
      invalid_event_params = event_params
      invalid_event_params[:draft] = false.to_s
      invalid_event_params.delete(:destination_ids)
      result = described_class.call(organizer: organizer, params: invalid_event_params)
      expect(result.message).to eq("Please select a valid destination")
    end
  end

end
