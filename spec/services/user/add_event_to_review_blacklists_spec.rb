require 'rails_helper'

RSpec.describe User::AddEventToReviewBlacklists do
  let(:user){ create(:user) }
  let(:event){ create(:event) }

  context "when creating including event to review blacklists" do
    it "should success with valid params" do
      result = described_class.call(user: user, event: event)
      expect(result).to be_success
    end
  end
end
