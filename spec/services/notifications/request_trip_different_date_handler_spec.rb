require 'rails_helper'

RSpec.describe Notifications::RequestTripDifferentDateHandler do

  let!(:requester)            { create(:joiner) }
  let!(:organizer)               { create(:user) }
  let!(:event)                { create(:event, owner: organizer) }
  let!(:params){
    {
      start_date_time:  "#{event.start_at}",
      end_date_time: "#{event.end_at}",
      requested_pax: event.max_pax
    }
  }
  let!(:payload)              {
    {
      category:       Notification.categories[:trip_date_request],
      alert_message:  "mabuhay you got a trip request"\
                      " for your trip #{event.name.titleize}"\
                      " dated #{(DateTime.parse params[:start_date_time]).try(:strftime,"%B %d, %Y %I:%M %p") }"\
                      " to #{DateTime.parse(params[:end_date_time]).try(:strftime,"%B %d, %Y %I:%M %p") }"\
                      " for #{params[:requested_pax]} person."\
                      " Keep creating amazing trips like this!"\
                      " Congratulations!",
      event_id:       event.id,
      event_photo:    event.decorate.cover_photo_complete_url(:thumb),
      organizer_uid:  organizer.uid,
      member_uid:     requester.uid
    }
  }

  context "#deliver" do
    before(:each) do
      @handler = Notifications::RequestTripDifferentDateHandler.new(event: event, requester: requester, params: params)
    end
    it "should add notifications to the trip organizer" do

      expect(@handler.send(:payload)).to eq(payload)
      expect do
        allow_any_instance_of(FCMHandler).to receive(:deliver).and_return(nil)
        @handler.deliver
      end.to change{organizer.notifications.count}.from(0).to(1)
    end
    it "should set event as notificationable" do
      @handler.deliver
      notification = organizer.notifications.last
      expect(notification.notificationable).to eq(event)
    end
  end

end
