require 'rails_helper'

RSpec.describe Notifications::TripSlotReminderHandler do

  let!(:organizer)  { create(:user, role: 1) }
  let!(:event)      { create(:event, owner: organizer) }
  let!(:payload)    {
    {
      category:       Notification.categories[:slot_reminder],
      alert_message:  "You still have #{event.decorate.slots_left} slots for your #{event.name}. Time to share share share and promote! Let’s fill up those slots!",
      event_id:       event.id,
      event_photo:    event.decorate.cover_photo_complete_url(:thumb),
      organizer_uid:  organizer.uid
    }
  }

  context "#deliver" do
    before(:each) do
      @handler = Notifications::TripSlotReminderHandler.new(event: event)
    end
    it "should add notifications for event organizer" do
      expect(@handler.send(:payload)).to eq(payload)
      expect{
        allow_any_instance_of(FCMHandler).to receive(:deliver).and_return(nil)
        @handler.deliver
      }.to change{organizer.notifications.count}.from(0).to(1)
    end
    it "should set event as notificationable" do
      @handler.deliver
      notification = organizer.notifications.last
      expect(notification.notificationable).to eq(event)
    end
  end

end
