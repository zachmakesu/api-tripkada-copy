require 'rails_helper'

RSpec.describe Notifications::FollowerReminderHandler do

  let!(:organizer)  { create(:user, role: 1) }
  let!(:joiner)     { create(:user, role: 0) }
  let!(:event)      { create(:event, owner: organizer) }
  let!(:payload)    {
    {
      category:           Notification.categories[:follower_reminder],
      alert_message:      "#{organizer.full_name} invites you to join #{event.name}! Book a slot while you can!",
      event_id:           event.id,
      event_photo:        event.decorate.cover_photo_complete_url(:thumb),
      organizer_uid:      organizer.uid,
      follower_uid:       joiner.uid
    }
  }

  context "#deliver" do
    before(:each) do
      @handler = Notifications::FollowerReminderHandler.new(event: event, follower: joiner)
    end
    it "should add notifications for a specific member of event" do
      organizer.followers << joiner

      expect(@handler.send(:payload)).to eq(payload)
      expect{
        allow_any_instance_of(FCMHandler).to receive(:deliver).and_return(nil)
        @handler.deliver
      }.to change{joiner.notifications.count}.from(0).to(1)
    end

    it "should set event as notificationable" do
      @handler.deliver
      notification = joiner.notifications.last
      expect(notification.notificationable).to eq(event)
    end
  end

end
