require 'rails_helper'

RSpec.describe Notifications::ReferralIncentiveNotifierHandler do

  let!(:referrer)            { create(:user) }
  let!(:joiner)              { create(:joiner) }
  let!(:event)               { create(:event) }
  let!(:referral_incentive)  { create(:referral_incentive) }
  let!(:payload)             {
    {
      category:       Notification.categories[:referral_incentive],
      alert_message:  "Congratulations you have earned referral incentive from Tripkada for"\
      "success referral booking of joiner #{joiner.full_name} ."\
      "You will receive trip credits worth of P #{referral_incentive.value} .",
      event_id:        event.id,
      joiner_uid:      joiner.uid,
      joiner_photo:    joiner.decorate.avatar_complete_url(:thumb),
      referrer_uid:    referrer.uid,
    }
  }

  context "#deliver" do
    before(:each) do
      @handler = Notifications::ReferralIncentiveNotifierHandler.new(event: event, referrer: referrer, joiner: joiner)
    end
    it "should add notifications for referrer" do

      expect(@handler.send(:payload)).to eq(payload)
      expect{
        allow_any_instance_of(FCMHandler).to receive(:deliver).and_return(nil)
        @handler.deliver
      }.to change{referrer.notifications.count}.from(0).to(1)
    end
    it "should set event as notificationable" do
      @handler.deliver
      notification = referrer.notifications.last
      expect(notification.notificationable).to eq(event)
    end
  end

end
