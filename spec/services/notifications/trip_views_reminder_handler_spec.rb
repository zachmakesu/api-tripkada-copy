require "rails_helper"

RSpec.describe Notifications::TripViewsReminderHandler do
  let!(:organizer) { create(:user, role: 1) }
  let!(:event) { create(:event, owner: organizer) }
  let!(:payload) do
    {
      category:       Notification.categories[:trip_views_notification],
      alert_message:  "Your trip #{event.name} got a total of "\
                       "#{event.number_of_views} views! People are looking "\
                       "at your trips, share more to get more interests.",
      event_id:        event.id,
      event_photo:     event.decorate.cover_photo_complete_url(:thumb),
      organizer_uid:   organizer.uid
    }
  end

  context "#deliver" do
    before(:each) do
      @handler = Notifications::TripViewsReminderHandler.new(event: event)
    end

    it "should add notifications for event organizer" do
      expect(@handler.send(:payload)).to eq(payload)
      expect {
        allow_any_instance_of(FCMHandler).to receive(:deliver).and_return(nil)
        @handler.deliver
      }.to change { organizer.notifications.count }.from(0).to(1)
    end
    it "should set event as notificationable" do
      @handler.deliver
      notification = organizer.notifications.last
      expect(notification.notificationable).to eq(event)
    end
  end
end
