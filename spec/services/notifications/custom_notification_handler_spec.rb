require 'rails_helper'

RSpec.describe Notifications::CustomNotificationHandler do

  let!(:recipient)  { create(:user) }
  let(:custom_notification)  { create(:custom_notification_message) }
  let!(:payload)    {
    {
      category:           Notification.categories[:custom],
      alert_message:      custom_notification.message 
    }
  }

  context "#deliver" do
    before(:each) do
      @handler = described_class.new(custom_notification_id: custom_notification.id, message: payload[:alert_message], receiver: recipient)
    end
    it "should add notifications to recepient" do
      expect(@handler.send(:payload)).to eq(payload)
      expect{
        allow_any_instance_of(FCMHandler).to receive(:deliver).and_return(nil)
        @handler.deliver
      }.to change{recipient.notifications.count}.from(0).to(1)
    end

    it "should set custom notification as notificationable" do
      @handler.deliver
      notification = recipient.notifications.last
      expect(notification.notificationable).to eq(custom_notification)
    end
  end

end
