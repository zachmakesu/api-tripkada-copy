require 'rails_helper'

RSpec.describe Notifications::TripPaymentReminderHandler do

  let!(:organizer)            { create(:user, role: 1) }
  let!(:joiner)               { create(:user, role: 0) }
  let!(:event)                { create(:event, owner: organizer) }
  let!(:joiner_membership)    { create(:event_membership, event_id: event.id, member_id: joiner.id) }
  let!(:payload)              {
    {
      category:       Notification.categories[:payment_reminder],
      alert_message:  "Are you ready for your trip? Dont forget to"\
                      " bring the rest of your payment (if any )"\
                      " before heading to the meeting place.",
      event_id:       event.id,
      event_photo:    event.decorate.cover_photo_complete_url(:thumb),
      organizer_uid:  organizer.uid,
      member_uid:     joiner.uid
    }
  }

  context "#deliver" do
    before(:each) do
      @handler = Notifications::TripPaymentReminderHandler.new(event: event, member: joiner)
    end
    it "should add notifications for a specific member of event" do

      expect(@handler.send(:payload)).to eq(payload)
      expect{
        allow_any_instance_of(FCMHandler).to receive(:deliver).and_return(nil)
        @handler.deliver
      }.to change{joiner.notifications.count}.from(0).to(1)
    end
    it "should set event as notificationable" do
      @handler.deliver
      notification = joiner.notifications.last
      expect(notification.notificationable).to eq(event)
    end
  end

end
