require "rails_helper"

RSpec.describe Notifications::BookmarkedReminderHandler do
  let!(:user) { create(:user) }
  let!(:organizer) { create(:user, role: 1) }
  let!(:event) { create(:event, owner: organizer) }
  let!(:days) do
    (event.start_at.to_date - Time.zone.now.to_date).to_i
  end
  let!(:payload) do
    {
      category:       Notification.categories[:bookmarked_event_reminder],
      alert_message:  "Hey there traveler! Only #{days} #{'day'.pluralize(days)} left before #{event.name}, Book now!",
      event_id:       event.id,
      event_photo:    event.decorate.cover_photo_complete_url(:thumb),
      member_uid:     user.uid
    }
  end

  context "#deliver" do
    before(:each) do
      user.bookmarks.create(event_id: event.id)
      @handler = Notifications::BookmarkedReminderHandler.new(event: event, joiner: user)
    end

    it "should add notifications for user" do
      expect(@handler.send(:payload)).to eq(payload)
      expect {
        allow_any_instance_of(FCMHandler).to receive(:deliver).and_return(nil)
        @handler.deliver
      }.to change { user.notifications.count }.from(0).to(1)
    end
    it "should set event as notificationable" do
      @handler.deliver
      notification = user.notifications.last
      expect(notification.notificationable).to eq(event)
    end
  end
end
