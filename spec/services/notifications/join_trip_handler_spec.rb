require 'rails_helper'

RSpec.describe Notifications::JoinTripHandler do

  let!(:organizer)            { create(:user, role: 1) }
  let!(:joiner)               { create(:user, role: 0) }
  let!(:event)                { create(:event, owner: organizer) }
  let!(:joiner_membership)    { create(:event_membership, event_id: event.id, member_id: joiner.id) }
  let!(:promo_code)           { create(:promo_code, value: 1) }
  let!(:payment)              { joiner_membership.payment.decorate }
  let!(:payload)              {
    {
      category:           Notification.categories[:trip_join],
      alert_message:      "Congratulations! You just got a joiner! Let's Keep promoting and fill up those slots!",
      event_id:           event.id,
      joiner_uid:         joiner.uid,
      joiner_photo:       joiner.decorate.avatar_complete_url(:thumb),
      organizer_uid:      organizer.uid
    }
  }
  let!(:payload_keys)         {[:category,:alert_message,:event_id,:joiner_uid,:joiner_photo,:organizer_uid]}

  context "#deliver" do
    it "should add notifications for event organizer" do
      handler = Notifications::JoinTripHandler.new(membership: joiner_membership)

      expect(handler.send(:payload)).to eq(payload)
      expect{
        allow_any_instance_of(FCMHandler).to receive(:deliver).and_return(nil)
        handler.deliver
      }.to change{organizer.notifications.count}.from(0).to(1)
    end

    it "should validate payload of membership with promo code present" do
      payment.update(promo_code: promo_code)
      handler = Notifications::JoinTripHandler.new(membership: joiner_membership)

      expect(handler.send(:payload)[:alert_message]).to eq("Congratulations! You just got a joiner! Let’s keep promoting and fill up those slots! #{payment.promo_code} gives joiners a PHP #{payment.promo_code_value} discount.")
    end

    it "should validate payload of membership with promo code value that exceeds the trip rate" do
      joiner_membership.payment.update(promo_code: promo_code)
      joiner_membership.payment.promo_code.update(value: 5000000)
      handler = Notifications::JoinTripHandler.new(membership: joiner_membership)

      expect(handler.send(:payload)[:alert_message]).to eq("Good job! You just got a joiner using our promo: #{payment.promo_code} ")
    end

    it "should set event as notificationable" do
      Notifications::JoinTripHandler.new(membership: joiner_membership).deliver
      notification = organizer.notifications.last
      expect(notification.notificationable).to eq(event)
    end
  end

end
