require 'rails_helper'

RSpec.describe Referrals::SendInvitations do
  let!(:referrer) { create(:user) }
  let(:email_params){
    {
      emails: "#{Faker::Internet.email}, #{Faker::Internet.email}, #{Faker::Internet.email}"
    }
  }

  describe "referral Invitations" do
    context "when sending Invitations" do
      it "should be success" do
        service = described_class.call(params: email_params, referrer: referrer)
        expect(service.success?).to be_truthy
      end
      it "should return mailer response" do
        service = described_class.call(params: email_params, referrer: referrer)
        expect(service.mailer_response).to be_present
      end
    end
  end

end
