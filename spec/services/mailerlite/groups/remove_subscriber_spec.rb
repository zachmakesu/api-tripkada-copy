require 'rails_helper'

RSpec.describe Mailerlite::Groups::RemoveSubscriber do
  let(:user){ create(:user) }
  let(:mailerlite_default_group){ create(:mailerlite_group_default_existing) }

  context "when removing group subscriber" do
    # set static email and group id for vcr
    before(:each){ @email = "example@example.com"}
    it "should success" do
      service = remove_from_group(group_id: mailerlite_default_group.group_id, email: @email)
      expect(service.success?).to be_truthy
    end
    it "should fail" do
      service = remove_from_group_invalid(group_id: 123, email: @email)
      expect(service.success?).not_to be_truthy
    end
  end

end
