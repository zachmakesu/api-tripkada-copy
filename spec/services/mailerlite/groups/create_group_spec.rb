require 'rails_helper'

RSpec.describe Mailerlite::Groups::CreateGroup do
  context "when creating mailerlite group" do
    it "should success" do
      service = create_mailerlite_group_organizer(name: Faker::Lorem.word)
      expect(service.success?).to be_truthy
    end
    it "should return error message for invalid params" do
      service = create_mailerlite_group_organizer(name: nil)
      expect(service.message).to eq("Please Provide Group Name")
    end
  end

end
