require 'rails_helper'

RSpec.describe Mailerlite::Groups::ImportSubscribers do
  let(:subscribers){ create_list(:user, 3)}
  context "when importing mailerlite group subscribers" do
    it "should success" do
      group_id = 8472118
      service = import_mailerlite_group_subscribers_organizer(group_id: group_id, subscribers: subscribers)
      expect(service.success?).to be_truthy
    end
  end

end
