require 'rails_helper'

RSpec.describe Mailerlite::Groups::DeleteGroup do
  context "when deleting mailerlite group" do
    it "should success" do
      group_id = 8472778
      service = delete_mailerlite_group_organizer(group_id: group_id)
      expect(service.success?).to be_truthy
    end
    it "should return error message for invalid group_id" do
      service = described_class.call(group_id: Faker::Number.number(1))
      expect(service.message).to eq("Can't find requested items")
    end
  end

end
