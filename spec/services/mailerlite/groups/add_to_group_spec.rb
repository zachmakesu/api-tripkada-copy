require 'rails_helper'

RSpec.describe Mailerlite::Groups::AddToGroup do
  let(:user){ create(:user) }
  let(:mailerlite_default_group){ create(:mailerlite_group_default_existing) }

  context "when adding subscriber to a group" do
    it "should success" do
      mailerlite_default_group
      service = add_subscriber_to_group(subscriber: user, booking_count: user.approved_event_memberships.count)
      expect(service.success?).to be_truthy
    end
    it "should fail" do
      service = add_subscriber_to_group_invalid(subscriber: user, booking_count: user.approved_event_memberships.count)
      expect(service.success?).not_to be_truthy
    end
  end

end
