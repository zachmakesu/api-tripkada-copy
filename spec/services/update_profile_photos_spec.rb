require 'rails_helper'
require 'light-service/testing'

describe UpdateProfilePhotos do

  let(:current_user){ create(:user) }
  let(:photo_params){
    {
      avatar: { image: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg') },
      cover_photo: { image: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg') },
      profile_photos: [{
                        photo: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg'),
                        label: ""
                      }]
    }
  }

  context "Updating Profile Photos" do
    it "should be success" do
      result = described_class.call(user: current_user.decorate, params: photo_params)

      expect(result).to be_success
    end

    it "should not be success" do
      photo_params[:profile_photos] = nil
      photo_params[:avatar] = nil
      result = described_class.call(user: current_user.decorate, params: photo_params)

      expect(result).not_to be_success
      expect(result.message).to eq("Profile photos params is not present")
    end
  end
end
