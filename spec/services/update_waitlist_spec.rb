require 'rails_helper'
require 'light-service/testing'

describe UpdateWaitlist do

  let(:user){ create(:joiner) }
  let(:current_user){ create(:user) }
  let(:event){ create(:event, user_id: current_user.id) }
  let(:waitlist){ Waitlist.create(user_id: user.id, event_id: event.id, status: 0) }
  let(:approve_params){
    {
      params: {
                waitlist_action: "approve",
                waitlist_id: waitlist.id,
                id: event.id
              },
      current_user: current_user
    }
  }

  let(:deny_params){
    {
      params: {
                waitlist_action: "deny",
                waitlist_id: waitlist.id,
                id: event.id
              },
      current_user: current_user
    }
  }

  context "Authenticate" do
    it "should fail if unauthorized access" do
      approve_params[:current_user] = user
      result = described_class.call(approve_params)
      expect(result).to_not be_success
      expect(result.message).to eq("Unauthorized access!")
    end

    it "should be success" do
      result = described_class.call(approve_params)
      expect(result).to be_success
      expect(result.waitlist.approved?).to be_truthy
    end
  end

  context "Approve a waitlist" do
    it "should be valid" do
      result = described_class.call(approve_params)
      expect(result).to be_success
      expect(result.waitlist.approved?).to be_truthy
    end

    it "should not be valid if event is full" do
      event.update_attribute(:max_pax, 0)
      result = described_class.call(approve_params)
      expect(result).to_not be_success
      expect(result.waitlist.approved?).to be_falsey
    end

    it "should not be valid if user is already a joiner" do
      EventMembership.create(member_id: user.id, event_id: event.id)
      result = described_class.call(approve_params)
      expect(result).to_not be_success
      expect(result.waitlist.approved?).to be_falsey
    end
  end

  context "Deny a waitlist" do
    it "should be success" do
      result = described_class.call(deny_params)
      expect(result).to be_success
      expect(result.waitlist.denied?).to be_truthy
    end
  end

end
