require 'rails_helper'
require 'light-service/testing'

describe Actions::UpdateAvatar do

  let(:current_user){ create(:user) }
  let(:avatar_params){
    {
      avatar: { image: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg') }
    }
  }

  context "Updating Avatar" do
    it "should be success" do
      result = described_class.execute(user: current_user, params: avatar_params)

      expect(result).to be_success
    end

    it "should not be success" do
      avatar_params[:avatar] = nil
      result = described_class.execute(user: current_user, params: avatar_params)

      expect(result).not_to be_success
      expect(result.message).to eq("Avatar params is not present")
    end
  end
end
