require 'rails_helper'
require 'light-service/testing'

describe Actions::ParseRequestedDateTime do
  let(:requester){ create(:joiner) }

  let(:organizer){ create(:user) }

  let(:event){ create(:event) }

  let(:request_params){
    {
      id: event.id,
      start_date_time: "#{DateTime.now + 2.days}",
      end_date_time: "#{DateTime.now + 3.days}",
      requested_pax: Faker::Number.number(2)
    }
  }

  context "when validating requested trip date" do
    before (:each) do
      event.update(user_id: organizer.id)
    end

    it "should be success" do
      request = described_class.execute(params: request_params)
      expect(request).to be_success
    end
    it "should return error message" do
      invalid_request_params = request_params
      invalid_request_params[:end_date_time] = nil

      request = described_class.execute(params: invalid_request_params)
      expect(request.message).to eq("Invalid Date Time")
    end

  end
end

