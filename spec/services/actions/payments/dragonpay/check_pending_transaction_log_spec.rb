require 'rails_helper'

RSpec.describe Actions::Payments::Dragonpay::CheckPendingTransactionLog do
  let(:user){ create(:user) }
  let(:membership){ create(:event_membership) }
  let(:dragonpay_params){
    {
      transaction_id: "1",
      status: "pending",
      response: Faker::Lorem.word,
      reference_number: "123",
      message: Faker::Lorem.word
    }
  }

  describe "when checking pending transaction log" do
    before(:each) do
      @payment = membership.payment
      @payment.pending!
      @payment.dragonpay_transaction_logs.create(dragonpay_params)
    end
    context "when transaction exists" do
      before(:each) do
        @service = described_class.execute(params: { txnid: "1" })
      end
      it "should set transaction exists to true" do
        expect(@service.transaction_exists).to be_truthy
      end
      it "should set pending transaction log" do
        expect(@service.pending_transaction_log).to be_present
      end
      it "should set payment" do
        expect(@service.payment).to be_present
      end
    end
    context "when transaction not exists" do
      before(:each) do
        @service = described_class.execute(params: { txnid: "2" })
      end
      it "should set transaction exists to false" do
        expect(@service.transaction_exists).to be_falsey
      end
      it "should set pending transaction log to nil" do
        expect(@service.pending_transaction_log).to be_nil
      end
      it "should set payment to nil" do
        expect(@service.payment).to be_nil
      end
    end
  end

end
