require 'rails_helper'

RSpec.describe Actions::Payments::Dragonpay::LogTransaction do
  let(:membership){ create(:event_membership) }
  let(:params){
    {
      txnid: "1",
      status: "pending",
      refno: "123",
      message: Faker::Lorem.word
    }
  }

  describe "when creating transaction log" do
    before(:each) do
      @payment = membership.payment
    end

    context "with valid params" do
      it "should success" do
        service = described_class.execute(payment: @payment, params: params, status: "approved", approved_payment: membership.payment)
        expect(service).to be_success
      end
    end

    context "with invalid params" do
      it "should raise error" do
        invalid_params = params
        invalid_params[:txnid] = nil
        expect{
          described_class.execute(payment: @payment, params: invalid_params, status: "approved", approved_payment: membership.payment)
        }.to raise_error(LightService::FailWithRollbackError)
      end
    end
  end

end
