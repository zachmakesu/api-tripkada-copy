require 'rails_helper'

RSpec.describe Actions::Payments::Dragonpay::LessPromoCodeAmount do
  let(:event){ create(:event) }
  let(:promo_code){ create(:promo_code) }

  describe "dragonpay transaction with promo code" do
    context "when promo code is present and rate is equal to downpayment" do
      it "should less promo code amount in total amount" do
        event.update(rate: 100, downpayment_rate: 100)
        service = described_class.execute(event: event, total_amount: event.rate, promo_code: promo_code)
        sub_total = event.rate - promo_code.value
        expect(service.total_amount_less_promo_code).to eq(sub_total)
      end
    end
    context "when promo code is not present and rate is not equal to downpayment" do
      it "should skip context when promo code is not present" do
        service = described_class.execute(event: event, total_amount: event.rate, promo_code: nil)
        expect(service.total_amount_less_promo_code).to be_nil
      end
      it "should skip context when rate is not equal to downpayment" do
        event.update(rate: 300, downpayment_rate: 100)
        service = described_class.execute(event: event, total_amount: event.rate, promo_code: nil)
        expect(service.total_amount_less_promo_code).to be_nil
      end
    end
  end
end
