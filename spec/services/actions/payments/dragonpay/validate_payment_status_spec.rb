require 'rails_helper'

RSpec.describe Actions::Payments::Dragonpay::ValidatePaymentStatus do
  describe "when validating payment status" do
    let(:params){
      {
        status: "S"
      }
    }
    it "should success" do
      service = described_class.execute(params: params)
      expect(service).to be_success
    end
    it "should set status to approved" do
      service = described_class.execute(params: params)
      expect(service.status).to eq("approved")
    end
    it "should set status to pending" do
      mod_params = params
      mod_params[:status] = "P"
      service = described_class.execute(params: mod_params)
      expect(service.status).to eq("pending")
    end
    it "should set status to declined" do
      mod_params = params
      mod_params[:status] = "F"
      service = described_class.execute(params: mod_params)
      expect(service.status).to eq("declined")
    end
  end

end
