require 'rails_helper'

RSpec.describe Actions::Payments::Dragonpay::CalculateTotalAmount do
  let(:event){ create(:event) }
  let!(:transaction_fee){ create(:dragonpay_transaction_fee) }

  describe "when validating calculating total amount" do
    context "without invited joiners" do
      it "should return correct amount" do
        downpayment = event.decorate.downpayment_rate_with_fee
        service = described_class.execute(event: event, with_invited_joiners: false, invited_joiners_count: 0)
        total = downpayment + transaction_fee.amount
        expect(service.total_amount).to eq(total)
      end
    end
    context "with invited joiners" do
      it "should return correct amount" do
        downpayment = event.decorate.downpayment_rate_with_fee
        joiner_count = 3 
        calculated_amount = downpayment * (joiner_count + 1)
        total = calculated_amount + transaction_fee.amount

        service = described_class.execute(event: event, with_invited_joiners: true, invited_joiners_count: joiner_count)
        expect(service.total_amount).to eq(total)
      end
    end
  end

end
