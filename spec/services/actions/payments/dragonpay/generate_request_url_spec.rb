require 'rails_helper'

RSpec.describe Actions::Payments::Dragonpay::GenerateRequestURL do
  let(:joiner){ create(:user) }
  let(:event){ create(:event) }
  let(:names){ "#{Faker::Name.name},#{Faker::Name.name},#{Faker::Name.name}"}
  let(:emails){
    "#{Faker::Lorem.characters(10)}@tripkada.com,"\
    "#{Faker::Lorem.characters(10)}@tripkada.com,"\
    "#{Faker::Lorem.characters(10)}@tripkada.com"
  }

  describe "when generating dragonpay request url" do
    context "generate default params" do
      before(:each) do
        @params = { code: Faker::Name.name }
        @service = described_class.execute(
          total_amount: Faker::Number.number(3),
          joiner: joiner, params: @params,
          event: event,
          total_amount_less_promo_code: Faker::Number.number(3)
        )
      end
      it "should return client generated request url" do
        expect(@service.generated_request_url).to be_present
      end
    end
  end

end
