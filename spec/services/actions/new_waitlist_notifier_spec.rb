require 'rails_helper'
require 'light-service/testing'

describe Actions::NewWaitlistNotifier do

  let(:current_user){ create(:joiner) }
  let(:notification){ create(:notification, user_id: organizer.id) }
  let(:organizer){ create(:user) }
  let(:event){ create(:event, user_id: organizer.id) }
  let(:new_waitlist_params){
    {
      user: current_user,
      event: event
    }
  }

  context "Sending notification" do
    it "should be success" do
      result = described_class.execute(new_waitlist_params)
      expect(result).to be_success
      expect(notification.user).to eq(event.owner)
    end
  end
end
