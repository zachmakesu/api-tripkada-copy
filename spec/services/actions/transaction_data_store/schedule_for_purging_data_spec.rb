require 'rails_helper'
require 'light-service/testing'

describe Actions::TransactionDataStore::ScheduleForPurgingData do
  context "When purging Transaction Data" do
    before(:each) do
      transaction = TransactionDataStore.create(transaction_id: Faker::Number.number(3), data: "")
      @service = described_class.execute(transaction_data: transaction)
    end
    it "should be success" do
      expect(@service).to be_success
    end

    it "should return response" do
      expect(@service.transaction_data_cleaner_response).to be_present
    end
  end
end
