require 'rails_helper'
require 'light-service/testing'

describe Actions::TransactionDataStore::RecordTransactionData do
  context "When creating transaction data" do
    it "should be success with valid params" do
      service = described_class.execute(transaction_id: Faker::Number.number(3), params: "")
      expect(service).to be_success
    end

    it "should not success if params are invalid" do
      service = described_class.execute(transaction_id: "", params: {})
      expect(service).to be_failure
    end
  end
end
