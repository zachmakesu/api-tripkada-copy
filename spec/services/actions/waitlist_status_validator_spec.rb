require 'rails_helper'
require 'light-service/testing'

describe Actions::WaitlistStatusValidator do

  let(:user){ create(:joiner) }
  let(:event){ create(:event) }
  let(:waitlist){ Waitlist.create(user_id: user.id, event_id: event.id, status: 0) }
  let(:approve_params){
    {
      user: user,
      event: event,
      waitlist: waitlist,
      event_membership: nil,
      params: { waitlist_action: "approve" }
    }
  }

  let(:deny_params){
    {
      user: user,
      event: event,
      waitlist: waitlist,
      event_membership: nil,
      params: { waitlist_action: "deny" }
    }
  }

  context "Approve a waitlist" do
    it "should be valid" do
      result = described_class.execute(approve_params)
      expect(result).to be_success
      expect(result.waitlist.approved?).to be_truthy

    end

    it "should not be valid if event is full" do
      event.max_pax = 0
      result = described_class.execute(approve_params)
      expect(result).to_not be_success
      expect(result.waitlist.approved?).to be_falsey
    end
  end

  context "Deny a waitlist" do
    it "should be success" do
      result = described_class.execute(deny_params)
      expect(result).to be_success
      expect(result.waitlist.denied?).to be_truthy
    end
  end

end
