require "rails_helper"

RSpec.describe Actions::User::ReferralIncentiveMailer do
  let(:joiner) { create(:joiner) }
  let(:referrer) { create(:user) }
  let(:event)    { create(:event) }
  context "when sending mailer" do
    it "should return success message" do
      handler = described_class.execute(joiner: joiner, referrer: referrer, event: event)
      expect(handler.mailer_response).to eq("Referral Mailer Successfully Sent")
    end
    it "should return error message" do
      handler = described_class.execute(joiner: joiner, referrer: nil, event: event)
      expect(handler.mailer_response).to eq("Please provide all parameters")
    end
  end
end
