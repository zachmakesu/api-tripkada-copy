require "rails_helper"

RSpec.describe Actions::User::FindReferrer do
  let(:current_user) { create(:joiner) }
  let(:referrer) { create(:user) }
  let(:referral_params){
    {
      user_id: referrer.id,
      joiner_id: current_user.id
    }
  }

  context "when finding referrer" do
    it "should find referrer" do
      Referral.create(referral_params)
      handler = described_class.execute(joiner: current_user)
      expect(handler.referrer).not_to be_nil
    end
    it "should not find referrer" do
      handler = described_class.execute(joiner: current_user)
      expect(handler.referrer).to be_nil
    end
  end
end
