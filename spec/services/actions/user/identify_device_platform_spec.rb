require "rails_helper"

RSpec.describe Actions::User::IdentifyDevicePlatform do
  let(:user) { create(:user) }
  context "when identifying user device platform" do
    before(:each) do
      @service = described_class.execute(params: { type: "controller" })
    end

    it "should success" do
      expect(@service).to be_success
    end
    it "should set web as default device platform" do
      expect(@service.device_platform).to eq("desktop")
    end
  end
  context "when identifying user device platform for android and ios" do
    it "should set device platform as android" do
      android = "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B)"\
                " AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19"
      service = described_class.execute(params: { user_agent: android, type: "controller" })
      expect(service.device_platform).to eq("android_web")
    end
    it "should set device platform as ios" do
      ios = "Mozilla/5.0 (iPhone; CPU iPhone OS 8_3 like Mac OS X)"\
             " AppleWebKit/600.1.4 (KHTML, like Gecko) FxiOS/1.0 Mobile/12F69 Safari/600.1.4"
      service = described_class.execute(params: { user_agent: ios, type: "controller" })
      expect(service.device_platform).to eq("ios_web")
    end
    it "should set device platform as web" do
      web = "Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0"
      service = described_class.execute(params: { user_agent: web, type: "controller" })
      expect(service.device_platform).to eq("desktop")
    end
  end
end
