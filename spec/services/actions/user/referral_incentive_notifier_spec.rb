require "rails_helper"

RSpec.describe Actions::User::ReferralIncentiveNotifier do
  let(:joiner) { create(:joiner) }
  let(:referrer) { create(:user) }
  let(:event)    { create(:event) }
  context "when creating notification" do
    it "should return success message" do
      handler = described_class.execute(joiner: joiner, referrer: referrer, event: event)
      expect(handler.notifier_response).to eq("Referral Incentive Notification Successfully Created")
    end
    it "should return error message" do
      handler = described_class.execute(joiner: joiner, referrer: nil, event: event)
      expect(handler.notifier_response).to eq("Please provide all parameters")
    end
  end
end

