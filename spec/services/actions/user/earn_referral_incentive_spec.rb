require "rails_helper"

RSpec.describe Actions::User::EarnReferralIncentive do
  let(:current_user) { create(:joiner) }
  let(:referrer) { create(:user) }
  let(:referral_incentive){ create(:referral_incentive) }
  let(:referral_params){
    {
      user_id: referrer.id,
      joiner_id: current_user.id
    }
  }

  context "when updating referrers credit promo code" do
    it "should update referrers credit value" do
      referral_incentive
      Referral.create(referral_params)
      described_class.execute(referrer: referrer)
      expect(referrer.credit_promo_code.credit_logs.earned_incentive.last.amount).to eq(referral_incentive.value)
    end
  end
end
