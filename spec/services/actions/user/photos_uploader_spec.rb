require "rails_helper"

RSpec.describe Actions::User::PhotosUploader do
  let!(:current_user) { create(:user) }
  let(:image) {
    Rack::Test::UploadedFile.new(
      Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg'
    )
  }
  let(:invalid_attachment)  {
    Rack::Test::UploadedFile.new(
      Rails.root.join('spec/fixtures/files/sample.pdf'), 'application/pdf'
    )
  }
  let(:photo_labels){ Faker::Lorem.words(2)}

  context "when uploading photos" do
    before (:each) do
      @params = {}
      @params[:photos] = []
      @params[:photos][0] = build_attachment(image: image)
      @params[:photos][1] = build_attachment(image: image)
      @params[:photo_labels] = photo_labels
    end

    it "should successfully create photos" do
      params = Actions::PhotosBuilder.execute(params: @params, user: current_user)
      handler = described_class.execute(user: current_user, photos: params.photos, category: "certificates")
      expect(handler).to be_success
    end
    it "should not create invalid photos" do
      invalid_params = @params
      invalid_params[:photos][0] = build_attachment(image: invalid_attachment)
      params = Actions::PhotosBuilder.execute(params: invalid_params, user: current_user)
      handler = described_class.execute(user: current_user, photos: params.photos, category: "certificates")
      expect(handler).not_to be_success
    end
    it "should not create photos with invalid category" do
      invalid_params = @params
      invalid_params[:photos][0] = build_attachment(image: invalid_attachment)
      params = Actions::PhotosBuilder.execute(params: invalid_params, user: current_user)
      handler = described_class.execute(user: current_user, photos: params.photos, category: Faker::Lorem.word)
      expect(handler).not_to be_success
    end
    it "should return error message for invalid category" do
      invalid_params = @params
      invalid_params[:photos][0] = build_attachment(image: invalid_attachment)
      params = Actions::PhotosBuilder.execute(params: invalid_params, user: current_user)
      handler = described_class.execute(user: current_user, photos: params.photos, category: Faker::Lorem.word)
      expect(handler.message).to eq("Invalid Photo Category")
    end
  end
end
