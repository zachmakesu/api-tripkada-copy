require "rails_helper"

RSpec.describe Actions::User::PhotoMailer do
  let(:user) { create(:user) }
  context "when sending mailer for photo verification" do
    it "should return success message" do
      handler = described_class.execute(user: user, category: "verification_photos", photos: [])
      expect(handler.mailer_response).to eq("Photo Verification Email for Admin Successfully Sent")
    end
  end
end
