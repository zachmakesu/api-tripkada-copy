require 'rails_helper'

RSpec.describe Actions::User::AddEventToReviewBlacklists do
  let(:user){ create(:user) }
  let(:event){ create(:event) }

  context "when creating including event to review blacklists" do
    it "should success with valid params" do
      result = described_class.execute(user: user, event: event)
      expect(result).to be_success
    end
    it "should successfully include event in review blacklist" do
      result = described_class.execute(user: user, event: event)
      expect(user.review_blacklists.last.event).to eq(result.event)
    end
    it "should not success if event is already in the blacklist" do
      user.review_blacklists.create(event_id: event.id)
      result = described_class.execute(user: user, event: event)
      expect(result).not_to be_success
    end
  end
end
