require 'rails_helper'

RSpec.describe Actions::Event::Organizer::UpdateJoinerPaymentStatus do
  let(:event){ create(:event) }
  let(:joiner){ create(:joiner) }
  let(:booking_fee){ create(:booking_fee) }

  describe "Update Joiner Payment Status" do
    before(:each) do
      @membership = event.event_memberships.create(member_id: joiner.id)
      @payment = @membership.build_payment(
        mode: "dragonpay", 
        made: "downpayment", 
        status: "approved", 
        booking_fee_id: booking_fee.id
      )
      @payment.save
    end
    context "when updating payment status to declined" do
      before(:each) do
        @service = described_class.execute(event: event, membership: @membership, status: "declined")
      end
      it "it should successfully update status to declined" do
        expect(@service).to be_success
      end
      it "it should set payment status to declined" do
        expect(@service.updated_payment.status).to eq("declined")
      end
    end
    context "when updating payment status with invalid status" do
      it "it should successfully update status to declined" do
        service = described_class.execute(event: event, membership: @membership, status: Faker::Lorem.word)
        expect(service).to be_failure
      end
    end
  end
end
