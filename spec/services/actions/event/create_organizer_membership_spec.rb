require 'rails_helper'

RSpec.describe Actions::Event::CreateOrganizerMembership do
  let(:organizer){ create(:user) }
  let!(:event_params){
    {
      name: Faker::Address.street_name,
      start_at: DateTime.now + 1.days,
      end_at: DateTime.now + 3.days,
      rate: 500,
      downpayment_rate: 100,
      min_pax: 5,
      max_pax: 10,
      highlights: "#{Faker::StarWars.character};#{Faker::StarWars.character}",
      inclusions: "#{Faker::Superhero.power};#{Faker::Superhero.power}",
      tags: "tag1;tag2",
      things_to_bring: "bag, phone, etc.",
      meeting_place: Faker::Address.street_name,
      itineraries_description: [Faker::Address.street_address,Faker::Address.street_address],
      lat: Faker::Address.latitude,
      lng: Faker::Address.longitude,
      photos: {}
    }
  }

  context "when creating membership for event organizer" do
    before(:each) do
      @params = Actions::Event::BuildOrganizerMembershipParams.execute(
        organizer: organizer
      )
      @event = Actions::Event::BuildEventParams.execute(
        params: event_params, organizer: organizer, set_as_draft: false
      )
      handler = Actions::Event::CreateEvent.execute(
        event_params: @event.event_params, photos_params: nil,
        organizer: organizer, set_as_draft: false
      )
      @event = handler.event
    end
    it "should success with valid params" do
      result = described_class.execute(
        event: @event, organizer: organizer, join_params: @params.join_params,
        organizer_payment_params: @params.organizer_payment_params
      )
      expect(result).to be_success
    end

    it "should set organizer as event owner" do
      expect(@event.owner).to eq(organizer)
    end

    it "should raise rollback if params are invalid" do
      invalid_payment_params = @params.organizer_payment_params
      invalid_payment_params[:mode] = nil
      expect{
        described_class.execute(
          event: @event, organizer: organizer, join_params: @params.join_params,
          organizer_payment_params: invalid_payment_params
        )
      }.to raise_error(LightService::FailWithRollbackError)
    end
  end

end
