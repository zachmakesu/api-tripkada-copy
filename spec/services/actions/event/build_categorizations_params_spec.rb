require 'rails_helper'

RSpec.describe Actions::Event::BuildCategorizationsParams do
  let(:event){ create(:event) }
  let(:categories){ create_list(:category, 3) }
  let(:category_params){
    {
      category_ids: categories.map(&:id)
    }
  }

  context "when building categorizations params" do
    before(:each) do
      @expected_params = build_categorizations_params(
        event: event, categorizations_ids: categories.map(&:id)
      )
    end

    it "should build correct params" do
      params = described_class.execute(
        category_ids: category_params[:category_ids], event: event
      )
      expect(params.categorizations_params).to eq(@expected_params)
    end

    it "should not build pararms if categorizations already exist" do
      event.categorizations.create(@expected_params)

      new_categorizations_ids = FactoryGirl.create_list(:category, 2).map(&:id)
      with_existing_categorizations = new_categorizations_ids + categories.map(&:id)
      build_with_categorizations = described_class.execute(
        category_ids: with_existing_categorizations.flatten, event: event
      )

      expected_params = build_categorizations_params(event: event, categorizations_ids: with_existing_categorizations)
      expect(build_with_categorizations.categorizations_params).to eq(expected_params)
      expect(build_with_categorizations.categorizations_params.map{|x| x[:category_id] }).to eq(new_categorizations_ids)
    end
  end

end
