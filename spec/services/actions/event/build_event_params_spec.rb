require 'rails_helper'

RSpec.describe Actions::Event::BuildEventParams do
  let(:organizer){ create(:user) }
  let!(:event_params){
    {
      name: Faker::Address.street_name,
      start_at: DateTime.now + 1.days,
      end_at: DateTime.now + 3.days,
      rate: 500,
      downpayment_rate: 100,
      min_pax: 5,
      max_pax: 10,
      highlights: "#{Faker::StarWars.character};#{Faker::StarWars.character}",
      inclusions: "#{Faker::Superhero.power};#{Faker::Superhero.power}",
      tags: "tag1;tag2",
      things_to_bring: "bag, phone, etc.",
      meeting_place: Faker::Address.street_name,
      itineraries_description: [Faker::Address.street_address,Faker::Address.street_address],
      lat: Faker::Address.latitude,
      lng: Faker::Address.longitude,
      photos: {}
    }
  }

  context "when building event params" do
    it "should build event published params" do
      result = described_class.execute(params: event_params, set_as_draft: false)
      expect(result.event_params[:published_at]).not_to be_nil
    end

    it "should build draft event params" do
      draft_event_params = event_params
      result = described_class.execute(params: draft_event_params, set_as_draft: true)
      expect(result.event_params[:published_at]).to be_nil
    end
  end

end
