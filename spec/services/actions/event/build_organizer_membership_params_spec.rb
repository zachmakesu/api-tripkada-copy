require 'rails_helper'

RSpec.describe Actions::Event::BuildOrganizerMembershipParams do
  let(:organizer){ create(:user) }
  let(:event){ create(:event) }

  context "when building membership for event organizer" do
    before(:each) do
      @params = described_class.execute(organizer: organizer)
    end
    it "should set payment mode as orgnizer" do
      expect(@params.organizer_payment_params[:mode]).to eq("organizer")
    end

    it "should set payment made as full payment" do
      expect(@params.organizer_payment_params[:made]).to eq("fullpayment")
    end

    it "should set membership to organzier" do
      expect(@params.join_params[:member]).to eq(organizer) 
    end
  end

end
