require 'rails_helper'

RSpec.describe Actions::Event::ValidateDraftStatus do

  context "when validating draft status" do
    it "should return true" do
      result = described_class.execute(params: { draft: "true" } )
      expect(result.set_as_draft).to be_truthy
    end

    it "should return false" do
      result = described_class.execute(params: { draft: "false" } )
      expect(result.set_as_draft).to be_falsey
    end
  end

end
