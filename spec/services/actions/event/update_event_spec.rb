require 'rails_helper'

RSpec.describe Actions::Event::UpdateEvent do
  let(:organizer){ create(:user) }
  let(:category){ create(:category) }
  let(:destination){ create(:destination) }
  let!(:event_params){
    {
      name: Faker::Address.street_name,
      start_at: DateTime.now + 1.days,
      end_at: DateTime.now + 3.days,
      rate: 500,
      downpayment_rate: 100,
      min_pax: 5,
      max_pax: 10,
      highlights: "#{Faker::StarWars.character};#{Faker::StarWars.character}",
      inclusions: "#{Faker::Superhero.power};#{Faker::Superhero.power}",
      tags: "tag1;tag2",
      things_to_bring: "bag, phone, etc.",
      meeting_place: Faker::Address.street_name,
      itineraries_description: [Faker::Address.street_address,Faker::Address.street_address],
      lat: Faker::Address.latitude,
      lng: Faker::Address.longitude,
      photos: {},
      category_ids: [category.id],
      destination_ids: [destination.id]
    }
  }

  context "when updating event" do
    before(:each) do
      @params = Actions::Event::BuildEventParams.execute(organizer: organizer, params: event_params, set_as_draft: false)
      result = Event::CreateTrip.call(organizer: organizer, params: event_params)
      @draft_event = result.event
    end
    it "should update event" do
      result = described_class.execute(event_params: @params.event_params, organizer: organizer, event: @draft_event)
      expect(result).to be_success
    end

    it "should return error message" do
      invalid_params = @params.event_params
      invalid_params[:name] = nil
      result = described_class.execute(event_params: invalid_params, organizer: organizer, event: @draft_event)
      expect(result.message).to eq("Name can't be blank")
    end

    it "should skip validation when event is draft" do
      invalid_params = @params.event_params
      invalid_params[:name] = nil
      invalid_params[:published_at] = nil
      result = described_class.execute(event_params: invalid_params, organizer: organizer, event: @draft_event)
      expect(result).to be_success
    end
  end

end
