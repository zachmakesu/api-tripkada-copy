require 'rails_helper'

RSpec.describe Actions::Event::CreateItineraries do
  let(:event){ create(:event) }
  let(:organizer){ create(:user) }
  let(:itinerary_params){
    {
      itineraries_description: [Faker::Address.street_address,Faker::Address.street_address]
    }
  }

  context "when updating itineraries" do
    before(:each) do
      event.itineraries.destroy_all
      @params = Actions::Event::BuildItinerariesParams.execute(params: itinerary_params)
    end
    it "should success with valid params" do
      result = described_class.execute(event: event, itineraries_params: @params.itineraries_params)
      expect(result).to be_success
    end

    it "should update event itineraries" do
      result = described_class.execute(event: event, itineraries_params: @params.itineraries_params)
      expect(event.itineraries).not_to be_empty
    end

    it "should return error message" do
      result = described_class.execute(event: event, itineraries_params: nil)
      expect(event.itineraries).to be_empty
    end
  end

end
