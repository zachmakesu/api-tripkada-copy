require 'rails_helper'

RSpec.describe Actions::Event::CreateEvent do
  let(:organizer){ create(:user) }
  let!(:event_params){
    {
      name: Faker::Address.street_name,
      start_at: DateTime.now + 1.days,
      end_at: DateTime.now + 3.days,
      rate: 500,
      downpayment_rate: 100,
      min_pax: 5,
      max_pax: 10,
      highlights: "#{Faker::StarWars.character};#{Faker::StarWars.character}",
      inclusions: "#{Faker::Superhero.power};#{Faker::Superhero.power}",
      tags: "tag1;tag2",
      things_to_bring: "bag, phone, etc.",
      meeting_place: Faker::Address.street_name,
      itineraries_description: [Faker::Address.street_address,Faker::Address.street_address],
      lat: Faker::Address.latitude,
      lng: Faker::Address.longitude,
      photos: {}
    }
  }

  context "when creating event" do
    before(:each) do
      @event = Actions::Event::BuildEventParams.execute(params: event_params, set_as_draft: false)
    end
    it "should create published event with valid params" do
      result = described_class.execute(event_params: @event.event_params, organizer: organizer)
      expect(result).to be_success
    end

    it "should return error message if event is not drafts" do
      invalid_params = @event.event_params
      invalid_params[:name] = nil
      result = described_class.execute(event_params: invalid_params, organizer: organizer)
      expect(result.message).to eq("Name can't be blank")
    end

    it "should not run validations for drafts" do
      invalid_params = @event.event_params
      invalid_params[:name] = nil
      invalid_params[:published_at] = nil
      result = described_class.execute(event_params: invalid_params, organizer: organizer)
      expect(result).to be_success
    end
  end

end
