require 'rails_helper'

RSpec.describe Actions::Event::BuildItinerariesParams do
  context "when building Itineraries params" do
    before (:each) do
      @itineraries_descriptions = []
      @itineraries_descriptions[0] = Faker::Lorem.word,
      @itineraries_descriptions[1] = Faker::Lorem.word
    end

    it "should build correct params" do
      expected_params = @itineraries_descriptions.reject(&:empty?).each_with_index.map do |x,i|
        { description: @itineraries_descriptions[i] }
      end
      params = described_class.execute(
        params: { itineraries_description: @itineraries_descriptions }
      )
      expect(params.itineraries_params).to eq(expected_params)
    end

    it "should return nil if params are nil" do
      params = described_class.execute(params: {})
      expect(params.itineraries_params).to be_nil
    end
  end

end
