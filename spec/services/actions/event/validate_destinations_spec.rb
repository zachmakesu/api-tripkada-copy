require 'rails_helper'

RSpec.describe Actions::Event::ValidateDestinations do
  let(:destination){ create(:destination) }
  let(:destination_params){
    {
      destination_ids: [destination.id],
    }
  }

  context "when validating destinations" do
    it "should success with valid params for published event" do
      result = described_class.execute(params: destination_params, set_as_draft: false)
      expect(result).to be_success
    end

    it "should skip validation for draft event" do
      invalid_params = destination_params
      invalid_params[:destination_ids] = []
      result = described_class.execute(params: invalid_params, set_as_draft: true)
      expect(result).to be_success
    end

    it "should return error message if event is published" do
      invalid_params = destination_params
      invalid_params[:destination_ids] = nil
      result = described_class.execute(params: invalid_params, set_as_draft: false)
      expect(result.message).to eq("Please select a valid destination")
    end
  end

end
