require 'rails_helper'
require 'light-service/testing'

RSpec.describe Actions::Event::CreatePhotos do
  let(:category){ create(:category) }
  let(:destination){ create(:destination) }
  let(:organizer){ create(:user) }
  let!(:event_params){
    {
      name: Faker::Address.street_name,
      start_at: DateTime.now + 1.days,
      end_at: DateTime.now + 3.days,
      rate: 500,
      downpayment_rate: 100,
      min_pax: 5,
      max_pax: 10,
      highlights: "#{Faker::StarWars.character};#{Faker::StarWars.character}",
      inclusions: "#{Faker::Superhero.power};#{Faker::Superhero.power}",
      tags: "tag1;tag2",
      things_to_bring: "bag, phone, etc.",
      meeting_place: Faker::Address.street_name,
      itineraries_description: [Faker::Address.street_address,Faker::Address.street_address],
      lat: Faker::Address.latitude,
      lng: Faker::Address.longitude,
      photos: {},
      category_ids: [category.id],
      destination_ids: [destination.id]
    }
  }

  let(:valid_image) {
    Rack::Test::UploadedFile.new(
      Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg'
    )
  }

  let(:invalid_image) {
    Rack::Test::UploadedFile.new(
      Rails.root.join('spec/fixtures/files/200x200.png'), 'image/jpg'
    )
  }

  context "when creating event photos" do
    before(:each) do
      result = Event::CreateTrip.call(organizer: organizer, params: event_params)
      @event = result.event
    end
    it "should success with valid photos" do
      valid_photos = []
      valid_photos[0] = build_attachment(image: valid_image)
      valid_photos[1] = build_attachment(image: valid_image)
      photos_params = {}
      photos_params[:photos] = valid_photos
      photos = Actions::Event::BuildPhotosParams.execute(params: photos_params)

      result = described_class.execute(event: @event, photos_params: photos.photos_params)
      expect(result).to be_success
    end

    it "should raise rollback error if photos are invalid" do
      invalid_photos = []
      photos_params = {}
      invalid_photos[0] = build_attachment(image: valid_image)
      invalid_photos[1] = build_attachment(image: invalid_image)
      photos_params[:photos] = invalid_photos
      photos = Actions::Event::BuildPhotosParams.execute(params: photos_params)
      expect{
        described_class.execute(event: @event, photos_params: photos.photos_params)
      }.to raise_error(LightService::FailWithRollbackError)
    end
  end
end
