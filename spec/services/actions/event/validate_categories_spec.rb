require 'rails_helper'

RSpec.describe Actions::Event::ValidateCategories do
  let(:category){ create(:category) }
  let(:category_params){
    {
      category_ids: [category.id],
    }
  }

  context "when validating categories" do
    it "should success with valid params for published event" do
      result = described_class.execute(params: category_params, set_as_draft: false)
      expect(result).to be_success
    end

    it "should return error message for published event" do
      invalid_params = category_params
      invalid_params[:category_ids] = []
      result = described_class.execute(params: invalid_params, set_as_draft: true)
      expect(result).to be_success
    end

    it "should return error message for published event" do
      invalid_params = category_params
      invalid_params[:category_ids] = nil
      result = described_class.execute(params: invalid_params, set_as_draft: false)
      expect(result.message).to eq("Please select a valid category")
    end
  end

end
