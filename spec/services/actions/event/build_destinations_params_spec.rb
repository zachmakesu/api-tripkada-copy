require 'rails_helper'

RSpec.describe Actions::Event::BuildDestinationsParams do
  let(:event){ create(:event) }
  let(:destinations){ create_list(:destination, 3) }
  let(:destination_params){
    {
      destination_ids: destinations.map(&:id)
    }
  }

  context "when building sorting destiantions params" do
    before(:each) do
      @expected_params = build_destinations_params(
        event: event, destinations_ids: destinations.map(&:id)
      )
    end

    it "should build correct params" do
      params = described_class.execute(
        destination_ids: destination_params[:destination_ids], event: event
      )
      expect(params.sorting_destinations_params).to eq(@expected_params)
    end

    it "should not build pararms if destinations already exist" do
      event.sorting_destinations.create(@expected_params)

      new_destinations_ids = FactoryGirl.create_list(:destination, 2).map(&:id)
      with_existing_destinations = new_destinations_ids + destinations.map(&:id)
      build_with_destinations = described_class.execute(
        destination_ids: with_existing_destinations.flatten, event: event
      )

      expected_params = build_destinations_params(event: event, destinations_ids: with_existing_destinations)
      expect(build_with_destinations.sorting_destinations_params).to eq(expected_params)
      expect(build_with_destinations.sorting_destinations_params.map{|x| x[:destination_id] }).to eq(new_destinations_ids)
    end
  end

end
