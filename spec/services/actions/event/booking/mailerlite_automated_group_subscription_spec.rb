require 'rails_helper'

RSpec.describe Actions::Event::Booking::MailerliteAutomatedGroupSubscription do
  let(:membership){ create(:event_membership) }
  let(:joiner){ create(:user) }

  describe "when creating executing automated group subscription" do
    it "should success" do
      service = described_class.execute(membership: membership, joiner: joiner, approved_payment: membership.payment)
      expect(service).to be_success
    end
  end

end
