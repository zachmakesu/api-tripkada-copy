require "rails_helper"

RSpec.describe Actions::Event::Booking::EnsureJoinerEmailNotUseForInvitedJoiners do
  let(:event) { create(:event) }
  let(:joiner) { create(:joiner) }
  let(:emails) { (1..3).map.with_index(1) { |_x, i| "joiner#{i}@gmail.com" } }

  describe "ensure joiner email not included in invited joiners email" do
    context "with valid invited joiners email" do
      it "returns success response" do
        service = described_class.execute(
          with_invited_joiners: false,
          joiner: joiner,
          invited_joiner_emails: emails
        )
        expect(service).to be_success
      end
    end
    context "with invalid invited joiners email" do
      before do
        # Add joiner email on invited joiner email lists
        emails << joiner.email
        @service = described_class.execute(
          with_invited_joiners: true,
          joiner: joiner,
          invited_joiner_emails: emails
        )
      end

      it "returns failure response" do
        expect(@service).to be_failure
      end

      it "returns error message" do
        error_message = "#{joiner.email} cannot be used for invited joiners"
        expect(@service.message).to eq(error_message)
      end
    end
  end
end
