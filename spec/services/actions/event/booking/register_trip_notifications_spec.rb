require 'rails_helper'

RSpec.describe Actions::Event::Booking::RegisterTripNotifications do
  let(:membership){ create(:event_membership) }
  let(:event){ create(:event) }

  describe "when executing trip notifications" do
    context "with approved payment" do
      it "should success" do
        service = described_class.execute(membership: membership, event: event, approved_payment: membership.payment)
        expect(service).to be_success
      end
    end
    context "with unapproved payment" do
      it "should skip context" do
        membership.payment.pending!
        service = described_class.execute(membership: membership, event: event, approved_payment: membership.payment)
        expect(service.trip_notifications_response).to be_empty
      end
    end
  end

end
