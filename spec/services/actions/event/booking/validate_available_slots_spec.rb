require 'rails_helper'

RSpec.describe Actions::Event::Booking::ValidateAvailableSlots do
  let(:event){ create(:event) }

  describe "when validating trip slots sufficiency" do
    context "with sufficient slots" do
      before(:each) do
        event.update(min_pax: 5, max_pax: 10)
        @service = described_class.execute(event: event, with_invited_joiners: true, invited_joiners_count: 4, transaction_exists: false)
      end

      it "should success" do
        expect(@service.success?).to be_truthy
      end
      it "should set response to true" do
        expect(@service.is_sufficient).to be_truthy
      end
    end
    context "with insufficient slots" do
      before(:each) do
        event.update(min_pax: 1, max_pax: 2)
        @service = described_class.execute(event: event, with_invited_joiners: true, invited_joiners_count: 4, transaction_exists: false)
      end

      it "should not success" do
        expect(@service.success?).to be_falsey
      end
      it "should set response to true" do
        expect(@service.is_sufficient).to be_falsey
      end
    end
  end

end
