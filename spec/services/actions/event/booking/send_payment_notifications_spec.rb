require 'rails_helper'

RSpec.describe Actions::Event::Booking::SendPaymentNotifications do
  let(:membership){ create(:event_membership) }

  describe "when sending payment notifications" do
    context "with approved payment" do
      it "should success" do
        service = described_class.execute(membership: membership, approved_payment: membership.payment)
        expect(service).to be_success
      end
    end
    context "with unapproved payment" do
      it "should skip context" do
        membership.payment.pending!
        service = described_class.execute(membership: membership, approved_payment: membership.payment)
        expect(service.payment_notification_response).to be_empty
      end
    end
  end

end
