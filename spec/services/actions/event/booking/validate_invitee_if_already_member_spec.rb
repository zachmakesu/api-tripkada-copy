require 'rails_helper'

RSpec.describe Actions::Event::Booking::ValidateInviteesIfAlreadyMember do
  let(:event) { create(:event) }
  let!(:current_user) { create(:user) }

  let!(:join_params) do
    {
      member: current_user,
      event: event
    }
  end

  describe "when validating invited joiners" do
    context "invited_joiner already a trip member" do
      before do
        EventMembership.create!(join_params)
        @params = {}
        @params[:invited_joiner_names] = %w[sample12345]
        @params[:invited_joiner_emails] = %W[#{current_user.email}]
        @service = described_class.execute(
          with_invited_joiners: true,
          event: event,
          invited_joiner_emails: @params[:invited_joiner_emails]
        )
      end
      it "should set invitee_is_already_a_trip_member to be true" do
        expect(@service.invitee_is_already_trip_member).to be_truthy
      end
      it "returns error message" do
        error_message = "#{current_user.email} is/are already member/s for this trip"
        expect(@service.message).to eq(error_message)
      end
      it "should fail" do
        expect(@service).to be_failure
      end
    end
    context "invited_joiner not a member of the trip" do
      before do
        @params = {}
        @params[:invited_joiner_names] = %w[sample12345]
        @params[:invited_joiner_emails] = %w[testforvalidation@yahoo.com]
        @service = described_class.execute(
          with_invited_joiners: true,
          event: event,
          invited_joiner_emails: @params[:invited_joiner_emails]
        )
      end
      it "should set invitee_is_already_a_trip_member to be false" do
        expect(@service.invitee_is_already_trip_member).to be_falsey
      end
      it "should be successful" do
        expect(@service).to be_success
      end
    end
  end
end
