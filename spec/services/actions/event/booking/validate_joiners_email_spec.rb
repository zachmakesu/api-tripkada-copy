require 'rails_helper'

RSpec.describe Actions::Event::Booking::ValidateJoinersEmail do
  let(:event){ create(:event) }
  let(:joiner){ create(:joiner) }
  # Ensure Generated Emails Are Valid
  let(:emails){
    (1..3).map.with_index(1) { |_x, i| "joiner#{i}@gmail.com" }
  }

  describe "when validating joiners email" do
    context "without invited joiners" do
      context "with valid email" do
        it "returns success response" do
          service = described_class.execute(
            with_invited_joiners: false,
            joiner: joiner,
            invited_joiner_emails: []
          )
          expect(service).to be_success
        end
      end
      context "with invalid email" do
        before do
          joiner.update(email: "test123@example.com")
          @service = described_class.execute(
            with_invited_joiners: false,
            joiner: joiner,
            invited_joiner_emails: []
          )
        end

        it "returns failure response" do
          expect(@service).to be_failure
        end

        it "returns error message" do
          error_message = "#{joiner.email} is not a valid email!,"\
                          " Please Provide Valid Email"
          expect(@service.message).to eq(error_message)
        end
      end
    end
    context "with invited joiners" do
      context "with valid email" do
        it "returns success response" do
          service = described_class.execute(
            with_invited_joiners: true,
            joiner: joiner,
            invited_joiner_emails: emails
          )
          expect(service).to be_success
        end
      end
      context "with invalid email" do
        context "without duplicate emails" do
          before do
            joiner.update(email: "test123@example.com")
            emails << joiner.email
            @service = described_class.execute(
              with_invited_joiners: true,
              joiner: joiner,
              invited_joiner_emails: emails
            )
          end

          it "returns failure response" do
            expect(@service).to be_failure
          end

          it "returns error message" do
            error_message = "#{joiner.email} is not a valid email!,"\
                            " Please Provide Valid Email"
            expect(@service.message).to eq(error_message)
          end
          context "with email @facebook.com" do
            before do
              joiner.update(email: "test123@facebook.com")
              emails << joiner.email
              @service = described_class.execute(
                with_invited_joiners: true,
                joiner: joiner,
                invited_joiner_emails: emails
              )
            end

            it "returns failure response" do
              expect(@service).to be_failure
            end

            it "returns error message" do
              error_message = "#{joiner.email} is not a valid email!,"\
                              " Please Provide Valid Email"
              expect(@service.message).to eq(error_message)
            end
          end
        end
        context "with duplicate emails" do
          before do
            @dup_email = emails.last
            emails << @dup_email
            @service = described_class.execute(
              with_invited_joiners: true,
              joiner: joiner,
              invited_joiner_emails: emails.flatten
            )
          end

          it "returns failure response" do
            expect(@service).to be_failure
          end

          it "returns error message" do
            error_message = "Please avoid duplicate emails! "\
                            "#{@dup_email}"
            expect(@service.message).to eq(error_message)
          end
        end
      end
    end
  end
end
