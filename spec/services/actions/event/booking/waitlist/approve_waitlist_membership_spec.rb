require 'rails_helper'

RSpec.describe Actions::Event::Booking::Waitlist::ApproveWaitlistMembership do
  let(:event){ create(:event) }
  let(:user){ create(:user) }

  describe "when updating waitlist membership" do
    context "with approved waitlits membership" do
      before(:each) do
        @waitlits = event.waitlists.create(user: user, status: "approved")
        @service = described_class.execute(waitlist: @waitlits, is_waitlist_member: true)
      end

      it "should successfully update waitlits status" do
        expect(@service).to be_success
      end
      it "should set waitlist response" do
        expect(@service.waitlist_response).to be_present
      end
      it "should set waitlist status to paid" do
        expect(@service.waitlist.status).to eq("paid")
      end
    end
    context "with unapproved waitlits membership" do
      before(:each) do
        @waitlits = event.waitlists.create(user: user, status: "denied")
      end

      it "should raise error" do
      expect{
        described_class.execute(waitlist: @waitlits, is_waitlist_member: true)
      }.to raise_error(LightService::FailWithRollbackError)
      end
    end
  end

end
