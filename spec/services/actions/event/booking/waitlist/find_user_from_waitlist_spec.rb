require 'rails_helper'

RSpec.describe Actions::Event::Booking::Waitlist::FindUserFromWaitlists do
  let(:event){ create(:event) }
  let(:user){ create(:user) }

  describe "when finding user from event waitlists" do
    context "with valid waitlist membership" do
      before(:each) do
        event.waitlists.create(user: user, status: "approved")
        @service = described_class.execute(event: event, joiner: user)
      end
      it "should success" do
        expect(@service).to be_success
      end
      it "should set waitlist" do
        expect(@service.waitlist).to be_present
      end
      it "should set is waitlist member o true" do
        expect(@service.is_waitlist_member).to be_truthy
      end
    end
    context "with invalid waitlist membership" do
      before(:each) do
        @service = described_class.execute(event: event, joiner: user)
      end
      it "should set waitlist to nil" do
        expect(@service.waitlist).to be_nil
      end
      it "should set is waitlist member o false" do
        expect(@service.is_waitlist_member).to be_falsey
      end
    end

  end

end
