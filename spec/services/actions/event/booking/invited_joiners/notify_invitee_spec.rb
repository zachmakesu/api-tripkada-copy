require 'rails_helper'

RSpec.describe Actions::Event::Booking::InvitedJoiners::NotifyInvitee do
  let(:memberships){ create_list(:event_membership, 2) }
  let(:user){ create(:user) }
  let(:joiner){ create(:joiner) }
  let(:event){ create(:event, user_id: user.id) }

  describe "when notifying invitee" do
    context "with memberships" do
      it "should success" do
        service = described_class.execute(memberships: memberships, joiner: joiner, event: event)
        expect(service).to be_success
      end
    end
    context "without memberships" do
      it "should skip context" do
        service = described_class.execute(memberships: [], joiner: joiner, event: event)
        expect(service.invitee_notifier_response).to be_empty
      end
    end
  end

end
