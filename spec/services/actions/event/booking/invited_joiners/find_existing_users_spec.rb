require 'rails_helper'

RSpec.describe Actions::Event::Booking::InvitedJoiners::FindExistingUsers do
  let(:invited_joiners){ create_list(:joiner, 2)}
  let(:invited_joiners_details){
    [
      { name: invited_joiners.first.first_name, email: invited_joiners.first.email },
      { name: invited_joiners.last.first_name, email: invited_joiners.last.email },
      { name: Faker::Name.name , email: Faker::Internet.email },
    ]
  }
  describe "when finding users" do
    context "with invited joiners" do
      before(:each) do
        @service = described_class.execute(invited_joiners_details: invited_joiners_details, with_invited_joiners: true)
      end
      it "should find existing users" do
        expect(@service.existing_users).to be_present
      end
      it "should set new users details" do
        expect(@service.new_users_details).to be_present
      end
    end
    context "without invited joiners" do
      before(:each) do
        @service = described_class.execute(invited_joiners_details: invited_joiners_details, with_invited_joiners: false)
      end
      it "should set existing users to empty" do
        expect(@service.existing_users).to be_empty
      end
      it "should set new user details to empty" do
        expect(@service.new_users_details).to be_empty
      end
    end
  end

end
