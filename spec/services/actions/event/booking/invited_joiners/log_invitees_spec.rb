require 'rails_helper'

RSpec.describe Actions::Event::Booking::InvitedJoiners::LogInvitees do
  let(:joiner){ create(:user)}
  let(:invited_joiners){ create_list(:joiner, 2)}
  describe "when registering invited joiners as joiner invitees" do
    context "with invited joiners" do
      before(:each) do
        @service = described_class.execute(invited_joiners: invited_joiners, joiner: joiner)
      end
      it "should success" do
        expect(@service).to be_success
      end
      it "should set invitations" do
        expect(@service.invitations).to be_present
      end
    end
    context "without invited joiners" do
      it "should set invitations to empty" do
        service = described_class.execute(invited_joiners: [], joiner: joiner)
        expect(service.invitations).to be_empty
      end
    end
  end

end
