require 'rails_helper'

RSpec.describe Actions::Event::Booking::InvitedJoiners::ApprovePayments do
  let(:event_memberships){ create_list(:event_membership, 2) }

  describe "when approving payment status" do
    before(:each) do
      event_memberships.each do |m|
        m.payment.pending!
      end
    end
    context "when memberships exists" do
      before(:each) do
        @service = described_class.execute(invited_joiners_memberships: event_memberships)
      end
      it "should successfully update payment status" do
        expect(@service).to be_success
      end
      it "should set payment status to approved" do
        expect(@service.approved_invited_joiners_payments.first.status).to eq("approved")
        expect(@service.approved_invited_joiners_payments.last.status).to eq("approved")
      end
    end
    context "when memberships not exists" do
      before(:each) do
        @service = described_class.execute(invited_joiners_memberships: [])
      end
      it "should set approved_invited_joiners_payments to nil" do
        expect(@service.approved_invited_joiners_payments).to be_empty
      end
    end
  end

end
