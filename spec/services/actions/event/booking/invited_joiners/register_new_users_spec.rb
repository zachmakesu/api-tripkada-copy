require 'rails_helper'

RSpec.describe Actions::Event::Booking::InvitedJoiners::RegisterNewUsers do
  let(:new_users_details){
    [
      { name: Faker::Name.name , email: Faker::Internet.email },
      { name: Faker::Name.name , email: Faker::Internet.email },
      { name: Faker::Name.name , email: Faker::Internet.email },
    ]
  }
  describe "when creating new users" do
    context "with new users details" do
      before(:each) do
        @service = described_class.execute(new_users_details: new_users_details)
      end
      it "should success" do
        expect(@service).to be_success
      end
      it "should set new users" do
        expect(@service.new_users).to be_present
      end
    end
    context "without new_users_details" do
      before(:each) do
        @service = described_class.execute(new_users_details: [])
      end
      it "should set existing users to empty" do
        expect(@service.new_users).to be_empty
      end
    end
  end

end
