require 'rails_helper'

RSpec.describe Actions::Event::Booking::InvitedJoiners::RunMailerliteAutomatedGroupSubscription do
  let(:memberships){ create_list(:event_membership, 2) }

  describe "when creating executing automated group subscription" do
    it "should success" do
      service = described_class.execute(memberships: memberships)
      expect(service).to be_success
    end
  end

end
