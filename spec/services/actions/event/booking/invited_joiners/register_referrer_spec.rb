require 'rails_helper'

RSpec.describe Actions::Event::Booking::InvitedJoiners::RegisterReferrer do
  let(:joiner) { create(:user) }
  let(:new_users) { create_list(:joiner, 2) }

  describe "when creating new users referrer" do
    context "with new users" do
      before(:each) do
        @service = described_class.execute(new_users: new_users, joiner: joiner)
      end
      it "should success" do
        expect(@service).to be_success
      end
      it "should set referrals" do
        expect(@service.referrals).to be_present
      end
    end
    context "without new users" do
      before(:each) do
        @service = described_class.execute(new_users: [], joiner: joiner)
      end
      it "should set referrals users to empty" do
        expect(@service.referrals).to be_empty
      end
    end
  end

end
