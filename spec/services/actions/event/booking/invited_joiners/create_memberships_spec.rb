require 'rails_helper'

RSpec.describe Actions::Event::Booking::InvitedJoiners::CreateMemberships do
  let(:event){ create(:event) }
  let(:joiners){ create_list(:user, 2) }

  describe "when creating memberships" do
    context "with invited joiners" do
      before(:each) do
        @service = described_class.execute(event: event, existing_users: joiners, with_invited_joiners: true, new_users: [])
      end
      it "should successfully create membership" do
        expect(@service).to be_success
      end
      it "should return membership" do
        expect(@service.memberships).to be_present
      end
      it "should raise error" do
        event.update(max_pax: 0, min_pax: 0)
        @failure_service = described_class.execute(event: event, existing_users: joiners, with_invited_joiners: true, new_users: [])
        expect(@failure_service).to be_failure
        expect(@failure_service.message).to be_present
      end
    end
    context "without invited joiners" do
      before(:each) do
        @service = described_class.execute(event: nil, existing_users: joiners, with_invited_joiners: false, new_users: [])
      end
      it "should not create membership" do
        expect(@service.memberships).to be_empty
      end
    end
  end

end
