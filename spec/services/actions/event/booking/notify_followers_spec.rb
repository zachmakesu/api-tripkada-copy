require 'rails_helper'

RSpec.describe Actions::Event::Booking::NotifyFollowers do
  let(:membership){ create(:event_membership) }
  let(:user){ create(:user) }
  let(:joiner){ create(:joiner) }
  let(:event){ create(:event, user_id: user.id) }

  describe "when notifying followers" do
    context "with approved payment" do
      it "should success" do
        service = described_class.execute(membership: membership, joiner: joiner, event: event, approved_payment: membership.payment)
        expect(service).to be_success
      end
    end
    context "with unapproved payment" do
      it "should skip context" do
        membership.payment.pending!
        service = described_class.execute(membership: membership, joiner: joiner, event: event, approved_payment: membership.payment)
        expect(service.follower_notification_response).to be_empty
      end
    end
  end

end
