require 'rails_helper'

RSpec.describe Actions::Event::Booking::ApprovePayment do
  let(:event_membership){ create(:event_membership) }

  describe "when approving payment status" do
    context "when transaction exists" do
      before(:each) do
        @payment = event_membership.payment
        @payment.pending!
        @service = described_class.execute(payment: @payment, transaction_exists: true, status: "approved")
      end
      it "should successfully update payment status" do
        expect(@service).to be_success
      end
      it "should set payment status to approved" do
        expect(@service.approved_payment.status).to eq("approved")
      end
    end
    context "when transaction not exists" do
      before(:each) do
        @payment = event_membership.payment
        @service = described_class.execute(payment: @payment, transaction_exists: false, status: "approved")
      end
      it "should set approved_payment to nil" do
        expect(@service.approved_payment).to be_nil
      end
      it "should set payment status to approved" do
        expect(@service.membership).to be_nil
      end
    end
  end

end
