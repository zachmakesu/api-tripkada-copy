require 'rails_helper'

RSpec.describe Actions::Event::Booking::CreatePayment do
  let(:event){ create(:event) }
  let(:joiner){ create(:user) }
  let!(:booking_fee){ create(:booking_fee) }
  let(:params){
    {
      mode: "dragonpay",
      made: "downpayment"
    }
  }

  describe "when creating payment" do
    before(:each) do
      @membership = joiner.event_memberships.create(event_id: event.id)
    end
    context "when transaction not exists" do
      before(:each) do
        @service = described_class.execute(params: params, status: "approved", device_platform: "desktop", transaction_exists: false, membership: @membership)
      end
      it "should successfully create payment" do
        expect(@service).to be_success
      end
      it "should return payment" do
        expect(@service.payment).to be_present
      end
    end
    context "when transaction exists" do
      before(:each) do
        @service = described_class.execute(params: params, status: "approved", device_platform: "desktop", transaction_exists: true, membership: @membership)
      end
      it "should not create payment" do
        expect(@service.payment).to be_nil
      end
    end
    context "with invalid params" do
      it "should raise error" do
        params[:mode] = nil
        @failure_service = described_class.execute(params: params, status: "approved", device_platform: "desktop", transaction_exists: false, membership: @membership)
        expect(@failure_service).to be_failure
        expect(@failure_service.message).to be_present
      end
    end
  end

end
