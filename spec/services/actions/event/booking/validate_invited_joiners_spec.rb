require 'rails_helper'

RSpec.describe Actions::Event::Booking::ValidateInvitedJoiners do
  let(:event){ create(:event) }
  let(:names){ "#{Faker::Name.name},#{Faker::Name.name},#{Faker::Name.name}"}
  let(:emails){ "#{Faker::Internet.email},#{Faker::Internet.email},#{Faker::Internet.email}"}

  describe "when validating invited joiners" do
    context "with invited joiners" do
      before(:each) do
        @params = {}
        @params[:invited_joiner_names] = names.split(",")
        @params[:invited_joiner_emails] = emails.split(",")
        @service = described_class.execute(event: event, params: @params, transaction_exists: false)
      end
      it "should set with invited joiners to true" do
        expect(@service.with_invited_joiners).to be_truthy
      end
      it "should return correct invited joiners count" do
        invited_joiners_count = names.split(',').count
        expect(@service.invited_joiners_count).to eq(invited_joiners_count)
      end
    end
    context "without invited joiners" do
      before(:each) do
        @service = described_class.execute(event: event, params: {}, transaction_exists: false)
      end
      it "should set with invited joiners to false" do
        expect(@service.with_invited_joiners).to be_falsey
      end
      it "should set invited joiners count to 0" do
        expect(@service.invited_joiners_count).to eq(0)
      end
    end
    context "with invalid invited joiner params" do
      before(:each) do
        @params = {}
        @params[:invited_joiner_emails] = emails.split(",")
        @params[:invited_joiner_names] = %W[#{Faker::Name.name}]
        @service = described_class.execute(event: event, params: @params, transaction_exists: false)
      end
      it "should not success" do
        expect(@service.success?).to be_falsey
      end
      it "should set return error message" do
        expect(@service.message).to be_present
      end
    end
  end

end
