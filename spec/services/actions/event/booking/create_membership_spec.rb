require 'rails_helper'

RSpec.describe Actions::Event::Booking::CreateMembership do
  let(:event){ create(:event) }
  let(:joiner){ create(:user) }

  describe "when creating membership" do
    context "when transaction not exists" do
      before(:each) do
        @service = described_class.execute(event: event, joiner: joiner, transaction_exists: false)
      end
      it "should successfully create membership" do
        expect(@service).to be_success
      end
      it "should return membership" do
        expect(@service.membership).to be_present
      end
      it "should raise error" do
        @failure_service = described_class.execute(event: event, joiner: joiner, transaction_exists: false)
        expect(@failure_service).to be_failure
        expect(@failure_service.message).to be_present
      end
    end
    context "when transaction exists" do
      before(:each) do
        @service = described_class.execute(event: event, joiner: joiner, transaction_exists: true)
      end
      it "should not create membership" do
        expect(@service.membership).to be_nil
      end
    end
  end

end
