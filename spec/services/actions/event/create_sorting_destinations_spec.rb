require 'rails_helper'

RSpec.describe Actions::Event::CreateSortingDestinations do
  let(:event){ create(:event) }
  let(:destination){ create(:destination) }
  let(:destination_params){
    {
      destination_ids: Destination.ids,
    }
  }

  context "when updating destinations" do
    before (:each) do
      destination
    end

    it "should success with valid params" do
      params = Actions::Event::BuildDestinationsParams.execute(
        destination_ids: destination_params[:destination_ids], event: event
      )
      result = described_class.execute(
        event: event, sorting_destinations_params: params.sorting_destinations_params,
        unused_sorting_destinations_ids: []
      )
      expect(result).to be_success
    end

    it "should raise rollback for invalid params" do
      invalid_destinations = []
      invalid_destinations[0] = SortingDestination.new.attributes
      event.sorting_destinations.create(destination_id: destination.id)
      expect{
        described_class.execute(
          event: event, sorting_destinations_params: invalid_destinations,
          unused_sorting_destinations_ids: []
        )
      }.to raise_error(LightService::FailWithRollbackError)
      expect(event.sorting_destinations).not_to be_empty
    end

    it "should restore existing record if params are invalid" do
      invalid_destinations = []
      invalid_destinations[0] = SortingDestination.new.attributes
      existing_destination = event.sorting_destinations
        .create(destination_id: destination.id)
      expect{
        described_class.execute(
          event: event, sorting_destinations_params: invalid_destinations,
          unused_sorting_destinations_ids: []
        )
      }.to raise_error(LightService::FailWithRollbackError)
      expect(event.reload.sorting_destinations.last).to eq(existing_destination)
    end
  end

end
