require 'rails_helper'

RSpec.describe Actions::Event::CreateCategorizations do
  let(:event){ create(:event) }
  let(:category){ create(:category) }
  let(:category_params){
    {
      category_ids: Category.ids,
    }
  }

  context "when updating categorizations" do
    before (:each) do
      category
    end

    it "should success with valid params" do
      params = Actions::Event::BuildCategorizationsParams.execute(
        category_ids: category_params[:category_ids], event: event
      )
      result = described_class.execute(
        event: event, categorizations_params: params.categorizations_params,
        unused_categorizations_ids: []
      )
      expect(result).to be_success
    end

    it "should raise rollback for invalid params" do
      invalid_categories = []
      invalid_categories[0] = Categorization.new.attributes
      event.categorizations.create(category_id: category.id)
      expect{
        described_class.execute(
          event: event, categorizations_params: invalid_categories,
          unused_categorizations_ids: []
        )
      }.to raise_error(LightService::FailWithRollbackError)
      expect(event.categorizations).not_to be_empty
    end

    it "should restore existing record if params are invalid" do
      invalid_categories = []
      invalid_categories[0] = Categorization.new.attributes
      existing_categorization = event.categorizations.create(category_id: category.id)
      expect{
        described_class.execute(
          event: event, categorizations_params: invalid_categories,
          unused_categorizations_ids: []
        )
      }.to raise_error(LightService::FailWithRollbackError)
      expect(event.reload.categorizations.last).to eq(existing_categorization)
    end
  end

end
