require 'rails_helper'

RSpec.describe Actions::Event::BuildPhotosParams do
  let(:original_event){ create(:event) }
  let(:valid_image) {
    Rack::Test::UploadedFile.new(
      Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg'
    )
  }
  context "when building Photos params" do
    it "should build correct params" do
      valid_photos = []
      valid_photos[0] = build_attachment(image: valid_image)
      valid_photos[1] = build_attachment(image: valid_image)
      expected_photos = []
      expected_photos[0] = valid_image
      expected_photos[1] = valid_image
      photos_params = {}
      photos_params[:photos] = valid_photos
      
      expected_params = build_photos_params(photos: expected_photos)
      photos = described_class.execute(params: photos_params)
      
      expect(photos.photos_params.count).to eq(expected_params.count)
      expect(
        photos.photos_params.map{ |x| x[:image].original_filename }
      ).to eq(
        expected_params.map{ |x| x[:image].original_filename }
      )
    end

    it "should be empty for nil params" do
      photos = described_class.execute(params: {})
      expect(photos.photos_params).to be_empty
    end
  end

  context "when building Duplicate photos params" do
    it "should build correct params" do
      valid_photos = []
      valid_photos[0] = build_attachment(image: valid_image)
      valid_photos[1] = build_attachment(image: valid_image)
      photos_params = {}
      photos_params[:photos] = valid_photos
      original_photos = described_class.execute(params: photos_params)
      original_event.photos.create(original_photos.photos_params)
      
      photos = described_class.execute(params: { event_id: original_event.id })
      
      expect(photos.photos_params.count).to eq(original_event.photos.count)
      expect(
        photos.photos_params.map{ |x| x[:image].original_filename }
      ).to eq(
        original_event.photos.map{ |x| x.image_file_name }
      )
    end
  end

end
