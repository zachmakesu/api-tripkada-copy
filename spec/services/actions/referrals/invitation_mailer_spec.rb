require 'rails_helper'

RSpec.describe Actions::Referrals::InvitationMailer do
  let!(:referrer) { create(:user) }
  let(:email_params){
    {
      emails: "#{Faker::Internet.email}, #{Faker::Internet.email}, #{Faker::Internet.email}"
    }
  }

  describe "referral Invitation mailer" do
    context "when sending Invitation Mailer" do
      it "should be success" do
        service = described_class.execute(params: email_params, referrer: referrer)
        expect(service.success?).to be_truthy
      end
      it "should return mailer response" do
        service = described_class.execute(params: email_params, referrer: referrer)
        expect(service.mailer_response).to eq("Invitation Successfully Sent")
      end
    end
  end

end
