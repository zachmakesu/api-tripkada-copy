require "rails_helper"

RSpec.describe Actions::PhotosBuilder do
  let!(:current_user) { create(:user) }
  let(:image) {
    Rack::Test::UploadedFile.new(
      Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg'
    )
  }
  let(:photo_labels){ Faker::Lorem.words(2)}

  context "when building certificates" do
    before (:each) do
      @params = {}
      @params[:photos] = []
      @params[:photos][0] = build_attachment(image: image)
      @params[:photos][1] = build_attachment(image: image)
      @params[:photo_labels] = photo_labels
    end

    it "should successfully build photos" do
      handler = described_class.execute(user: current_user, params: @params)
      expect(handler).to be_success
    end
    it "should not upload invalid photos" do
      invalid_params = @params
      invalid_params[:photos] = []
      handler = described_class.execute(user: current_user, params: invalid_params)
      expect(handler).not_to be_success
    end
  end
end
