require 'rails_helper'
require 'light-service/testing'

describe Actions::UpdateCoverPhoto do

  let(:current_user){ create(:user) }
  let(:cover_photo_params){
    {
      cover_photo: { image: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg') }
    }
  }

  context "Updating Cover Photo" do
    it "should be success" do
      result = described_class.execute(user: current_user, params: cover_photo_params)

      expect(result).to be_success
    end
  end
end
