require 'rails_helper'
require 'light-service/testing'

describe Actions::SendWaitlistStatusMailer do

  let(:user){ create(:joiner) }
  let(:event){ create(:event) }
  let(:waitlist){ Waitlist.create(user_id: user.id, event_id: event.id, status: 0) }

  context "When sending waitlist status mailer" do
    before(:each) do
      @result = described_class.execute(user: user, event: event, waitlist: waitlist)
    end
    it "should be success" do
      expect(@result).to be_success
    end
    it "should return response" do
      expect(@result.waitlist_membership_mailer_response).to be_present
    end
  end
end
