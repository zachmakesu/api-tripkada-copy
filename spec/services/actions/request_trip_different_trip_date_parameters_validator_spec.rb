require 'rails_helper'
require 'light-service/testing'

describe Actions::RequestTripDifferentDateParametersValidator do
  let(:requester){ create(:joiner) }

  let(:organizer){ create(:user) }

  let(:event){ create(:event) }

  let(:request_params){
    {
      id: event.id,
      start_date_time: "#{DateTime.now + 2.days}",
      end_date_time: "#{DateTime.now + 3.days}",
      requested_pax: Faker::Number.number(2)
    }
  }

  context "when validating requested trip date" do
    before (:each) do
      event.update(user_id: organizer.id)
    end

    it "should be success" do
      request = described_class.execute(event: event, params: request_params, requester: requester)
      expect(request).to be_success
    end
    it "should return error message" do
      invalid_request_params = request_params
      invalid_request_params.delete(:start_date_time)
      request = described_class.execute(event: event, params: invalid_request_params, requester: requester)
      expect(request.message).to eq("Please Complete All Details")
    end

  end
end

