require 'rails_helper'

RSpec.describe Actions::Mailerlite::Groups::SendRequest do
  let(:subscribers){ create_list(:user,3) }
  
  context "when creating mailerlite group" do
    it "should return create http status" do
      create_group_service = create_mailerlite_group(name: Faker::Lorem.word)
      expect(create_group_service.status).to eq(201)
    end
  end
  context "when fetching mailerlite groups" do
    it "should return success http status" do
      fetch_groups = fetch_mailerlite_groups
      expect(fetch_groups.status).to eq(200)
    end
  end
  context "when deleting mailerlite group" do
    it "should return success http status" do
      # set static group id to prevent unintentional deletion of group
      group_id = 8471784
      delete_group = delete_mailerlite_group(group_id: group_id)
      expect(delete_group.status).to eq(200)
    end
    it "should return invalid http status for invalid request" do
      # set static group id to prevent
      # VCR::Errors::UnhandledHTTPRequestError
      # due to dynamic uri
      group_id = 123 
      delete_group = delete_mailerlite_group_invalid(group_id: group_id)
      expect(delete_group.status).to eq(404)
    end
  end
  context "when importing mailerlite group subscribers" do
    it "should return success http status" do
      # set static group id to prevent unintentional importing of subscribers
      group_id =  8472118
      import_subscribers = import_mailerlite_group_subscribers(group_id: group_id, subscribers: subscribers)
      expect(import_subscribers.status).to eq(200)
    end
  end

end
