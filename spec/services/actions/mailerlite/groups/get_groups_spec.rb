require 'rails_helper'

RSpec.describe Actions::Mailerlite::Groups::GetGroups do
  context "when fetching mailerlite groups" do
    before(:each) do
      @service = described_class.execute()
    end
    it "should set request to GET" do
      expect(@service.request_type).to eq("get")
    end

    it "should set url to groups" do
      expect(@service.path).to eq("groups")
    end
  end

end
