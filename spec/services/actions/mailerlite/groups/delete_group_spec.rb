require 'rails_helper'

RSpec.describe Actions::Mailerlite::Groups::DeleteGroup do
    before(:each) do
      @group_id = Faker::Number.number(1)
      @service = described_class.execute(group_id: @group_id)
    end
  context "when deleting mailerlite group" do
    it "should set request to DELETE" do
      expect(@service.request_type).to eq("delete")
    end

    it "should set url to groups/group_id" do
      expect(@service.path).to eq("groups/#{@group_id}")
    end
  end

end
