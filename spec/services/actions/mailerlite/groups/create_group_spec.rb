require 'rails_helper'

RSpec.describe Actions::Mailerlite::Groups::CreateGroup do
  context "when creating mailerlite group" do
    let(:mailerlite_group_params){
      {
        name: Faker::Lorem.word,
        body_params: {}
      }
    }
    before(:each) do
      @service = described_class.execute(mailerlite_group_params)
    end
    it "should include name in body params" do
      expect(@service.name).to eq(@service.body_params[:name])
    end

    it "should set request to POST" do
      expect(@service.request_type).to eq("post")
    end

    it "should set url to groups" do
      expect(@service.path).to eq("groups")
    end
  end

end
