require 'rails_helper'

RSpec.describe Actions::Mailerlite::Groups::RemoveSubscriber do
  let(:mailerlite_group){ create(:mailerlite_group_default) }
  context "when removing subscriber from group" do
    before(:each) do
      mailerlite_group
      @subscriber_email = Faker::Internet.email
      @service = described_class.execute(group_id: mailerlite_group.group_id, subscriber_email: @subscriber_email)
    end
    it "should set request to delete" do
      expect(@service.request_type).to eq("delete")
    end

    it "should set url to groups/:group_id/subscribers/:subscriber_email" do
      expect(@service.path).to eq("groups/#{mailerlite_group.group_id}/subscribers/#{@subscriber_email}")
    end
  end

end
