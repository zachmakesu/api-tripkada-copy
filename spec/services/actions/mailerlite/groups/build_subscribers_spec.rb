require 'rails_helper'

RSpec.describe Actions::Mailerlite::Groups::BuildSubscribers do
  let(:subscribers){ create_list(:user, 3) }

  context "when building subscribers list" do
    it "should return list of subscribers email" do
      service = described_class.execute(subscribers: subscribers)
      subscribers_email = service.subscribers.map{ |x| x[:email] }
      expect(subscribers_email).to eq(subscribers.map(&:email))
    end
    it "should return error message if subscribers are not provided" do
      service = described_class.execute(subscribers: nil)
      expect(service.message).to eq("Please Provide Subscribers")
    end
  end

end
