require 'rails_helper'

RSpec.describe Actions::Mailerlite::Groups::FindGroup do
  let(:mailerlite_group){ create(:mailerlite_group_default) }
  context "when fetching mailerlite group by category" do
    before(:each) { mailerlite_group }

    it "should be success" do
      service  =  described_class.execute(category: "default")
      expect(service).to be_success
    end

    it "should return correct group id" do
      service  =  described_class.execute(category: "default")
      expect(mailerlite_group.group_id).to eq(service.group_id)
    end

    it "should fail with invalid category" do
      service  =  described_class.execute(category: Faker::Name.name)
      expect(service).not_to be_success
    end
  end

end
