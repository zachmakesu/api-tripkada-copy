require 'rails_helper'

RSpec.describe Actions::Mailerlite::Groups::ImportSubscribers do
  context "when creating mailerlite group" do
    let(:subscribers){ create_list(:user, 3)}
    let(:params){
      {
        group_id: Faker::Number.number(1),
        body_params: {}
      }
    }
    before(:each) do
      subscribers_builder = Actions::Mailerlite::Groups::BuildSubscribers.execute(subscribers: subscribers, body_params: {})
      mailerlite_params = params
      mailerlite_params[:subscribers] = subscribers_builder.subscribers
      @service = described_class.execute(mailerlite_params)
    end
    it "should include subscribers_email in body params" do
      expect(subscribers.map(&:email)).to eq(@service.body_params[:subscribers].map{|x| x[:email]})
    end

    it "should set request to POST" do
      expect(@service.request_type).to eq("post")
    end

    it "should set url to groups/group_id/subscribers/import" do
      expect(@service.path).to eq("groups/#{params[:group_id]}/subscribers/import")
    end
  end

end
