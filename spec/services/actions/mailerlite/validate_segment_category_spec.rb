require 'rails_helper'

RSpec.describe Actions::Mailerlite::Groups::ValidateSegmentCategory do
  context "when validating segment category by booking count" do
    it "should return default for 0 booking count" do
      service = described_class.execute(booking_count: 0)
      expect(service.category).to eq("default")
    end
    it "should return one_off for 1 booking count" do
      service = described_class.execute(booking_count: 1)
      expect(service.category).to eq("one_off")
    end
    it "should return repeat for 2 booking count" do
      service = described_class.execute(booking_count: 2)
      expect(service.category).to eq("repeat")
    end
    it "should return loyal for 3 or more booking count" do
      service = described_class.execute(booking_count: 6)
      expect(service.category).to eq("loyal")
    end
  end

end
