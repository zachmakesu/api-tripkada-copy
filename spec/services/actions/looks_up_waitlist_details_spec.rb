require 'rails_helper'
require 'light-service/testing'

describe Actions::LooksUpWaitlistDetails do

  let(:user){ create(:joiner) }
  let(:current_user){ create(:user) }
  let(:event){ create(:event, user_id: current_user.id) }
  let(:waitlist){ Waitlist.create(user_id: user.id, event_id: event.id, status: 0) }
  let(:waitlist_params){
    {
      params: {
                waitlist_action: "approve",
                waitlist_id: waitlist.id,
                id: event.id,
              },
      current_user: current_user
    }
  }

  context "Authorization" do
    it "should be success" do
      result = described_class.execute(waitlist_params)
      expect(result).to be_success
    end

    it "should fail" do
      waitlist_params[:current_user] = user
      result = described_class.execute(waitlist_params)
      expect(result).to_not be_success
      expect(result.message).to eq("Unauthorized access!")
    end
  end

  it "should not be valid if user is already a joiner" do
    waitlist_params[:event_membership] = EventMembership.create(member_id: user.id, event_id: event.id)
    result = described_class.execute(waitlist_params)
    expect(result).to_not be_success
    expect(result.waitlist.approved?).to be_falsey
  end
end
