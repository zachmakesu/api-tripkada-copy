require 'rails_helper'

RSpec.describe Actions::PromoCode::ValidateUsage do
  let(:promo_code){ create(:promo_code) }

  context "when validating usability" do
    it "should return true for valid corporate promo code" do
      service = described_class.execute(promo_code: promo_code)
      expect(service.success?).to be_truthy
    end

    it "should return false for invalid corporate promo code" do
      invalid_promo_code = promo_code
      invalid_promo_code.update(usage_limit: 0)
      service = described_class.execute(promo_code: invalid_promo_code)
      expect(service.message).to eq('promo code is not usable')
    end
  end

end
