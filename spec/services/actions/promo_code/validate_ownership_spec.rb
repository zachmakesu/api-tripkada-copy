require 'rails_helper'

RSpec.describe Actions::PromoCode::ValidateOwnership do
  let(:promo_code){ create(:promo_code) }
  let(:user){ create(:user) }
  let(:other_user){ create(:joiner) }

  context "when validating ownership" do
    it "should success for valid promo code" do
      service = described_class.execute(
        promo_code: user.credit_promo_code,
        user: user
      )
      expect(service.success?).to be_truthy
    end

    it "should return error message for invalid promo code" do
      service = described_class.execute(
        promo_code: user.credit_promo_code,
        user: other_user
      )
      expect(service.message).to eq('promo code does not belong to user')
    end
  end

end
