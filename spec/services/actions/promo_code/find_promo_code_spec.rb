require 'rails_helper'

RSpec.describe Actions::PromoCode::FindPromoCode do
  let(:promo_code){ create(:promo_code) }

  context "when finding promo code" do
    it "should success" do
      service = described_class.execute(code: promo_code.code)
      expect(service.success?).to be_truthy
    end

    it "should return error message" do
      service = described_class.execute(code: Faker::Name.first_name)
      expect(service.message).to eq('Promo Code not found')
    end
  end

end
