require 'rails_helper'
require 'light-service/testing'

describe Actions::JoinWaitlist do

  let(:current_user){ create(:joiner) }
  let(:event){ create(:event) }
  let(:new_waitlist_params){
    {
      user: current_user,
      event: event
    }
  }

  context "Joining the waitlist" do
    it "should be success" do
      result = described_class.execute(new_waitlist_params)
      waitlist = Waitlist.last
      expect(result).to be_success
      expect(waitlist.user).to eq(current_user)
    end

    it "should not be success if already in the waitlist" do
      Waitlist.create(user_id: current_user.id, event_id: event.id)
      result = described_class.execute(new_waitlist_params)
      expect(result).to_not be_success
      expect(result.message).to eq("User is already on the waitlist.")
    end
  end
end
