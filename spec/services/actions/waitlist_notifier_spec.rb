require 'rails_helper'
require 'light-service/testing'

describe Actions::WaitlistNotifier do

  let(:user){ create(:joiner) }
  let(:event){ create(:event) }
  let(:waitlist){ Waitlist.create(user_id: user.id, event_id: event.id, status: 0) }
  let(:approve_params){
    {
      user: user,
      event: event,
      waitlist: waitlist,
      event_membership: nil,
      status: Waitlist.statuses[:approved],
      params: { waitlist_action: "approve" }
    }
  }

  let(:deny_params){
    {
      user: user,
      event: event,
      waitlist: waitlist,
      event_membership: nil,
      status: Waitlist.statuses[:denied],
      params: { waitlist_action: "deny" }
    }
  }

  context "Feedback when approving a waitlist" do
    it "should be success" do
      result = described_class.execute(approve_params)
      expect(result).to be_success
      expect(result.notification.persisted?).to be_truthy
    end
  end

  context "Deny a waitlist" do
    it "should be success" do
      result = described_class.execute(deny_params)
      expect(result).to be_success
      expect(result.notification.persisted?).to be_truthy
    end
  end

end
