require 'rails_helper'
require 'light-service/testing'

describe Actions::UpdateProfilePhoto do

  let(:current_user){ create(:user) }
  let(:profile_photo_params){
    {
      profile_photos: [{
                        photo: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/files/test.jpg'), 'image/jpg'),
                        label: ""
                      }]
    }
  }

  context "Updating Profile Photos" do
    it "should be success" do
      result = described_class.execute(user: current_user.decorate, params: profile_photo_params)

      expect(result).to be_success
    end

    it "should not be success" do
      profile_photo_params[:profile_photos] = nil
      result = described_class.execute(user: current_user.decorate, params: profile_photo_params)

      expect(result).not_to be_success
      expect(result.message).to eq("Profile photos params is not present")
    end
  end
end
