require 'rails_helper'
require 'light-service/testing'

describe Actions::RequestTripDifferentDateMailer do
  let(:requester){ create(:joiner) }

  let(:organizer){ create(:user) }

  let(:event){ create(:event) }

  let(:request_params){
    {
      id: event.id,
      start_date_time: "#{DateTime.now + 2.days}",
      end_date_time: "#{DateTime.now + 3.days}",
      requested_pax: Faker::Number.number(2)
    }
  }

  context "when requesting a different trip date" do
    before (:each) do
      event.update(user_id: organizer.id)
    end

    it "should be success" do
      start_date = DateTime.parse request_params[:start_date_time]
      end_date = DateTime.parse request_params[:end_date_time]
      request = described_class.execute(event: event, params: request_params, requester: requester, start_datetime: start_date, end_datetime: end_date)
      expect(request).to be_success
    end
  end
  
end

