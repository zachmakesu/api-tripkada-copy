FactoryGirl.define do
  factory :notification do
    message               Faker::Lorem.sentence
    category              Faker::Number.between(from = 1, to = 6)
    seen                  false
    notificationable_id   Faker::Number.number(2)
    notificationable_type "Event"
  end
end
