FactoryGirl.define do
  factory :mailerlite_group_default, class: "MailerliteGroup" do
    category        "default"
    name            "#{Faker::Name.name}"
    group_id        Faker::Number.number(6)
  end
  factory :mailerlite_group_default_existing, class: "MailerliteGroup" do
    category        "default"
    name            "Tripkada Official Mailing List"
    group_id        8718256
  end
end
