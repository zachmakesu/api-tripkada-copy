FactoryGirl.define do
  factory :payment do
    association :booking_fee, factory: :booking_fee
    association :event_membership, factory: :event_membership
    promo_code_id nil
    mode "dragonpay"
    status "pending"
    created_at Time.zone.now - 2.days
    device_platform "desktop"
  end

  factory :payment_one_hour, class: "Payment" do
    association :booking_fee, factory: :booking_fee
    association :event_membership, factory: :event_membership
    promo_code_id nil
    mode "dragonpay"
    status "pending"
    created_at Time.zone.now - 60.minutes
    device_platform "desktop"
  end

  factory :payment_five_minutes, class: "Payment" do
    association :booking_fee, factory: :booking_fee
    association :event_membership, factory: :event_membership
    promo_code_id nil
    mode "dragonpay"
    status "pending"
    created_at Time.zone.now - 5.minutes
    device_platform "desktop"
  end
end
