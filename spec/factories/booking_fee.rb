FactoryGirl.define do
  factory :booking_fee do
    value       Faker::Number.number(3)
    created_by  Faker::Name.name
  end
end
