FactoryGirl.define do
  factory :event do
    name "test"
    start_at (DateTime.now + 1.days)
    end_at (DateTime.now + 2.days)
    meeting_place "Sample"
    min_pax 2
    max_pax 5
    rate 5
    downpayment_rate 2
    published_at DateTime.now
  end

  factory :canceled_event, class: "Event" do
    name "test"
    start_at (DateTime.now + 1.days)
    end_at (DateTime.now + 2.days)
    meeting_place "Sample"
    min_pax 2
    max_pax 5
    rate 5
    downpayment_rate 2
    deleted_at (DateTime.now)
    trip_cancel_reason "Weather"
    published_at DateTime.now
  end

  factory :mod_event, class: "Event" do
    id 12001
    name "test"
    start_at (DateTime.now + 1.days)
    end_at (DateTime.now + 2.days)
    meeting_place "Sample"
    min_pax 2
    max_pax 5
    rate 5
    downpayment_rate 2
    deleted_at (DateTime.now)
    trip_cancel_reason "Weather"
    published_at DateTime.now
  end

  factory :upcoming_event, class: "Event" do
    name "test"
    start_at (DateTime.now + 2.days)
    end_at (DateTime.now + 5.days)
    meeting_place "Sample"
    min_pax 1
    max_pax 10
    rate 2000
    downpayment_rate 500
    published_at DateTime.now
  end

  factory :past_event, class: "Event" do
    name "test"
    start_at (DateTime.now - 5.days)
    end_at (DateTime.now - 2.days)
    meeting_place "Sample"
    min_pax 1
    max_pax 10
    rate 2000
    downpayment_rate 2
    published_at DateTime.now
  end
end
