FactoryGirl.define do
  factory :ios_joiner_version, class: "AppVersion" do
    platform        "ios"
    category        "joiner"
    created_by      "#{Faker::Name.name}"
    version_code    "#{Faker::Lorem.word}"
  end
  factory :ios_organizer_version, class: "AppVersion" do
    platform        "ios"
    category        "organizer"
    created_by      "#{Faker::Name.name}"
    version_code    "#{Faker::Lorem.word}"
  end
  factory :android_joiner_version, class: "AppVersion" do
    platform        "android"
    category        "joiner"
    created_by      "#{Faker::Name.name}"
    version_code    "#{Faker::Lorem.word}"
  end
  factory :android_organizer_version, class: "AppVersion" do
    platform        "android"
    category        "organizer"
    created_by      "#{Faker::Name.name}"
    version_code    "#{Faker::Lorem.word}"
  end
end
