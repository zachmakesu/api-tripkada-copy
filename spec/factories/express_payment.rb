FactoryGirl.define do
  factory :express_payment do
    payer_id            Faker::Internet.email
    payment_tracking_id Faker::PhoneNumber.phone_number
    payment_status      "verified"
  end
end
