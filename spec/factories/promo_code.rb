FactoryGirl.define do
  factory :promo_code do
    code        SecureRandom.hex(6)
    value       5000
    usage_limit 10
    active      true
  end
  factory :private_use_promo_code, class: "PromoCode" do
    transient{ user{ create(:user) } }

    code        SecureRandom.hex(6)
    value       0.0
    active      true
    usage_limit 10
    category    "private_use"
    user_id     { user.id }
  end
end
