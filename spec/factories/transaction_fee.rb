FactoryGirl.define do
  factory :dragonpay_transaction_fee, class: "TransactionFee" do
    amount      Faker::Number.number(3)
    category    "dragonpay"
  end
  factory :uncategorized_transaction_fee, class: "TransactionFee" do
    amount      Faker::Number.number(3)
    category    "uncategorized"
  end
end
