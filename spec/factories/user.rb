FactoryGirl.define do
  sequence :email do |n|
    Faker::Internet.free_email("email#{n}")
  end
  sequence :uid do |n|
    "#{n}#{SecureRandom.hex(16)}"
  end
  factory :user do
    uid
    email
    password    "password123"
    first_name  Faker::Name.first_name
    last_name   Faker::Name.last_name
    gender      Faker::Number.between(1,2)
    image_url   Faker::Placeholdit.image
    country     Faker::Address.country
    city        Faker::Address.city
    address     Faker::Address.street_address
    mobile      Faker::PhoneNumber.cell_phone
    birthdate   20.years.ago
    role        "joiner_and_organizer"
    username    { "#{Faker::Name.name}".gsub(' ', '-').gsub(/[^0-9a-z-]/i, '') }

    after :create do |u|
      u.providers.create(uid: "#{SecureRandom.hex(16)}", role: "organizer", provider: "facebook")
      (1..10).each do |n|
        mod_event            = attributes_for(:upcoming_event)
        mod_event[:rate]     = n * 2000
        mod_event[:start_at] = DateTime.now + 1.days
        mod_event[:end_at]   = DateTime.now + ((n * 2).days + 1.days)
        upcoming_event = u.organized_events.create(mod_event)
        #4 upcoming events below 10000 rate
        #6 upcoming events above 10000 rate
        #4 upcoming events below 10 number_of_days
        #6 upcoming events above 10 number_of_days

        u.event_memberships.create(event_id: upcoming_event.id)

        mod_event = attributes_for(:past_event)
        mod_event[:rate]     = n * 2000
        mod_event[:start_at] = DateTime.now - ((n * 2).days + 1.days)
        mod_event[:end_at]   = DateTime.now - 1.days
        past_event = u.organized_events.new(mod_event)
        past_event.save(validate: false)
        #4 past events below 10000 rate
        #6 past events above 10000 rate
        #4 past events below 10 number_of_days
        #6 past events above 10 number_of_days
        u.event_memberships.create(event_id: past_event.id)
      end
    end
  end

  factory :joiner, class: "User" do
    uid
    email
    password    "password123"
    first_name  Faker::Name.first_name
    last_name   Faker::Name.last_name
    gender      Faker::Number.between(1,2)
    image_url   Faker::Placeholdit.image
    country     Faker::Address.country
    city        Faker::Address.city
    address     Faker::Address.street_address
    mobile      Faker::PhoneNumber.cell_phone
    birthdate   20.years.ago
    role        "joiner"

    after :create do |u|
      u.providers.create(uid: "#{SecureRandom.hex(16)}", role: "joiner", provider: "facebook")
    end
  end
end
