FactoryGirl.define do
  factory :bank_account do
    bank_name Faker::Company.name
    account_number Faker::Company.duns_number
  end
end
