FactoryGirl.define do
  factory :custom_notification_message do
    message { Faker::Lorem.paragraph }
    created_by { Faker::Name.name }
  end
end
