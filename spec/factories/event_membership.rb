FactoryGirl.define do
  factory :event_membership do
    after :build do |em|
      if em.event_id.nil?
        create(:event)
        em.event_id = Event.last.id
      end
      if em.member_id.nil?
        create(:user)
        em.member_id = User.last.id
      end
    end
    after :create do |em|
      create(:booking_fee)
      em.create_payment(mode: em.member.joiner_and_organizer? ? 3 : 5, booking_fee_id: BookingFee.last.id, status: 1)
    end
  end
end
