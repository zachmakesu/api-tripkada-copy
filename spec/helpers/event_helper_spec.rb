require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the EventHelper. For example:
#
# describe EventHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe EventHelper, type: :helper do
  describe "#trips_per_page(trips)" do
    let(:trips_12){ FactoryGirl.create_list(:event, 12) }
    let(:trips_14){ FactoryGirl.create_list(:event, 14) }
    let(:trips_16){ FactoryGirl.create_list(:event, 16) }
    let(:trips_20){ FactoryGirl.create_list(:event, 20) }
    context "When returning number of trips per slider" do
      it "returns 6 if trips.count <= 12" do
        expect(helper.trips_per_slider(trips_12)).to eq(6)
      end
      it "returns 8 if trips.count > 12 && <=16" do
        expect(helper.trips_per_slider(trips_16)).to eq(8)
      end
      it "returns half of the trip if trips.count >= 16" do
        @sliced_trip = (trips_20.count / 2.0).round
        expect(helper.trips_per_slider(trips_20)).to eq(@sliced_trip)
      end
      it "returns default no. of trips if trips.count <= 16" do
        @sliced_trip = (trips_14.count / 2.0).round
        expect(helper.trips_per_slider(trips_14)).to_not eq(@sliced_trip)
      end
    end
  end
end
