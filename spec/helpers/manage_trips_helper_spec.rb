require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the ManageTripsHelper. For example:
#
# describe ManageTripsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe ManageTripsHelper, type: :helper do
  let(:upcoming_event){ FactoryGirl.create(:event) }
  let(:canceled_event){ FactoryGirl.create(:canceled_event) }
  let(:past_event){
    event = FactoryGirl.build(:event)
    event[:start_at] = DateTime.now - 2.days
    event[:end_at] = DateTime.now - 1.days
    event.save(validation: false)
    event
  }
  let(:ongoing_event){
    event = FactoryGirl.build(:event)
    event[:start_at] = DateTime.now
    event[:end_at] = DateTime.now + 1.days
    event.save(validation: false)
    event
  }
  describe "#validate_status_of(event)" do
    context "When validating status of event" do
      it "returns Upcoming for Upcoming Events" do
        expect(helper.validate_status_of(upcoming_event.decorate)).to include("Upcoming")
      end
      it "returns On-Going for On-Going Events" do
        expect(helper.validate_status_of(ongoing_event.decorate)).to include("On-Going")
      end
      it "returns Done for Past Events" do
        expect(helper.validate_status_of(past_event.decorate)).to include("Done")
      end
      it "returns Canceled for canceled Events" do
        expect(helper.validate_status_of(canceled_event.decorate)).to include("Canceled")
      end
    end
  end
  describe "#generate_cancel_trip_options_for(event)" do
    context "When generating cancel trip options for event" do
      it "returns NO for Past Events and Canceled Events w/out Trip Cancel Reason" do
        expect(helper.generate_cancel_trip_options_for(past_event.decorate)).to include("NO")
      end
      it "returns Trip Cancel Reason for Canceled Events" do
        expect(helper.generate_cancel_trip_options_for(canceled_event.decorate)).to include(canceled_event.trip_cancel_reason)
      end
      it "returns Cancel Trip Option for Upcoming Events" do
        expect(helper.generate_cancel_trip_options_for(upcoming_event.decorate)).to include("CANCEL TRIP")
      end
    end
  end
end
